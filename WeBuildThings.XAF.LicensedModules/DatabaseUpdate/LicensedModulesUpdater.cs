﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.LicensedModules.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace WeBuildThings.LicensedModules.DatabaseUpdate
{
    public static class LicensedModulesUpdater
    {
        public static void Update(IObjectSpace objectSpace, Type type)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var assemblies = type.Assembly.GetReferencedAssemblies();
                foreach (var referencedAssembly in assemblies)
                {
                    Assembly assembly = Assembly.Load(referencedAssembly.FullName);
                    foreach (Type loadedType in assembly.GetTypes())
                    {
                        if (loadedType.GetInterfaces().Contains(typeof(ILicensedModule)))
                        {
                            LicensedModule licensedModule = LicensedModule.CreateLicensedModule(uow, loadedType);
                            var methods = loadedType.GetMethods();
                            foreach (MethodInfo methodInfo in methods)
                            {
                                var attributes = methodInfo.GetCustomAttributes(typeof(FeatureLicense), false);
                                if (attributes != null && attributes.Length > 0)
                                {
                                    FeatureLicense featureLicense = attributes[0] as FeatureLicense;
                                    LicensedFeature licensedFeature = LicensedFeature.CreateLicensedFeature(uow, licensedModule, methodInfo);
                                    licensedFeature.Save();
                                } 
                            } 
                            licensedModule.Save();
                        }
                        uow.CommitChanges();
                    }
                }
            }
            objectSpace.CommitChanges();
        }
    }
}
