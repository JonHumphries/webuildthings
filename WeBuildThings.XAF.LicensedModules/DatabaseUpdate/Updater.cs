﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.LicensedModules;

namespace WeBuildThings.LicensedModules.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater {

        public const string DEFAULT_LICENSE_NAME = "DEMO";
        public const string DEFAULT_CLIENT_ID = "Demo1";
        
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }
        
        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            UpdateLicensingSystemSettings(ObjectSpace);
        }

        public static void UpdateLicensingSystemSettings(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var systemSettings = SystemSettings.GetInstance<LicensingSystemSettings>(uow);
                if (uow.IsNewObject(systemSettings))
                {
                    systemSettings.LicensedTo = DEFAULT_LICENSE_NAME;
                    systemSettings.CoreClientId = DEFAULT_CLIENT_ID;
                }
                systemSettings.Save();
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
        }

        public override void UpdateDatabaseBeforeUpdateSchema() { base.UpdateDatabaseBeforeUpdateSchema();  }
    }
}
