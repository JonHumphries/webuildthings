﻿using DevExpress.ExpressApp.Model;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.LicensedModules
{
    public class LicensingSystemSettings : SystemSettings
    {
        #region Fields
        private string _LicensedTo;
        private string _CoreClientId;
        
        #endregion Fields

        #region Constructor
        public LicensingSystemSettings(Session session) : base(session) { }

        public LicensingSystemSettings() : base(Session.DefaultSession) { }
        #endregion Constructor

        #region Properties
        public string LicensedTo
        {
            get { return _LicensedTo; }
            set { SetPropertyValue("LicensedTo", ref _LicensedTo, value); }
        }

        [ModelDefault("AllowEdit","False")]
        public string CoreClientId
        {
            get { return _CoreClientId; }
            set { SetPropertyValue("CoreClientId", ref _CoreClientId, value); }
        }
        #endregion Properties
    }
}
