﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.LicensedModules.BusinessObjects
{
    public class LicensingViolation :Exception
    {
        public ILicensedFeature LicensedFeature { get; set; }
        public ILicensedModule LicensedModule { get; set; }

        public LicensingViolation(ILicensedFeature licensedFeature) :base()
        {
            LicensedFeature = licensedFeature;
        }

        public LicensingViolation(ILicensedModule licensedModule) : base()
        {
            LicensedModule = licensedModule;
        }
    }
}
