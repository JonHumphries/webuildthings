﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.LicensedModules.BusinessObjects;
using System.Collections.Generic;

namespace WeBuildThings.LicensedModules
{
    public interface ILicensedModule
    {
        string LicenseKey { get; }
    }

    public interface ILicensedFeature
    {
        string LicenseKey { get; }
    }

    public static class ILicensedModuleExtensions
    {

        public static void RegisterModule(this ILicensedModule module)
        {
            OfflineLicenseProvider.RegisterLicense(module.ModuleName(), module.LicenseKey);
        }


        public static string ModuleName(this ILicensedModule module)
        {
            return module.GetType().Assembly.FullName;
        }

        public static bool ValidateLicensedModule(this ILicensedModule module, Session session)
        {
            bool result = false;
            using (UnitOfWork uow = new UnitOfWork(session.ObjectLayer))
            {
                var clientKey = uow.FindObject<LicensedModule>(new BinaryOperator("ClassItem.Name", module.ModuleName()));
                result = (clientKey != null && OfflineLicenseProvider.Validate(module.ModuleName(), clientKey.ClientKey, module));
            }
            return result;
        }
        
    }

    public static class ILicensedFeatureExtensions
    {
        public static void RegisterFeature(this ILicensedFeature feature)
        {
            OfflineLicenseProvider.RegisterLicense(feature.FeatureName(), feature.LicenseKey);
        }

        public static string FeatureName(this ILicensedFeature feature)
        {
            return feature.GetType().FullName;
        }

        public static bool ValidateLicensedFeature(this ILicensedFeature feature, Session session)
        {
            bool result = false;
            using (UnitOfWork uow = new UnitOfWork(session.ObjectLayer))
            {
                var clientKey = uow.FindObject<LicensedFeature>(new BinaryOperator("Name", feature.FeatureName()));
                result = (clientKey != null && OfflineLicenseProvider.Validate(feature.FeatureName(), clientKey.ClientKey, feature));
            }
            return result;
        }
    }

    
}
