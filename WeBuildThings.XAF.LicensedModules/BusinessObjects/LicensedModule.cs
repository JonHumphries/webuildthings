﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common.Reflection;
using System;
using System.ComponentModel;

namespace WeBuildThings.LicensedModules.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("ClassItem.Name")]
    public class LicensedModule : BaseObject
    {
        #region Fields
        private PersistedType _ClassItem;
        private string _ClientKey;
        #endregion Fields

        #region Constructors
        public LicensedModule(Session session)
            : base(session)
        { }

        public static LicensedModule CreateLicensedModule(Session session, Type type)
        {
            LicensedModule result = session.FindObject<LicensedModule>(new BinaryOperator("ClassItem.FullName", type.FullName));
            if (result == null)
            {
                result = new LicensedModule(session);
                result.ClassItem = PersistedType.CreatePersistedType(session, type);
            }
            return result;
        }
        #endregion Constructors

        #region Properties
        [ModelDefault("AllowEdit", "False")]
        public PersistedType ClassItem
        {
            get { return _ClassItem; }
            set { SetPropertyValue("ClassItem", ref _ClassItem, value); }
        }

        public string ClientKey
        {
            get { return _ClientKey; }
            set { SetPropertyValue("ClientKey", ref _ClientKey, value); }
        }

        [Association("LicensedModule-LicensedFeature", typeof(LicensedFeature)), Aggregated]
        public XPCollection<LicensedFeature> LicensedFeatures
        {
            get { return GetCollection<LicensedFeature>("LicensedFeatures"); }
        }
        #endregion Properties
    }
}
