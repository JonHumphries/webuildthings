﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using WeBuildThings.Security;

namespace WeBuildThings.LicensedModules.BusinessObjects
{
    public static class OfflineLicenseProvider
    {
        private static Dictionary<string, string> Keys;
        static byte[] saltBytes = new byte[] { 4, 9, 6, 1, 1, 0, 1, 2, 5, 7, 0, 9, 3, 8, 7, 2 };
        private const string ClientData = "I101J%^jkaqe10.00ll~/$$456!@@5323";
        private const string SerializedParameter1 = "<ModuleName>";
        private const string SerializedParameter2 = "</ModuleName>";

        static OfflineLicenseProvider()
        {
            Keys = new Dictionary<string, string>();
        }

        public static void RegisterAssembly(string assemblyName)
        {
            throw new NotImplementedException();
        }

        public static void RegisterLicense(string name, string key)
        {
            if (!Keys.Keys.Contains(name))
            {
                Keys.Add(name, key);
            }
        }

        public static bool Validate(string moduleName, string clientKey, ILicensedModule licensedModule)
        {
            bool result = false;
            try
            {
                ValidateKey(Keys[moduleName], moduleName, clientKey);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            if (!result)
            {
                throw new LicensingViolation(licensedModule);
            }
            return result;
        }

        public static bool Validate(string moduleName, string clientKey, ILicensedFeature licensedFeature)
        {
            bool result = false;
            try
            {
                ValidateKey(Keys[moduleName], moduleName, clientKey);
                result = true;
            }
            catch (Exception e)
            {
                result = false;
            }
            if (!result)
            {
                throw new LicensingViolation(licensedFeature);
            }
            return result;
        }

        private static bool ValidateKey(string encryptedKey, string moduleName, string testKey)
        {
            string trueModuleName = ModuleName(DecryptClientKey(encryptedKey));
            string encryptedClientModuleName = ModuleName(DecryptClientKey(testKey));
            return (encryptedKey != testKey && trueModuleName == moduleName && trueModuleName == encryptedClientModuleName);
        }

        private static string ModuleName(string inputString)
        {
            int start = inputString.IndexOf(SerializedParameter1) + SerializedParameter1.Length;
            return inputString.Substring(start, inputString.IndexOf(SerializedParameter2) - start);
        }

        private static string DecryptClientKey(string input)
        {
            // Get the bytes of the string
            byte[] bytesToBeDecrypted = Convert.FromBase64String(input);
            byte[] passwordBytes = Encoding.UTF8.GetBytes(ClientData);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            byte[] bytesDecrypted = Cryptography.Decrypt(bytesToBeDecrypted, passwordBytes, saltBytes);
            return Encoding.UTF8.GetString(bytesDecrypted);
        }

        public static string GenerateLicenseKey(string moduleName, string privateKey)
        {
            byte[] dataToEncrypt = Encoding.UTF8.GetBytes(string.Format(string.Format("{2}{0}{3}<LicenseKey>{1}</LicenseKey>", moduleName, privateKey, SerializedParameter1, SerializedParameter2)));
            byte[] passwordBytes = Encoding.UTF8.GetBytes(ClientData);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            return Convert.ToBase64String(Cryptography.Encrypt(dataToEncrypt, passwordBytes, saltBytes));
        }

        public static string GenerateClientKey(string moduleName, string clientKey)
        {
            byte[] dataToEncrypt = Encoding.UTF8.GetBytes(string.Format("{2}{0}{3}<ClientKey>{1}</ClientKey>", moduleName, clientKey, SerializedParameter1, SerializedParameter2));
            byte[] passwordBytes = Encoding.UTF8.GetBytes(ClientData);
            passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
            return Convert.ToBase64String(Cryptography.Encrypt(dataToEncrypt, passwordBytes, saltBytes));
        }


    }

  

}
