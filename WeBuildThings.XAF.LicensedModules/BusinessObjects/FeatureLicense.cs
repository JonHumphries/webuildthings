﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.LicensedModules.BusinessObjects
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class FeatureLicense : Attribute, ILicensedFeature
    {
        public string LicenseKey { get; set; }

        public FeatureLicense(string licenseKey)
        {
            LicenseKey = licenseKey;
        }
    }
}
