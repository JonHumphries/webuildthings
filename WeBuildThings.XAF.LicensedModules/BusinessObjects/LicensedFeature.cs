﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common.Reflection;
using System.Reflection;

namespace WeBuildThings.LicensedModules.BusinessObjects
{
    [DisplayProperty("MethodInfo.Name")]
    public class LicensedFeature : BaseObject
    {
        #region Fields
        private MethodItem _MethodItem;
        private string _ClientKey;
        private LicensedModule _LicensedModule;
        #endregion Fields

        #region Constructors
        public LicensedFeature(Session session)
            : base(session)
        { }

        public static LicensedFeature CreateLicensedFeature(Session session, LicensedModule licensedModule, MethodInfo methodInfo)
        {
            LicensedFeature result = CreateLicensedFeature(session, methodInfo);
            result.LicensedModule = licensedModule;
            return result;
        }

        public static LicensedFeature CreateLicensedFeature(Session session, MethodInfo methodInfo)
        {
            GroupOperator criteria = new GroupOperator(GroupOperatorType.And, new BinaryOperator("MethodItem.TypeFullName", methodInfo.DeclaringType.FullName), new BinaryOperator("MethodItem.MethodName",methodInfo.Name));
            LicensedFeature result = session.FindObject<LicensedFeature>(criteria);
            if (result == null)
            {
                result = new LicensedFeature(session);
                result.MethodItem = MethodItem.CreateMethodItem(session, methodInfo);
            }
            return result;
        }
        #endregion Constructors

        #region Properties
        [ModelDefault("AllowEdit", "False")]
        public MethodItem MethodItem
        {
            get { return _MethodItem; }
            set { SetPropertyValue("MethodItem", ref _MethodItem, value); }
        }

        public string ClientKey
        {
            get { return _ClientKey; }
            set { SetPropertyValue("ClientKey", ref _ClientKey, value); }
        }

        [Association("LicensedModule-LicensedFeature", typeof(LicensedModule))]
        public LicensedModule LicensedModule
        {
            get { return _LicensedModule; }
            set { SetPropertyValue("LicensedModule", ref _LicensedModule, value); }
        }

        #endregion Properties

    }
}
