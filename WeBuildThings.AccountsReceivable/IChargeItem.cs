﻿using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common;

namespace WeBuildThings.AccountsReceivable
{
    public interface IChargeItem : IInvoiceItem
    {
        IInvoice Invoice { get; set; }
        decimal Balance { get; set; }
        string Description { get; set; }
        IEnumerable<IPaymentItem> PaymentItems { get; }

        /// <summary>
        /// This method must ensure referrential integrity between a charge item
        /// and payment item when adding a payment item.  It should also
        /// call OnPaymentItemAdded method.
        /// </summary>
        /// <example>
        ///  if (!PaymentItems.Contains(paymentItem))
        ///  {
        ///      PaymentItems.Add((PaymentItem)paymentItem);
        ///  }
        ///  this.OnPaymentAdded(paymentItem);
        /// </example>
        void AddPaymentItem(IPaymentItem paymentItem);

        /// <summary>
        /// This method must ensure referrential integrity between a charge item
        /// and payment item when adding a payment item.  It should also
        /// call the OnPaymentItemRemoved method.
        /// </summary>
        /// <example>
        /// if (PaymentItems.Contains(paymentItem))
        /// {
        ///     PaymentItems.Remove((PaymentItem)paymentItem);
        /// }
        /// this.OnPaymentItemRemoved(paymentItem);
        /// </example>
        void RemovePaymentItem(IPaymentItem paymentItem);
    }

    public static class ChargeItemExtensions
    {
        public static void SetBalance(this IChargeItem chargeItem)
        {
            decimal totalPayments = (chargeItem.PaymentItems == null) ? 0m : StandardCalculations.SumValues(chargeItem.PaymentItems.Select(n => n.Amount));
            chargeItem.Balance = chargeItem.Amount + totalPayments;
            if (chargeItem.Invoice != null)
            {
                chargeItem.Invoice.SetBalance();
            }
        }

        public static void AmountChanged(this IChargeItem chargeItem)
        {
            chargeItem.SetBalance();
            if (chargeItem.Invoice != null)
            {
                chargeItem.Invoice.SetTotalCharges();
                chargeItem.Invoice.SetTotalChargeAdjustments();
                chargeItem.Invoice.SetBalance();
            }
        }

        public static void InvoiceChanged(this IChargeItem chargeItem)
        {
            if (chargeItem.Invoice != null)
            {
                chargeItem.Invoice.AddCharge(chargeItem);
            }
        }

        public static void OnPaymentAdded(this IChargeItem chargeItem, IPaymentItem paymentItem)
        {
            paymentItem.ChargeItem = chargeItem;
            chargeItem.SetBalance();
        }

        public static void OnPaymentItemRemoved(this IChargeItem chargeItem, IPaymentItem paymentItem)
        {
            paymentItem.ChargeItem = null;
            chargeItem.SetBalance();
        }
    }
}
