﻿using System.Collections.Generic;

namespace WeBuildThings.AccountsReceivable
{
    public interface IPayee
    {
        IEnumerable<IInvoice> Invoices { get; }
        IEnumerable<IPayment> Payments { get; }

        decimal OutstandingBalance { get; set; }
    }
}
