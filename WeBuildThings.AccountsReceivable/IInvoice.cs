﻿using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common;

namespace WeBuildThings.AccountsReceivable
{

    public interface IInvoice
    {
        IPayee Payee { get; set; }
        decimal TotalCharges { get; set; }
        decimal TotalChargeAdjustments { get; set; }
        decimal TotalPayments { get; set; }
        decimal TotalPaymentAdjustments { get; set; }
        decimal Balance { get; set; }
        IEnumerable<IChargeItem> ChargeItems { get; }
        IEnumerable<IPayment> Payments { get; }

        /// <summary>
        /// This method must ensure referrential integrity between the invoice and charge item
        /// when adding a charge item.
        /// It must also call the AddCharge method and pass the chargeItem
        /// </summary>
        /// <example>
        /// if (!ChargeItems.Contains(chargeItem))
        /// {
        ///     ChargeItems.Add((ChargeItem)chargeItem);
        ///     this.AddCharge(chargeItem);
        /// }
        /// </example>
        void AddChargeItem(IChargeItem chargeItem);

        /// <summary>
        /// This method must ensure referrential integrity between the invoice and charge item
        /// when removing a charge item.
        /// </summary>
        /// <example>
        /// if (ChargeItems.Contains(chargeItem))
        /// {
        ///     ChargeItems.Remove((ChargeItem)chargeItem);
        ///     this.RemoveCharge(chargeItem);
        /// }
        /// </example>
        void RemoveChargeItem(IChargeItem chargeItem);

        /// <summary>
        /// This method must ensure referrential integrity between the invoice and payment
        /// when adding a payment.
        /// </summary>
        /// <example>
        /// if (!Payments.Contains(payment))
        /// {
        ///     Payments.Add((Payment)payment);
        /// }
        /// payment.AddInvoice(this);
        /// </example>
        void AddPayment(IPayment payment);

        /// <summary>
        /// This method must ensure referrential integrity between invoice and payment
        /// when removing a payment.
        /// </summary>
        /// <example>
        /// if (Payments.Contains(payment))
        /// {
        ///     Payments.Remove((Payment)payment);
        ///     var paymentItems = payment.PaymentItems.Where( n=> this.ChargeItems.Contains(n.ChargeItem)).ToList();
        ///     for (int i = 0; i < paymentItems.Count; i++)
        ///     {
        ///         paymentItems[i].ChargeItem.RemovePaymentItem(paymentItems[i]);
        ///     }
        ///     this.OnRemovePayment(payment);
        ///}
        ///</example>
        void RemovePayment(IPayment payment);
    }

    public static class InvoiceExtensions
    {
        /// <summary>
        /// Sets the invoice property of the charge item.  Calls the add charge item method of the invoice.
        /// Updates the invoice's total charge adjustments, total charges, and balance.
        /// </summary>
        public static void AddCharge(this IInvoice invoice, IChargeItem chargeItem)
        {
            chargeItem.Invoice = invoice;
            invoice.AddChargeItem(chargeItem);
            invoice.SetTotalChargeAdjustments();
            invoice.SetTotalCharges();
            invoice.SetTotalPayments();
            invoice.SetBalance();
        }

        /// <summary>
        /// Call this when removing a charge item from an invoice.
        /// It will remove the associated payments items and update the 
        /// invoice total charges and balance.
        /// </summary>
        public static void RemoveCharge(this IInvoice invoice, IChargeItem chargeItem)
        {
            chargeItem.Invoice = null;
            var paymentItems = chargeItem.PaymentItems.ToList();
            for (int i = 0; i < paymentItems.Count; i++)
            {
                chargeItem.RemovePaymentItem(paymentItems[i]);
                paymentItems[i].Payment.RemovePaymentItem(paymentItems[i]);
            }
            invoice.RemoveChargeItem(chargeItem);
            invoice.SetTotalCharges();
            invoice.SetTotalPayments();
            invoice.SetBalance();
        }

        /// <summary>
        /// Call this when a payment is removed from an invoice.
        /// It will call the RemoveInvoice method of the payment
        /// and update the invoice's balance.
        /// </summary>
        public static void OnRemovePayment(this IInvoice invoice, IPayment payment)
        {
            payment.RemoveInvoice(invoice);
            invoice.SetTotalPayments();
            invoice.SetBalance();
        }

        /// <summary>
        /// Call this to set the total charges property of the invoice
        /// It is the sum of the amount of all charge items.
        /// </summary>
        public static void SetTotalCharges(this IInvoice invoice)
        {
            invoice.TotalCharges = (invoice.ChargeItems == null) ? 0m : StandardCalculations.SumValues(invoice.ChargeItems.Select(n => n.Amount));
        }

        /// <summary>
        /// Call this to set the total charge adjustments property of the invoice.
        /// It will sum all charge items where the type is adjustment.
        /// </summary>
        public static void SetTotalChargeAdjustments(this IInvoice invoice)
        {
            invoice.TotalChargeAdjustments = (invoice.ChargeItems == null) ? 0m : StandardCalculations.SumValues(invoice.ChargeItems.Where(n => n.Type == InvoiceItemType.Adjustment).Select(n => n.Amount));
        }

        /// <summary>
        /// Call this to set the total payment adjustments property of the invoice.
        /// It will sum of the amount of the payment items where the type is adjustment.
        /// </summary>
        public static void SetTotalPaymentAdjustments(this IInvoice invoice)
        {
            decimal result = 0m;
            if (invoice.ChargeItems != null)
            {
                foreach (var invoiceItem in invoice.ChargeItems)
                {
                    result += (invoiceItem.PaymentItems == null) ? 0m : StandardCalculations.SumValues(invoiceItem.PaymentItems.Where(n => n.Type == InvoiceItemType.Adjustment).Select(n => n.Amount));
                }
            }
            invoice.TotalPaymentAdjustments = result;
        }

        /// <summary>
        /// Call this to set the total balance property of the invoice.
        /// It is the sum of the balance of each charge item.
        /// </summary>
        public static void SetBalance(this IInvoice invoice)
        {
            invoice.Balance = (invoice.ChargeItems == null) ? 0m : StandardCalculations.SumValues(invoice.ChargeItems.Select(n => n.Balance));
        }

        public static void SetTotalPayments(this IInvoice invoice)
        {
            invoice.TotalPayments = (invoice.ChargeItems == null) ? 0m : StandardCalculations.SumValues(invoice.ChargeItems.Where(n => n.PaymentItems != null).SelectMany(n => n.PaymentItems).Select(n => n.Amount));
        }
        /// <summary>
        /// This method ensures referrential integrity between invoice and payment
        /// It then creates a payment item for each charge item with a balance until
        /// the payment unapplied amount is 0 or the balance of all charge items is 0.
        /// </summary>
        public static void ApplyPayment(this IInvoice invoice, IPayment payment)
        {
            invoice.AddPayment(payment);
            payment.AddInvoice(invoice);
            foreach (var invoiceItem in invoice.ChargeItems)
            {
                var appliedAmount = (invoiceItem.Balance <= payment.UnappliedAmount * -1) ? -invoiceItem.Balance : payment.UnappliedAmount;
                if (appliedAmount != 0)
                {
                    var paymentItem = payment.CreatePaymentItem();
                    invoiceItem.AddPaymentItem(paymentItem);
                    paymentItem.Amount = appliedAmount;
                    invoiceItem.SetBalance();
                    payment.SetUnappliedAmount();
                }
            }
            invoice.SetTotalPayments();
        }
    }


}
