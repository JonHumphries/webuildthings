﻿using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common;

namespace WeBuildThings.AccountsReceivable
{

    public interface IPayment
    {
        IPayee Payee { get; set; }
        decimal TotalPaymentAmount { get; set; }
        decimal TotalAdjustments { get; set; }
        decimal UnappliedAmount { get; set; }
        IEnumerable<IPaymentItem> PaymentItems { get; }
        IEnumerable<IInvoice> Invoices { get; }

        void AddInvoice(IInvoice invoice);
        void RemoveInvoice(IInvoice invoice);
        void AddItem(IPaymentItem paymentItem);
        void RemoveItem(IPaymentItem paymentItem);

        IPaymentItem CreatePaymentItem();
    }

    public static class PaymentExtensions
    {
        /// <summary>
        /// Sets the payment on the payment item, adds the payment item to the payment's payment item collection.
        /// Sets the unapplied amount, sets the total adjustments, and sets the invoice's total payment adjustment values
        /// </summary>
        public static void AddPaymentItem(this IPayment payment, IPaymentItem paymentItem)
        {
            paymentItem.Payment = payment;
            payment.AddItem(paymentItem);
            payment.SetUnappliedAmount();
            payment.SetTotalAdjustments();
            payment.SetInvoiceTotalPaymentAdjustments();
            payment.SetInvoiceBalances();
        }

        /// <summary>
        /// Call this when removing a payment item from the payment's payment item collection.
        /// It clears the payment from the payment item and updates the payments Unapplied Amount.
        /// </summary>
        public static void RemovePaymentItem(this IPayment payment, IPaymentItem paymentItem)
        {
            paymentItem.Payment = null;
            payment.RemoveItem(paymentItem);
            payment.SetUnappliedAmount();
        }

        /// <summary>
        /// Sets the payments unapplied amount
        /// Payment's total payment amount - Sum of all applied amounts from all payment items.
        /// </summary>
        public static void SetUnappliedAmount(this IPayment payment)
        {
            decimal totalAppliedAmount = (payment.PaymentItems == null) ? 0m : StandardCalculations.SumValues(payment.PaymentItems.Select(n => n.Amount));
            payment.UnappliedAmount = payment.TotalPaymentAmount - totalAppliedAmount;
        }

        /// <summary>
        /// Sum of all payment items where the type is equal to adjustment
        /// </summary>
        public static void SetTotalAdjustments(this IPayment payment)
        {
            payment.TotalAdjustments = (payment.PaymentItems == null) ? 0m : StandardCalculations.SumValues(payment.PaymentItems.Where(n => n.Type == InvoiceItemType.Adjustment).Select(n => n.Amount));
        }

        /// <summary>
        /// Calls SetTotalPaymentAdjustments for every invoice attached to this payment
        /// </summary>
        public static void SetInvoiceTotalPaymentAdjustments(this IPayment payment)
        {
            foreach (var invoice in payment.Invoices)
            {
                invoice.SetTotalPaymentAdjustments();
            }
        }

        /// <summary>
        /// Calls Set Balance for every invoice attached to this payment
        /// </summary>
        public static void SetInvoiceBalances(this IPayment payment)
        {
            foreach (var invoice in payment.Invoices)
            {
                invoice.SetBalance();
            }
        }

        /// <summary>
        /// Because the collection type is unknown, this interface cannot maintain referential integrity.
        /// Call this once the invoice has been removed from the payment collection.  It will remove the 
        /// payment from the invoice and recalculate the payment's unapplied amount.
        /// </summary>
        public static void OnRemoveInvoice(this IPayment payment, IInvoice invoice)
        {
            invoice.RemovePayment(payment);
            payment.SetUnappliedAmount();
        }
    }

}
