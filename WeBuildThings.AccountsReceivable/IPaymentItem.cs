﻿
namespace WeBuildThings.AccountsReceivable
{
    public interface IPaymentItem : IInvoiceItem
    {
        IChargeItem ChargeItem { get; set; }
        IPayment Payment { get; set; }
    }

    public static class PaymentItemExtensions
    {
        public static void InvoiceItemChanged(this IPaymentItem paymentItem)
        {
            if (paymentItem.ChargeItem != null)
            {
                paymentItem.ChargeItem.AddPaymentItem(paymentItem);
            }
            else
            {
                paymentItem.Payment.RemovePaymentItem(paymentItem);
            }
        }

        public static void AppliedAmountChanged(this IPaymentItem paymentItem)
        {
            if (paymentItem.Payment != null)
            {
                paymentItem.Payment.SetUnappliedAmount();
            }
        }
    }
}
