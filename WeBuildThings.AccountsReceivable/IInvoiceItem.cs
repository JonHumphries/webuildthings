﻿
namespace WeBuildThings.AccountsReceivable
{
    
    public interface IInvoiceItem
    {
        InvoiceItemType Type { get; set; }
        decimal Amount { get; set; }
    }

}
