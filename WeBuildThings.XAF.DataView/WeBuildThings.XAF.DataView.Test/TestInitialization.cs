﻿using DevExpress.Xpo;
using System.Configuration;
using System.Reflection;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.DataViews.Test
{
    internal static class TestInitialization
    {
        internal static Assembly[] Assemblies = new Assembly[] { typeof(DataView).Assembly};

    }
}
