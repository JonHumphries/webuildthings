﻿using DevExpress.ExpressApp;
using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.DataViews.Test
{
    [TestClass]
    public class DataViewTests
    {

        public DataViewTests()
        {
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }

        #region XAF Initialization
        public Session Session
        {
            get { return XAFInitialization.Session; }
        }
        #endregion XAF Initialization

        [TestMethod]
        public void ValidateType_WithType()
        {
            bool expectedResult = true;
            
            var dataView = new DataView(Session);
            var filterCriteriaType = new PersistedType(Session);
            filterCriteriaType.ObjectType = typeof(DataViewTests);
            dataView.FilterCriteria.FilterObject = filterCriteriaType;

            var actualResult = DataView.ValidateType(false, dataView);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void ValidateType_WithoutType()
        {
            bool expectedResult = false;
            var dataView = new DataView(Session);
            
            var actualResult = DataView.ValidateType(false, dataView);
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void ValidateUniqueName_UniqueName()
        {
            var securitySystemTest = new TestSecurityStrategyBase();
            SecuritySystem.SetInstance(securitySystemTest);
            securitySystemTest.User = new XUser(Session) { UserName = "Admin" };           
            
            bool expectedResult = true;
            var dataView = new DataView(Session);
            var filterCriteriaType = new PersistedType(Session);
            dataView.Name = "Test Name";
            filterCriteriaType.ObjectType = typeof(DataViewTests);
            dataView.FilterCriteria.FilterObject = filterCriteriaType;
            var actualResult = DataView.ValidateUniqueName(false, dataView);
            Assert.AreEqual(expectedResult, actualResult);
            SecuritySystem.SetInstance(null);
        }

        [TestMethod]
        public void ValidateUniqueName_DuplicateName()
        {
            var securitySystemTest = new TestSecurityStrategyBase();
            SecuritySystem.SetInstance(securitySystemTest);
            securitySystemTest.User = new XUser(Session) { UserName = "Admin" };    

            bool expectedResult = false;
            var dataView = new DataView(Session);
            dataView.Name = "Test Name";
            dataView.SharedName = "Test name 1";
            var filterCriteriaType = new PersistedType(Session);
            filterCriteriaType.ObjectType = typeof(DataViewTests);
            dataView.FilterCriteria.FilterObject = filterCriteriaType;
            dataView.Save();

            var dataView2 = new DataView(Session);
            dataView2.Name = "Test Name";
            dataView2.SharedName = "Test name 2";
            dataView2.FilterCriteria.FilterObject = filterCriteriaType;
            dataView2.Save();
            
            var actualResult = DataView.ValidateUniqueName(false, dataView2);
            Assert.AreEqual(expectedResult, actualResult);
            Session.DropChanges();
        }
    }
}
