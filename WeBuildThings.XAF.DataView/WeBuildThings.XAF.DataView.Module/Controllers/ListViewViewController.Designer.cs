﻿namespace WeBuildThings.XAF.DataViews.Controllers
{
    partial class ListViewViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ConfigFiltering_Action = new DevExpress.ExpressApp.Actions.SingleChoiceAction(this.components);
            this.SaveAs_Action = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            this.Delete_Action = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.SetDefault_Action = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Save_Action = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Edit_Action = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ConfigFiltering_Action
            // 
            this.ConfigFiltering_Action.Caption = "Data View:";
            this.ConfigFiltering_Action.Category = "View";
            this.ConfigFiltering_Action.ConfirmationMessage = null;
            this.ConfigFiltering_Action.Id = "ConfigFiltering_Action";
            this.ConfigFiltering_Action.ToolTip = null;
            this.ConfigFiltering_Action.Execute += new DevExpress.ExpressApp.Actions.SingleChoiceActionExecuteEventHandler(this.OnConfigFiltering_ActionExecute);
            // 
            // SaveAs_Action
            // 
            this.SaveAs_Action.AcceptButtonCaption = "Save";
            this.SaveAs_Action.ActionMeaning = DevExpress.ExpressApp.Actions.ActionMeaning.Accept;
            this.SaveAs_Action.CancelButtonCaption = null;
            this.SaveAs_Action.Caption = "Save As";
            this.SaveAs_Action.Category = "View";
            this.SaveAs_Action.ConfirmationMessage = null;
            this.SaveAs_Action.Id = "SaveAs_Action";
            this.SaveAs_Action.ImageName = "Action_Save_New";
            this.SaveAs_Action.ToolTip = null;
            this.SaveAs_Action.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.OnSaveAs_ActionCustomizePopupWindowParams);
            // 
            // Delete_Action
            // 
            this.Delete_Action.Caption = "Delete";
            this.Delete_Action.Category = "View";
            this.Delete_Action.ConfirmationMessage = "Are you sure you want to delete this data view?";
            this.Delete_Action.Id = "Delete_Action";
            this.Delete_Action.ImageName = "Action_Delete";
            this.Delete_Action.ToolTip = "Delete";
            this.Delete_Action.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OnDelete_ActionExecute);
            // 
            // SetDefault_Action
            // 
            this.SetDefault_Action.Caption = "Make Default";
            this.SetDefault_Action.Category = "View";
            this.SetDefault_Action.ConfirmationMessage = null;
            this.SetDefault_Action.Id = "SetDefault_Action";
            this.SetDefault_Action.ImageName = "Action_ShowItemOnDashboard";
            this.SetDefault_Action.ToolTip = null;
            this.SetDefault_Action.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OnSetDefault_ActionExecute);
            // 
            // Save_Action
            // 
            this.Save_Action.Caption = "Save";
            this.Save_Action.Category = "View";
            this.Save_Action.ConfirmationMessage = null;
            this.Save_Action.Id = "Save_Action";
            this.Save_Action.ImageName = "Action_Save";
            this.Save_Action.ToolTip = null;
            this.Save_Action.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OnSave_ActionExecute);
            // 
            // Edit_Action
            // 
            this.Edit_Action.Caption = "Edit";
            this.Edit_Action.Category = "View";
            this.Edit_Action.ConfirmationMessage = null;
            this.Edit_Action.Id = "Edit_Action";
            this.Edit_Action.ImageName = "Action_Edit";
            this.Edit_Action.ToolTip = "Edit";
            this.Edit_Action.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OnEdit_ActionExecute);
            // 
            // ListViewViewController
            // 
            this.Actions.Add(this.ConfigFiltering_Action);
            this.Actions.Add(this.SaveAs_Action);
            this.Actions.Add(this.Delete_Action);
            this.Actions.Add(this.SetDefault_Action);
            this.Actions.Add(this.Save_Action);
            this.Actions.Add(this.Edit_Action);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SingleChoiceAction ConfigFiltering_Action;
        private DevExpress.ExpressApp.Actions.SimpleAction Save_Action;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction SaveAs_Action;
        private DevExpress.ExpressApp.Actions.SimpleAction SetDefault_Action;
        private DevExpress.ExpressApp.Actions.SimpleAction Delete_Action;
        private DevExpress.ExpressApp.Actions.SimpleAction Edit_Action;
    }
}
