﻿using DevExpress.Data;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Utils;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.XAF.DataViews.Controllers
{
    public partial class ListViewViewController : ViewController
    {
        #region Fields
        private bool _DefaultViewLoaded;
        #endregion Fields

        #region Constructors

        public ListViewViewController()
        {
            InitializeComponent();
            Initialize();
        }

        private void Initialize()
        {
            TargetViewType = ViewType.ListView;
            RegisterActions(components);
            SaveAs_Action.Active.SetItemValue("SaveUserFilterAction", true);
            Save_Action.Active.SetItemValue("EditUserFilterAction", false);
            Delete_Action.Active.SetItemValue("DeleteUserFilterAction", false);
            SetDefault_Action.Active.SetItemValue("DefaultUserFilterAction", false);
            Edit_Action.Active.SetItemValue("EditConfigAction", false);
        }
        #endregion Constructors

        #region Properties
        internal bool DefaultViewLoaded
        {
            get { return _DefaultViewLoaded; }
            set { if (_DefaultViewLoaded != value) _DefaultViewLoaded = value; }
        }
        #endregion Properties

        #region Event Methods
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            if (((ListView)View).Editor.Control is GridControl)
            {
                GridControl gridControl = ((ListView)View).Editor.Control as GridControl;
                if (gridControl != null)
                {
                    gridControl.DataSourceChanged -= OnGridControlDataSourceChanged;
                    gridControl.DataSourceChanged += OnGridControlDataSourceChanged;
                    if (View.ObjectTypeInfo != null)
                    {
                        if (View.IsRoot || (!View.IsRoot && ((((IModelDetailView)(((IModelListView)(View.Model)).DetailView)).ModelClass).TypeInfo).IsPersistent && !(Frame.Template is ILookupPopupFrameTemplate)))
                        {
                            SetConfigFilterActionItems();
                            SetDefaultFilter((XPObjectSpace)Application.CreateObjectSpace());
                        }
                    }
                }
            }
        }

        private void OnGridControlDataSourceChanged(object sender, EventArgs e)
        {
            if (View != null)
            {
                SetColumnWidth();
            }
        }

        private void OnConfigFiltering_ActionExecute(object sender, SingleChoiceActionExecuteEventArgs e)
        {
            if (e.SelectedChoiceActionItem.Data != null)
            {
                SetFilter(e.SelectedChoiceActionItem.Data as DataView);
            }
        }

        private void OnSave_ActionExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            DataView popupDataView = GetDataView<DataView>(objectSpace, true);
            popupDataView.Save();
            popupDataView.Session.CommitTransaction();
            objectSpace.CommitChanges();
        }

        private void OnSaveAs_ActionCustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            e.DialogController.AcceptAction.ToolTip = "Save As";
            DataView popupDataView = GetDataView<DataView>(objectSpace);

            var imodeldetailView = Application.Model.Views.GetNode("DataView_DetailView_Popup") as IModelDetailView;
            e.View = Application.CreateDetailView(objectSpace, imodeldetailView, true, popupDataView);
            objectSpace.Committed += (objectSpaceSender, objectSpaceE) =>
            {
                SetConfigFilterActionItems();
                SetFilter(popupDataView);
            };
        }

        private void OnEdit_ActionExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            DataView popupDataView = GetDataView<DataView>(objectSpace, true);
            e.ShowViewParameters.CreatedView = Application.CreateDetailView(objectSpace, popupDataView);
        }

        private void OnSetDefault_ActionExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            DataView popupDataView = null;
            ListView listView = View as ListView;
            XPObjectSpace objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            if (ConfigFiltering_Action.SelectedItem != null)
            {
                popupDataView = objectSpace.GetObject<DataView>((DataView)ConfigFiltering_Action.SelectedItem.Data);
                DefaultDataView userLastSavedFilter = objectSpace.FindObject<DefaultDataView>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("ClassType.FullName", listView.ObjectTypeInfo.FullName)
                                                                                                                                       , new BinaryOperator("CreatedBy.Oid", ((SecuritySystemUser)SecuritySystem.CurrentUser).Oid)));
                if (userLastSavedFilter != null)
                {
                    userLastSavedFilter.Filter = popupDataView;
                }
                else
                {
                    userLastSavedFilter = DefaultDataView.CreateDefaultDataView(objectSpace.Session, View.ObjectTypeInfo.Type, popupDataView);
                }
                userLastSavedFilter.Save();
                userLastSavedFilter.Session.CommitTransaction();
                objectSpace.CommitChanges();
                SetDefault_Action.Active.SetItemValue("DefaultUserFilterAction", false);
            }
        }

        private void OnDelete_ActionExecute(object sender, SimpleActionExecuteEventArgs e)
        {
            var objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            DataView popupDataView = objectSpace.GetObject<DataView>(ConfigFiltering_Action.SelectedItem.Data as DataView);
            popupDataView.Delete();
            /*
            DefaultDataView lastSavedFilter = objectSpace.FindObject<DefaultDataView>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("ClassType", View.ObjectTypeInfo.Type.FullName)
                                                                                                                               , new BinaryOperator("Filter", popupDataFilter)
                                                                                                                               , new BinaryOperator("CreatedBy.UserName", SecuritySystem.CurrentUserName)));
            if (lastSavedFilter != null)
            {
                lastSavedFilter.Filter = null;
                lastSavedFilter.Save();
                lastSavedFilter.Session.CommitTransaction();
            }
            popupDataFilter.Session.CommitTransaction();
             */
            objectSpace.CommitChanges();
             
            SetConfigFilterActionItems();
            SetActionVisibility();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetConfigFilterActionItems()
        {
            ConfigFiltering_Action.Items.Clear();
            var objectSpace = Application.CreateObjectSpace() as XPObjectSpace;
            IEnumerable<DataView> filter = DataView.UserConfigs<DataView>(objectSpace, View.ObjectTypeInfo.Type).Cast<DataView>();
            foreach (DataView userFilter in filter)
            {
                ConfigFiltering_Action.Items.Add(new ChoiceActionItem(userFilter.UniqueName, userFilter));
            }
            if (ConfigFiltering_Action.Items.Count == 0)
            {
                ConfigFiltering_Action.Items.Add(new ChoiceActionItem(string.Empty, null));
            }
        }

        private bool SetFilter(DataView filter)
        {
            var result = false;
            if ((filter != null && filter.CreatedBy != null && filter.CreatedBy.UserName.Equals(SecuritySystem.CurrentUserId)) || filter.IsAccessible(View.ObjectSpace))
            {
                SetGridView(filter);
                SetConfigFilterActionSelectedItem(filter);
                SetActionVisibility();

                result = true;
            }
            return result;
        }

        private bool SetDefaultFilter(XPObjectSpace objectSpace)
        {
            var result = false;
            if (!DefaultViewLoaded)
            {
                var defaultConfig = DataView.GetDefaultConfig<DataView>(objectSpace, View.ObjectTypeInfo.Type);
                if (defaultConfig != null && SetFilter(defaultConfig))
                {
                    DefaultViewLoaded = true;
                    result = true;
                }
            }
            return result;
        }

        private void SetConfigFilterActionSelectedItem(DataView userFilter)
        {
            if (ConfigFiltering_Action.SelectedItem == null || userFilter.Name != ConfigFiltering_Action.SelectedItem.Id)
            {
                ConfigFiltering_Action.SelectedItem = ConfigFiltering_Action.Items.FindItemByID(userFilter.SharedName);
                SetActionVisibility();
            }
        }

        private void SetActionVisibility()
        {
            SaveAs_Action.Active.SetItemValue("SaveUserFilterAction", true);

            if (ConfigFiltering_Action.SelectedItem != null)
            {
                DataView listViewConfig = ConfigFiltering_Action.SelectedItem.Data as DataView;

                Save_Action.Active.SetItemValue("EditUserFilterAction", listViewConfig.IsGranted(View.ObjectSpace, ObjectAccess.Write));
                Delete_Action.Active.SetItemValue("DeleteUserFilterAction", listViewConfig.IsGranted(View.ObjectSpace, ObjectAccess.Delete));
                Edit_Action.Active.SetItemValue("EditConfigAction", listViewConfig.IsGranted(View.ObjectSpace, ObjectAccess.Write));

                DefaultDataView userLastSavedFilter = View.ObjectSpace.FindObject<DefaultDataView>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("ClassType.FullName", View.ObjectTypeInfo.Type.FullName)
                                                                                                                                            , new BinaryOperator("CreatedBy.Oid", ((SecuritySystemUser)SecuritySystem.CurrentUser).Oid)));

                bool defaultActive = ((userLastSavedFilter != null && userLastSavedFilter.Filter != null &&
                                      (userLastSavedFilter.Filter.CreatedBy.UserName == SecuritySystem.CurrentUserName || userLastSavedFilter.Filter.IsAccessible(View.ObjectSpace))) &&
                                      (ConfigFiltering_Action.SelectedItem == null || ConfigFiltering_Action.SelectedItem.Data == null || (ConfigFiltering_Action.SelectedItem.Data is DataView &&
                                      ((DataView)(ConfigFiltering_Action.SelectedItem.Data)).Oid != userLastSavedFilter.Filter.Oid))) || ((userLastSavedFilter == null || userLastSavedFilter.Filter == null) && ConfigFiltering_Action.SelectedItem != null);
                SetDefault_Action.Active.SetItemValue("DefaultUserFilterAction", defaultActive);
            }
            else
            {
                Save_Action.Active.SetItemValue("EditUserFilterAction", false);
                Delete_Action.Active.SetItemValue("DeleteUserFilterAction", false);
                SetDefault_Action.Active.SetItemValue("DefaultUserFilterAction", false);
                Edit_Action.Active.SetItemValue("EditConfigAction", false);
            }
        }

        private void SetColumnWidth()
        {
            var gridControl = View.Control as GridControl;
            if (gridControl != null)
            {
                GridView gridView = (GridView)gridControl.FocusedView;
                if (gridView != null)
                {
                    int estimatedBestWidth = 0;
                    gridView.BestFitMaxRowCount = 20;
                    foreach (GridColumn column in gridView.Columns)
                    {
                        estimatedBestWidth += column.GetBestWidth();
                        if (estimatedBestWidth > gridView.GridControl.Width)
                        {
                            gridView.OptionsView.ColumnAutoWidth = false;
                            //gridView.BestFitColumns();
                            gridView.GridControl.Width = estimatedBestWidth;
                        }
                    }
                }
            }
        }

        private void SetGridView(DataView dataView)
        {
            GridControl gridControl = (GridControl)((ListView)View).Editor.Control;
            if (gridControl != null)
            {
                GridView gridView = (GridView)gridControl.FocusedView;
                if (dataView != null && gridView != null)
                {
                    var currentCursor = System.Windows.Forms.Cursor.Current;
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    gridView.BeginUpdate();
                    ITypeInfo typeInstance = XafTypesInfo.Instance.FindTypeInfo(View.ObjectTypeInfo.Type);
                    foreach (GridColumn column in gridView.Columns)
                    {
                        ResetGridViewColumn(dataView, typeInstance, column);
                    }
                    dataView.DataViewItems.Filter = CriteriaOperator.Parse(String.Empty);

                    SetGridViewColumns(dataView, gridView, typeInstance);
                    SetGridViewGrouping(dataView, gridView);
                    SetGridViewSorting(dataView, gridView);
                    SetGridViewFilterCriteria(dataView, gridView);

                    gridView.EndUpdate();
                    System.Windows.Forms.Cursor.Current = currentCursor;
                }
            }
        }

        private void SetGridViewColumns(DataView dataView, GridView gridView, ITypeInfo typeInstance)
        {
            dataView.DataViewItems.Sorting = new SortingCollection(new SortProperty("Number", SortingDirection.Ascending));

            foreach (DataViewItem dataViewItem in dataView.DataViewItems)
            {
                if (!string.IsNullOrWhiteSpace(dataViewItem.PropertyName))
                {
                    GridColumn gridColumn = gridView.Columns[dataViewItem.PropertyName];
                    if (gridColumn == null)
                    {
                        gridColumn = gridView.Columns.AddField(dataViewItem.PropertyName);
                    }
                    SetColumnFilter(typeInstance, gridColumn, dataViewItem);
                }
                else if (!string.IsNullOrWhiteSpace(dataViewItem.Expression))
                {
                    GridColumn gridColumn = new GridColumn();
                    gridColumn.Name = dataViewItem.Caption;
                    gridColumn.Caption = Name;
                    gridColumn.FieldName = Name;
                    gridColumn.ShowUnboundExpressionMenu = true;
                    gridColumn.UnboundType = UnboundColumnType.String;
                    gridColumn.UnboundExpression = dataViewItem.Expression;
                    gridView.Columns.Add(gridColumn);
                    SetColumnFilter(typeInstance, gridColumn, dataViewItem);
                }
            }
        }

        private static void SetGridViewFilterCriteria(DataView dataView, GridView gridView)
        {
            dataView.DataViewItems.Filter = CriteriaOperator.Parse(String.Empty);
            if (dataView.FilterCriteria != null)
            {
                gridView.ActiveFilterCriteria = dataView.FilterCriteria.GetOperator();
            }
        }

        private static void SetGridViewSorting(DataView dataView, GridView gridView)
        {
            dataView.DataViewItems.Filter = CriteriaOperator.Parse("SortIndex >= ?", 0);
            dataView.DataViewItems.Sorting = new SortingCollection(new SortProperty("SortIndex", SortingDirection.Ascending));
            foreach (DataViewItem dataViewItem in dataView.DataViewItems)
            {
                if (!string.IsNullOrWhiteSpace(dataViewItem.PropertyName))
                {
                    GridColumn gridColumn = gridView.Columns[dataViewItem.PropertyName];
                    gridColumn.SortOrder = dataViewItem.SortOrder;
                    gridColumn.SortIndex = dataViewItem.SortIndex;
                }
                else if (!string.IsNullOrWhiteSpace(dataViewItem.Expression))
                {
                    GridColumn gridColumn = gridView.Columns.ColumnByName(dataViewItem.Caption);
                    gridColumn.SortOrder = dataViewItem.SortOrder;
                    gridColumn.SortIndex = dataViewItem.SortIndex;
                }
            }
        }

        private static void SetGridViewGrouping(DataView dataView, GridView gridView)
        {
            dataView.DataViewItems.Filter = CriteriaOperator.Parse(String.Empty);
            dataView.DataViewItems.Filter = CriteriaOperator.Parse("GroupIndex >= ?", 0);
            dataView.DataViewItems.Sorting = new SortingCollection(new SortProperty("GroupIndex", SortingDirection.Ascending));
            foreach (DataViewItem dataViewItem in dataView.DataViewItems)
            {
                if (!string.IsNullOrWhiteSpace(dataViewItem.PropertyName))
                {
                    GridColumn gridColumn = gridView.Columns[dataViewItem.PropertyName];
                    gridColumn.GroupIndex = dataViewItem.GroupIndex;
                }
                else if (!string.IsNullOrWhiteSpace(dataViewItem.Expression))
                {
                    GridColumn gridColumn = gridView.Columns.ColumnByName(dataViewItem.Caption);
                    gridColumn.GroupIndex = dataViewItem.GroupIndex;
                }
            }
        }

        private void SetColumnFilter(ITypeInfo typeInstance, GridColumn gridColumn, DataViewItem filterItem)
        {
            if (filterItem.Number > -1)
            {
                gridColumn.Caption = filterItem.Caption;
                gridColumn.VisibleIndex = filterItem.Number;

                if (gridColumn.ColumnType != null && gridColumn.ColumnType.BaseType != null && gridColumn.ColumnType.BaseType.ToString() == "System.Enum")
                {
                    gridColumn.AppearanceCell.TextOptions.HAlignment = HorzAlignment.Near;
                }
                if (gridColumn.ColumnType != null && gridColumn.ColumnType == typeof(Decimal) && gridColumn.RealColumnEdit.DisplayFormat.Format == null)
                {
                    gridColumn.DisplayFormat.FormatType = FormatType.Numeric;
                    gridColumn.DisplayFormat.FormatString = "c2";
                }
                if (gridColumn.OptionsColumn != null)
                {
                    gridColumn.OptionsColumn.AllowEdit = IsColumnEdit(typeInstance, filterItem.PropertyName);
                }
            }
        }

        private static void ResetGridViewColumn(DataView dataView, ITypeInfo typeInstance, GridColumn column)
        {
            column.Visible = false;
            column.VisibleIndex = -1;
            column.GroupIndex = -1;
            column.SortIndex = -1;
            column.SortOrder = ColumnSortOrder.None;
            if (column.FieldName.Contains("!"))
            {
                string fieldName = column.FieldName;
                IMemberInfo columnMember = typeInstance.FindMember(column.FieldName);
                if (columnMember != null && columnMember.MemberTypeInfo != null && columnMember.MemberTypeInfo.DefaultMember != null)
                {
                    fieldName = column.FieldName.Replace("!", ".") + columnMember.MemberTypeInfo.DefaultMember.BindingName;
                }
                dataView.DataViewItems.Filter = CriteriaOperator.Parse("PropertyName = ?", fieldName);
                if (dataView.DataViewItems != null && dataView.DataViewItems.Count > 0)
                {
                    column.OptionsColumn.ShowInCustomizationForm = false;
                }
            }
        }

        private T GetDataView<T>(XPObjectSpace objectSpace, bool isEdit = false) where T : DataView
        {
            T result = null;
            ITypeInfo typeInstance = XafTypesInfo.Instance.FindTypeInfo(View.ObjectTypeInfo.Type);
            GridControl grid = ((ListView)View).Editor.Control as GridControl;
            GridView gridView = (GridView)grid.FocusedView;

            if (gridView != null)
            {
                var criteriaOperator = CriteriaOperator.Clone(gridView.ActiveFilterCriteria);
                new FilterWithObjectsProcessor(ObjectSpace, View.ObjectTypeInfo, false).Process(criteriaOperator, DevExpress.ExpressApp.Filtering.FilterWithObjectsProcessorMode.ObjectToString);

                string criteriaString = ReferenceEquals(criteriaOperator, null) ? string.Empty : criteriaOperator.ToString().Replace("!]", "]");

                if (isEdit)
                {
                    result = objectSpace.GetObject<T>(ConfigFiltering_Action.SelectedItem.Data as T);
                    result.SharingCriteria.Criterion = criteriaString;
                    if (result.DataViewItems != null)
                    {
                        foreach (DataViewItem filterItem in result.DataViewItems)
                        {
                            filterItem.Number = -1;
                            filterItem.SortIndex = -1;
                            filterItem.GroupIndex = -1;
                            filterItem.SortOrder = ColumnSortOrder.None;
                        }
                    }
                }
                else
                {
                    result = DataView.Create<T>(objectSpace.Session, View.ObjectTypeInfo.Type, criteriaString);
                }
                result.ObjectSpace = objectSpace;
                result.ObjectTypeInfo = typeInstance;
                foreach (GridColumn gridColumn in gridView.Columns)
                {
                    SetDataViewItem<T>(objectSpace, result, typeInstance, gridColumn);
                }
            }
            return result;
        }

        private static void SetDataViewItem<T>(XPObjectSpace objectSpace, T dataView, ITypeInfo typeInstance, GridColumn gridColumn) where T : DataView
        {
            if (gridColumn.Visible || gridColumn.GroupIndex > -1 || (gridColumn.SortIndex > -1 && gridColumn.SortOrder != ColumnSortOrder.None))
            {
                string fieldName = gridColumn.FieldName;
                if (fieldName.Contains("!"))
                {
                    IMemberInfo memberInfo = typeInstance.FindMember(gridColumn.FieldName);
                    if (memberInfo.MemberTypeInfo != null && memberInfo.MemberTypeInfo.DefaultMember != null)
                    {
                        fieldName = gridColumn.FieldName.Replace("!", ".") + memberInfo.MemberTypeInfo.DefaultMember.BindingName;
                    }
                }
                var criteria = CriteriaOperator.Parse("PropertyName = ? AND DataView.Oid = ?", fieldName, dataView.Oid);
                DataViewItem dataViewItem = objectSpace.FindObject<DataViewItem>(criteria);
                if (dataViewItem != null)
                {
                    dataViewItem.Number = gridColumn.VisibleIndex;
                    dataViewItem.GroupIndex = gridColumn.GroupIndex;
                    dataViewItem.SortIndex = gridColumn.SortIndex;
                    dataViewItem.SortOrder = gridColumn.SortOrder;
                }
                else
                {
                    dataViewItem = DataViewItem.CreateDataViewItem(objectSpace.Session, gridColumn.Caption, fieldName, gridColumn.VisibleIndex, gridColumn.SortOrder, gridColumn.SortIndex, gridColumn.GroupIndex);
                    dataViewItem.DataView = dataView;
                    dataView.DataViewItems.Add(dataViewItem);
                }
            }
        }

        private bool IsColumnEdit(ITypeInfo iTypeInfo, string propertyName)
        {
            bool result = false;
            if ((((ListView)View) != null && ((ListView)View).AllowEdit != null))
            {
                result = (((ListView)View).AllowEdit.ResultValue);
                if (result)
                {
                    IModelListView listView = Application.Model.Views.GetNode(Application.GetListViewId(iTypeInfo.Type)) as IModelListView;
                    if (listView != null && listView.Columns != null && listView.Columns.Count > 0)
                    {
                        IModelColumn listViewColumn = listView.Columns.GetNode(propertyName) as IModelColumn;
                        result = !(listViewColumn == null);
                    }
                }
            }
            return result;
        }
        #endregion Set Methods
    }
}
