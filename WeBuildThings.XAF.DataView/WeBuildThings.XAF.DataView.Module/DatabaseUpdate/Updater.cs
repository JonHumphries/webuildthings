﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using System;

namespace WeBuildThings.XAF.DataViews.DatabaseUpdate {
    
    public class Updater : ModuleUpdater {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion)  { }

        public override void UpdateDatabaseAfterUpdateSchema() 
        { 
            base.UpdateDatabaseAfterUpdateSchema();
        }

        public override void UpdateDatabaseBeforeUpdateSchema() 
        {
            base.UpdateDatabaseBeforeUpdateSchema();
        }
       
    }
}
