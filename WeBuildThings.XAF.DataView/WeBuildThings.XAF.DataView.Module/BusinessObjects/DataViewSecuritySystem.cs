﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System;
using System.ComponentModel;

namespace WeBuildThings.XAF.DataViews
{
    /*
    [MapInheritance(MapInheritanceType.ParentTable)]
    public class DataViewSecuritySystem : DataView
    {
        #region Fields
        private SecuritySystemUser _User;
        private XPCollection<SecuritySystemUser> _UsersLookup;
        #endregion Fields

        #region Constructors

        public DataViewSecuritySystem(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            User = Session.GetObjectByKey<SecuritySystemUser>(Session.GetKeyValue(SecuritySystem.CurrentUser));
        }

        #endregion Constructors

        #region Properties
        [Browsable(false)]
        [RuleRequiredField]
        [Persistent(mapTo: "SecurityUser")]
        public SecuritySystemUser User
        {
            get { return _User; }
            set { SetPropertyValue("User", ref _User, value); }
        }       

        [NonPersistent]
        [XafDisplayName("Sharing"), DataSourceCriteriaProperty("UserSharingCriterion")]
        public XPCollection<SecuritySystemUser> UsersLookup
        {
            get
            {
                if (_UsersLookup == null)
                {
                    _UsersLookup = new XPCollection<SecuritySystemUser>(Session);
                }
                return _UsersLookup;
            }
        }
        #endregion Properties
    }
     */
}
