﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.DataViews
{
    public class DefaultDataView : StandardTracking
    {
        #region Consts
        internal const string DEFAULT = "default";
        #endregion Consts

        #region Fields
        private DataView _Filter;
        private PersistedType _ClassType;
        #endregion Fields

        #region Constructors
        public DefaultDataView(Session session) : base(session) { }

        public DefaultDataView() : base() { }

        public static DefaultDataView CreateDefaultDataView(Session session, Type classType, DataView userFilter)
        {
            var result = session.FindObject<DefaultDataView>(new BinaryOperator("ClassType.FullName", classType.FullName));
            if (result == null)
            {
                result = new DefaultDataView(session);
                result.ClassType = PersistedType.CreatePersistedType(session, classType);
            }
            result.Filter = userFilter;
            return result;
        }
        #endregion Constructors

        #region Properties
        [Size(256)]
        public PersistedType ClassType
        {
            get { return _ClassType; }
            set { SetPropertyValue("ClassType", ref _ClassType, value); }
        }

        [Browsable(false)]
        public DataView Filter
        {
            get { return _Filter; }
            set { SetPropertyValue("Filter", ref _Filter, value); }
        }
        #endregion Properties
    }
}