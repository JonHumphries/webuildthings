﻿using DevExpress.Data;
using DevExpress.ExpressApp.Core;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.DataViews
{
    public class DataViewItem : BaseObject, INumberedList
    {
        #region Fields
        private string _Caption;
        private DataView _DataView;
        private string _PropertyName;
        private string _Expression;
        private Int32 _Number;
        private Int32 _SortIndex;
        private ColumnSortOrder _SortOrder;
        private Int32 _GroupIndex;
        #endregion Fields

        #region Constructors
        public DataViewItem(Session session) : base(session) { }

        public static DataViewItem CreateDataViewItem(Session session, string caption, string propertyName, Int32 number, ColumnSortOrder sortOrder, Int32 sortIndex, Int32 groupIndex)
        {
            DataViewItem result = new DataViewItem(session);
            result.Caption = caption;
            result.PropertyName = propertyName;
            result.Number = number;
            result.SortOrder = sortOrder;
            result.SortIndex = sortIndex;
            result.GroupIndex = groupIndex;
            return result;
        }

        public override void AfterConstruction()
        {
            GroupIndex = -1;
            SortIndex = -1;
        }
        #endregion Constructors

        #region Properties
        public string Caption
        {
            get { return _Caption; }
            set { SetPropertyValue("Caption", ref _Caption, value); }
        }

        public string PropertyName
        {
            get { return _PropertyName; }
            set { SetPropertyValue("PropertyName", ref _PropertyName, value); }
        }

        [ElementTypeProperty("TargetObjectType")]
        [ModelDefault("PropertyEditorType", "DevExpress.ExpressApp.Win.Editors.PopupExpressionPropertyEditor")]
        public string Expression
        {
            get { return _Expression; }
            set { SetPropertyValue("Expression", ref _Expression, value); }
        }

        public PersistedType TargetObjectType
        {
            get { return DataView.FilterCriteria.FilterObject; }
        }

        public Int32 Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public Int32 SortIndex
        {
            get { return _SortIndex; }
            set { SetPropertyValue("SortIndex", ref _SortIndex, value); }
        }

        public ColumnSortOrder SortOrder
        {
            get { return _SortOrder; }
            set { SetPropertyValue("SortOrder", ref _SortOrder, value); }
        }

        public Int32 GroupIndex
        {
            get { return _GroupIndex; }
            set { SetPropertyValue("GroupIndex", ref _GroupIndex, value); }
        }

        [Browsable(false)]
        [Association("DataView-DataViewItems")]
        public DataView DataView
        {
            get { return _DataView; }
            set { SetPropertyValue("DataView", ref _DataView, value); }
        }
        #endregion Properties

        #region INumberedList Implementation
        public IEnumerable<INumberedList> List
        {
            get { return DataView.DataViewItems; }
        }
        #endregion INumberedList Implementation
    }
}