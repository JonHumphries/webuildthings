﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Validation;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.Common.Utilities;

namespace WeBuildThings.XAF.DataViews
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class DataView : StandardTracking
    {
        #region Fields
        private string _Name;
        private string _Description;
        private string _SharedName;
        private FilterCriteria _SharingCriteria;
        private FilterCriteria _FilterCriteria;
        #endregion Fields

        #region Constructors
        public DataView(Session session) : base(session) { }

        public static T Create<T>(Session session, Type type, string filterCriteria) where T : DataView
        {
            var result = new DataView(session) as T;
            result.FilterCriteria.FilterObject = PersistedType.CreatePersistedType(session, type);
            result.FilterCriteria.Criterion = filterCriteria;
            return result;
        }

        public override void AfterConstruction()
        {
            SharingCriteria = new FilterCriteria(Session);
            SharingCriteria.FilterObject = PersistedType.CreatePersistedType(Session, typeof(XUser).FullName);
            FilterCriteria = new FilterCriteria(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [Browsable(false)]
        public IObjectSpace ObjectSpace;

        [Browsable(false)]
        public DevExpress.ExpressApp.DC.ITypeInfo ObjectTypeInfo;

        public string Name
        {
            get { return _Name; }
            set { if (SetPropertyValue("Name", ref _Name, value) && !IsLoading) { OnNameChanged(); } }
        }

        public string SharedName
        {
            get { return _SharedName; }
            set { SetPropertyValue("SharedName", ref _SharedName, value); }
        }

        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public FilterCriteria SharingCriteria
        {
            get { return _SharingCriteria; }
            set { SetPropertyValue("SharingCriteria", ref _SharingCriteria, value); }
        }

        public FilterCriteria FilterCriteria
        {
            get { return _FilterCriteria; }
            set { SetPropertyValue("FilterCriteria", ref _FilterCriteria, value); }
        }

        [Association("DataView-DataViewItems", typeof(DataViewItem)), Aggregated, ImmediatePostData]
        public XPCollection<DataViewItem> DataViewItems
        {
            get { return GetCollection<DataViewItem>("DataViewItems"); }
        }

        [Browsable(false)]
        public string UniqueName
        {
            get { return (this.CreatedBy != null && this.CreatedBy.UserName == SecuritySystem.CurrentUserName || String.IsNullOrWhiteSpace(SharedName)) ? Name : SharedName; }
        }

        public bool IsAccessible(IObjectSpace objectSpace)
        {
            return ((this.CreatedBy != null && this.CreatedBy.UserName == SecuritySystem.CurrentUserName) || (IsGranted(objectSpace, ObjectAccess.Read)) || (!string.IsNullOrWhiteSpace(SharingCriteria.CriterionText) && FilterCriteria.CheckCriteria((XPBaseObject)SecuritySystem.CurrentUser, SharingCriteria.CriterionText)));
        }

        public virtual bool IsGranted(IObjectSpace objectSpace, ObjectAccess access)
        {
            return true;
            /*
            object objectHandle = ObjectHandleHelper.CreateObjectHandle(XafTypesInfo.Instance, GetType(), Session.GetKeyValue(this).ToString());
            return SecuritySystem.IsGranted(new PermissionRequest(objectSpace, GetType(), objectHandle.GetType().Name, objectHandle, access.ToString()));
             */
        }

        public static List<T> UserConfigs<T>(IObjectSpace objectSpace, Type type) where T : DataView
        {
            XPCollection<T> sharedFilters = new XPCollection<T>(((XPObjectSpace)objectSpace).Session, new BinaryOperator("FilterCriteria.FilterObject.FullName", type.FullName));
            List<T> filters = new List<T>();
            foreach (T sharedDataFilters in sharedFilters)
            {
                if (sharedDataFilters.IsAccessible(objectSpace))
                {
                    filters.Add(sharedDataFilters);
                }
            }
            return filters;
        }

        public static DataView GetDefaultConfig<T>(IObjectSpace objectSpace, Type type) where T : DataView
        {
            XPCollection<DefaultDataView> defaultDataViews = new XPCollection<DefaultDataView>(((XPObjectSpace)objectSpace).Session, new BinaryOperator("ClassType.FullName", type.FullName));
            DataView result = null;
            if (defaultDataViews.Count > 0 && defaultDataViews[0].Filter != null)
            {
                result = defaultDataViews[0].Filter;
            }
            else
            {
                var list = UserConfigs<T>(objectSpace, type);
                if (list != null)
                {
                    result = list.Where(n => n.Name.ToLower() == DefaultDataView.DEFAULT).FirstOrDefault();
                }
            }
            return result;
        }
        #endregion Properties

        #region Event Methods
        private void OnNameChanged()
        {
            SetSharedName();
            SetFilterName();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetFilterName()
        {
            FilterCriteria.Name = Name;
            SharingCriteria.Name = Name;
        }

        private void SetSharedName()
        {
            if (!string.IsNullOrWhiteSpace(Name) && string.IsNullOrWhiteSpace(SharedName))
            {
                SharedName = Name;
            }
        }
        #endregion Set Methods

        #region Override Methods
        protected override void OnSaving()
        {
            if (ValidateSaving(true, this))
            {
                base.OnSaving();
            }
        }
        #endregion Override Methods

        #region Validation Methods
        public static bool ValidateSaving(bool throwException, DataView dataView)
        {
            bool result = true;
            if (!dataView.IsDeleted)
            {
                bool validateNameRequired = ValidationRules.ValidateRequiredField(throwException, dataView.Name, "Name");
                bool validateUniqueName = ValidateUniqueName(throwException, dataView);
                bool validateSharedNameRequired = ValidationRules.ValidateRequiredField(throwException, dataView.SharedName, "SharedName");
                bool validateUniqueSharedName = ValidateUniqueSharedName(throwException, dataView);
                bool validateType = ValidateType(throwException, dataView);
                result = (validateUniqueName && validateUniqueSharedName && validateType && validateNameRequired && validateSharedNameRequired);
            }
            return result;
        }

        /// <summary>
        /// Returns true if the name selected is unique
        /// </summary>
        /// <param name="throwException">throw an exception rather than returning false</param>
        public static bool ValidateUniqueName(bool throwException, DataView dataView)
        {
            bool result = false;
            var items = new XPCollection<DataView>(dataView.Session, new GroupOperator(GroupOperatorType.And, new BinaryOperator("Name", dataView.Name)
                                                                                                            , new BinaryOperator("FilterCriteria.FilterObject", dataView.FilterCriteria.FilterObject)
                                                                                                            , new BinaryOperator("CreatedBy.Oid", ((XUser)SecuritySystem.CurrentUser).Oid)));
            if (!items.Contains(dataView)) items.Add(dataView);
            items.TopReturnedObjects = 3;
            result = ValidationRules.ValidateUnique<DataView>(throwException, items);
            return result;
        }

        /// <summary>
        /// Returns true if the shared name is unique
        /// </summary>
        /// <param name="throwException">throw an exception rather than returning false</param>
        public static bool ValidateUniqueSharedName(bool throwException, DataView dataView)
        {
            bool result = false;
            var items = new XPCollection<DataView>(dataView.Session, new GroupOperator(GroupOperatorType.And, new BinaryOperator("SharedName", dataView.SharedName)
                                                                                                            , new BinaryOperator("FilterCriteria.FilterObject", dataView.FilterCriteria.FilterObject)));
            if (!items.Contains(dataView)) items.Add(dataView);
            items.TopReturnedObjects = 3;
            result = ValidationRules.ValidateUnique<DataView>(throwException, items);
            return result;
        }

        /// <summary>
        /// Returns true is the sharing criteria can be executed
        /// </summary>
        /// <param name="throwException">throw an exception rather than returning false</param>
        public static bool ValidateType(bool throwException, DataView dataView)
        {
            bool result = true;
            if (dataView.FilterCriteria != null )
            {
                result = ValidationRules.ValidateRequiredField(throwException, dataView.FilterCriteria.FilterObject, "DataView.FilterCriteria.FilterObject");
            }
            return result;
        }
        #endregion Validation Methods
    }
}
