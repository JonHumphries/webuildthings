﻿namespace WeBuildThings.XAF.DataViews
{
    public enum FilterWithObjectsProcessorMode { ObjectToString, StringToObject, ObjectToObject };
}
