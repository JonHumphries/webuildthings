﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using WeBuildThings.DataMergeProcess;

namespace WeBuildThings.DataMergeProcess.Test
{
    [TestClass]
    public class DataMergeTests
    {
        [TestMethod]
        public void MergeTest1()
        {
            string template = "This was a #Qualifier# Test";
            string expectedResult = "This was a Good Test";
            var dataMap = new List<DataMergeItem>();
            dataMap.Add(new DataMergeItem("#Qualifier#", "Qualifier") { Value = "Good" });
            string actualResult = DataMerge.Merge(template, dataMap);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void MergeTest2()
        {
            string template = "This was a #Qualifier# Test";
            string expectedResult = "This was a Good Test";
            var dataMap = new List<DataMergeItem>();
            dataMap.Add(new DataMergeItem("#Qualifier#", "Qualifier") { Value = "Good" });
            dataMap.Add(new DataMergeItem("#CustomerId#", "CustomerId") { Value = "123456" });
            string actualResult = DataMerge.Merge(template, dataMap);
            Assert.AreEqual(actualResult, expectedResult);
        }
        
        [TestMethod]
        public void MergeTest3()
        {
            string template = "This was a #Qualifier# Test for #CustomerId#";
            string expectedResult = "This was a Good Test for 123456";
            var dataMap = new List<DataMergeItem>();
            dataMap.Add(new DataMergeItem("#Qualifier#", "Qualifier") { Value = "Good" });
            dataMap.Add(new DataMergeItem("#CustomerId#", "CustomerId") { Value = "123456" });
            string actualResult = DataMerge.Merge(template, dataMap);
            Assert.AreEqual(actualResult, expectedResult);
        }

        [TestMethod]
        public void MergeTest4()
        {
            string template = "This was a #Qualifier# Test for #CustomerId#";
            string expectedResult = "This was a Good Test for 123456";
            var testObject = new TestItem();
            testObject.Qualifier = "Good";
            testObject.CustomerId = 123456;
            DataMerge dataMerge = new DataMerge();
            dataMerge.DataMergeItems.Add(new DataMergeItem("#Qualifier#", "Qualifier"));
            dataMerge.DataMergeItems.Add(new DataMergeItem("#CustomerId#", "CustomerId"));
            string actualResult = dataMerge.Merge(template, testObject);
            Assert.AreEqual(actualResult, expectedResult);
        }
    }

    public class TestItem
    {
        public string Qualifier { get; set; }
        public int CustomerId { get; set; }
    }
}
