﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common;
using WeBuildThings.DataMergeProcess.Extensions;
using WeBuildThings.Common.Extensions;
using WeBuildThings.DataMergeProcess;

namespace WeBuildThings.DataMergeProcess.Test
{
    [TestClass]
    public class DataMergeItemTests
    {

        [TestMethod]
        public void SetValueTest1()
        {
            DataMerge targetObject = new DataMerge();
            targetObject.Name = "TEST NAME";
            DataMergeItem dataMergeItem = new DataMergeItem();
            dataMergeItem.PropertyName = "Name";
            dataMergeItem.NullValueProcess = NullValueProcess.Null;
            dataMergeItem.DateFormat = DateFormats.MMDDYYYY2;
            dataMergeItem.StringLength = -1;
            dataMergeItem.SetStringValue(targetObject);
            Assert.AreEqual(targetObject.Name, dataMergeItem.Value);
        }
    }
}
