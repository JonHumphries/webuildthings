﻿using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.DataMergeProcess.Extensions
{
    public interface IDataMergeItem
    {
        string Key { get; }
        string Value { get; set; }
        string PropertyName { get; }
        NullValueProcess NullValueProcess { get; }
        DateFormats DateFormat { get; }
        int StringLength { get; }
    }

    public static class IDataMergeExtensions
    {
        public static void SetStringValue(this IDataMergeItem dataMergeItem, object targetObject)
        {
            if (!string.IsNullOrWhiteSpace(dataMergeItem.PropertyName))
            {
                var tempValue = TypeExtensions.GetProperty(targetObject, dataMergeItem.PropertyName);
                var dateFormat = DateFormat.GetDateFormat(dataMergeItem.DateFormat);
                dataMergeItem.Value = tempValue.ToFormattedString(dataMergeItem.NullValueProcess, dateFormat, dataMergeItem.StringLength);
            }           
        }

    }
}
