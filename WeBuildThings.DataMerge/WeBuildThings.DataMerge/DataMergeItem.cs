﻿using System;
using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;
using WeBuildThings.DataMergeProcess.Extensions;

namespace WeBuildThings.DataMergeProcess
{
    public class DataMergeItem : IDataMergeItem
    {
        #region Fields
        #endregion Fields

        #region Constructors
        public DataMergeItem() { Initialize(); }

        public DataMergeItem(string key, string propertyName) { Initialize(); Key = key; PropertyName = propertyName; }

        public DataMergeItem(string key) { Initialize(); Key = key; }

        private void Initialize() { StringLength = -1; DateFormat = DateFormats.MMDDYYYY2; }
        #endregion Constructors

        #region Properties
        public string Key { get; set; }

        public string PropertyName { get; set; }

        public string Value { get; set; }

        public NullValueProcess NullValueProcess  { get; set; }

        public DateFormats DateFormat { get; set; }

        public int StringLength { get; set; }
        #endregion Properties


        #region Public Methods
        public void SetValue(object targetObject)
        {
            this.SetStringValue(targetObject);
        }
        #endregion Public Methods

    }
}
