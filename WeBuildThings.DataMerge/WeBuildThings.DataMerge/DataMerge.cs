﻿using System.Collections.Generic;
using WeBuildThings.DataMergeProcess.Extensions;

namespace WeBuildThings.DataMergeProcess
{
    public class DataMerge
    {
        #region Fields
        private HashSet<DataMergeItem> _DataMergeItems;
        #endregion Fields

        #region Constructors
        public DataMerge() { Initialize(); }

        private void Initialize()
        { DataMergeItems = new HashSet<DataMergeItem>(); }
        #endregion Constructors

        #region Properties
        public string Name { get; set; }

        public HashSet<DataMergeItem> DataMergeItems
        {
            get { return _DataMergeItems; }
            set { if (_DataMergeItems != value) _DataMergeItems = value; }
        }
        #endregion Properties
 
        public string Merge(string template, object targetObject)
        {
            string result = template;
            foreach (var item in DataMergeItems)
            {
                item.SetValue(targetObject);
            }
            result = Merge(template, DataMergeItems);
            return result;
        }

        public static string Merge(string template, IEnumerable<IDataMergeItem> dataMap)
        {
            string result = template;
            foreach (var kvp in dataMap)
            {
                result = result.Replace(kvp.Key, kvp.Value);
            }
            return result;
        }

    }
}
