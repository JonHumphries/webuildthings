﻿using System;
using System.Xml.Serialization;

namespace WeBuildThings.Exceptions
{
    public class SerializationException : ExceptionBase
    {

        public SerializationException(string message) : base(message) {}

        public SerializationException(string message, Type type) : base(message, type) { }

        public SerializationException(string message, Type type, Exception innerException) : base(message, type, innerException) { }

    }
    
    public class UnknownNodeException : Exception
    {
        public int LineNumber { get; set; }
        public int LinePosition { get; set; }
        public string LocalName { get; set; }
        public string Name { get; set; }
        public string NamespaceURI { get; set; }
        public System.Xml.XmlNodeType NodeType { get; set; }
        public object ObjectBeingDeserialized { get; set; }
        public string Text { get; set; }

        public UnknownNodeException(string message) : base(message) { }

        public UnknownNodeException(XmlNodeEventArgs e)
        {
            LineNumber = e.LineNumber;
            LinePosition = e.LinePosition;
            LocalName = e.LocalName;
            Name = e.Name;
            NamespaceURI = e.NamespaceURI;
            NodeType = e.NodeType;
            ObjectBeingDeserialized = e.ObjectBeingDeserialized;
            Text = e.Text;
        }
    }

    public class UnreferencedObjectException : Exception
    {
        public string UnreferencedId { get; set; }
        public object UnreferencedObject { get; set; }

        public UnreferencedObjectException(string message) : base(message) { }

        public UnreferencedObjectException(UnreferencedObjectEventArgs e) : base(e.UnreferencedObject.ToString())
        {
            UnreferencedId = e.UnreferencedId;
            UnreferencedObject = e.UnreferencedObject;
        }
    }
}
