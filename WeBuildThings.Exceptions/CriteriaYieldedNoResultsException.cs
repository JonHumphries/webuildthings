﻿using System;

namespace WeBuildThings.Exceptions
{
    public class CriteriaYieldedNoResultsException : Exception
    {
        public CriteriaYieldedNoResultsException(string message) : base(message) { }
    }
}
