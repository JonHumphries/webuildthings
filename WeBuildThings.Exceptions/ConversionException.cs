﻿
namespace WeBuildThings.Exceptions
{
    public class ConversionException : ExceptionBase
    {
        public ConversionException(string message) : base(message) { }
    }
}
