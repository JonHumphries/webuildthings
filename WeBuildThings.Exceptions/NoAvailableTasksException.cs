﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Exceptions
{
    public class NoAvailableTasksException : ExceptionBase
    {
        public NoAvailableTasksException() : base() { }

        public NoAvailableTasksException(string message, Exception innerException) : base(message, innerException) { }

        public NoAvailableTasksException(string message) : base(message) { }

        public NoAvailableTasksException(string message, Type type) : base(message, type) { }
    }
}
