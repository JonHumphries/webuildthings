﻿using System;
using System.Data.SqlClient;

namespace WeBuildThings.Exceptions
{
    public class EDITimeoutException : Exception
    {
        public int RowNumber { get; set; }
        public string RowId { get; set; }
        public SqlException OriginalException { get; set; }

        public EDITimeoutException(int rowNumber, string rowId, SqlException originalException)
        {
            RowNumber = rowNumber;
            RowId = rowId;
            OriginalException = originalException;
        }
    }
}
