﻿using System;
using System.Runtime.Serialization;

namespace WeBuildThings.Exceptions
{
    public class UniqueRecordException : ExceptionBase
    {
        public UniqueRecordException(Type type) : base("Duplicate Records Found", type) { }

        public UniqueRecordException(string message, Type type) : base(message, type)  { }

        public UniqueRecordException(string message) : base(message) { }

        public UniqueRecordException() : base() { }

        public UniqueRecordException(string message, Exception innerException) : base(message, innerException) { }

        public UniqueRecordException(SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
