﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Exceptions
{
    public class InvalidPhoneNumberException: ExceptionBase
    {
        public string PhoneNumber { get; set; }

        public InvalidPhoneNumberException(string message, Type type, string phoneNumber) : base(message, type) 
        {
            PhoneNumber = PhoneNumber;
        }
    }
}
