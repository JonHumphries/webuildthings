﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Exceptions.Extensions;

namespace WeBuildThings.Test
{
    [TestClass]
    public class ExceptionExtensionsTests
    {
        public Exception exception
        {
            get
            {
                return new Exception("Level 1 Exception", new Exception ("Level 2 Exception", new Exception("Level 3 Exception")));
            }
        }

        [TestMethod]
        public void InnerExceptionMessages_ThreeLayers()
        {
            string expectedResult = string.Format("Level 1 Exception{0}-Level 2 Exception{0}--Level 3 Exception", Environment.NewLine);
            var actualResult = exception.InnerExceptionMessages();
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
