﻿using System;
using System.Runtime.Serialization;

namespace WeBuildThings.Exceptions
{
    public class NewObjectException : ExceptionBase
    {
        public NewObjectException (Type type) : base("Duplicate Records Found", type) { }

        public NewObjectException (string message, Type type) : base(message, type)  { }

        public NewObjectException (string message) : base(message) { }

        public NewObjectException () : base() { }

        public NewObjectException (string message, Exception innerException) : base(message, innerException) { }

        public NewObjectException (SerializationInfo info, StreamingContext context) : base(info, context) { }
    }
}
