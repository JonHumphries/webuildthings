﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Exceptions.Extensions
{
    public static class ExceptionExtensions
    {

        public static string InnerExceptionMessages(this Exception exception, string layerIdentifier = "")
        {
            string result = exception.Message;
            if (exception.InnerException != null)
            {
                layerIdentifier += "-";
                result += string.Format("{0}{1}{2}",Environment.NewLine, layerIdentifier, exception.InnerException.InnerExceptionMessages(layerIdentifier));
            }
            return result.Trim();
        }
    }
}
