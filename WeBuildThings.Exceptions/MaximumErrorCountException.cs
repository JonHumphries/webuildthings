﻿
namespace WeBuildThings.Exceptions
{
    public class MaximumErrorCountException: ExceptionBase
    {
        public MaximumErrorCountException(string message) : base(message) { }
    }
}
