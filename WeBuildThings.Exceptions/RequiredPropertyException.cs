﻿using System;

namespace WeBuildThings.Exceptions
{
    [Serializable]
    public class RequiredPropertyException: Exception
    {
        public Type Type { get; set; }
        
        public RequiredPropertyException(Type type, string message): base(message)
        {
            Type = type;
        }

        public override void GetObjectData(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
