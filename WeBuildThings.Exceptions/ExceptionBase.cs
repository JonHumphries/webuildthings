﻿using System;
using System.Runtime.Serialization;

namespace WeBuildThings.Exceptions
{
    public abstract class ExceptionBase : Exception
    {
        public ExceptionBase() : base() { }

        public ExceptionBase(string message) : base(message) { }

        public ExceptionBase(string message, Type type) : base(message)  { this.Source = type.FullName; }

        public ExceptionBase(string message, Exception innerException) : base(message, innerException) { }

        public ExceptionBase(string message, Type type, Exception innerException) : base(message, innerException) { Source = type.FullName; }

        public ExceptionBase(SerializationInfo info, StreamingContext context) : base(info, context) { }


    }
}
