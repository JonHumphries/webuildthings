﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Exceptions
{
    public class PasswordsDoNotMatchException : ExceptionBase
    {
        private const string PASSWORD_EXCEPTION_MESSAGE = "The Passwords do not match, please try again.";

        public PasswordsDoNotMatchException() : base(PASSWORD_EXCEPTION_MESSAGE) { }
    }
}
