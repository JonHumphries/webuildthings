﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Dashboards.Security
{
    public class DashboardRole : StandardTracking
    {
        #region Fields
        private XRole _BaseRole;
        #endregion Fields

        #region Constructors
        public DashboardRole(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public XRole BaseRole
        {
            get { return _BaseRole; }
            set { SetPropertyValue("BaseRole", ref _BaseRole, value); }
        }

        [Association("Dashboard-DashboardRole", typeof(Dashboard))]
        public XPCollection<Dashboard> Dashboards
        {
            get { return GetCollection<Dashboard>("Dashboards"); }
        }
        #endregion Properties

        #region Public Methods
        public static IEnumerable<Dashboard> GetActiveDashboards(XRole[] roles)
        {
            IEnumerable<Dashboard> result = null;
            if (roles != null && roles.Length > 0)
            {
                var session = roles[0].Session;
                var dashboardRoles = new XPCollection<DashboardRole>(session, new InOperator("BaseRole", roles));
                if (dashboardRoles != null)
                {
                    result = dashboardRoles.SelectMany(n => n.Dashboards).Where(d => d.Active).OrderBy(d => d.Number);
                }
            }
            return result;
        }
        #endregion Public Methods
    }
}
