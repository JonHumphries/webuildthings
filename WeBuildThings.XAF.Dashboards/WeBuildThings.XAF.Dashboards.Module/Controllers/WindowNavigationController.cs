﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Security;
using System;
using System.Linq;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Dashboards.Security;


namespace WeBuildThings.XAF.Dashboards
{
    /// <summary>
    /// Custom Controller for real time Navigation updating
    /// </summary>
    public partial class WindowNavigationController : WindowController
    {
        #region Consts
        private const string ListView = "_ListView";
        private string DashboardListView = typeof(Dashboard).Name + ListView;
        #endregion Consts

        #region Fields
        private ShowNavigationItemController _NavigationController;
        #endregion Fields

        #region Constructors
        public WindowNavigationController()
        {
            TargetWindowType = WindowType.Main;
        }
        #endregion Constructors

        #region Properties
        public ShowNavigationItemController NavigationController
        {
            get { return _NavigationController; }
            set { ShowNavigationItemController oldController = _NavigationController;  if (_NavigationController != value) { _NavigationController = value; OnNavigationControllerChanged(oldController); } }
        }
        #endregion Properties

        #region Event Methods
        protected override void OnFrameAssigned()
        {
            SetNavigationController();
            base.OnFrameAssigned();
        }

        protected virtual void OnNavigationControllerChanged(ShowNavigationItemController oldNavigationController)
        {
            if (oldNavigationController != null)
            {
                oldNavigationController.ItemsInitialized -= NavigationController_OnItemsInitialized;
            }
            if (NavigationController != null)
            {
                NavigationController.ItemsInitialized -= NavigationController_OnItemsInitialized;
                NavigationController.ItemsInitialized += NavigationController_OnItemsInitialized;
            }
        }

        protected virtual void NavigationController_OnItemsInitialized(object sender, EventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            ChoiceActionItemCollection navigationItems = ((ShowNavigationItemController)sender).ShowNavigationItemAction.Items;
            LoadDashboardsNavigationItems(objectSpace, navigationItems);
        }
        #endregion Event Methods

        #region Set Methods
        private void SetNavigationController()
        {
            NavigationController = Frame.GetController<ShowNavigationItemController>();
        }
        #endregion Set Methods

        #region Methods
        /// <summary>
        /// Add Custom Dashboards Navigation Group
        /// </summary>
        /// <param name="navigationItems"></param>
        private void LoadDashboardsNavigationItems(IObjectSpace objectSpace, ChoiceActionItemCollection navigationItems)
        {
            var dashboardsSetupNavItem = navigationItems.Find(DashboardListView, ChoiceActionItemFindType.Recursive, ChoiceActionItemFindTarget.Leaf);
            if (dashboardsSetupNavItem != null)
            {
                dashboardsSetupNavItem.Active["Active"] = SecuritySystem.IsGranted(new ClientPermissionRequest(typeof(Dashboard), null, null, DevExpress.ExpressApp.Security.SecurityOperations.Create));
            }
            var userRoles = ((XUser)SecuritySystem.CurrentUser).GetXRoles();
            var dashboards = DashboardRole.GetActiveDashboards(userRoles.ToArray());
            if (dashboards.Count() > 0)
            {
                ChoiceActionItem navigationGroup = new ChoiceActionItem("Dashboards", null) { ImageName = "BO_Folder" };
                navigationItems.Add(navigationGroup);
                var dashboardGroups = dashboards.SelectMany(d => d.NavigationGroups).Distinct();
                foreach (NavigationGroup dashboardGroup in dashboardGroups)
                {
                    var action = new ChoiceActionItem(dashboardGroup.Oid.ToString(), dashboardGroup.Name, null);
                    action.ImageName = "BO_Folder";
                    navigationGroup.Items.Add(action);
                }
                foreach (Dashboard dashboard in dashboards)
                {
                    if (dashboard.NavigationGroups != null && dashboard.NavigationGroups.Count > 0)
                    {
                        foreach (NavigationGroup group in dashboard.NavigationGroups)
                        {
                            var action = new ChoiceActionItem(string.Format("{0}_{1}", dashboard.Oid, group.Oid), dashboard.Name, new ViewShortcut("DashboardViewer_DetailView", dashboard.Oid.ToString()));
                            action.ImageName = "BO_Unknown";
                            action.Active["HasRights"] = true;
                            navigationGroup.Items.FindItemByID(group.Oid.ToString()).Items.Add(action);
                        }
                    }
                    else
                    {
                        var action = new ChoiceActionItem(dashboard.Oid.ToString(), dashboard.Name, new ViewShortcut(typeof(Dashboard), dashboard.Oid.ToString(), "DashboardViewer_DetailView"));
                        action.ImageName = "BO_Unknown";
                        action.Model.Index = dashboard.Number;
                        action.Active["HasRights"] = true;
                        navigationGroup.Items.Add(action);
                    }
                }
            }
        }
        #endregion Methods

        #region IDisposable Implementation
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_NavigationController != null)
                {
                    _NavigationController.NavigationItemCreated -= NavigationController_OnItemsInitialized;
                }
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion IDisposable Implementation

    }

}
