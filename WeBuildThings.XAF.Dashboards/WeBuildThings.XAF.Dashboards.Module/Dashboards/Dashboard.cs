﻿using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;
using System;
using System.Drawing;
using WeBuildThings.XAF.Common.Security;
using WeBuildThings.XAF.Dashboards.Security;

namespace WeBuildThings.XAF.Dashboards
{
    [DefaultClassOptions]
    [SecurityOperations("Dashboard", "DashboardAccess")]
    public class Dashboard : BaseObject, IDashboard, IDisposable
    {
        #region Fields
        private bool _Disposed;
        private Image _Icon;
        private string _XmlFilePath;
        private bool _Active;
        private int _Number;
        private string _Name;
        private bool _UseStoredProcedure;
        private bool _HasParameter;
        private string _StoredProcedureName;
        private string _ParameterList;
        #endregion Fields

        #region Constructors
        public Dashboard(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public int Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public bool Active
        {
            get { return _Active; }
            set { SetPropertyValue("Active", ref _Active, value); }
        }
        
        public string XmlFilePath
        {
            get { return _XmlFilePath; }
            set { SetPropertyValue("XmlFilePath", ref _XmlFilePath, value); }
        }

        [ValueConverter(typeof(ImageValueConverter)), ImmediatePostData, Size(SizeAttribute.Unlimited)]
        public Image Icon
        {
            get { return _Icon; }
            set { SetPropertyValue("Icon", ref _Icon, value); }
        }
        
        [ImmediatePostData]
        public bool UseStoredProcedure
        {
            get { return _UseStoredProcedure; }
            set { SetPropertyValue("UseStoredProcedure", ref _UseStoredProcedure, value); }
        }

        [Appearance("useStoredProcedure", Visibility = ViewItemVisibility.Hide, Criteria = "UseStoredProcedure = false")]
        public string StoredProcedureName
        {
            get { return _StoredProcedureName; }
            set { SetPropertyValue("StoredProcedureName", ref _StoredProcedureName, value); }
        }

        [ImmediatePostData]
        public bool HasParameter
        {
            get { return _HasParameter; }
            set { SetPropertyValue("HasParameter", ref _HasParameter, value); }
        }

        [Appearance("hasParameter", Visibility = ViewItemVisibility.Hide, Criteria = "HasParameter = false")]
        public string ParameterList
        {
            get { return _ParameterList; }
            set { SetPropertyValue("ParameterList", ref _ParameterList, value); }
        }

        [Association("Dashboard-NavigationGroup", typeof(NavigationGroup))]
        public XPCollection<NavigationGroup> NavigationGroups
        {
            get { return GetCollection<NavigationGroup>("NavigationGroups"); }
        }

        [Association("Dashboard-DashboardRole", typeof(DashboardRole))]
        public XPCollection<DashboardRole> Roles
        {
            get { return GetCollection<DashboardRole>("Roles"); }
        }
        #endregion Properties

        #region IDisposable Implementation
        ~Dashboard()
        {
            Dispose(false);
        }
        
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    if (_Icon != null)
                    {
                        _Icon.Dispose();
                        _Icon = null;
                    }
                    _Disposed = true;
                }
            }
        }
        #endregion IDisposable Implementation
    }
}
