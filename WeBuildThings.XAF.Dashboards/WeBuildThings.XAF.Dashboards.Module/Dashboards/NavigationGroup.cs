﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.Linq;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Validation;

namespace WeBuildThings.XAF.Dashboards
{
    [DefaultClassOptions]
    public class NavigationGroup : BaseObject, INavigationGroup, INumberedList
    {
        #region Fields
        private string _Name;
        private NavigationGroup _Parent;
        private string _Description;
        private bool _Active;
        private int _Number;
        #endregion Fields

        #region Constructors
        public NavigationGroup(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public int Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public bool Active
        {
            get { return _Active; }
            set { SetPropertyValue("Active", ref _Active, value); }
        }

        [Association("Parent-Groups", typeof(NavigationGroup))]
        public NavigationGroup Parent
        {
            get { return _Parent; }
            set { SetPropertyValue("Parent", ref _Parent, value); }
        }

        [Association("Parent-Groups", typeof(NavigationGroup))]
        public XPCollection<NavigationGroup> SubGroups
        {
            get { return GetCollection<NavigationGroup>("SubGroups"); }
        }

        [Association("Dashboard-NavigationGroup", typeof(Dashboard))]
        public XPCollection<Dashboard> Dashboards
        {
            get { return GetCollection<Dashboard>("Dashboards"); }
        }
        #endregion Properties

        #region Validation Methods
        public bool ValidateNavigationGroup(bool throwException)
        {
            return ValidateNavigationGroup(throwException, this);
        }

        public static bool ValidateNavigationGroup(bool throwException, NavigationGroup navigationGroup)
        {
            bool thisUnique = true;
            bool parentValid = true;
            if (navigationGroup.Parent != null && navigationGroup.Parent.SubGroups != null)
            {
                thisUnique = ValidationRules.ValidateUnique<string>(throwException, navigationGroup.Parent.SubGroups.Select(n => n.Name));
                parentValid = navigationGroup.Parent.ValidateNavigationGroup(throwException);
            }
            return (thisUnique && parentValid);
        }
        #endregion Validation Methods

        #region Override Methods
        protected override void OnSaving()
        {
            if (!IsDeleted && ValidateNavigationGroup(true))
            {
                base.OnSaving();
            }
        }
        #endregion Override Methods

        #region INumberedList Implementation
        public System.Collections.Generic.IEnumerable<INumberedList> List
        {
            get { return Parent.SubGroups; }
        }
        #endregion INumberedList Implementation
    }
}
