﻿using DevExpress.Xpo;

namespace WeBuildThings.XAF.Dashboards
{
    interface INavigationGroup
    {
        int Number { get; set; }

        string Name { get; set; }

        string Description { get; set; }

        bool Active { get; set; }
       
        NavigationGroup Parent { get; set; }
        
        XPCollection<Dashboard> Dashboards { get; }

        XPCollection<NavigationGroup> SubGroups { get; }
    }
}