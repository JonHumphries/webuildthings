﻿using System.Drawing;

namespace WeBuildThings.XAF.Dashboards
{
    public interface IDashboard
    {
        int Number { get; set; }
        string Name { get; set; }
        Image Icon { get; set; }
        bool Active { get; set; }
        string XmlFilePath { get; set; }
        bool HasParameter { get; set; }
        string ParameterList { get; set; }
    }
}
