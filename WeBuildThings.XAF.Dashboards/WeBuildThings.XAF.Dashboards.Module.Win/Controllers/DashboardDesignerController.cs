using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using WeBuildThings.XAF.Dashboards;
using WeBuildThings.XAF.Dashboards.Win;

namespace WeBuildThings.XAF.Dashboards.Module.Win.Controllers {
    public partial class DashboardDesignerController : ViewController {
        public DashboardDesignerController() {
            InitializeComponent();
            RegisterActions(components);
            TargetObjectType = typeof(IDashboard);
        }

        public SimpleAction DashboardEditAction {
            get { return dashboardEdit; }
        }

        protected override void OnActivated() {
            base.OnActivated();
            View.SelectionChanged += (s, e) => UpdateActionState();
            UpdateActionState();
        }

        void UpdateActionState() 
        {
            if (SecuritySystem.Instance is ISecurityComplex) 
            {
                bool isGranted = true;
                foreach (object selectedObject in View.SelectedObjects) 
                {
                    var clientPermissionRequest = new PermissionRequest(View.ObjectSpace, typeof(IDashboard), "XmlFilePath", ObjectSpace.GetObjectHandle(selectedObject), SecurityOperations.Write);
                    isGranted = SecuritySystem.IsGranted(clientPermissionRequest);
                }
                dashboardEdit.Active["SecurityIsGranted"] = isGranted;

                //dashboardEdit.Active["SecurityIsGranted"] = ((XUser)SecuritySystem.CurrentUser).IsAdministrator();
            }

            return;
        }

        void dashboardEdit_Execute(object sender, SimpleActionExecuteEventArgs e) {
            using (var form = new DashboardDesignerForm(View.CurrentObject as IDashboard))
            {
                form.ShowDialog();
            }
        }
    }
}
