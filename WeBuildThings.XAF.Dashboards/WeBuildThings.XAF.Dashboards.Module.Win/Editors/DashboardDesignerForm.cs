﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.DashboardWin.Native;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using WeBuildThings.XAF.Dashboards;

namespace WeBuildThings.XAF.Dashboards.Win
{
    public partial class DashboardDesignerForm : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        private WeBuildThings.XAF.Dashboards.IDashboard _DashboardDefinition;

        public DashboardDesignerForm(WeBuildThings.XAF.Dashboards.IDashboard dashboard)
        {
            InitializeComponent();
            _DashboardDefinition = dashboard;
            if (System.IO.File.Exists(_DashboardDefinition.XmlFilePath))
                dashboardDesigner1.Dashboard.LoadFromXml(_DashboardDefinition.XmlFilePath);
            else
                dashboardDesigner1.Dashboard = new DevExpress.DashboardCommon.Dashboard();
        }

        void DashboardDesignerForm_Load(object sender, EventArgs e)
        {
            fileNewBarItem1.Visibility = BarItemVisibility.Never;
            fileOpenBarItem1.Visibility = BarItemVisibility.Never;
            fileSaveBarItem1.Visibility = BarItemVisibility.Never;
            fileSaveAsBarItem1.Visibility = BarItemVisibility.Never;

            //ribbonControl.Toolbar.ItemLinks.Remove(fileSaveBarItem1);
            fileSaveBarItem1.Enabled = false;
            // barButtonItemSave.Glyph = GetImage("MenuBar_Save_32x32.png");
            //fileSaveBarItem1.ItemClick += Save;
            barButtonItemSaveandclose.ItemClick += SaveAndClose;
            barButtonItemSaveandclose.Enabled = false;
            // barButtonItemSave.Glyph = GetImage("MenuBar_SaveAndClose_32x32.png");
        }


        protected override void OnClosed(EventArgs e)
        {
            dashboardDesigner1.Dashboard.Dispose();
            dashboardDesigner1 = null;
            base.OnClosed(e);
        }

        void Save(object sender, ItemClickEventArgs e)
        {
            dashboardDesigner1.Dashboard.SaveToXml(_DashboardDefinition.XmlFilePath);
            fileSaveBarItem1.Enabled = barButtonItemSaveandclose.Enabled = false;
        }

        void SaveAndClose(object sender, ItemClickEventArgs e)
        {
            Save(null, null);
            DialogResult = DialogResult.OK;
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            if (dashboardDesigner1.IsDashboardModified)
            {
                DialogResult result = XtraMessageBox.Show(LookAndFeel, this, "Do you want to save changes ?", "Dashboard Designer",
                    MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Cancel)
                    e.Cancel = true;
            }
        }

        private void Close_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (dashboardDesigner1.IsDashboardModified)
            {
                var result = XtraMessageBox.Show("Do you want to save changes?", "Dashboard Designer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Save(null, null);
                    DialogResult = DialogResult.Yes;
                }
                else if (result == DialogResult.No)
                    DialogResult = DialogResult.Cancel;
            }
            else
            {
                DialogResult = DialogResult.Yes;
            }
        }
    }
}