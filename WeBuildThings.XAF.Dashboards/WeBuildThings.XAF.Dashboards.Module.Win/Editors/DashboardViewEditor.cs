﻿using System;
using System.Windows.Forms;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.DashboardWin;
using WeBuildThings.XAF.Dashboards;

namespace WeBuildThings.XAF.Dashboards.Win
{
    [PropertyEditor(typeof(String), false)]
    public class DashboardViewEditor : WinPropertyEditor
    {
        public DashboardViewEditor(Type objectType, IModelMemberViewItem model) : base(objectType, model) { }

        public DashboardViewer DashboardViewer
        {
            get { return (DashboardViewer)Control; }
        }

        protected override object CreateControlCore()
        {
			return new DashboardViewer 
                { 
                    Margin = new Padding(0), 
                    Padding = new Padding(0), 
                    AllowPrintDashboard = true, 
                    AllowPrintDashboardItems = true 
                };
		}

        protected override void ReadValueCore()
        {
            var dashboardDef = CurrentObject as IDashboard;
            var dashboard = new DevExpress.DashboardCommon.Dashboard();
            
            if (System.IO.File.Exists(dashboardDef.XmlFilePath))
            {
                try
                {
                    dashboard.LoadFromXml(dashboardDef.XmlFilePath);

                    if (dashboardDef.HasParameter) // Todo: Handle Exceptions!
                    {
                        string[] paramsList = dashboardDef.ParameterList.Split(';');
                        foreach (string paramString in paramsList)
                        {
                            string[] param = paramString.Split('=');
                            dashboard.Parameters[param[0]].Value = bool.Parse(param[1]);    // Todo: Allow for Parsing Parameters other then Bool
                        }
                    }
                }
                catch (Exception e)
                {
                    dashboard.Dispose();
                    // TODO: Handle Exception
                    DevExpress.Persistent.Base.Tracing.Tracer.LogError(e);
                }
            }

            DashboardViewer.Dashboard = dashboard;

            return;
        }
    }

}
