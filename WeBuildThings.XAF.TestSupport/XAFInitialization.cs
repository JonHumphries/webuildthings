﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.TestSupport
{
    /// <summary>
    /// Initializes a static XAF Session using the object model in the assemblies connection
    /// and the connection string "TestConnectionString".
    /// </summary>
    public static class XAFInitialization
    {

        private static Session _Session;


        /// <summary>
        /// Add the Assemblies needed to access the test object models.
        /// </summary>
        /// <example>typeof(TestType).Assembly</example>
        public static List<Assembly> Assemblies = new List<Assembly>() { typeof(PhoneNumber).Assembly, typeof(XPhoneNumber).Assembly };

        /// <summary>
        /// Name of the connection string name in app.config.
        /// Defaults to "TestConnectionString"
        /// </summary>
        public static string ConnectionStringConfigName = "TestConnectionString";


        /// <summary>
        /// Either uses the existing static Session or creates a new one.
        /// </summary>
        public static Session Session
        {
            get
            {
                if (_Session == null)
                {
                    _Session = ConnectionHelper.CreateSession(ConfigurationManager.ConnectionStrings[ConnectionStringConfigName].ConnectionString, Assemblies.ToArray());
                }
                return _Session;
            }
        }

    }
}
