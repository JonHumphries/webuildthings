﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Exceptions;

namespace WeBuildThings.Validation
{
    public static class ValidationRules
    {
        #region Consts
        public const string EMPTY_PARAMETER_EXCEPTION_MESSAGE = "Property Name is a required parameter.";
        #endregion Consts

        public static bool ValidateUnique<T>(bool throwException, IEnumerable<T> searchResults)
        {
            bool result = false;
            if (searchResults == null || searchResults.Count() <= 1)
            {
                result = true;
            }
            if (throwException && !result)
            {
                throw new UniqueRecordException(typeof(T));
            }
            return result;
        }

        public static bool ValidateAccount<T>(bool throwException, string accountSid, string authToken)
        {
            string message = string.Empty;
            if (string.IsNullOrEmpty(accountSid) || string.IsNullOrEmpty(authToken))
            {
                message = "Both Account SID and Auth Token must be set before lookup";
            }
            if (throwException && !string.IsNullOrEmpty(message))
            {
                throw new RequiredPropertyException(typeof(T), message);
            }
            return string.IsNullOrEmpty(message);
        }

        private static int[] phoneNumberLengths = new int[] { 10, 11 };
        public static bool ValidatePhoneNumber<T>(bool throwException, string phoneNumber)
        {
            string message = string.Empty;
            if (string.IsNullOrWhiteSpace(phoneNumber))
            {
                message = "Phone Number cannot be blank";
            }
            else if (!phoneNumberLengths.Contains(phoneNumber.Length))
            {
                message = "Phone Number must be 10 or 11 digits long";
            }
            else if (!phoneNumber.Replace("+","").IsDigitsOnly())
            {
                message = "Phone Number must be all numeric digits";
            }
            if (throwException && !string.IsNullOrEmpty(message))
            {
                throw new InvalidPhoneNumberException(message, typeof(T), phoneNumber);
            }
            return string.IsNullOrEmpty(message);
        }

        private static string[] InvalidAreaCodeFirstDigits = new string[] { "0", "1" };
        public static bool ValidateAreaCode<T>(bool throwException, string number)
        {
            string message = string.Empty;
            if (string.IsNullOrWhiteSpace(number))
            {
                message = "Area Code cannot be blank";
            }
            else if (number.Length != 3)
            {
                message = "Area code must be three digits";
            }
            else if (!number.IsDigitsOnly())
            {
                message = "Area codes must be all numeric digits";
            }
            else if (InvalidAreaCodeFirstDigits.Contains(number.Substring(0,1)))
            {
                message = "Area codes cannot start with a 0 or 1";
            }
            if (throwException && !string.IsNullOrWhiteSpace(message))
            {
                throw new InvalidPhoneNumberException(message, typeof(T), number);
            }
            return string.IsNullOrWhiteSpace(message);
        }

        public static bool ValidateType(bool throwException, Type actualType, Type expectedType)
        {
            string result = string.Empty;

            Type objectType = actualType;
            bool found = false;
            while (objectType != null)
            {
                if (objectType == expectedType)
                {
                    found = true;
                    break;
                }
                objectType = objectType.BaseType;
            }
            if (!found)
            {
                result = string.Format("{0} is not equal to {1}", actualType, expectedType);
            }

            if (throwException && !found)
            {
                var argumentException = new ArgumentException(result);
                argumentException.Source = "PersistedType.ValidateType";
                throw argumentException;
            }
            return string.IsNullOrWhiteSpace(result);
        }

        public static bool ValidateIsDate(bool throwException, string date)
        {
            bool result = false;
            DateTime tryParseResult;
            DateTime.TryParse(date, out tryParseResult);
            if (tryParseResult == DateTime.MinValue)
            {
                if (date.Length == 8 && date.IsDigitsOnly())
                {
                    result = true;
                }
            }
            else
            {
                result = true;
            }
            if (throwException && !result)
            {
                throw new ConversionException(string.Format("{0} cannot be converted from string to date", date));
            }
            return result;
        }

        /// <summary>
        /// Returns true if number is a properly formatted SSN with or without dashes.
        /// </summary>
        /// <see cref="http://www.codeproject.com/Articles/651609/Validating-Social-Security-Numbers-through-Regular"/>
        public static bool ValidateSocialSecurityNumber(string number)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(number))
            {
                if (number.Contains('-'))
                {
                    result = Regex.Match(number, @"^(?!\b(\d)\1+-(\d)\1+-(\d)\1+\b)(?!123-45-6789|219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$").Success;
                }
                else
                {
                    result = Regex.Match(number, @"^(?!\b(\d)\1+\b)(?!123456789|219099999|078051120)(?!666|000|9\d{2})\d{3}(?!00)\d{2}(?!0{4})\d{4}$").Success;
                }
            }
            return result;
        }

        public static bool ValidateRequiredField(bool throwException, object field, string propertyName)
        {
            string message = string.Empty;
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                throw new RequiredPropertyException(typeof(ValidationRules), EMPTY_PARAMETER_EXCEPTION_MESSAGE);
            }
            if (field == null)
            {
                message = string.Format("{0} is a required field", propertyName);
            }
            else if (field.GetType() == typeof(string) && string.IsNullOrWhiteSpace(field as string))
            {
                message = string.Format("{0} is a required field", propertyName);
            }
            else if (field.GetType() == typeof(DateTime) && (DateTime)field == DateTime.MinValue)
            {
                message = string.Format("{0} cannot be the minimum date time.",propertyName);
            }
            if (throwException && !string.IsNullOrEmpty(message))
            {
                throw new RequiredPropertyException(field.GetType(), message);
            }
            return string.IsNullOrWhiteSpace(message);
        }
    }
}
