﻿using System;
using System.Globalization;
using System.Text;
using System.Linq;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Reflection;
using System.Collections;
using System.ComponentModel;

namespace WeBuildThings.Common.Extensions
{
    public static class StringExtensions
    {
        #region Convert String To DateTime
        /// <summary>
        /// Allows single entry point for various DateTime conversions as defined by IncomingDateTime enumeration
        /// </summary>
        public static DateTime ToDateTime(this string date, DateFormat incomingDateFormat)
        {
            bool incomingFormatChanged = false;
            DateTime result = DateTime.MinValue;
            if (ValidateIsDate(date))
            {
                incomingDateFormat = CorrectIncomingDateFormat(date, incomingDateFormat, ref incomingFormatChanged);
                if (incomingDateFormat == DateFormat.YYYYMMDD)
                {
                    result = ToDateTimeYYYYMMDD(date);
                }
                else if (incomingDateFormat == DateFormat.DaysAsInt)
                {
                    result = ToDateTimeDaysAsInt(date);
                }
                else if (incomingDateFormat == DateFormat.MMDDYYYY)
                {
                    result = ToDateTimeMMDDYYYY(date);
                }
                else if (incomingDateFormat == DateFormat.DDMMYYYY)
                {
                    result = ToDateTimeDDMMYYYY(date);
                }
                else if (incomingDateFormat == DateFormat.MMDDYYYY2)
                {
                    result = ToDateTimeMMDDYYYY2(date);
                }
                else if (incomingDateFormat == DateFormat.MMDDYY)
                {
                    result = ToDateTimeMMDDYY(date);
                }
                else if (incomingDateFormat == null)
                {
                    result = Convert.ToDateTime(date);
                }
                else
                {
                    throw new NotImplementedException(string.Format("BaseExtensions.ToDateTime: ToDateTime has no instructions for {0}", incomingDateFormat));
                }
                if (incomingDateFormat == DateFormat.DaysAsInt && incomingFormatChanged && (result > DateTime.Now.AddYears(1) || result < DateTime.Now.AddYears(90)) && (date.Length == 5 || date.Length == 4))
                {
                    int year = Convert.ToInt16(date.Substring(date.Length - 2, 2));
                    string fullYear = CalcFullYear(year);
                    string tempDate = date.Substring(0, date.Length - 2) + fullYear;
                    result = ToDateTimeMMDDYYYY(tempDate);
                }
            }
            return result;
        }

        private static bool ValidateIsDate(string date)
        {
            bool result = false;
            bool formatChanged = false;
            if (!string.IsNullOrWhiteSpace(date))
            {
                DateTime tryParseResult;
                DateTime.TryParse(date, out tryParseResult);
                result = (tryParseResult != DateTime.MinValue || ((date.Length <= 8 || date.Length >= 6) && date.IsDigitsOnly()) || CorrectIncomingDateFormat(date, null, ref formatChanged) == DateFormat.DaysAsInt);
                if (!result)
                {
                    result = (date.IsDigitsOnly() && (date.Length == 5 || date.Length == 4)); //At this point we are hoping that they dropped the first two digits of the year i.e. 41515;
                }

            }
            return result;
        }

        private static string[] Epoc = { "19", "20" };
        private static string[] NovDec = { "11", "12" };

        public static string CalcFullYear(int year)
        {
            string result = string.Empty;
            if (year >= 0 && year < 100)
            {
                int currentEpoc = Convert.ToInt16(DateTime.Now.Year.ToString().Substring(0, 2));
                if (year < Convert.ToInt16((DateTime.Now.Year + 1).ToString().Substring(2, 2)))
                {
                    result = currentEpoc.ToString() + year.ToString();
                }
                else
                {
                    result = (currentEpoc - 1).ToString() + year.ToString();
                }
            }
            else
            {
                throw new ArgumentException("CalcFullYear expects a value between 0 and 99");
            }
            return result;
        }

        /// <summary>
        /// Compares a string to the supplied incoming date format and may return a different value.
        /// </summary>
        public static DateFormat CorrectIncomingDateFormat(string date, DateFormat incomingDateFormat, ref bool incomingFormatChanged)
        {
            DateFormat result = incomingDateFormat;
            if (incomingDateFormat != DateFormat.DaysAsInt)
            {
                int periodCount = date.Where(n => n == '.').Count(); //Checking for decimal which would represent a time stamp.
                if (incomingDateFormat == DateFormat.MMDDYY && date.Length <= 6)
                {
                    result = DateFormat.MMDDYY;
                }
                else if (incomingDateFormat != DateFormat.MMDDYYYY2 && date.Contains("/") || date.Contains("-") || date.Contains(@"\"))
                {
                    result = DateFormat.MMDDYYYY2;
                    if (incomingDateFormat != DateFormat.MMDDYYYY) incomingFormatChanged = true;
                }
                else if (incomingDateFormat == DateFormat.MMDDYYYY2 && date.Contains("/") && date.Length == 10)
                {
                    result = DateFormat.MMDDYYYY2;
                }
                else if ((incomingDateFormat == DateFormat.MMDDYYYY || incomingDateFormat == DateFormat.MMDDYY) && Epoc.Contains(date.Substring(date.Length - 4, 2)))
                {
                    result = DateFormat.MMDDYYYY;
                }
                else if (incomingDateFormat != DateFormat.YYYYMMDD && !date.Contains(".") && date.Length > 5 && Convert.ToInt32(date.Substring(0, 4)) > 1912)
                {
                    result = DateFormat.YYYYMMDD;
                    incomingFormatChanged = true;
                }
                else if (periodCount == 1 || periodCount == 0)
                {
                    string leftDate = (periodCount == 1) ? date.Substring(0, date.IndexOf('.')) : date;
                    if (leftDate.Length.Between(3, 5))
                    {
                        result = DateFormat.DaysAsInt;
                        incomingFormatChanged = true;
                    }
                }
            }
            return result;
        }

        private static DateTime ToDateTimeMMDDYY(this string value)
        {
            DateTime result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(value))
            {
                int year = Convert.ToInt16(value.Substring(value.Length - 2, 2));
                int month = 0;
                int day = 0;
                string fullYear = CalcFullYear(year);
                if (value.Length == 6)
                {
                    month = Convert.ToInt16(value.Substring(0, 2));
                    day = Convert.ToInt16(value.Substring(2, 2));
                }
                else
                {
                    var tempValue = value.Substring(0, value.Length - 2);
                    ParseMMDDmissingZero(tempValue, ref month, ref day);
                }
                result = new DateTime(Convert.ToInt16(fullYear), month, day);
            }
            return result;
        }

        /// <summary>
        /// Parses string to datetime when the string is in DDMMYYYY format
        /// </summary>
        private static DateTime ToDateTimeDDMMYYYY(this string value)
        {
            DateTime result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(value))
            {
                int year = Convert.ToInt16(value.Substring(4, 4));
                int month = Convert.ToInt16(value.Substring(2, 2));
                int day = Convert.ToInt16(value.Substring(0, 2));
                result = new DateTime(year, month, day);
            }
            return result;
        }

        /// <summary>
        /// Parses string to datetime when the string is in MM/DD/YYYY format
        /// </summary>
        private static DateTime ToDateTimeMMDDYYYY2(this string value)
        {
            DateTime result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(value))
            {
                if (value.Contains(' '))
                {
                    value = value.Split(' ')[0];
                }
                string[] splitValue = new string[0];
                if (value.Contains('/'))
                {
                    splitValue = value.Split('/');
                }
                if (splitValue.Length == 3)
                {
                    int year = Convert.ToInt16(splitValue[2]);
                    int month = Convert.ToInt16(splitValue[0]);
                    int day = Convert.ToInt16(splitValue[1]);
                    result = new DateTime(year, month, day);
                }
            }
            return result;
        }

        private static void ParseMMDDmissingZero(string value, ref int month, ref int day)
        {
            if (value.Length == 2)
            {
                month = Convert.ToInt16(value.Substring(0, 1));
                day = Convert.ToInt16(value.Substring(1));
            }
            else if (value.Length == 3 && value.Substring(1, 1) == "0") //was a month with leading zero where zero got dropped somewhere could be wrong if intention was October
            {
                month = Convert.ToInt16(value.Substring(0, 1));
                day = Convert.ToInt16(value.Substring(1, 2));
            }
            else//assume there was a leading zero that got dropped i.e. 1251952 could be Jan 25 or Dec 5, we can't know
            {
                month = Convert.ToInt16(value.Substring(0, 1));
                day = Convert.ToInt16(value.Substring(1, 2));
            }
        }

        /// <summary>
        /// Parses string to DateTime when DateTime is in MMDDYYYY format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private static DateTime ToDateTimeMMDDYYYY(this string value)
        {
            DateTime result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(value))
            {
                int year = Convert.ToInt16(value.Substring(value.Length - 4, 4));
                int month = 0;
                int day = 0;
                if (value.Length == 8)
                {
                    month = Convert.ToInt16(value.Substring(0, 2));
                    day = Convert.ToInt16(value.Substring(2, 2));
                }
                else
                {
                    var tempValue = value.Substring(0, value.Length - 4);
                    ParseMMDDmissingZero(tempValue, ref month, ref day);
                }
                result = new DateTime(year, month, day);
            }
            return result;
        }

        /// <summary>
        /// Allows conversion from int datatype as well as string and Date
        /// Convert.ToDateTime cannot handle ints by default.
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/727466/how-do-i-convert-an-excel-serial-date-number-to-a-net-datetime"/>
        private static DateTime ToDateTimeDaysAsInt(this string value)
        {
            DateTime result;
            int i = 0;
            if (value.Where(n => n == '.').Count() == 1)
            {
                value = value.Substring(0, value.IndexOf('.'));
            }
            if (int.TryParse(value, out i))
            {
                if (i > 59) i -= 1; //Excel/Lotus 2/29/1900 bug   
                result = new DateTime(1899, 12, 31).AddDays(i);
            }
            else
            {
                result = Convert.ToDateTime(value);
            }
            return result;
        }

        /// <summary>
        /// Converts a string in YYYYMMDD format to a DateTime
        /// </summary>
        /// <param fullName="date">a string in YYYYMMDD format</param>
        /// <returns>A new Date Time object</returns>
        private static DateTime ToDateTimeYYYYMMDD(this string date)
        {
            DateTime result = DateTime.MinValue;
            if (!string.IsNullOrWhiteSpace(date) && date.Length == 8)
            {
                int year = Convert.ToInt16(date.Substring(0, 4));
                int month = Convert.ToInt16(date.Substring(4, 2));
                int day = Convert.ToInt16(date.Substring(6, 2));
                result = new DateTime(year, month, day);
            }
            return result;
        }
        #endregion Convert String To DateTime

        #region Convert String to Boolean
        private static string[] TrueValues = new string[] { "yes", "true", "y", "t" };
        private static string[] FalseValues = new string[] { "no", "false", "n", "f" };

        public static bool ValidBoolean(this string value)
        {
            return (TrueValues.Contains(value.ToLower()) || FalseValues.Contains(value.ToLower()));
        }

        public static bool ToBoolean(this string value)
        {
            bool result = false;
            if (TrueValues.Contains(value.ToLower()))
            {
                result = true;
            }
            else if (FalseValues.Contains(value.ToLower()))
            {
                result = false;
            }
            else
            {
                var exception = new ArgumentOutOfRangeException(string.Format("Unable to translate {0} to boolean", value));
                exception.Source = typeof(StringExtensions).Name;
                throw exception;
            }
            return result;
        }

        #endregion Convert String To Boolean

        #region String Modifications
        
        /// <summary>
        /// Adds leading zeros to a string until the length is met.
        /// </summary>
        public static string ToString(this int value, int length)
        {
            string result = value.ToString();
            while (result.Length < length)
            {
                result = "0" + result;
            }
            return result;
        }
        
        
        /// <summary>
        /// Inserts spaces between capital letters in a string
        /// </summary>
        public static string SeparateCamelCase(this string value)
        {
            value = value.RemoveSpaces();
            return Regex.Replace(value, "((?<=[a-z])[A-Z]|[A-Z](?=[a-z]))", " $1");
        }

        /// <summary>
        /// Removes Accents and other non standard characters from the input string
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/249087/how-do-i-remove-diacritics-accents-from-a-string-in-net?lq=1"/>
        /// <param name="text">The string to have Diacritic Characters removed from</param>
        /// <returns>Diacritic free string</returns>
        public static string ConvertDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        /// <summary>
        /// This removes numbers, punctuation, etc... from a string
        /// It leaves spaces.
        /// </summary>
        /// <param name="value">The string to be modified</param>
        /// <returns>The modified string</returns>
        public static string RemoveNonLetters(this string value)
        {
            return new string(value.ToCharArray().Where(c => c.Equals(' ') || char.IsLetter(c)).ToArray());
        }

        /// <summary>
        /// Just a bit of laziness so we don't have to find To Lower dll in Create Fuzzy List
        /// </summary>
        public static string ToLower(this string value)
        {
            return value.ToLower();
        }

        /// <summary>
        /// Just a bit of laziness so we don't have to find To Upper dll in Create Fuzzy List
        /// </summary>
        public static string ToUpper(this string value)
        {
            return value.ToUpper();
        }
        
        /// <summary>
        /// Used by Create Fuzzy List
        /// </summary>
        public static string RemoveSpaces(this string value)
        {
            return value.Replace(" ", string.Empty);
        }

        /// <summary>
        /// Creates a list of common alternate spellings for the string supplied.
        /// Including the original value, all lower, no diacritics, all upper, camel case, & no spaces
        /// and all combinations of the above.
        /// </summary>
        public static IEnumerable<string> CreateFuzzyList(this string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                var argumentException = new ArgumentNullException("Value cannot be null");
                argumentException.Source = typeof(StringExtensions).ToString();
                throw argumentException;
            }
            HashSet<string> result = new HashSet<string>();
            result.Add(value);
            HashSet<MethodInfo> methods = new HashSet<MethodInfo>();
            methods.Add(typeof(StringExtensions).GetMethod("RemoveNonLetters"));
            methods.Add(typeof(StringExtensions).GetMethod("ConvertDiacritics"));
            methods.Add(typeof(StringExtensions).GetMethod("ToLower"));
            methods.Add(typeof(StringExtensions).GetMethod("ToUpper"));
            methods.Add(typeof(StringExtensions).GetMethod("SeparateCamelCase"));
            methods.Add(typeof(StringExtensions).GetMethod("RemoveSpaces"));
            bool found = true;
            while (found)
            {
                found = false;
                var items = result.ToList();
                foreach (string item in items)
                {
                    foreach (var method in methods)
                    {
                        object[] parameters = new object[] { item };
                        string executionResult = method.Invoke(item, parameters).ToString().TrimStart().TrimEnd();
                        if (!result.Contains(executionResult) && !string.IsNullOrWhiteSpace(executionResult))
                        {
                            result.Add(executionResult);
                            found = true;
                        }
                    }
                }
            }
            return result;
        }

        #region Remove Non Decimal Characters
        private static bool[] _lookupNonDecimal;

        private static void InitializeNonDecimalLookup()
        {
            _lookupNonDecimal = new bool[65536];
            for (char c = '0'; c <= '9'; c++) _lookupNonDecimal[c] = true;
            for (char c = 'A'; c <= 'Z'; c++) _lookupNonDecimal[c] = true;
            for (char c = 'a'; c <= 'z'; c++) _lookupNonDecimal[c] = true;
            _lookupNonDecimal['.'] = true;
            _lookupNonDecimal['-'] = true;
        }

        /// <summary>
        /// Removes Special characters from a string, exception for . and _
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/1120198/most-efficient-way-to-remove-special-characters-from-string"/>
        public static string RemoveNonDecimal(this string str)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(str))
            {
                if (_lookupNonDecimal == null) InitializeNonDecimalLookup();
                char[] buffer = new char[str.Length];
                int index = 0;
                foreach (char c in str)
                {
                    if (_lookupNonDecimal[c])
                    {
                        buffer[index] = c;
                        index++;
                    }
                }
                result = new string(buffer, 0, index);
            }
            return result;
        }
        #endregion Remove Non Decimal Characters

        #region Remove Special Characters
        private static bool[] _lookup;

        private static void InitializeLookup()
        {
            _lookup = new bool[65536];
            for (char c = '0'; c <= '9'; c++) _lookup[c] = true;
            for (char c = 'A'; c <= 'Z'; c++) _lookup[c] = true;
            for (char c = 'a'; c <= 'z'; c++) _lookup[c] = true;
            _lookup['.'] = true;
            _lookup['_'] = true;
        }

        /// <summary>
        /// Removes Special characters from a string, exception for . and _
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/1120198/most-efficient-way-to-remove-special-characters-from-string"/>
        public static string RemoveSpecialCharacters(this string str)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(str))
            {
                if (_lookup == null) InitializeLookup();
                char[] buffer = new char[str.Length];
                int index = 0;
                foreach (char c in str)
                {
                    if (_lookup[c])
                    {
                        buffer[index] = c;
                        index++;
                    }
                }
                result = new string(buffer, 0, index);
            }
            return result;
        }
        #endregion Remove Special Characters
        #endregion String Modifications

        #region Spelled Numbers to Full Numbers
        #region Dictionaries
        private static Dictionary<string, int> smallNumbers = CreateSmallNumbers();
        private static Dictionary<string, int> CreateSmallNumbers()
        {
            var result = new Dictionary<string, int>
        {{"zero",0},{"one",1},{"two",2},{"three",3},{"four",4},
        {"five",5},{"six",6},{"seven",7},{"eight",8},{"nine",9}, {"minus",-1}};
            return result;
        }

        private static Dictionary<string, long> numberTable = CreateNumberTable();
        private static Dictionary<string, long> CreateNumberTable()
        {
            var result = new Dictionary<string, long> {{"ten",10},{"eleven",11},{"twelve",12},{"thirteen",13}, {"fourteen",14},{"fifteen",15},{"sixteen",16},
                {"seventeen",17},{"eighteen",18},{"nineteen",19},{"twenty",20}, {"thirty",30},{"forty",40},{"fifty",50},{"sixty",60}, {"seventy",70},{"eighty",80},{"ninety",90},{"hundred",100},
                {"thousand",1000},{"million",1000000},{"billion",1000000000}, {"trillion",1000000000000},{"quadrillion",1000000000000000}, {"quintillion",1000000000000000000}};
            foreach (var item in smallNumbers)
            {
                result.Add(item.Key, item.Value);
            }
            return result;
        }

        private static Dictionary<string, decimal> decimalTable = CreateDecimalTable();
        private static Dictionary<string, decimal> CreateDecimalTable()
        {
            Dictionary<string, decimal> result = new Dictionary<string, decimal>();
            foreach (var item in numberTable)
            {
                result.Add(item.Key, item.Value);
            }
            result.Add("and", 0.0m);
            return result;
        }

        private static Dictionary<string, string> moneyTable = CreateMoneyTable();
        private static Dictionary<string, string> CreateMoneyTable()
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            foreach (var item in decimalTable)
            {
                result.Add(item.Key, item.Value.ToString());
            }
            result.Add("dollars", "$");
            result.Add("cents", string.Empty);
            return result;
        }
        #endregion Dictionaries

        #region Calc Conversion Type
        private enum ConversionType { None, SmallNumbers, LongNumbers, Decimal, Money, Date };

        /// <summary>
        /// Returns the ConversionType to use for a given string
        /// </summary>
        private static ConversionType CalcConversionType(string str)
        {
            ConversionType result = ConversionType.None;
            if (StringInList<string, int>(str, smallNumbers))
            {
                result = ConversionType.SmallNumbers;
            }
            else if (StringInList<string, long>(str, numberTable))
            {
                result = ConversionType.LongNumbers;
            }
            else if (StringInList<string, decimal>(str, decimalTable))
            {
                result = ConversionType.Decimal;
            }
            else if (StringInList<string, string>(str, moneyTable))
            {
                result = ConversionType.Money;
            }
            return result;
        }

        /// <summary>
        /// Compares the space delimited string to the list and returns true if all items in the string are in the list.
        /// </summary>
        internal static bool StringInList<X, T>(string str, Dictionary<string, T> list)
        {
            bool result = true;
            foreach (string item in str.Split(' '))
            {
                if (!string.IsNullOrWhiteSpace(item) && !list.ContainsKey(item))
                {
                    result = false;
                    break;
                }
            }
            return result;
        }
        #endregion Calc Conversion Type

        #region Conversion Methods
        private static string ConvertTextToNumber(string str, ConversionType conversionType)
        {
            string result = str;
            if (conversionType == ConversionType.SmallNumbers)
            {
                result = str.ToIndividualNumbers();
            }
            else if (conversionType == ConversionType.LongNumbers)
            {
                result = str.ToLong().ToString();
            }
            else if (conversionType == ConversionType.Decimal)
            {
                result = str.ToDecimal().ToString();
            }
            else if (conversionType == ConversionType.Money)
            {
                result = str.ToMoney();
            }
            else if (conversionType == ConversionType.Date)
            {
                result = str.ToDate();
            }
            else if (conversionType == ConversionType.None)
            {
                result = str;
            }
            else
            {
                var exception = new NotImplementedException(string.Format("No instructions for {0}", conversionType));
                exception.Source = typeof(StringExtensions).Name;
                throw exception;
            }
            return result;
        }

        public static string ToDate(this string dateString)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts a string of individual numbers from text to numbers
        /// </summary>
        public static string ToIndividualNumbers(this string numberString)
        {
            string result = numberString;
            var numbers = Regex.Matches(numberString, @"\w+").Cast<Match>()
                 .Select(m => m.Value.ToLowerInvariant())
                 .Where(v => numberTable.ContainsKey(v))
                 .Select(v => numberTable[v]);
            if (numbers != null)
            {
                result = string.Empty;
            }
            foreach (var n in numbers)
            {
                result += n;
            }
            return result;
        }

        /// <summary>
        /// Converts a string less than 9 quintillion to a long of the same value.
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/11278081/convert-words-string-to-int"/>
        public static long ToLong(this string numberString)
        {
            var numbers = Regex.Matches(numberString, @"\w+").Cast<Match>()
                 .Select(m => m.Value.ToLowerInvariant())
                 .Where(v => numberTable.ContainsKey(v))
                 .Select(v => numberTable[v]);
            long acc = 0, total = 0L;
            foreach (var n in numbers)
            {
                if (n >= 1000)
                {
                    total += (acc * n);
                    acc = 0;
                }
                else if (n >= 100)
                {
                    acc *= n;
                }
                else acc += n;
            }
            return (total + acc) * (numberString.StartsWith("minus",
                  StringComparison.InvariantCultureIgnoreCase) ? -1 : 1);
        }

        /// <summary>
        /// Converts a decimal string less than 9 quintillion to a decimal of the same value.
        /// </summary>
        public static decimal ToDecimal(this string numberString)
        {
            var numbers = Regex.Matches(numberString, @"\w+").Cast<Match>()
                 .Select(m => m.Value.ToLowerInvariant())
                 .Where(v => decimalTable.ContainsKey(v))
                 .Select(v => decimalTable[v]);
            decimal acc = 0, total = 0L;
            foreach (var n in numbers)
            {
                if (n >= 1000)
                {
                    total += (acc * n);
                    acc = 0;
                }
                else if (n >= 100)
                {
                    acc *= n;
                }
                else acc += n;
            }
            return (total + acc) * (numberString.StartsWith("minus",
                  StringComparison.InvariantCultureIgnoreCase) ? -1 : 1);
        }

        /// <summary>
        /// Converts a string with dollars or cents to a number string with the same value.
        /// </summary>
        public static string ToMoney(this string numberString)
        {
            string modifiedString = numberString.ToLower().Replace("dollars", string.Empty).Replace("cents", string.Empty);
            var numbers = modifiedString.ToDecimal();
            return string.Format("${0}", numbers);
        }
        #endregion Conversion Methods

        /// <summary>
        /// Converts any spelled out numbers to numbers
        /// </summary>
        public static string ConvertTextNumbersToNumbers(this string sentence)
        {
            string result = string.Empty;
            string currentNumber = string.Empty;
            foreach (string word in sentence.Split(' '))
            {
                if (moneyTable.ContainsKey(word))
                {
                    currentNumber += " " + word;
                }
                else
                {
                    result += " ";
                    if (!string.IsNullOrWhiteSpace(currentNumber))
                    {
                        ConversionType conversionType = CalcConversionType(currentNumber);
                        result += string.Format("{0} {1}", ConvertTextToNumber(currentNumber, conversionType), word);
                        currentNumber = string.Empty;
                    }
                    else
                    {
                        result += word;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(currentNumber))
            {
                result += " ";
                result += ConvertTextToNumber(currentNumber, CalcConversionType(currentNumber));
                currentNumber = string.Empty;
            }
            return result.Trim();
        }

        #endregion Spelled Numbers to Full Numbers

        public static string RemoveInvalidChar(this string str)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(str))
            {
                foreach (char c in str)
                {
                    if (c != 65533)
                        result += c;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns true unless a character not equal to c is found.
        /// Then returns false.
        /// </summary>
        public static bool IsOnlyChar(this string str, char c)
        {
            bool result = true;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] != c)
                {
                    result = false;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns false if there are any numbers in the string
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/7461080/fastest-way-to-check-if-string-contains-only-digits"/>
        public static bool IsDigitsOnly(this string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Returns false if there are any numbers in the string
        /// </summary>
        public static bool IsNumeric(this string str)
        {
            decimal value;
            return (Decimal.TryParse(str, out value));
        }

        /// <summary>
        /// Removes any kind of whitespace from inside a string. i.e. double spaces, tabs, etc...
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/206717/how-do-i-replace-multiple-spaces-with-a-single-space-in-c"/>
        public static string RemoveWhiteSpace(this string str)
        {
            return Regex.Replace(str, @"\s+", " ").Trim();
        }

        /// <summary>
        /// Like normal string.Split; however, it can use a string instead of a single char.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="delimiter"></param>
        /// <returns></returns>
        public static string[] Split(this string str, string delimiter)
        {
            var result = new List<string>();
            while (str.Contains(delimiter))
            {
                result.Add(str.Substring(0, str.IndexOf(delimiter)));
                str = str.Substring(str.IndexOf(delimiter) + delimiter.Length);
            }
            if (str.Length > 0) result.Add(str);
            return result.ToArray();
        }

        #region Remove Non Numeric
        private static Regex digitsOnly = new Regex(@"[^\d]");

        /// <summary>
        /// Removes non numeric characters from a string
        /// </summary>
        public static string RemoveNonNumeric(this string str)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(str))
            {
                result = digitsOnly.Replace(str, "");
            }
            return result;
        }
        #endregion Remove Non Numeric
    }

    public static class IntExtensions
    {
        /// <summary>
        /// Returns true if the integer is greater than or equal to the start range and less than or equal to the end range
        /// </summary>
        public static bool Between(this int value, int startRange, int endRange)
        {
            return (value >= startRange && value <= endRange);
        }
    }

    public static class CollectionExtensions
    {
        public static object[] ToArray(this IList list)
        {
            var result = new object[list.Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = list[i];
            }
            return result;
        }

        public static T[] ToArray<T>(this HashSet<T> hashSet)
        {
            if (hashSet == null) return null;
            T[] result = new T[hashSet.Count];
            int i = 0;
            foreach (T item in hashSet)
            {
                result[i] = item;
                i++;
            }
            return result;
        }

        public static HashSet<T> ToHashSet<T, X>(this IEnumerable<X> value) where T : X
        {
            HashSet<T> result = new HashSet<T>();
            foreach (X item in value)
            {
                result.Add((T)item);
            }
            return result;
        }

        /// <summary>
        /// Randomly change the order of items in the sequence array
        /// </summary>
        public static T[] Shuffle<T>(this T[] sequence, Random random)
        {
            for (int i = 0; i < sequence.Length; ++i)
            {
                int r = random.Next(i, sequence.Length);
                T tmp = sequence[r];
                sequence[r] = sequence[i];
                sequence[i] = tmp;
            }
            return sequence;
        }
    }

    public static class DateTimeExtensions
    {

        public static bool InRange(this DateTime date, DateTime rangeStart, DateTime rangeEnd)
        {
            return (date >= rangeStart && date <= rangeEnd);
        }
    }

    public static class RandomizationExtensions
    {
        public static double NextDouble(this Random random, double start, double end)
        {
            return (end - start) * random.NextDouble() + start;
        }

    }
}
