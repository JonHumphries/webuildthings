﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Linq;

namespace WeBuildThings.Common.Extensions
{
    public static class TypeExtensions
    {
        internal static string[] NUMERIC_PROPERTY_TYPE_NAMES = new string[] { "Decimal" };

        public static string UserFriendlyName(this Type type)
        {
            return type.Name.SeparateCamelCase();
        }

        public static bool ContainsInterface(this Type type, Type comparingInterface)
        {
            bool result = false;
            var interfaces = type.GetInterfaces();
            foreach (Type item in interfaces)
            {
                if (item == comparingInterface)
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Iterative method for searching base types of a type
        /// </summary>
        public static bool ContainsType(this Type type, Type comparingType)
        {
            Type objectType = type;
            bool result = false;
            while (objectType != null)
            {
                if (objectType == comparingType)
                {
                    result = true;
                    break;
                }
                objectType = objectType.BaseType;
            }
            return result;
        }

        /// <summary>
        /// Returns the data type of the property supplied or a null reference error
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool ValidatePropertyName(bool exception, string propertyName)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                result = "Property name cannot be null";
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                var argumentNullException = new ArgumentNullException(result);
                argumentNullException.Source = typeof(TypeExtensions).Assembly.FullName;
                throw argumentNullException;
            }
            return (!string.IsNullOrWhiteSpace(result));
        }

        /// <summary>
        /// Sets a value in an object, used to hide all the logic that goes into
        /// handling this sort of thing, so that is works elegantly in a single line.
        /// </summary>
        //public static void SetProperty(dynamic target, string propertyName, string propertyValue, DateFormat? dateFormat)
        //{

        //    ValidatePropertyName(true, propertyName);
        //    string[] propertyTree = propertyName.Split('.');
        //    string finalPropertyName = propertyTree[propertyTree.Length - 1];
        //    dynamic actualTarget = (propertyTree.Length > 1) ? ActualTarget(target, propertyTree) : target;
        //    PropertyInfo oProp = actualTarget.GetType().GetProperty(finalPropertyName);
        //    Type tProp = oProp.PropertyType;
        //    //Nullable properties have to be treated differently, since we 
        //    //  use their underlying property to set the value in the object
        //    if (tProp.IsGenericType && tProp.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
        //    {
        //        //if it's null, just set the value from the reserved word null, and return
        //        if (propertyValue == null)
        //        {
        //            oProp.SetValue(actualTarget, null, null);
        //        }

        //        //Get the underlying type property instead of the nullable generic
        //        tProp = new NullableConverter(oProp.PropertyType).UnderlyingType;
        //    }
        //    // DateTimes require special parsing, separate method to handle that.
        //    else if (oProp.PropertyType == typeof(DateTime))
        //    {
        //        oProp.SetValue(actualTarget, propertyValue.ToDateTime(dateFormat), null);
        //    }
        //    //use the converter to get the correct value
        //    else if (!string.IsNullOrWhiteSpace(propertyValue) && oProp.CanWrite)
        //    {
        //        TypeConverter conv = TypeDescriptor.GetConverter(tProp);
        //        dynamic obj = conv.ConvertFromString(propertyValue); // or ConvertFromInvariantString
        //        oProp.SetValue(actualTarget, Convert.ChangeType(obj, tProp), null);
        //    }
        //}

        /// <summary>
        /// Sets the property of a object to a particular value.  Handles nested objects.
        /// </summary>
        public static void SetProperty(dynamic target, string propertyName, object value, DateFormat dateFormat)
        {
            ValidatePropertyName(true, propertyName);
            string[] containingObjectPath = CalcContainingObjectPath(propertyName);
            dynamic containingObject = CalcContainingObject(target, containingObjectPath);
            PropertyInfo propertyType = containingObject.GetType().GetProperty(propertyName.Split('.').Last());
            SetPropertyValue(value, containingObject, propertyType, dateFormat);
        }

        /// <summary>
        /// Sets the property of an object to a particular value. Handles nested objects.
        /// </summary>
        public static void SetProperty(dynamic target, string propertyName, object propertyValue)
        {
            SetProperty(target, propertyName, propertyValue, null);
        }

        /// <summary>
        /// Sets the property of an object to a particular value.  Does not handle nested objects.
        /// </summary>
        /// <param name="value">The value the property will be set to.</param>
        /// <param name="containingObject">The object that has the property</param>
        /// <param name="propertyInfo">The property info for objects property</param>
        /// <param name="valueType">The type of the target object</param>
        private static void SetPropertyValue(object value, dynamic containingObject, PropertyInfo propertyInfo, DateFormat dateFormat)
        {
            Type valueType = value.GetType();
            Type propertyType = propertyInfo.PropertyType;
            if (value == null)
            {
                propertyInfo.SetValue(containingObject, null, null);
            }
            else if (propertyType == valueType)
            {
                propertyInfo.SetValue(containingObject, value, null);
            }
            else if (propertyType.IsEnum && valueType == typeof(string))
            {
                string matchingValue = FuzzyMatchEnumValue(propertyType, value.ToString());
                propertyInfo.SetValue(containingObject, Enum.Parse(propertyType, matchingValue, true), null);
            }
            // DateTimes require special parsing, separate method to handle that.
            else if (propertyType == typeof(DateTime) && valueType == typeof(string))
            {
                string propValue = (value.ToString());
                propertyInfo.SetValue(containingObject, propValue.ToDateTime(dateFormat), null);
            }
            else if (valueType == typeof(string) )
            {
                try
                {
                    TypeConverter conv = TypeDescriptor.GetConverter(propertyType);
                    if (NUMERIC_PROPERTY_TYPE_NAMES.Contains(propertyType.Name) && string.IsNullOrWhiteSpace(value.ToString()))
                    {
                        propertyInfo.SetValue(containingObject, Convert.ChangeType(0, propertyType), null);
                    }
                    else
                    {
                        dynamic obj = conv.ConvertFromString(value.ToString()); // or ConvertFromInvariantString
                        propertyInfo.SetValue(containingObject, Convert.ChangeType(obj, propertyType), null);
                    }
                }
                catch (NotSupportedException e)
                {
                    InvokeConversionException(propertyInfo, valueType, propertyType, e);
                }
            }
            else
            {
                InvokeConversionException(propertyInfo, valueType, propertyType, null);
            }
        }

        private static void InvokeConversionException(PropertyInfo propertyInfo, Type valueType, Type propertyType, Exception e)
        {
            var exception = new NotImplementedException(string.Format("Failure on Column {2}. No instructions for dynamically converting {0} to {1}", valueType, propertyType, propertyInfo), e);
            exception.Source = typeof(TypeExtensions).Name;
            throw exception;
        }

        /// <summary>
        /// used for calculating the second to last property in a period delimited list.
        /// </summary>
        private static string[] CalcContainingObjectPath(string propertyName)
        {
            string[] result;
            string[] propertyTree = propertyName.Split('.');
            if (propertyTree.Length > 1)
            {
                result = new string[propertyTree.Length - 1];
                Array.Copy(propertyTree, result, propertyTree.Length - 1);
            }
            else
            {
                result = null;
            }
            return result;
        }

        private static object CalcContainingObject(object target, string[] propertyTree)
        {
            object result = target;
            if (propertyTree != null)
            {
                for (int i = 0; i < propertyTree.Length; i++)
                {
                    if (result != null)
                    {
                        var propertyItem = result.GetType().GetProperty(propertyTree[i]);
                        if (propertyItem == null)
                        {
                            var exception = new ArgumentException(string.Format("Unable to locate Property {1} within {0}.", result.GetType().Name, propertyTree[i]));
                            exception.Source = typeof(TypeExtensions).Name;
                            throw exception;
                        }
                        result = propertyItem.GetValue(result, null);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Uses some simple fuzzy matching to see if the supplied value is in the enumeration
        /// </summary>
        /// <returns>The correctly spelled enum value</returns>
        private static string FuzzyMatchEnumValue(Type type, string propertyValue)
        {
            if (!type.IsEnum)
            {
                var exception = new InvalidCastException(string.Format("{0} is not an enumeration", type.Name));
                exception.Source = typeof(TypeExtensions).Name;
                throw exception;
            }
            Array enumValues = System.Enum.GetValues(type);
            string result = string.Empty;
            foreach (var value in enumValues)
            {
                string stringValue = value.ToString();
                if (propertyValue == stringValue || propertyValue.Replace(" ", "").ToUpper() == stringValue.ToUpper())
                {
                    result = stringValue;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the value of the specified property of the target object
        /// </summary>
        public static object GetProperty(object target, string propertyName)
        {
            if (string.IsNullOrWhiteSpace(propertyName))
            {
                var exception = new ArgumentNullException("Property Name is a required parameter");
                exception.Source = typeof(TypeExtensions).Name;
                throw exception;
            }
            return GetProperty(target, propertyName.Split('.'));
        }

        /// <summary>
        /// Returns the get property value for the last property in the array.
        /// Assumes references between properties in the array.
        /// </summary>
        public static object GetProperty(object target, string[] propertyTree)
        {
            var property = target.GetType().GetProperty(propertyTree[0]);
            if (property == null)
            {
                var exception = new ArgumentException(string.Format("Unable to locate Property {1} within {0}.", target.GetType().Name, propertyTree[0]));
                exception.Source = typeof(TypeExtensions).Name;
                throw exception;
            }
            object result = property.GetValue(target, null);
            if (propertyTree.Length > 1)
            {
                for (int i = 1; i < propertyTree.Length; i++)
                {
                    if (result != null)
                    {
                        var propertyItem = result.GetType().GetProperty(propertyTree[i]);
                        if (propertyItem == null)
                        {
                            var exception = new ArgumentException(string.Format("Unable to locate Property {1} within {0}.", result.GetType().Name, propertyTree[i]));
                            exception.Source = typeof(TypeExtensions).Name;
                            throw exception;
                        }
                        result = propertyItem.GetValue(result, null);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Returns a collection of all Types within a type including collections
        /// </summary>
        public static IList<Type> GetAllReferencedTypes(this Type type, IList<Type> knownTypes)
        {
            if (!knownTypes.Contains(type))
            {
                knownTypes.Add(type);
            }
            foreach (var property in type.GetProperties())
            {
                Type propertyType = (property.PropertyType.IsCollection() && property.PropertyType.GetGenericArguments() != null && property.PropertyType.GetGenericArguments().Any()) ? property.PropertyType.GetGenericArguments()[0] : property.PropertyType;
                if (!knownTypes.Contains(propertyType) && propertyType.Namespace.Substring(0, 6) != "System" && propertyType.Namespace.Substring(0, 10) != "DevExpress")
                {
                    knownTypes = propertyType.GetAllReferencedTypes(knownTypes).ToList();
                }
            }
            return knownTypes;
        }

        /// <summary>
        /// Returns true if the type is a collection
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/10864611/how-to-determine-if-a-type-is-a-type-of-collection"/>
        public static bool IsCollection(this Type type)
        {
            bool result = (type.GetInterface("ICollection") != null);
            if (!result && type.GetInterface("IEnumerable") != null)
            {
                Type[] falsePositives = new Type[] { typeof(string) };
                result = !falsePositives.Contains(type);
            }
            return result;
        }

        /// <summary>
        /// Returns a collection of all Referenced Types as well as types within the assembly that can be referenced by the types found.
        /// Was built to assist with XMLInclude exceptions.
        /// </summary>
        /// <seealso cref="http://www.codeproject.com/Tips/51437/Get-rid-of-XmlInclude-when-using-XmlSerializer"/>
        public static IList<Type> GetAllReferencedAndAssignableTypes(this Type type, IList<Type> knownTypes)
        {
            IList<Type> result = type.GetAllReferencedTypes(knownTypes);
            foreach (Type item in result.ToList())
            {
                IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(n => n.FullName.Substring(0, 6) != "System" && n.FullName.Substring(0, 10) != "DevExpress");
                foreach (Assembly assembly in assemblies)
                {
                    var types = assembly.GetTypes().Where(t => !string.IsNullOrWhiteSpace(t.Namespace) && t.Namespace.Length >= 6 && t.Namespace.Substring(0, 6) != "System" && item.IsAssignableFrom(t) && !result.Contains(t));
                    foreach (Type assignableType in types)
                    {
                        if (!result.Contains(assignableType))
                        {
                            IList<Type> allAssignableAndReferencedTypes = assignableType.GetAllReferencedAndAssignableTypes(result);
                            foreach (Type subType in allAssignableAndReferencedTypes)
                            {
                                if (!result.Contains(subType))
                                {
                                    result.Add(subType);
                                }
                            }
                        }
                    }
                }
            }
            return result;
        }

        public static Type GetTypeFromMyAssemblies(string fullName)
        {
            Type result = null;
            IEnumerable<Assembly> assemblies = AppDomain.CurrentDomain.GetAssemblies().Where(n => n.FullName.Substring(0, 6) != "System" && n.FullName.Substring(0, 10) != "DevExpress");
            foreach (Assembly assembly in assemblies)
            {
                result = assembly.GetType(fullName);
                if (result != null) break;
            }
            return result;
        }
    }
}
