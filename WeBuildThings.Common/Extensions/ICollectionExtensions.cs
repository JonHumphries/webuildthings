﻿using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.Common.Extensions
{
    public static class ICollectionExtensions
    {
        public static string ToCommaSeparatedString<T>(this ICollection<T> collection)
        {
            return collection != null ? string.Join(",", collection.ToArray()) : null;
        }

        public static string Trim(this string text, int limit, string suffix = "...")
        {
            if (text.Length > limit)
            {
                return text.Substring(0, limit) + suffix;
            }
            return text;
        }
    }
}
