﻿
namespace WeBuildThings.Common.Extensions
{
    public class ZipCode
    {
        public static string Format(string zipCode)
        {
            string result = string.Empty;
            if (!string.IsNullOrWhiteSpace(zipCode))
            {
                if (zipCode.Length == 9)
                {
                    result = string.Format("{0}-{1}",zipCode.Substring(0,5), zipCode.Substring(5,4));
                }
                else
                {
                    result = zipCode;
                }
            }
            return result;
        }
    }
}
