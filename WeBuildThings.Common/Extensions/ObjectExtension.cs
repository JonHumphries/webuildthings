﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Common.Extensions
{
    public enum NullValueProcess { Null, MinimumValue };

    public static class ObjectExtension
    {
        public static string ToFormattedString(this object value, NullValueProcess nullValueProcess, DateFormat dateFormat, int maxLength)
        {
            var formattedValue = value;
            if (value == null || (value.GetType() == typeof(DateTime) && (DateTime)value == DateTime.MinValue))
            {
                if (nullValueProcess == NullValueProcess.Null)
                {
                    formattedValue = null;
                }
                else if (nullValueProcess == NullValueProcess.MinimumValue)
                {
                    if (value.GetType() == typeof(DateTime))
                    {
                        formattedValue = DateTime.MinValue;
                    }
                    else if (value.GetType() == typeof(int))
                    {
                        formattedValue = 0;
                    }
                }
                else
                {
                    throw new NotImplementedException(string.Format("No instructions for Null Value Process {0}", nullValueProcess));
                }
            }
            else if (value.GetType() == typeof(DateTime))
            {
                formattedValue = ((DateTime)value).ToString(dateFormat.Format);
            }
            string result = (formattedValue == null) ? null : formattedValue.ToString();
            if (!string.IsNullOrWhiteSpace(result) && maxLength != -1 && result.Length > maxLength)
            {
                result = result.Substring(0, maxLength);
            }
            return result;
        }
    }
}
