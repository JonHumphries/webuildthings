﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace WeBuildThings.Common.Extensions
{
    public static class EnumerationExtensions
    {
        public static string GetEnumDescription(this Enum value)
        {
            string result = value.ToString();
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if (attributes != null && attributes.Length > 0)
            {
                result = attributes[0].Description;
            }
            return result;
        }
    }
}
