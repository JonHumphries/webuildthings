﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.Common
{
    public static class StandardCalculations
    {
        public static decimal SumValues(IEnumerable<decimal> amounts)
        {
            return (amounts != null) ? amounts.Sum(n => n) : 0;
        }

        public static double YValueOfParabolicCurve(double x, double xMaximum, double yMaximum, double speedOfCurve)
        {
            return speedOfCurve * Math.Pow(x + xMaximum, 2) + yMaximum;
        }

        public static double CartesianDistance(double aX, double aY, double aZ, double sX, double sY, double sZ)
        {
            return Math.Sqrt(Math.Pow(sX - aX, 2) + Math.Pow(sY - aY, 2) + Math.Pow(sZ - aZ, 2));
        }

        public static double MultiDimensionalDistance(double[] a, double[] b)
        {
            double result = Double.NaN;
            if (a.Length != b.Length)
            {
                throw new ArgumentException("Both arrays must be the same length");
            }
            double summedValues = 0;
            for (int i = 0; i < a.Length; i++)
            {
                summedValues += Math.Pow(b[i] - a[i], 2);
            }
            result = Math.Sqrt(summedValues);
            return result;
        }

        public static double DotProduct(double[] a, double[] b)
        {
            if (a.Length != b.Length) throw new ArgumentException("Both arrays must be the same length");
            double result = 0;
            for (int i = 0; i < a.Length; i++)
            {
                result += a[i] * b[i];
            }
            return result;
        }

    }
}
