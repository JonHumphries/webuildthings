﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.Common.Extensions
{
    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Traverses an object hierarchy and return a flattened list of elements
        /// based on a predicate.
        /// </summary>
        /// <typeparam name="TSource" The type of object in your collection.</typeparam>
        /// <param name="source" The collection of your topmost TSource objects.</param>
        /// <param name="selectorFunction" A predicate for choosing the objects you want.</param>
        /// <param name="getChildrenFunction" A function that fetches the child collection from an object.</param>
        /// <returns> A flattened list of objects which meet the criteria in selectorFunction.</returns>
        /// <example> var flattenedNodes = nodes.FlattenHierarchy( p => true, (Node n) => { return n.Children; } );</example>
        /// <see cref="https://stackoverflow.com/questions/141467/recursive-list-flattening/229442#229442?newreg=d39c06d99dff435684e4eedc0c1e4adc"/>
        public static IEnumerable<TSource> FlattenHierarchy<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> selectorFunction, Func<TSource, IEnumerable<TSource>> getChildrenFunction)
        {
            // Add what we have to the stack
            var flattenedList = source.Where(selectorFunction);

            // Go through the input enumerable looking for children,
            // and add those if we have them
            foreach (TSource element in source)
            {
                flattenedList = flattenedList.Concat(
                  getChildrenFunction(element).FlattenHierarchy(selectorFunction, getChildrenFunction));
            }
            return flattenedList;
        }

        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> list)
        {
            HashSet<T> result = new HashSet<T>();
            foreach (T item in list)
            {
                result.Add(item);
            }
            return result;
        }

        public static IEnumerable<string> ToLower(this IEnumerable<string> list)
        {
            foreach (string item in list)
            {
                item.ToLower();
            }
            return list;
        }

        public static IEnumerable<string> Trim(this IEnumerable<string> list)
        {
            foreach (string item in list)
            {
                item.Trim();
            }
            return list;
        }

        public static HashSet<T> ToHashSet<T>(this IList<T> list)
        {
            HashSet<T> result = new HashSet<T>();
            foreach (T item in list)
            {
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// Returns true if all strings in the collection are numeric (including decimals/negative numbers)
        /// </summary>
        public static bool IsNumeric(this IEnumerable<string> values)
        {
            bool result = true;
            int i = 0;
            int max = values.Count();
            foreach (string item in values)
            {
                if (!item.IsNumeric())
                {
                    result = false;
                    break;
                }
                i++;
            }
            return result;
        }

        public static string[][] ParseData(this IEnumerable<string> rawData, char delimiter)
        {
            int numRows = rawData.Count();
            int numCols = rawData.First().Split(delimiter).Length;
            string[][] result = new string[numRows][];
            int i = 0;
            foreach (var row in rawData)
            {
                result[i] = new string[numCols];
                string[] tokens = row.Split(delimiter);
                Array.Copy(tokens, result[i], numCols);
                i++;
            }
            return result;
        }

        public static string ToDelimitedString<T>(this IEnumerable<T> data, char delimiter)
        {
            string result = string.Empty;
            foreach (var item in data)
            {
                result += item.ToString() + delimiter;
            }
            if (result.Length > 0)
            {
                result = result.Remove(result.Length - 1);
            }
            return result;
        }
    }
}
