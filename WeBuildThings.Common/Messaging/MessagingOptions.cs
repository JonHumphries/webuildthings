﻿
namespace WeBuildThings.Messaging
{
    public class MessagingOptions: IMessagingOptions
    {
        public bool UpdateConsole { get; set; }

        public bool UpdateDatabase { get; set; }

        public bool ThrowErrors {get; set; }

        public bool MessageErrors { get; set; }

        public static IMessagingOptions Default()
        {
            return new MessagingOptions() { UpdateConsole = true, UpdateDatabase = false, ThrowErrors = true, MessageErrors = true };
        }
    }
}
