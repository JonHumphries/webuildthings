﻿namespace WeBuildThings.Messaging
{
    public interface IMessagingOptions
    {
        bool UpdateConsole { get; set; }
        bool UpdateDatabase { get; set; }
        bool ThrowErrors { get; set; }
        bool MessageErrors { get; set; }
    }
}
