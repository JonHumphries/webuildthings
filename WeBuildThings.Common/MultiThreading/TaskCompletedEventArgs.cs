﻿using System;
using System.Threading.Tasks;

namespace WeBuildThings.MultiThreading
{
    public class TaskEventArgs : EventArgs
    {
        public Task Task;

        public TaskEventArgs(Task task) : base() { Task = task; }
    }
}
