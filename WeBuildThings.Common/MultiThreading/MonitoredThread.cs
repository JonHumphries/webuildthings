﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace WeBuildThings.MultiThreading
{
    public class MonitoredThread
    {
        #region consts
        internal static TaskStatus[] CompletedTaskStatuses = new TaskStatus[] { TaskStatus.Canceled, TaskStatus.Faulted, TaskStatus.RanToCompletion };
        #endregion consts

        #region Fields
        private int _Id;
        private Task _Task;
        private TaskStatus _PreviousStatus;
        private bool _Completed;
        #endregion Fields

        #region Constructors
        public MonitoredThread(string message, Task task)
        {
            Message = message;
            Task = task;
        }

        public MonitoredThread(string message, int taskId)
        {
            Message = message;
            Id = taskId;
        }
        #endregion Constructors

        #region Properties
        public int Id { get { return _Id; } private set { _Id = value; } }

        public TaskStatus PreviousStatus
        {
            get { return _PreviousStatus; }
            private set { if (_PreviousStatus != value) { _PreviousStatus = value; OnTaskStatusChanged(); } }
        }

        public TaskStatus Status
        {
            get { var result = Task.Status; PreviousStatus = result; return result; }
        }

        public string Message { get; set; }
        public Task Task { get { return _Task; } private set { _Task = value; if (value != null) Id = value.Id; } }

        public bool Completed
        {
            get { return _Completed; }
            private set { if (_Completed != value) { _Completed = value; OnCompletedChanged(); } }
        }

        #endregion Properties

        #region Event
        public event EventHandler<TaskEventArgs> StatusChanged;
        public event EventHandler<TaskEventArgs> TaskCompleted;
        #endregion Event

        #region Event Methods
        private void OnTaskStatusChanged()
        {
            SetCompleted();
            InvokeStatusChanged(Task);
        }

        private void OnCompletedChanged()
        {
            InvokeTaskCompleted(Task);
        }
        #endregion Event Methods

        #region Set Methods
        private void SetCompleted()
        {
            if (CompletedTaskStatuses.Contains(PreviousStatus))
            {
                Completed = true;
            }
        }
        #endregion Set Methods

        #region Invoke Methods
        private void InvokeStatusChanged(Task task)
        {
            var handler = StatusChanged;
            if (handler != null) handler.Invoke(this, new TaskEventArgs(task));
        }

        private void InvokeTaskCompleted(Task task)
        {
            var handler = TaskCompleted;
            if (handler != null) handler.Invoke(this, new TaskEventArgs(task));
        }
        #endregion Invoke Methods

        #region Public Methods
        public void CheckStatus()
        {
            if (PreviousStatus != Task.Status) PreviousStatus = Task.Status;
        }
        #endregion Public Methods
    }
}
