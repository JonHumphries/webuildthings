﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WeBuildThings.MultiThreading
{
    /// <summary>
    /// Aggregates current thread status and makes available to other parts of the application
    /// </summary>
    public static class ThreadMonitor
    {

        #region Fields
        private static Thread MonitoringThread;
        #endregion Fields

        #region Properties
        public static bool Running = true;
        public static ConcurrentBag<MonitoredThread> Threads = new ConcurrentBag<MonitoredThread>();
       // public static ConcurrentDictionary<int, string> Messages = new ConcurrentDictionary<int, string>();
        #endregion Properties

        #region Events
        public static event EventHandler<TaskEventArgs> TaskCompleted;
        public static event EventHandler<TaskEventArgs> StatusChanged;
        #endregion Events

        #region Invoke Methods
        private static void InvokeTaskCompleted(Task task)
        {
            var handler = TaskCompleted;
            if (handler != null) { handler.Invoke(null, new TaskEventArgs(task)); }
        }

        private static void InvokeStatusChanged(Task task)
        {
            var handler = StatusChanged;
            if (handler != null) { handler.Invoke(null, new TaskEventArgs(task)); }
        }
        #endregion Invoke Methods

        #region Public Methods
        public static int Add(Task task)
        {
            SetMonitoringThread();
            var monitoredThread = new MonitoredThread(string.Empty, task);
            monitoredThread.TaskCompleted -= monitoredThread_TaskCompleted;
            monitoredThread.TaskCompleted += monitoredThread_TaskCompleted;
            monitoredThread.StatusChanged -= monitoredThread_StatusChanged;
            monitoredThread.StatusChanged += monitoredThread_StatusChanged;
            Threads.Add(monitoredThread);
            return task.Id;
        }
        
        public static int Add(string message,Task task)
        {
            SetMonitoringThread();
            var monitoredThread = new MonitoredThread(message, task);
            monitoredThread.TaskCompleted -= monitoredThread_TaskCompleted;
            monitoredThread.TaskCompleted += monitoredThread_TaskCompleted;
            monitoredThread.StatusChanged -= monitoredThread_StatusChanged;
            monitoredThread.StatusChanged += monitoredThread_StatusChanged;
            Threads.Add(monitoredThread);
            return task.Id;
        }

        public static void AddMessage(int taskId, string message)
        {
            var thread = Threads.Where(n => n.Task != null && n.Task.Id == taskId).FirstOrDefault();
            if (thread != null) thread.Message = message;
            else
            {
                thread = new MonitoredThread(message, taskId);
                Threads.Add(thread);
            }
        }

        public static string GetMessage(int taskId)
        {
            string result = string.Empty;
            var thread = Threads.Where(n => n.Task != null && n.Task.Id == taskId).FirstOrDefault();
            if (thread != null) result = thread.Message;
            return result;
        }

        public static void CheckStatus()
        {
            while (Running)
            {
                foreach (var thread in Threads.Where(n => !n.Completed))
                {
                    thread.CheckStatus();
                }
                Thread.Sleep(1000);
            }
        }
        #endregion Public Methods

        #region Event Methods
        static void monitoredThread_TaskCompleted(object sender, TaskEventArgs e)
        {
            InvokeTaskCompleted(e.Task);
        }

        static void monitoredThread_StatusChanged(object sender, TaskEventArgs e)
        {
            InvokeStatusChanged(e.Task);
        }
        
        #endregion Event Methods

        #region Set Methods
        public static void SetMonitoringThread()
        {
            Running = true;
            MonitoringThread = new Thread(new ThreadStart(CheckStatus));
            MonitoringThread.Start();
        }
        #endregion Set Methods

        public static void Exit()
        {
            Running = false;
        }
    }

    
}
