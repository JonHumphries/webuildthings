using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace WeBuildThings.Utilities
{
    public static class ThreadUtil
    {
        private static readonly bool IsSingleCpuMachine = (Environment.ProcessorCount == 1);

        public static void StallThread(int Milliseconds)
        {
            if (IsSingleCpuMachine) NativeMethods.SwitchToThread();
            else Thread.Sleep(Milliseconds);
        }
    }


    internal static class NativeMethods
    {
        [DllImport("kernel32", ExactSpelling = true)]
        [return: MarshalAs(UnmanagedType.Bool)] 
        internal static extern bool SwitchToThread();
    }
}
