﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace WeBuildThings.Utilities.Extensions
{
    public static class ConfigMgrHelper
    {

        public static void ValidateConnectionStringName(string connectionStringName)
        {
            if (ConfigurationManager.ConnectionStrings[connectionStringName] == null)
            {
                throw new ArgumentNullException(string.Format("{0} missing from App Settings", connectionStringName));
            }
        }

        public static void TestAppSetting(string settingName)
        {
            if (ConfigurationManager.AppSettings[settingName] == null)
            {
                throw new ArgumentNullException(string.Format("{0} missing from App Settings", settingName));
            }
        }

        public static string GetAppSetting(string settingName)
        {
            TestAppSetting(settingName);
            return ConfigurationManager.AppSettings[settingName];
        }

       
    }
}
