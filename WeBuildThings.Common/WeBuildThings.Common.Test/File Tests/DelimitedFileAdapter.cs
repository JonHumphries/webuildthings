﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Excel;
using WeBuildThings.Common.Interfaces;
using System.IO;
using WeBuildThings.Common.PipeFileAdapter;

namespace WeBuildThings.Common.Test
{
    /// <summary>
    /// Summary description for DelimitedFileAdapter
    /// </summary>
    [TestClass]
    public class DelimitedFileAdapter
    {
        private string excelTestFilePath = @"C:\Testing\ExcelFile.xlsx";
        private string pipeTestFilePath = @"C:\Testing\PipeFile.txt";

        [TestMethod]
        public void ExcelAdapterCount()
        {
            int expectedResult = 4;
            using (IDelimitedFileAdapter adapter = new ExcelAdapter())
            {
                adapter.File = new FileInfo(excelTestFilePath);
                Assert.AreEqual(expectedResult, adapter.Count);
            }
        }

        [TestMethod]
        public void PipeAdapterCount()
        {
            int expectedResult = 4;
            using (IDelimitedFileAdapter adapter = new PipeAdapter())
            {
                adapter.File = new FileInfo(pipeTestFilePath);
                Assert.AreEqual(expectedResult, adapter.Count);
            }
        }

      
    }
}
