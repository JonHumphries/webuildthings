﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using WeBuildThings.Utilities;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class FileOperationTests
    {
        [TestMethod]
        public void FileContains()
        {

            string path = @"c:\Testing\MyTest.txt";

            if (File.Exists(path))
            {
                File.Delete(path);
            }
            // Create a file to write to.
            string createText = "Hello and Welcome" + Environment.NewLine;
            File.WriteAllText(path, createText);

            // This text is always added, making the file longer over time
            // if it is not deleted.
            string appendText = "This is extra text" + Environment.NewLine;
            File.AppendAllText(path, appendText);

            // Open the file to read from.

            Assert.IsTrue(FileOperations.FileContains(path,"extra"));
            if (File.Exists(path))
            {
                File.Delete(path);
            }
        }

        [TestMethod]
        public void MoveFileTest1()
        {
            var newPath = Path.Combine(@"C:\Documents", "archive", "test.txt");
            Assert.AreEqual(@"C:\Documents\archive\test.txt",newPath);
        }
    }
}
