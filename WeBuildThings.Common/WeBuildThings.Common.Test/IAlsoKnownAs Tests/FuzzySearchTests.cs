﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Interfaces;
using System.Collections.Generic;


namespace WeBuildThings.Common.Test.IAlsoKnownAs_Tests
{
    [TestClass]
    public class FuzzySearchTests
    {

        internal class TestAKA : IAlsoKnownAs
        {
            public string Name { get; set; }
            public List<TestAKAItem> AkaItems { get; set; }

            public TestAKA() { AkaItems = new List<TestAKAItem>(); }

            public void AddAKA(string newAka)
            {
                if (!AkaItems.Where(n => n.Name == newAka).Any())
                {
                    var item = new TestAKAItem() { Name = newAka, TestAka = this };
                    AkaItems.Add(item);
                }
            }

            public override string ToString()
            {
                return Name;
            }



            public IEnumerable<string> AKAList
            {
                get { return AkaItems.Select(n => n.Name); }
            }
        }

        internal class TestAKAItem
        {

            public string Name { get; set; }

            public TestAKA TestAka { get; set; }

            public override string ToString()
            {
                return Name;
            }
        }

        internal TestAKA Medicare
        {
            get
            {
                var testAKA = new TestAKA() { Name = "Medicare" };
                testAKA.AddAKA("Medicare");
                testAKA.AddAKA("Medicare (Part A & B)");
                testAKA.AddAKA(" ");
                return testAKA;
            }
        }

        internal TestAKA Medica
        {
            get
            {
                var testAKA = new TestAKA() { Name = "Medica" };
                testAKA.AddAKA("Medica");
                return testAKA;
            }
        }

        internal IEnumerable<TestAKA> PayerList
        {
            get { return new List<TestAKA>() { Medicare, Medica }; }
        }

        [TestMethod]
        public void PayerListCount()
        {
            Assert.AreEqual(2, PayerList.Count());
        }

        [TestMethod]
        public void MedicaVsMedicare1()
        {
            var actualResult = IAlsoKnownAsExtensions.FuzzySearch<TestAKA>(PayerList, "Medicare", false);
            Assert.AreEqual(Medicare.Name, actualResult.Name);
        }

        [TestMethod]
        public void MedicaVsMedicare2()
        {
            var actualResult = IAlsoKnownAsExtensions.FuzzySearch<TestAKA>(PayerList, "Medicare (Part A & B)", false);
            Assert.IsNotNull(actualResult);
            Assert.AreEqual(Medicare.Name, actualResult.Name);
        }

        [TestMethod]
        public void MedicaVsMedicare3()
        {
            var actualResult = IAlsoKnownAsExtensions.FuzzySearch<TestAKA>(PayerList, "Medicare A", false);
            Assert.IsNotNull(actualResult);
            Assert.AreEqual(Medicare.Name, actualResult.Name);
        }

        [TestMethod]
        public void NoMatchTest()
        {
            var actualResult = IAlsoKnownAsExtensions.FuzzySearch<TestAKA>(PayerList, "Some Crazy Name", false);
            Assert.IsNotNull(actualResult);
            Assert.AreEqual(Medicare.Name, actualResult.Name);
        }
    }
}
