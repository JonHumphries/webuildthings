﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class StringFormatingTests
    {
        [TestMethod]
        public void NumberWithComma()
        {
            string actualResult = 1234.56.ToString("n2");
            Assert.AreEqual("1,234.56", actualResult);
        }

        [TestMethod]
        public void NumberWithoutComma()
        {
            string actualResult = 1234.56.ToString("F2");
            Assert.AreEqual("1234.56", actualResult);
        }

        [TestMethod]
        public void DateAndTimeFormats()
        {
            var actualResult = new DateTime(2016,11,3,12,24,30).ToString("yyyyMMddHHmmss");
            Assert.AreEqual("20161103122430", actualResult);
        }
    }
}
