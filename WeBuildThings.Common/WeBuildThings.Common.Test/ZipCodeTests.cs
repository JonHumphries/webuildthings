﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class ZipCodeTests
    {
        [TestMethod]
        public void FormatOn9()
        {
            var actualResult = ZipCode.Format("123456789");
            var expectedResult = "12345-6789";
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void FormatOn5()
        {
            var actualResult = ZipCode.Format("12345");
            var expectedResult = "12345";
            Assert.AreEqual(expectedResult, actualResult);
        }

        [TestMethod]
        public void FormatOnNull()
        {
            Assert.AreEqual(string.Empty, ZipCode.Format(null));
        }
    }
}
