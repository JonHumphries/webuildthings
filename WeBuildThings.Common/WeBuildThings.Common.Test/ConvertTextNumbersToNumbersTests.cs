﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class ConvertTextNumbersToNumbersTests
    {
        [TestMethod]
        public void IndividualNumbersTests()
        {
            string numberString = "zero one two three four five six seven eight nine";
            string expectedresult = "0123456789";
            Assert.AreEqual(expectedresult, numberString.ToIndividualNumbers());
        }

        [TestMethod]
        public void NumbersTests()
        {
            string numberString = "two hundred ninety one million eight hundred seventy six thousand five hundred forty three";
            long expectedResult = 291876543;
            Assert.AreEqual(expectedResult, numberString.ToLong());
        }

        [TestMethod]
        public void DecimalTests()
        {
            string moneyString = "two hundred seventy five and eight";
            string expectedResult = "$275.08";
            Assert.AreEqual(expectedResult, moneyString.ToMoney());
        }

        [TestMethod]
        public void MoneyTests()
        {
            string moneyString = "two hundred seventy five dollars and eight cents";
            string expectedResult = "$275.08";
            Assert.AreEqual(expectedResult, moneyString.ToMoney());
        }

        [TestMethod]
        public void SentenceTests()
        {
            string numberString = "The Number is zero one two three four five six seven eight nine";
            string expectedresult = "The Number is 0123456789";
            Assert.AreEqual(expectedresult, numberString.ConvertTextNumbersToNumbers());
        }
    }
}
