﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Windows;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class CurrentWindowTests
    {
        [TestMethod]
        public void TestDimensions()
        {
            var actualResults = CurrentWindow.Dimensions;
            Assert.IsTrue(actualResults.Top != -1);
            Assert.IsTrue(actualResults.Bottom != -1);
            Assert.IsTrue(actualResults.Left != -1);
            Assert.IsTrue(actualResults.Right != -1);
        }

        [TestMethod]
        public void TestSize()
        {
            var actualResults = CurrentWindow.Size;
            Assert.IsTrue(actualResults.Width != -1);
            Assert.IsTrue(actualResults.Height != -1);
        }
    }
}
