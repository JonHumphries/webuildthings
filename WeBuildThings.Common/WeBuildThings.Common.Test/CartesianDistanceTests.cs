﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class CartesianDistanceTests
    {
        [TestMethod]
        public void IsIntegerTest1()
        {
            int result;
            Assert.IsFalse(int.TryParse("1.234", out result));
            Assert.IsTrue(int.TryParse("-12", out result));
        }


        [TestMethod]
        public void SimpleDistanceTest1()
        {
            Assert.AreEqual(1, StandardCalculations.CartesianDistance(0, 0, 0, 0, 0, 1));
        }

        [TestMethod]
        public void NegativeDistanceTest1()
        {
            Assert.AreEqual(1, StandardCalculations.CartesianDistance(0, 0, 0, 0, 0, -1));
        }


        [TestMethod]
        public void NegativeDistanceTest2()
        {
            Assert.AreEqual(2, StandardCalculations.CartesianDistance(0, 0, -1, 0, 0, 1));
        }

        [TestMethod]
        public void MDDistanceTest1()
        {
            Assert.AreEqual(1, StandardCalculations.MultiDimensionalDistance(new double[] {0, 0, 0}, new double[]{ 0, 0, 1}));
        }

        [TestMethod]
        public void MDNegativeDistanceTest1()
        {
            Assert.AreEqual(1, StandardCalculations.MultiDimensionalDistance(new double[] { 0, 0, 0 }, new double[] { 0, 0, -1 }));
        }


        [TestMethod]
        public void MDNegativeDistanceTest2()
        {
            Assert.AreEqual(2, StandardCalculations.MultiDimensionalDistance(new double[] { 0, 0, -1 }, new double[] { 0, 0, 1 }));
        }

        [TestMethod]
        public void FiveDimensionalDistanceTest1()
        {
            Assert.AreEqual(1, StandardCalculations.MultiDimensionalDistance(new double[] { 0, 0, 0, 0, 0 }, new double[] { 0, 0, 1, 0, 0 }));
        }
    }
}
