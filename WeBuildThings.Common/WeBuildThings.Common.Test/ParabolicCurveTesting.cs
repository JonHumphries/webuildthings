﻿using System;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;

namespace WeBuildThings.Common.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ParabolicCurveTesting
    {

        [TestMethod]
        public void ParabolicCurveTest1()
        {
            decimal expectedResult = 13.6m;
            var actualValue = (decimal)StandardCalculations.YValueOfParabolicCurve(0, -120, 100, -0.006);
            Assert.AreEqual(expectedResult, actualValue);
        }

        [TestMethod]
        public void ParabolicCurveTest2()
        {
            decimal expectedResult = 51.4m;
            var actualValue = (decimal)StandardCalculations.YValueOfParabolicCurve(30, -120, 100, -0.006);
            Assert.AreEqual(expectedResult, actualValue);
        }

        [TestMethod]
        public void TimeZoneInvestigation()
        {
            var list = TimeZoneInfo.GetSystemTimeZones().ToList();
            foreach (var item in list)
            {
                Debug.WriteLine(item);
            }
        }
    }
}
