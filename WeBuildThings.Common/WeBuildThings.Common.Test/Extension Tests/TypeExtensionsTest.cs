﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class TypeExtensionsTest
    {
        [TestMethod]
        public void GetAllReferencedTypesTest()
        {
            List<Type> expectedValue = new List<Type>() { typeof(TestType), typeof(AnotherTestType)};
            IEnumerable<Type> actualValue = typeof(TestType).GetAllReferencedTypes(new List<Type>());
            foreach (var item in expectedValue)
            {
                Assert.IsTrue(actualValue.Contains(item));
            }
            foreach(var item in actualValue)
            {
                Assert.IsTrue(expectedValue.Contains(item));
            }
        }

        [TestMethod]
        public void GetAllReferencedAndAssignableTypesTest()
        {
            List<Type> expectedValue = new List<Type>() { typeof(TestType), typeof(AnotherTestType), typeof(YetAnotherTestType), typeof(TestEnum) };
            IEnumerable<Type> actualValue = typeof(TestType).GetAllReferencedAndAssignableTypes(new List<Type>());
            foreach (var item in expectedValue)
            {
                Assert.IsTrue(actualValue.Contains(item));
            }
            foreach (var item in actualValue)
            {
                Assert.IsTrue(expectedValue.Contains(item));
            }
        }

        #region IsCollection
        [TestMethod]
        public void IsCollectionWithIEnumerableTest()
        {
            IEnumerable<Type> sampleCollection = new List<Type>();
            bool actualResult = sampleCollection.GetType().IsCollection();
            Assert.IsTrue(actualResult);
        }

        [TestMethod]
        public void IsCollectionWithCollectionTest()
        {
            List<Type> sampleCollection = new List<Type>();
            bool actualResult = sampleCollection.GetType().IsCollection();
            Assert.IsTrue(actualResult);
        }

        [TestMethod]
        public void IsCollectionWithoutCollectionTest()
        {
            string sampleValue = "abc123";
            bool actualResult = sampleValue.GetType().IsCollection();
            Assert.IsFalse(actualResult);
        }

        [TestMethod]
        public void IsCollectionWithFalsePositivesTest()
        {
            Type[] primitiveTypes = new Type[] { typeof(string), typeof(int), typeof(float), typeof(decimal), typeof(TestEnum) };
            foreach (Type type in primitiveTypes)
            {
                Assert.IsFalse(type.IsCollection());
            }
        }
        #endregion IsCollection

        [TestMethod]
        public void ContainsInterface()
        {
            var testType = typeof(TestType);
            Assert.IsTrue(testType.ContainsInterface(typeof(ITestInterface)));
            var validateTest = typeof(AnotherTestType);
            Assert.IsFalse(validateTest.ContainsInterface(typeof(ITestInterface)));
        }
    }

    public enum TestEnum { Test1, Test2 };

    public class TestType: ITestInterface
    {
        public string Name { get { return "Name"; } }
        public string String { get; set; }
        public TestType TestType1 { get; set; }
        public decimal Decimal { get; set; }
        public float Float { get; set; }
        public IEnumerable<AnotherTestType> AnotherTestType { get; set; }
    }

    public class AnotherTestType
    {
        public int Integer { get; set; }
    }

    public class YetAnotherTestType : AnotherTestType
    {
        public TestEnum TestEnum { get; set; }
    }

    public interface ITestInterface
    {
        string Name { get; }
    }
}
