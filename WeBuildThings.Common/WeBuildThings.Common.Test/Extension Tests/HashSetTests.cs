﻿using System;
using System.Linq;
using WeBuildThings.Common.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class HashSetTests
    {
        private class OtherClass
        {
            public string FirstString { get; set; }

            public OtherClass() { }
        }

        private class DoubleString : OtherClass
        {
            public string SecondString { get; set; }

            public DoubleString() : base() { }
        }

        [TestMethod]
        public void ToHashSet_T_X_Test()
        {
            var originalList = new List<DoubleString>();
            DoubleString item1 = new DoubleString() { FirstString = "String1", SecondString = "String2"};
            DoubleString item2 = new DoubleString() { FirstString = "item2 string1", SecondString = "item2 string2" };
            originalList.Add(item1);
            originalList.Add(item2);
            IEnumerable<OtherClass> convertedList = (IEnumerable<OtherClass>)originalList.Where(n => n.FirstString == "String1");
            var actualList = convertedList.ToHashSet<DoubleString,OtherClass>();
            Assert.IsTrue(actualList.FirstOrDefault() == originalList.Where(n => n.FirstString == "String1").FirstOrDefault());
            Assert.AreEqual(1, actualList.Count());
        }
    }
}
