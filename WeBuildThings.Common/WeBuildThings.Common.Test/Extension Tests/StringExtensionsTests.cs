﻿using System;
using WeBuildThings.Common.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        public void RemoveWhiteSpace()
        {
            string testData = " This  is    my  string";
            string expectedResult = "This is my string";
            Assert.AreEqual(expectedResult, testData.RemoveWhiteSpace());
        }

        [TestMethod]
        public void LeadingZeroTest1()
        {
            int x = 24;
            var actualResult = x.ToString(5);
            Assert.AreEqual("00024", actualResult);
        }

        [TestMethod]
        public void LeadingZeroTest2()
        {
            int x = 0;
            var actualResult = x.ToString(5);
            Assert.AreEqual("00000", actualResult);
        }
        [TestMethod]
        public void LeadingZeroTest3()
        {
            int x = 2410;
            var actualResult = x.ToString(0);
            Assert.AreEqual("2410", actualResult);
        }

        [TestMethod]
        public void LeadingZeroTest4()
        {
            int x = 24;
            var actualResult = x.ToString(-1);
            Assert.AreEqual("24", actualResult);
        }

        [TestMethod]
        public void SplitTest1()
        {
            string testData = "  This -is a string -with three splits";
            var actualResult = testData.Split(" -");
            Assert.AreEqual(3, actualResult.Length);
        }

        [TestMethod]
        public void SplitTest2()
        {
            string testData = "  This is a string with no splits";
            var actualResult = testData.Split(" -");
            Assert.AreEqual(1, actualResult.Length);
        }

        [TestMethod]
        public void SplitTest3()
        {
            string testData = "  This is a string with weird -splits -";
            var actualResult = testData.Split(" -");
            Assert.AreEqual(2, actualResult.Length);
        }

        [TestMethod]
        public void SplitTest4()
        {
            string testData = "";
            var actualResult = testData.Split(" -");
            Assert.AreEqual(0, actualResult.Length);
        }
    }
}
