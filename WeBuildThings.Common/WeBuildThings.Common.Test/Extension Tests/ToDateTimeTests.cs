﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Test.Extension_Tests
{
    [TestClass]
    public class ToDateTimeTests
    {
        [TestMethod]
        public void MDDYYYYTest1()
        {
            var actualResult = "5251952".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(1952, 5, 25), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest2()
        {
            var actualResult = "1051952".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(1952, 1, 5), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest3()
        {
            var actualResult = "1151952".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(1952, 1, 15), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest4()
        {
            var actualResult = "151952".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(1952, 1, 5), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest5()
        {
            var actualResult = "4415".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(2015, 4, 4), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest6()
        {
            var actualResult = "41515".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(2015, 4, 15), actualResult);
        }

        [TestMethod]
        public void MDDYYYYTest7()
        {
            var actualResult = "41592".ToDateTime(DateFormat.MMDDYYYY);
            Assert.AreEqual(new DateTime(1992, 4, 15), actualResult);
        }

        [TestMethod]
        public void MMDDYYTest3()
        {
            var actualResult = "11042015".ToDateTime(DateFormat.MMDDYY);
            Assert.AreEqual(new DateTime(2015, 11, 4), actualResult);
        }
        
        [TestMethod]
        public void MMDDYYTest1()
        {
            var actualResult = "110415".ToDateTime(DateFormat.MMDDYY);
            Assert.AreEqual(new DateTime(2015, 11, 4), actualResult);
        }

        [TestMethod]
        public void MMDDYYTest2()
        {
            var actualResult = "11415".ToDateTime(DateFormat.MMDDYY);
            Assert.AreEqual(new DateTime(2015, 1, 14), actualResult);
        }

        [TestMethod]
        public void CalcFullYearTest1()
        {
            var actualResult = StringExtensions.CalcFullYear(20);
            Assert.AreEqual("1920", actualResult);
        }

        [TestMethod]
        public void CalcFullYearTest2()
        {
            var actualResult = StringExtensions.CalcFullYear(15);
            Assert.AreEqual("2015", actualResult);
        }

        [TestMethod]
        public void CalcDaysAsIntTest1()
        {
            var actualResult = StringExtensions.ToDateTime("17627", DateFormat.DaysAsInt);
            Assert.AreEqual(new DateTime(1948,4,4) , actualResult);
        }
    }
}
