﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class IEnumerableExtensionTests
    {
        class Node
        {
            public int NodeId { get; set; }
            public int LevelId { get; set; }
            public IEnumerable<Node> Children { get; set; }

            public override string ToString()
            {
                return String.Format("Node {0}, Level {1}", this.NodeId, this.LevelId);
            }
        }

        private IEnumerable<Node> GetNodes()
        {
            // Create a 3-level deep hierarchy of nodes
            Node[] nodes = new Node[]
            {
                new Node 
                { 
                    NodeId = 1, 
                    LevelId = 1, 
                    Children = new Node[]
                    {
                        new Node { NodeId = 2, LevelId = 2, Children = new Node[] {} },
                        new Node
                        {
                            NodeId = 3,
                            LevelId = 2,
                            Children = new Node[]
                            {
                                new Node { NodeId = 4, LevelId = 3, Children = new Node[] {} },
                                new Node { NodeId = 5, LevelId = 3, Children = new Node[] {} }
                            }
                        }
                    }
            },
            new Node { NodeId = 6, LevelId = 1, Children = new Node[] {} }
            };
            return nodes;
        }

        [TestMethod]
        public void Flatten_Nested_Heirachy()
        {
            IEnumerable<Node> nodes = GetNodes();
            var flattenedNodes = nodes.FlattenHierarchy( p => true, (Node n) => { return n.Children; } );
            foreach (Node flatNode in flattenedNodes)
            {
                Console.WriteLine(flatNode.ToString());
            }

            // Make sure we only end up with 6 nodes
            Assert.AreEqual(6, flattenedNodes.Count());
        }

        [TestMethod]
        public void Only_Return_Nodes_With_Even_Numbered_Node_IDs()
        {
            IEnumerable<Node> nodes = GetNodes();
            var flattenedNodes = nodes.FlattenHierarchy(
              p => (p.NodeId % 2) == 0,
              (Node n) => { return n.Children; }
            );
            foreach (Node flatNode in flattenedNodes)
            {
                Console.WriteLine(flatNode.ToString());
            }
            // Make sure we only end up with 3 nodes
            Assert.AreEqual(3, flattenedNodes.Count());
        }
    }
}
