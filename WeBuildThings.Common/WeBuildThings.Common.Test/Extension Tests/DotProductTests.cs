﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Common.Test.Extension_Tests
{
    [TestClass]
    public class DotProductTests
    {
        [TestMethod]
        public void DotProductTest1()
        {
            var x = new double[] { 1, 3, -5 };
            var y = new double[] { 4, -2, -1 };
            var actualResult = StandardCalculations.DotProduct(x, y);
            Assert.AreEqual(3, actualResult);
        }
    }
}
