﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.Extensions;
using System.Linq;

namespace WeBuildThings.Common.Test
{
    [TestClass]
    public class BaseExtensionsTest
    {
        [TestMethod]
        public void RemoveSpacesTest()
        {
            string expectedValue = "ThisStringHasNoSpaces";
            string actualValue = "This String Has No Spaces".RemoveSpaces();
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void CreateFuzzyListTest()
        {
            string[] expectedValues = new string[] { "Test String", "TEST STRING", "TESTSTRING", "teststring", "TestString", "test string","Test_String", "test_string","TEST_STRING","Test_ String","test_ string", "TEST_ STRING"};
            var actualValue = "Test_String".CreateFuzzyList();
            foreach (string value in expectedValues)
            {
                Assert.IsTrue(actualValue.Contains(value));
            }
            foreach (string value in actualValue)
            {
                Assert.IsTrue(expectedValues.Contains(value));
            }
        }

        [TestMethod]
        public void DaysAsIntToDateTime()
        {
            string date = "42134";
            DateTime expectedValue = new DateTime(2015, 5, 10);
            var actualValue = date.ToDateTime(DateFormat.DaysAsInt);
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
