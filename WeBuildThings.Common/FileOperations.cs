﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace WeBuildThings.Utilities
{
    public static class FileOperations
    {
        /* Illegal characters in filename
        # pound	< left angle bracket	$ dollar sign	+ plus sign
        % percent	> right angle bracket	! exclamation point	` backtick
        & ampersand	* asterisk	‘ single quotes	| pipe
        { left bracket	? question mark	“ double quotes	= equal sign
        } right bracket	/ forward slash	: colon	 
        \ back slash	blank spaces	@ at sign
         */

        /// <summary>
        /// Get the no. of files exists in Full Path
        /// </summary>
        /// <param _Name="FullPath"></param>
        /// <returns></returns>
        public static int GetNumberOfFilesInFolder(string FullPath)
        {
            if (!Directory.Exists(FullPath))
            {
                Directory.CreateDirectory(FullPath);
            }
            return Directory.GetFiles(FullPath, "*.*", SearchOption.TopDirectoryOnly).Length;
        }

        /// <summary>
        /// Check to see if the _File exists according to the Path.
        /// </summary>
        /// <param _Name="_FullPath"> The full path of the _File being checked</param>
        /// <returns>bool, true = exists, false = does not exist</returns>
        public static bool FileExists(string fullPath)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(fullPath))
            {
                result = File.Exists(fullPath);
            }
            return result;
        }

        /// <summary>
        /// Delete the _File if full path exists.
        /// </summary>
        /// <param _Name="_FullPath"></param>
        public static void Delete(string fullPath)
        {
            if (!string.IsNullOrWhiteSpace(fullPath) && File.Exists(fullPath))
            {
                File.Delete(fullPath);
            }
        }

        /// <summary>
        /// Check to see if the directory exists according the directory Path
        /// </summary>
        /// <param _Name="DirectoryPath"></param>
        /// <returns>bool, true = exists, false = does not exist</returns>
        public static bool DirectoryExists(string DirectoryPath)
        {
            return Directory.Exists(DirectoryPath);
        }

        /// <summary>
        /// Create given directory if it does not exist.  Also strips out any filename passed.
        /// </summary>
        /// <returns>bool, true = Created directory</returns>
        public static bool CreateNewDirectory(string directoryPath)
        {
            bool success = false;
            var isUnc = new Uri(directoryPath).IsUnc;

            if (isUnc && !IsValidUnc(directoryPath))
            {
                directoryPath += @"\";
            }

            FileInfo fileInfo = new FileInfo(directoryPath);
            string path = (!string.IsNullOrWhiteSpace(fileInfo.Extension))? fileInfo.DirectoryName : directoryPath;
            if (!DirectoryExists(path))
            {
                Directory.CreateDirectory(path);
                success = true;
            }
            return success;
        }

        /// <summary>
        /// Copy the files from souce to destination
        /// </summary>
        /// <param _Name="sourcePath"></param>
        /// <param _Name="destinationPath"></param>
        public static void FileTransfer(string sourcePath, string destinationPath)
        {
            File.Copy(sourcePath, destinationPath);
        }

        /// <summary>
        /// Read the stream from the given path.
        /// </summary>
        /// <param _Name="sourcePath"></param>
        /// <returns>FileStream for the given path</returns>
        public static Stream ReadStreamFromFile(string sourcePath)
        {
            return new FileStream(sourcePath, FileMode.Open, FileAccess.Read);
        }

        /// <summary>
        /// Searches a file and returns true if it contains the expected Text.
        /// </summary>
        public static bool FileContains(string filePath, string expectedText)
        {
            return File.ReadAllText(filePath).Contains(expectedText);
        }

        /// <summary>
        /// Write the steam to the destination path.
        /// </summary>
        /// <param _Name="input"></param>
        /// <param _Name="destinationPath"></param>
        public static void WriteStreamToFile(Stream input, string destinationPath)
        {
            using (Stream destinationStream = File.OpenWrite(destinationPath))
            {
                byte[] buffer = new byte[8 * 1024];
                int len;
                while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    destinationStream.Write(buffer, 0, len);
                }
            }
        }

        private static bool IsValidUnc(string path)
        {
            return !Regex.IsMatch(path, @"^[\\/]+[^\\/]+[\\/]+[^\\/]+$");
        }
    }
}