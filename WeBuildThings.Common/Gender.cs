﻿using System;

namespace WeBuildThings.Common.Enumerations
{
    public enum Gender { Unknown, Male, Female };

    public static class GenderExtensions
    {
        public static Gender Opposite(this Gender value)
        {
            Gender result = Gender.Unknown;
            if (value == Gender.Male) result = Gender.Female;
            else if (value == Gender.Female) result = Gender.Male;
            return result;
        }

        public static Gender ToGender(this string value)
        {
            Gender result;
            if (value == "U") value = "Unknown";
            if (value == "M") value = "Male";
            if (value == "F") value = "Female";
            result = (Gender)Enum.Parse(typeof(Gender), value, true);
            return result;
        }
    }
}
