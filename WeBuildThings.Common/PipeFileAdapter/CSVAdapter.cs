﻿using System.Collections.Generic;
using System.IO;

namespace WeBuildThings.Common.PipeFileAdapter
{
    public class CSVAdapter : PipeAdapter
    {
        #region Constructor
        public CSVAdapter() : base() { }

        public CSVAdapter(FileInfo file) : base(file) { }

        public CSVAdapter(FileInfo file, string delimiter) : base(file, delimiter) { }

        public CSVAdapter(string filePath) : base(filePath) { }

        public CSVAdapter(string filePath, string delimiter) : base(filePath, delimiter) { }

        protected override void Initialize()
        {
            Delimiter = ','.ToString();
        }
        #endregion Constructor

        #region Public Methods
        public override void SetRows(long index, long readTo)
        {
            if (ValidateFile(File))
            {
                LastReadTo = readTo;
                Rows = new List<Dictionary<string, string>>();
                int x = 0;
                foreach (var line in System.IO.File.ReadLines(File.FullName))
                {
                    //string cleanLine = CleanValue(line);
                    string[] currentLine = CleanArray(line.Split((char)44));
                    if (x == 0)
                    {
                        HeaderRow = currentLine;
                    }
                    else if (x >= index)
                    {
                        Dictionary<string, string> rowValues = new Dictionary<string, string>();
                        for (int i = 0; i < currentLine.Length; i++)
                        {
                            if (i < HeaderRow.Length)
                            {
                                rowValues.Add(HeaderRow[i], currentLine[i]);
                            }
                        }
                        Rows.Add(rowValues);
                        if (x >= readTo)
                        {
                            break;
                        }
                    }
                    x++;
                }
                if (x >= Count)
                {
                    EndOfFile = true;
                }
            }
        }
        #endregion Public Methods

        private string[] CleanArray(string[] values)
        {
            for (int i = 0; i < values.Length; i++)// (string value in values)
            {
                values[i] = CleanValue(values[i]);
            }
            return values;
        }

        private string CleanValue(string value)
        {
            return value.Replace(((char)34).ToString(), string.Empty);
        }
    }
}
