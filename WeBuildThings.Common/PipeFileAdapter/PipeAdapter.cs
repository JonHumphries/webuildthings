﻿using WeBuildThings.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Common.PipeFileAdapter
{
    public class PipeAdapter : IDelimitedFileAdapter
    {
        #region Consts
        private const int ReadBlock = 150000;
        #endregion Consts

        #region Fields
        private bool _Loaded;
        private string _Delimiter;
        private FileInfo _File;
        private List<Dictionary<string, string>> _Rows;
        private long _LastReadTo;
        private string[] _HeaderRow;
        private int _Count;
        private bool _Disposed;
        private bool _EndOfFile;
        #endregion Fields

        #region Constructors
        public PipeAdapter()
        {
            Initialize();
        }

        public PipeAdapter(FileInfo file)
        {
            Initialize();
            File = file;
        }

        public PipeAdapter(FileInfo file, string delimiter)
        {
            Initialize();
            Delimiter = delimiter;
            File = file;
        }

        public PipeAdapter(string filePath)
        {
            Initialize();
            File = new FileInfo(filePath);
        }

        public PipeAdapter(string filePath, string delimiter)
        {
            Initialize();
            Delimiter = delimiter;
            File = new FileInfo(filePath);
        }

        protected virtual void Initialize()
        {
            Delimiter = '|'.ToString();
        }
        #endregion Constructors

        #region Properties
        public bool Loaded
        {
            get { return _Loaded; }
            set { if (_Loaded != value) _Loaded = value; }
        }

        public string Delimiter
        {
            get { return _Delimiter; }
            set { if (_Delimiter != value) { _Delimiter = value; OnDelimiterChanged(); } }
        }

        public List<Dictionary<string, string>> Rows
        {
            get { return _Rows; }
            set { if (_Rows != value) _Rows = value; }
        }

        public long LastReadTo
        {
            get { return _LastReadTo; }
            protected set { if (_LastReadTo != value) _LastReadTo = value; }
        }

        public string[] HeaderRow
        {
            get { return _HeaderRow; }
            set { if (_HeaderRow != value) _HeaderRow = value; }
        }

        public FileInfo File
        {
            get { return _File; }
            set { if (_File != value) { _File = value; OnFileChanged(); } }
        }

        public int Count
        {
            get { return _Count; }
            protected set { if (_Count != value) _Count = value; }
        }

        IEnumerable<Dictionary<string, string>> IDelimitedFileAdapter.Rows
        {
            get { return Rows; }
        }

        public bool EndOfFile
        {
            get { return _EndOfFile; }
            protected set { if (_EndOfFile != value) _EndOfFile = value; }
        }
        #endregion Properties

        #region Event Methods
        private void OnFileChanged()
        {
            LoadFile();
        }

        private void OnDelimiterChanged()
        {
            LoadFile();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetCount()
        {
            if (ValidateFile(File))
            {
                Count = System.IO.File.ReadLines(File.FullName).Count();
            }
        }

        public void LoadRowBlock()
        {
            SetRows(LastReadTo, LastReadTo + ReadBlock);
        }

        public virtual void SetRows(long index, long readTo)
        {
            if (ValidateFile(File))
            {
                LastReadTo = readTo;
                Rows = new List<Dictionary<string, string>>();
                int x = 0;
                foreach (var line in System.IO.File.ReadLines(File.FullName))
                {
                    string[] currentLine = line.Split(Delimiter.ToCharArray());
                    if (x == 0)
                    {
                        HeaderRow = currentLine;
                    }
                    else if (x >= index)
                    {
                        Dictionary<string, string> rowValues = new Dictionary<string, string>();
                        for (int i = 0; i < HeaderRow.Length && i < currentLine.Length; i++)
                        {
                            if (rowValues.ContainsKey(currentLine[i]))
                            {
                                throw new ArgumentException("Header Row contains duplicate.  Duplicated value is : '" + currentLine[i]+ ",");
                            }
                            rowValues.Add(HeaderRow[i], currentLine[i]);
                        }
                        Rows.Add(rowValues);
                        if (x >= readTo)
                        {
                            break;
                        }
                    }
                    x++;
                }
                if (x >= Count)
                {
                    EndOfFile = true;
                }
            }
        }
        #endregion Set Methods

        #region Public Methods
        public void LoadFile()
        {
            EndOfFile = false;
            Loaded = false;
            if (ValidateFile(File))
            {
                SetCount();
                SetRows(0, ReadBlock);
                Loaded = true;
            }
        }
        #endregion Public Methods

        #region Validation Methods
        public static bool ValidateFile(FileInfo file)
        {
            return (file != null && !string.IsNullOrWhiteSpace(file.FullName) && file.Exists);
        }
        #endregion Validation Methods

        #region IDelimitedFileAdapater
        IEnumerable<string> IDelimitedFileAdapter.HeaderRow
        {
            get { return HeaderRow.Select(n => n); }
        }
        #endregion IDelimitedFileAdapater

        #region IDisposable Implementation
        ~PipeAdapter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    _Disposed = true;
                }
            }
        }
        #endregion IDisposable Implementation
    }
}
