﻿using System;
using System.IO;
using System.Security.Permissions;

namespace WeBuildThings.Common.FileSystem
{
    public class FileSystemMonitor
    {
        public event FileSystemEventHandler FileChanged;
        public event FileSystemEventHandler FileCreated;
        public event FileSystemEventHandler FileDeleted;
        public event FileSystemEventHandler FileRenamed;

        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        public void Run(string filter, string directoryPath, bool includeSubDirectories)
        {
            // If a directory is not specified, exit program.
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Path = directoryPath;

            /* Watch for changes in LastAccess and LastWrite times, and the renaming of files or directories. */
            watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            watcher.IncludeSubdirectories = includeSubDirectories;
            // Only watch text files.
            watcher.Filter = filter;

            // Add event handlers.
            watcher.Changed -= new FileSystemEventHandler(OnFileChanged);
            watcher.Changed += new FileSystemEventHandler(OnFileChanged);
            watcher.Created -= new FileSystemEventHandler(OnFileCreated);
            watcher.Created += new FileSystemEventHandler(OnFileCreated);
            watcher.Deleted -= new FileSystemEventHandler(OnFileDeleted);
            watcher.Deleted += new FileSystemEventHandler(OnFileDeleted);
            watcher.Renamed -= new RenamedEventHandler(OnFileRenamed);
            watcher.Renamed += new RenamedEventHandler(OnFileRenamed);

            // Begin watching.
            watcher.EnableRaisingEvents = true;

            // Wait for the user to quit the program.
            Console.WriteLine("Press \'q\' to quit the sample.");
            while (Console.Read() != 'q') ;
        }

        // Define the event handlers.
        private void OnFileChanged(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            var handler = FileChanged;
            if (handler != null) handler.Invoke(source, e);
        }

        // Define the event handlers.
        private void OnFileCreated(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            var handler = FileCreated;
            if (handler != null) handler.Invoke(source, e);
        }

        // Define the event handlers.
        private void OnFileDeleted(object source, FileSystemEventArgs e)
        {
            // Specify what is done when a file is changed, created, or deleted.
            var handler = FileDeleted;
            if (handler != null) handler.Invoke(source, e);
        }

        private void OnFileRenamed(object source, RenamedEventArgs e)
        {
            // Specify what is done when a file is renamed.
            var handler = FileRenamed;
            if (handler != null) handler.Invoke(source, e);
        }
    }
}


    