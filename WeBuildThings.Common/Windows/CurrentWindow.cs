﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace WeBuildThings.Common.Windows
{

    internal static class SafeNativeMethods
    {
        [DllImport("user32.dll")]
        internal static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        internal static extern bool GetWindowRect(IntPtr hWnd, out RECT lpRect);

        
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left;        // x position of upper-left corner  
        public int Top;         // y position of upper-left corner  
        public int Right;       // x position of lower-right corner  
        public int Bottom;      // y position of lower-right corner  
    }

    public class CurrentWindow
    {
        public static RECT Dimensions
        {
            get { 
                RECT result = new RECT();
                SafeNativeMethods.GetWindowRect(SafeNativeMethods.GetForegroundWindow(), out result);
                return result;
            }
        }

        public static Size Size
        {
            get
            {
                RECT rect = new RECT();
                SafeNativeMethods.GetWindowRect(SafeNativeMethods.GetForegroundWindow(), out rect);
                Size result = new Size();
                result.Width = rect.Right - rect.Left;
                result.Height = rect.Bottom - rect.Top;
                return result;
            }
        }

    }
}
