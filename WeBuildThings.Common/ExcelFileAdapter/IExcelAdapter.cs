using WeBuildThings.Common.Interfaces;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using System.Data;

namespace WeBuildThings.Excel
{
    public interface IExcelAdapter : IDelimitedFileAdapter
    {
        void AddColumn(string columnName);
        Dictionary<string, string> GetRow(int rowNumber);
        string GetValue(string columnName, int rowNumber);
        void SetValue(string columnName, int rowNumber, string value);
        string[] ListSheets();
    }

    public enum ExcelAdapterType { OLE, Raw };

    public static class IExcelRowExtensions
    {
        //TODO this is pretty slow, and it is misleading
        //it results in a copy of the sheet in memory which takes a while to create and
        //any manipulation of the values doesn't get copied to the sheet.
        //needs to be more of a reference to the actual sheet.
        public static System.Collections.Generic.IEnumerable<Dictionary<string, string>> ToIExcelRow(this Range value)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            List<string> headerRow = new List<string>();
            int maxUsedColumns = 450;
            for (int columnNumber = 1; columnNumber < maxUsedColumns; columnNumber++)
            {
                Range rng = value[1, columnNumber];
                if (rng.Value2 != null)
                {
                    headerRow.Add(rng.Value2.ToString().Trim());
                }
                else
                {
                    break;
                }
            }
            int maxRows = value.Rows.Count;
            for (int rowNumber = 2; rowNumber <= maxRows; rowNumber++)
            {
                Dictionary<string, string> resultRow = new Dictionary<string, string>();
                for (int columnNumber = 0; columnNumber < headerRow.Count; columnNumber++)
                {
                    Range rng = value.Rows.Cells[rowNumber, columnNumber + 1];
                    if (rng.Value2 != null)
                    {
                        resultRow.Add(headerRow[columnNumber], rng.Value2.ToString().Trim());
                    }
                    else
                    {
                        resultRow.Add(headerRow[columnNumber], string.Empty);
                    }
                }
                yield return resultRow;
            }
            //return result;
        }

        public static List<Dictionary<string, string>> ToIExcelRow(this DataRowCollection value)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            List<string> headerRow = new List<string>();
            for (int columnNumber = 0; columnNumber < value[0].Table.Columns.Count; columnNumber++)
            {
                headerRow.Add(value[0].Table.Columns[columnNumber].ToString().Trim());
            }
            for (int rowNumber = 1; rowNumber < value.Count; rowNumber++)
            {
                Dictionary<string, string> resultRow = new Dictionary<string, string>();
                for (int columnNumber = 0; columnNumber < value[rowNumber].Table.Columns.Count; columnNumber++)
                {
                    resultRow.Add(headerRow[columnNumber], value[rowNumber][columnNumber].ToString().Trim());
                }
                result.Add(resultRow);
            }
            return result;
        }


    }
}
