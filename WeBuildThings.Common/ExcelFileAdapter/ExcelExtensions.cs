﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.Excel
{
    public static class ExcelExtensions
    {

        /// <summary>
        /// Returns the column name for the column number supplied.
        /// </summary>
        /// <see cref="http://stackoverflow.com/questions/181596/how-to-convert-a-column-number-eg-127-into-an-excel-column-eg-aa"/>
        public static string CalcExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }
    }
}
