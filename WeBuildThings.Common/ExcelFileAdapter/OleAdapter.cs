using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace WeBuildThings.Excel
{
    public class OleAdapter : IExcelAdapter
    {
        #region Fields
        private FileInfo _File;
        private string _ConnectionString;
        private OleDbConnection _Connection;
        private OleDbDataAdapter _Adapter;
        private string _SelectSQL;
        private string _UpdateSQL;
        private string _InsertSQL;
        private bool _Disposed;
        private DataSet _DataSet;
        #endregion

        #region Properties

        public IEnumerable<Dictionary<string, string>> Rows
        {
            get { return _DataSet.Tables[0].Rows.ToIExcelRow(); }
        }

        public string SelectSQL
        {
            get { return _SelectSQL; }
            set { if (_SelectSQL != value) { _SelectSQL = value; OnSelectSQLChanged(); } }
        }

        public string UpdateSQL
        {
            get { return _UpdateSQL; }
            set { if (_UpdateSQL != value) { _UpdateSQL = value; OnUpdateSQLChanged(); } }
        }

        public string InsertSQL
        {
            get { return _InsertSQL; }
            set { if (_InsertSQL != value) { _InsertSQL = value; OnInsertSQLChanged(); } }
        }

        public OleDbDataAdapter Adapter
        {
            get { return _Adapter; }
            set { if (_Adapter != value) _Adapter = value; }
        }

        public FileInfo File
        {
            get { return _File; }
            set { if (_File != value) { _File = value; OnFileChanged(); } }
        }

        public OleDbConnection Connection
        {
            get { return _Connection; }
            set { if (_Connection != value) { _Connection = value; OnConnectionChanged(); } }
        }

        public string ConnectionString
        {
            get { return _ConnectionString; }
            set { if (_ConnectionString != value) { _ConnectionString = value; OnConnectionStringChanged(); } }
        }

        public int Count
        {
            get { return _DataSet.Tables[0].Rows.ToIExcelRow().Count; }
        }
        #endregion

        #region Constructors

        public OleAdapter(FileInfo file)
        {
            File = file;
        }

        public OleAdapter(FileInfo file, string selectSQL)
        {
            File = file;
            SelectSQL = selectSQL;
        }

        public OleAdapter()
        {
        }
        #endregion

        #region Public Methods
        public string GetValue(string columnName, int rowNumber)
        {
            throw new NotImplementedException("Get Value is not implemented on OleAdapter yet");
        }

        public Dictionary<string,string> GetRow(int rownumber)
        {
            throw new NotImplementedException("Not implemented");
        }
        /*TODO build paramters dynamically
 *     _Adapter.UpdateCommand.Parameters.Add("@StreetAddressLine2", OleDbType.Char, 255).SourceColumn = "STREET ADDRESS LINE 2";
            _Adapter.UpdateCommand.Parameters.Add("@DEPSSN", OleDbType.Char, 255).SourceColumn = "Dep SSN";
        */
        public void AddColumn(string columnName)
        {
            throw new NotImplementedException("The OleAdapter does not support Adding Columns");
        }
        #endregion

        #region Event Methods
        private void OnSelectSQLChanged()
        {
            SetAdapter();
        }

        private void OnUpdateSQLChanged()
        {
            SetAdapter();
        }

        private void OnInsertSQLChanged()
        {
            SetAdapter();
        }

        private void OnConnectionChanged()
        {
            SetAdapter();
        }

        private void OnFileChanged()
        {
            SetConnectionString();
        }

        private void OnConnectionStringChanged()
        {
            SetConnection();
        }
        #endregion

        #region Set Methods
        private void SetAdapter()
        {
            AdapterDispose();
            if (!string.IsNullOrWhiteSpace(SelectSQL) && Connection != null)
            {
                Adapter = new OleDbDataAdapter(SelectSQL, Connection);
                if (!string.IsNullOrWhiteSpace(UpdateSQL))
                {
                    Adapter.UpdateCommand = new OleDbCommand(UpdateSQL, Connection);
                }
                if (!string.IsNullOrWhiteSpace(InsertSQL))
                {
                    Adapter.InsertCommand = new OleDbCommand(InsertSQL, Connection);
                }
                DataSetDispose();
                _DataSet = new DataSet();
                Adapter.Fill(_DataSet);
            }
        }


        private void SetConnection()
        {
            Connection = new OleDbConnection(ConnectionString);
        }

        public void SetValue(string columnName, int rowNumber, string value)
        {
            throw new NotImplementedException("OleAdapter has not and may not implemented SetValue.");
        }

        private void SetConnectionString()
        {
            if (File.FullName.EndsWith("xls"))
            {
                ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", File.FullName);
            }
            else if (File.FullName.EndsWith("csv"))
            {
                ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=\"text;HDR=Yes;FMT=CSVDelimited\"", File.FullName);
            }
            else if (File.FullName.EndsWith("xlsx"))
            {
                ConnectionString = File.FullName.EndsWith("xlsx") ? string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", File.FullName) : null;
            }
            else
            {
                throw new InvalidCastException(string.Format("{0} is not a supported filetype", File.FullName));
            }
        }


        public string[] ListSheets()
        {
            Workbook workbook = null;
            try
            {
                Application ExcelInstance = new Application() { DisplayAlerts = false };
                workbook = ExcelInstance.Workbooks.Open(File.FullName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                string[] result = new string[ExcelInstance.Worksheets.Count];
                int i = 0;
                foreach (Microsoft.Office.Interop.Excel.Worksheet worksheet in ExcelInstance.Worksheets)
                {
                    result[i] = worksheet.Name;
                    i++;
                }
                return result;
            }
            finally
            {
                if (workbook != null)
                {
                    Marshal.ReleaseComObject(workbook);
                    workbook = null;
                }
            }
        }
        #endregion

        #region Dispose
        ~OleAdapter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    AdapterDispose();
                    DataSetDispose();
                    _Disposed = true;
                }
            }
        }

        private void AdapterDispose()
        {
            if (_Adapter != null)
            {
                _Adapter.Dispose();
                _Adapter = null;
            }
        }

        private void DataSetDispose()
        {
            if (_DataSet != null)
            {
                _DataSet.Dispose();
                _DataSet = null;
            }
        }
        #endregion

        #region IExcelAdapter Implementation

        public IEnumerable<string> HeaderRow
        {
            get { throw new NotImplementedException(); }
        }
        #endregion IExcelAdapter Implementation

        string Common.Interfaces.IDelimitedFileAdapter.Delimiter
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }


        public bool EndOfFile
        {
            get { throw new NotImplementedException(); }
        }
    }
}
