﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Common
{
    public enum DateFormats { [Description("N/A")] NA = -1, YYYYMMDD, DaysAsInt, MMDDYYYY, DDMMYYYY,  [Description("MM/DD/YYYY")] MMDDYYYY2, MMDDYY};

    public class DateFormat
    {
        public static DateFormat[] AllFormats = new DateFormat[] { DateFormat.YYYYMMDD, DateFormat.MMDDYYYY, DateFormat.DDMMYYYY, DateFormat.DaysAsInt, DateFormat.MMDDYYYY2, DateFormat.MMDDYY};

        #region Field
        public int ID { get; set; }
        public string Name { get; set; }
        public string Format { get; set; }
        #endregion Fields

        #region Constructors
        public DateFormat(int iD, string name, string format)
        {
            ID = iD;
            Name = name;
            Format = format;
        }

        public DateFormat(DateFormats name)
        {
            var item = AllFormats.Where(n => n.ID == (int)name).FirstOrDefault();
            if (item == null)
            {
                item = DateFormat.MMDDYYYY;
            }
            ID = item.ID;
            Name = item.Name;
            Format = item.Format;
        }

        public DateFormat(string name)
        {
            var item = AllFormats.Where(n => n.Name == name).FirstOrDefault();
            if (item == null)
            {
                item = DateFormat.MMDDYYYY;
            }
            ID = item.ID;
            Name = item.Name;
            Format = item.Format;
        }

        public DateFormat(int iD)
        {
            var item = AllFormats.Where(n => n.ID == iD).FirstOrDefault();
            if (item == null)
            {
                item = DateFormat.MMDDYYYY;
            }
            ID = item.ID;
            Name = item.Name;
            Format = item.Format;
        }
        #endregion Constructors

        #region Operators
        public override bool Equals(Object obj)
        {
            bool result = false;
            DateFormat dateFormat = obj as DateFormat;
            result = (!ReferenceEquals(dateFormat, null) && ID.Equals(dateFormat.ID));
            return result;
        }

        public override int GetHashCode()
        {
            return this.ID.GetHashCode();
        }

        public static bool operator ==(DateFormat x, DateFormat y)
        {
            bool result = false;
            if (ReferenceEquals(x, null) && ReferenceEquals(y, null))
            {
                result = true;
            }
            else if (!ReferenceEquals(x, null) && !ReferenceEquals(y, null))
            {
                result = x.Equals(y);
            }
            return result;
        }

        public static bool operator !=(DateFormat x, DateFormat y)
        {
            return !(x == y);
        }
        #endregion Operators

        #region Overrides
        public override string ToString()
        {
            return Name;
        }

        #endregion Overrides

        public static DateFormat GetDateFormat(DateFormats dateFormats)
        {
            DateFormat result = DateFormat.DaysAsInt;
            switch(dateFormats)
            {
                case DateFormats.DaysAsInt:
                    result = DateFormat.DaysAsInt;
                    break;
                case DateFormats.DDMMYYYY:
                    result = DateFormat.DDMMYYYY;
                    break;
                case DateFormats.MMDDYYYY:
                    result = DateFormat.MMDDYYYY;
                    break;
                case DateFormats.MMDDYYYY2:
                    result = DateFormat.MMDDYYYY2;
                    break;
                case DateFormats.YYYYMMDD:
                    result = DateFormat.YYYYMMDD;
                    break;
                case DateFormats.MMDDYY:
                    result = DateFormat.MMDDYY;
                    break;
                default:
                    throw new ArgumentException(string.Format("No conversion instructions for changeing DateFormats.{0}", dateFormats));
            }
            return result;
        }

        #region Instances
        public static DateFormat YYYYMMDD { get { return new DateFormat(0, "YYYYMMDD", "yyyyMMdd"); } }

        public static DateFormat DaysAsInt { get { return new DateFormat(1, "DaysAsInt", "yyyyMMdd"); } }

        public static DateFormat MMDDYYYY { get { return new DateFormat(2, "MMDDYYYY", "MMddyyyy"); } }

        public static DateFormat DDMMYYYY { get { return new DateFormat(3, "DDMMYYYY", "ddMMyyyy"); } }

        public static DateFormat MMDDYYYY2 { get { return new DateFormat(4, "MM/DD/YYYY", "MM/dd/yyyy"); } }

        public static DateFormat MMDDYY { get { return new DateFormat(5, "MMDDYY", "MMddyy"); } }
        #endregion Instances
    }
}
