﻿using System;

namespace WeBuildThings.Common
{
    public class Timer
    {
        #region Fields
        private DateTime _ReleaseAt;
        #endregion Fields

        #region Constructors
        public Timer() { }

        public Timer(TimeSpan timeSpan)
        {
            SetReleaseAt(timeSpan);
        }

        public Timer(int hours, int minutes, int seconds)
        {
            SetReleaseAt(new TimeSpan(hours, minutes, seconds));
        }

        public Timer (DateTime dateTime)
        {
            ReleaseAt = dateTime;
        }
        #endregion Constructors

        #region Properties
        public DateTime ReleaseAt
        {
            get { return _ReleaseAt; }
            set { if (_ReleaseAt != value) _ReleaseAt = value; }
        }
        #endregion Properties

        #region Set Methods
        private void SetReleaseAt(TimeSpan timeSpan)
        {
            ReleaseAt = DateTime.Now + timeSpan;
        }
        #endregion Set Methods

        #region Public Methods
        public bool Check()
        {
            return (DateTime.Now >= ReleaseAt);
        }
        #endregion Public Methods
    }
}
