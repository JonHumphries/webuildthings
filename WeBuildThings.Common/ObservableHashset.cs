﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;

namespace WeBuildThings.Common
{
    public class ObservableHashset<T> : HashSet<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {
        //ObservableCollection<double> 
        #region Constructors
        public ObservableHashset() : base() { }

        public ObservableHashset(IEnumerable<T> collection) : base(collection) { }

        //
        // Summary:
        //     Initializes a new instance of the System.Collections.Generic.HashSet<T> class
        //     that is empty and uses the specified equality comparer for the set type.
        //
        // Parameters:
        //   comparer:
        //     The System.Collections.Generic.IEqualityComparer<T> implementation to use
        //     when comparing values in the set, or null to use the default System.Collections.Generic.EqualityComparer<T>
        //     implementation for the set type.
        public ObservableHashset(IEqualityComparer<T> comparer) : base(comparer) { }

        //
        // Summary:
        //     Initializes a new instance of the System.Collections.Generic.HashSet<T> class
        //     that uses the specified equality comparer for the set type, contains elements
        //     copied from the specified collection, and has sufficient capacity to accommodate
        //     the number of elements copied.
        //
        // Parameters:
        //   collection:
        //     The collection whose elements are copied to the new set.
        //
        //   comparer:
        //     The System.Collections.Generic.IEqualityComparer<T> implementation to use
        //     when comparing values in the set, or null to use the default System.Collections.Generic.EqualityComparer<T>
        //     implementation for the set type.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     collection is null.
        public ObservableHashset(IEnumerable<T> collection, IEqualityComparer<T> comparer) : base(collection, comparer) { }

        //
        // Summary:
        //     Initializes a new instance of the System.Collections.Generic.HashSet<T> class
        //     with serialized data.
        //
        // Parameters:
        //   info:
        //     A System.Runtime.Serialization.SerializationInfo object that contains the
        //     information required to serialize the System.Collections.Generic.HashSet<T>
        //     object.
        //
        //   context:
        //     A System.Runtime.Serialization.StreamingContext structure that contains the
        //     source and destination of the serialized stream associated with the System.Collections.Generic.HashSet<T>
        //     object.
        protected ObservableHashset(SerializationInfo info, StreamingContext context) : base(info, context) { }
        #endregion Constructors

        #region INotifyCollectionChanged Implementation

        public event NotifyCollectionChangedEventHandler CollectionChanged;

        #endregion INotifyCollectionChange Implementation

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion INotifyPropertyChanged Implementation

        #region Override methods
        // Summary:
        //     Adds the specified element to a set.
        //
        // Parameters:
        //   item:
        //     The element to add to the set.
        //
        // Returns:
        //     true if the element is added to the System.Collections.Generic.HashSet<T>
        //     object; false if the element is already present.   
        public new bool Add(T item)
        {
            bool result = base.Add(item);
            if (result)
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, item, null));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
            return result;
        }

        //
        // Summary:
        //     Modifies the current System.Collections.Generic.HashSet<T> object to contain
        //     only elements that are present in that object and in the specified collection.
        //
        // Parameters:
        //   other:
        //     The collection to compare to the current System.Collections.Generic.HashSet<T>
        //     object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     other is null.
        public new void IntersectWith(IEnumerable<T> other)
        {
            base.IntersectWith(other);
            IList<T> remainingItems = this.ToList();
            if (remainingItems.Any())
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, remainingItems));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
        }

        //
        // Summary:
        //     Removes all elements from a System.Collections.Generic.HashSet<T> object.      
        public new void Clear()
        {
            IList<T> changedItems = this.ToList();
            if (changedItems.Any())
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset, changedItems));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
        }

        //
        // Summary:
        //     Removes the specified element from a System.Collections.Generic.HashSet<T>
        //     object.
        //
        // Parameters:
        //   item:
        //     The element to remove.
        //
        // Returns:
        //     true if the element is successfully found and removed; otherwise, false.
        //     This method returns false if item is not found in the System.Collections.Generic.HashSet<T>
        //     object.
        public new bool Remove(T item)
        {
            bool result = base.Remove(item);
            if (result)
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, item, null));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
            return result;
        }

         //
        // Summary:
        //     Removes all elements in the specified collection from the current System.Collections.Generic.HashSet<T>
        //     object.
        //
        // Parameters:
        //   other:
        //     The collection of items to remove from the System.Collections.Generic.HashSet<T>
        //     object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     other is null.
        public new void ExceptWith(IEnumerable<T> other)
        {
            IList<T> removedItems = this.Where(n => other.Contains(n)).ToList();
            base.ExceptWith(other);
            if (removedItems != null && removedItems.Any())
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItems));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
        }

        //
        // Summary:
        //     Removes all elements that match the conditions defined by the specified predicate
        //     from a System.Collections.Generic.HashSet<T> collection.
        //
        // Parameters:
        //   match:
        //     The System.Predicate<T> delegate that defines the conditions of the elements
        //     to remove.
        //
        // Returns:
        //     The number of elements that were removed from the System.Collections.Generic.HashSet<T>
        //     collection.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     match is null.
        public new int RemoveWhere(Predicate<T> match)
        {
            IList<T> removedItems = Array.FindAll(this.ToArray(), match).ToList();
            var result = base.RemoveWhere(match);
            if (removedItems != null && removedItems.Any())
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove, removedItems));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
            return result; 
        }

        //
        // Summary:
        //     Modifies the current System.Collections.Generic.HashSet<T> object to contain
        //     all elements that are present in itself, the specified collection, or both.
        //
        // Parameters:
        //   other:
        //     The collection to compare to the current System.Collections.Generic.HashSet<T>
        //     object.
        //
        // Exceptions:
        //   System.ArgumentNullException:
        //     other is null.      
        public new void UnionWith(IEnumerable<T> other)
        {
            base.UnionWith(other);
            IList<T> itemsAdded = other.ToList();
            if (itemsAdded.Any())
            {
                InvokeCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add, itemsAdded, null));
                InvokePropertyChanged(new PropertyChangedEventArgs("Count"));
            }
        }
        #endregion Override Methods

        #region Invoke Methods
        protected virtual void InvokeCollectionChanged(NotifyCollectionChangedEventArgs e)
        {
            var handler = CollectionChanged;
            if (handler != null) handler.Invoke(this, e);
        }

        protected virtual void InvokePropertyChanged(PropertyChangedEventArgs e)
        {
            var handler = PropertyChanged;
            if (handler != null) handler.Invoke(this, e);
        }
        #endregion Invoke Methods
    }
}
