﻿using System;

namespace WeBuildThings.Common.Serialization
{
    public interface ISerialize
    {
        Type SerializedType { get; }
        object ChangeType(Type type, object convertedResult);
    }

    public static class ISerializeExtensions
    {
        public static void Export(this ISerialize originalObject, string filePath)
        {
            object obj = originalObject.ChangeType(originalObject.SerializedType, null);
            if (obj == null)
            {
                throw new Exception(string.Format("Failed to Serialize {0}", originalObject));
            }
            XMLHelper.ExportToXML(filePath, obj);
        }

        public static void Export(this ISerialize originalObject, string filePath,  object[] objects)
        {
            var serializedObjects = new SerializedCollection();
            serializedObjects.TypeFullName = originalObject.SerializedType.AssemblyQualifiedName;
            for (int i = 0; i < objects.Length; i++)
            {
                serializedObjects.SerializedItems.Add(((ISerialize)objects[i]).ChangeType(originalObject.SerializedType, null));
            }
            XMLHelper.ExportToXML(filePath, serializedObjects);
        }
    }
}
