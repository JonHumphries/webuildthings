﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Exceptions;
using WeBuildThings.Exceptions.Extensions;
using WeBuildThings.Utilities;

namespace WeBuildThings.Common.Serialization
{
    public class XMLHelper
    {
        #region Fields
        private string _Path;
        private bool _ExceptionOnFailure;
        #endregion Fields

        #region Properties
        public bool ExceptionOnFailure
        {
            get { return _ExceptionOnFailure; }
            set { _ExceptionOnFailure = value; }
        }

        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }
        #endregion Properties

        #region Constructors
        public XMLHelper()
        {
            ExceptionOnFailure = true;
        }
        #endregion Constructors

        #region Public Methods

        public static void ExportToXML(string path, object obj)
        {
            // use reflection to get all derived types
            var knownTypes = CreateKnownTypesList(obj);
            try
            {
                System.Xml.Serialization.XmlSerializer writer = new System.Xml.Serialization.XmlSerializer(obj.GetType(), knownTypes.ToArray());
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(path))
                {
                    writer.Serialize(file, obj);
                    file.Close();
                }
            }
            catch (InvalidOperationException exception)
            {
                string innerExceptionMessage = exception.InnerExceptionMessages();
                string message = string.Format("Unable to Serialize {0}{1}{2}", obj.GetType(), Environment.NewLine, innerExceptionMessage);
                throw new SerializationException(message, obj.GetType(), exception);
            }
        }
        
        public static List<Type> CreateKnownTypesList(object obj)
        {
            List<Type> result = new List<Type>();
            foreach (var item in obj.GetType().GetAllReferencedAndAssignableTypes(new List<Type>()).Where(n => !n.IsInterface))
            {
                result.Add(item);
            }
            if (obj.GetType() == typeof(SerializedCollection))
            {
                var col = (SerializedCollection)obj;
                Type type = col.SerializedItems[0].GetType();
                var currentList = type.GetAllReferencedAndAssignableTypes(result).Where(n => !n.IsInterface).ToList();
                foreach (var item in currentList)
                {
                    result.Add(item);
                }
            }
            return result;
        }
        
        public object ImportFromXML(string path, Type type)
        {
            return ImportFromXML(path, type, true);
        }

        public object ImportFromXML(string path, Type type, bool throwExceptions)
        {
            HashSet<Type> knownTypes = new HashSet<Type>();
            foreach (var item in type.GetAllReferencedAndAssignableTypes(new List<Type>()).Where(n => !n.IsInterface))
            {
                knownTypes.Add(item);
            }
            System.Xml.Serialization.XmlSerializer reader;
            if (ValidateSerializedCollection(path))
            {

                if (!knownTypes.Contains(typeof(SerializedCollection)))
                {
                    knownTypes.Add(typeof(SerializedCollection));
                }
                XmlRootAttribute rootAttribute = new XmlRootAttribute();
                rootAttribute.DataType = typeof(SerializedCollection).FullName;
                rootAttribute.ElementName = typeof(SerializedCollection).Name;
                //XmlAttributeOverrides attributeOverrides = new XmlAttributeOverrides();
                reader = new System.Xml.Serialization.XmlSerializer(typeof(SerializedCollection), null, knownTypes.ToArray(), rootAttribute, null);
            }
            else
            {
                reader = new XmlSerializer(type, knownTypes.ToArray());
            }

            using (System.IO.StreamReader file = new System.IO.StreamReader(path))
            {
                if (throwExceptions)
                {
                    reader.UnreferencedObject -= OnUnreferencedObject;
                    reader.UnreferencedObject += OnUnreferencedObject;
                    reader.UnknownNode -= OnUnknownNode;
                    reader.UnknownNode += OnUnknownNode;
                }
                return reader.Deserialize(file);
            }
        }

        public static bool ValidateSerializedCollection(string filePath)
        {
            return FileOperations.FileContains(filePath, SerializedCollection.XMLContains);
        }

        public static object ImportFromXML(XmlDocument document, Type type)
        {
            var knownTypes = type.GetAllReferencedAndAssignableTypes(new List<Type>()).Where(n => !n.IsInterface);
            System.Xml.Serialization.XmlSerializer reader = new System.Xml.Serialization.XmlSerializer(type, knownTypes.ToArray());
            Uri uri = new Uri(document.BaseURI);
            System.IO.StreamReader file = new System.IO.StreamReader(uri.LocalPath);
            return reader.Deserialize(file);
        }

        public void OnUnreferencedObject(object sender, UnreferencedObjectEventArgs e)
        {
            if (ExceptionOnFailure) throw new UnreferencedObjectException(e);
        }

        public void OnUnknownNode(object sender, XmlNodeEventArgs e)
        {
            if (ExceptionOnFailure) throw new UnknownNodeException(e);
        }

        #endregion Public Methods

    }
     

    public class SerializedCollection
    {
        public const string XMLContains = "<SerializedCollection xmlns";

        public string TypeFullName { get; set; }
        public List<object> SerializedItems { get; set; }

        public SerializedCollection() { SerializedItems = new List<object>(); }

        public SerializedCollection(Type type, object[] objects)
        {
            TypeFullName = type.FullName;
            SerializedItems = new List<object>();
            foreach (var item in objects)
            {
                SerializedItems.Add(item);
            }
        }
    }
}
