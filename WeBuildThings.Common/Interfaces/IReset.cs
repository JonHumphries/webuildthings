﻿using System;
using System.Linq;
using System.Reflection;

namespace WeBuildThings.Common.Interfaces
{
    /// <summary>
    /// IReset exposes the Reset extension method which allows you to return an object 
    /// to initial values.
    /// </summary>
    public interface IReset
    {
        /// <summary>
        /// Reinitialize will be called once the Reset method has completed 
        /// allowing you to override default values once an object has been reset.
        /// </summary>
        void ReInitialize();
    }

    public static class IResetExtensions
    {
        /// <summary>
        /// Returns an object to initial values and calls the reinitialize method
        /// </summary>
        public static void Reset(this IReset value)
        {
            Type type = value.GetType();
            var instance = Activator.CreateInstance(type);
            PropertyInfo[] properties = type.GetProperties();
            for (int i = 0; i < properties.Count(); ++i)
                properties[i].SetValue(value, properties[i].GetValue(instance));
            value.ReInitialize();
        }
    }
}
