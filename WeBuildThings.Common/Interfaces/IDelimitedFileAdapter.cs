﻿using System;
using System.Collections.Generic;
using System.IO;

namespace WeBuildThings.Common.Interfaces
{
    public interface IDelimitedFileAdapter: IDisposable
    {
        FileInfo File { get; set; }
        string Delimiter { get; set; }
        int Count { get; }
        bool EndOfFile { get; }
        IEnumerable<string> HeaderRow { get; }
        IEnumerable<Dictionary<string, string>> Rows { get; }
    }
}
