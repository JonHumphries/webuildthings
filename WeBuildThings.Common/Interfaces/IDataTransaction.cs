﻿using System;

namespace WeBuildThings.Common.Interfaces
{
    public enum TransactionStatus { Error = -1, New, Loaded, InProcess, Processed };

    public interface IDataTransaction
    {
        DateTime DateCreated { get; set; }
        string TransactionSource { get; set; }
        TransactionStatus Status { get; set; }
        void Execute();
    }
}
