﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Interfaces
{
    public interface IMergeable
    {
        void Merge(IMergeable source);
    }

    public static class IMergeableExtensions
    {
        public static void Merge(this IMergeable source, IList targets)
        {
            foreach (IMergeable target in targets)
            {
                if (target != source)
                {
                    source.Merge(target);
                }
            }
        }
    }
}
