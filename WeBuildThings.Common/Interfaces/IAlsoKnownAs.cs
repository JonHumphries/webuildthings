﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Interfaces
{
    public interface IAlsoKnownAs
    {
        string Name { get; }
        IEnumerable<string> AKAList { get; }
        void AddAKA(string newAKA);
    }

    public static class IAlsoKnownAsExtensions
    {
        #region Consts
        private const int CONTAINS_MINIMUM_LENGTH = 4;
        private const int FUZZY_STRING_COMPARE_LENGTH = 7;
        #endregion Consts

        private static List<string> Keys;

        public static bool Match(this IAlsoKnownAs value, string matchValue)
        {
            return Match(value, matchValue, false);
        }

        public static bool Match(this IAlsoKnownAs value, string matchValue, bool caseSensitive)
        {
            bool result = false;
            Keys = BuildKeys(value.Name, value.AKAList);
            RegexOptions options = (caseSensitive) ? RegexOptions.None : RegexOptions.IgnoreCase;
            foreach (string key in Keys)
            {
                if (KeyValueMatch(key, matchValue, options))
                {
                    result = true;
                    break;
                }
            }
            return result;
        }

        private static List<string> BuildKeys(string primaryKey, IEnumerable<string> alternateKeys)
        {
            List<string> result = new List<string>();
            if (!string.IsNullOrWhiteSpace(primaryKey))
            {
                result.Add(primaryKey.Trim());
            }
            var filteredAlternateKeys = alternateKeys.Where(n => !string.IsNullOrWhiteSpace(n));
            if (filteredAlternateKeys != null && filteredAlternateKeys.Any())
            {
                result.AddRange(filteredAlternateKeys.Trim());
            }
            return result;
        }

        private static bool KeyValueMatch(string key, string matchValue, RegexOptions options)
        {
            bool result = false;
            string trimmedMatchValue = matchValue.Trim();
            foreach (string name in key.CreateFuzzyList())
            {
                if (trimmedMatchValue.Contains(key))
                {
                    result = true;
                    break;
                }
                else
                {
                    Regex regex = new Regex(string.Format(@".*{0}.*", name), RegexOptions.IgnoreCase);
                    Match match = regex.Match(matchValue);
                    if (match.Success)
                    {
                        result = true;
                        break;
                    }
                }
            }
            return result;
        }

        private static bool FuzzyStringCompare(string lhs, string rhs)
        {
            string mylhs = lhs.Trim().ToLower();
            string myrhs = rhs.Trim().ToLower();
            bool result = (mylhs == myrhs);
            if (!result && myrhs.Length >= CONTAINS_MINIMUM_LENGTH)
            {
                result = (mylhs.Contains(myrhs));
            }
            if (!result && mylhs.Length >= CONTAINS_MINIMUM_LENGTH)
            {
                result = (myrhs.Contains(mylhs));
            }
            return result;
        }

        /// <summary>
        /// Searches the persisted list of alternate spellings for a match.
        /// </summary>
        /// <typeparam name="T">The type of object in the list.  Must inherit IAlsoKnownAs</typeparam>
        /// <param name="list">The list of AKA objects to be searched</param>
        /// <param name="matchTo">The value being searched for</param>
        /// <param name="learnMatches">If true then AddAka will be called to update the list</param> 
        /// <returns>Returns the AKA value which should reference it's parent</returns>
        public static T FuzzySearch<T>(this IEnumerable<T> list, string matchTo, bool learnMatches) where T : IAlsoKnownAs
        {
            T result = default(T);
            result = list.Where(n => !string.IsNullOrWhiteSpace(n.Name) && n.Name.ToLower() == matchTo.ToLower() || n.AKAList.Where(x => x.ToLower() == matchTo.ToLower()).Any()).FirstOrDefault();
            bool useReverseSearch = matchTo.Length > 6 || matchTo.Split(' ').Length > 1;
            if (result == null)
            {
                foreach (var item in list)
                {
                    result = FuzzyListExactMatch<T>(item, matchTo, learnMatches);
                    if (result == null && useReverseSearch)
                    {
                        result = FuzzyListExactMatch<T>(matchTo, learnMatches, item);
                    }
                    if (result == null && useReverseSearch)
                    {
                        result = FuzzyListStringContainsMatch<T>(item, matchTo, learnMatches);
                        if (result == null)
                        {
                            result = FuzzyListStringContainsMatch<T>(matchTo, learnMatches, item);
                        }
                    }
                    if (result != null) break;
                }
            }
            return result;
        }

        private static IEnumerable<string> StrongFuzzyList(this string str)
        {
            return str.CreateFuzzyList().Where(n => n.Length >= FUZZY_STRING_COMPARE_LENGTH || n.Split(' ').Length > 1);
        }

        private static T FuzzyListStringContainsMatch<T>(string matchTo, bool learnMatches, T item) where T : IAlsoKnownAs
        {
            T result = default(T);
            var list = matchTo.StrongFuzzyList();
            foreach (var listItem in list)
            {
                if (FuzzyStringCompare(listItem, item.Name))
                {
                    result = item;
                    if (learnMatches) item.AddAKA(matchTo);
                    break;
                }
            }
            if (result == null)
            {
                foreach (var knownAs in item.AKAList.Where(n => !string.IsNullOrWhiteSpace(n)))
                {
                    if (matchTo.StrongFuzzyList().Contains(knownAs))
                    {
                        result = item;
                        if (learnMatches) item.AddAKA(matchTo);
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Checks to see if the each string in the fuzzy list contains the matchTo value 
        /// </summary>
        private static T FuzzyListStringContainsMatch<T>(T item, string matchTo, bool learnMatches) where T : IAlsoKnownAs
        {
            T result = default(T);
            var list = item.Name.StrongFuzzyList();
            result = FuzzyListCompare<T>(item, matchTo, learnMatches, list);
            if (result == null)
            {
                foreach (var knownAs in item.AKAList.Where(n => !string.IsNullOrWhiteSpace(n)))
                {
                    try
                    {
                        list = knownAs.StrongFuzzyList();
                        result = FuzzyListCompare<T>(item, matchTo, learnMatches, list);
                        if (result != null) break;
                    }
                    catch (Exception exception)
                    {
                        string message = string.Format("Failure searching processing {0} - {1}", item, knownAs);
                        throw new Exception(message, exception);
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Performs a Fuzzy String Compare on each item in the list and returns the first match
        /// </summary>
        private static T FuzzyListCompare<T>(T item, string matchTo, bool learnMatches, IEnumerable<string> list) where T : IAlsoKnownAs
        {
            T result = default(T);
            foreach (var fuzzyItem in list)
            {
                if (FuzzyStringCompare(fuzzyItem, matchTo))
                {
                    result = item;
                    if (learnMatches) item.AddAKA(matchTo);
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Checks to see if MatchTo's fuzzy list contains the item
        /// </summary>
        private static T FuzzyListExactMatch<T>(string matchTo, bool learnMatches, T item) where T : IAlsoKnownAs
        {
            T result = default(T);
            if (matchTo.CreateFuzzyList().Contains(item.Name))
            {
                result = item;
                if (learnMatches) item.AddAKA(matchTo);
            }
            else
            {
                foreach (var knownAs in item.AKAList.Where(n => !string.IsNullOrWhiteSpace(n)))
                {
                    if (matchTo.CreateFuzzyList().Contains(knownAs))
                    {
                        result = item;
                        if (learnMatches) item.AddAKA(matchTo);
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// Checks to see if the item's fuzzy list contains the match to
        /// </summary>
        private static T FuzzyListExactMatch<T>(T item, string matchTo, bool learnMatches) where T : IAlsoKnownAs
        {
            T result = default(T);
            if (!string.IsNullOrWhiteSpace(item.Name) && item.Name.CreateFuzzyList().Contains(matchTo))
            {
                result = item;
                if (learnMatches) item.AddAKA(matchTo);
            }
            else
            {
                foreach (var knownAs in item.AKAList.Where(n => !string.IsNullOrWhiteSpace(n)))
                {
                    if (knownAs.CreateFuzzyList().Contains(matchTo))
                    {
                        result = item;
                        if (learnMatches) item.AddAKA(matchTo);
                        break;
                    }
                }
            }
            return result;
        }
    }
}
