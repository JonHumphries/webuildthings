﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.Common
{
    public interface IUniqueId
    {
        int Id { get; set; }
        IEnumerable<int> Collection { get; }
    }

    public static class IUniqueIdExtensions
    {
        public static int NextId(this IUniqueId uniqueId)
        {
            return NextId(uniqueId.Collection);
        }

        public static int NextId(IEnumerable<int> collection)
        {
            return (collection != null && collection.Any()) ? collection.Max() + 1 : 1;
        }
    }
}
