﻿using DevExpress.ExpressApp;
using System;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.DataMergeProcess.DatabaseUpdate;

namespace WeBuildThings.XAF.BusinessRuleEngine.DatabaseUpdate
{
    public static class BusinessRuleRoleUpdater
    {
        #region Constants
        public const string BusinessRuleCreatorRoleName = "Business Rule Creator";
        public const string BusinessRuleCreatorRoleDescription = "This role grants full access to business rules and tactics.  Use this for users who are familiar with creating/modifying business rules and tactics.";
        #endregion Constants

        #region Type Definitions
        private static Type[] DefaultReadNavigatePermissions = new Type[] { typeof(BasicTactic), typeof(ReferenceTactic), typeof(Tactic), typeof(TacticSet), typeof(BusinessRule), typeof(BusinessRuleSet)};
        private static Type[] BusinessRuleCreatorFullPermissions = new Type[] { typeof(BasicTactic), typeof(ReferenceTactic), typeof(Tactic), typeof(TacticSet), typeof(BusinessRule), typeof(BusinessRuleSet)};
        #endregion Type Definitions

        public static void Update(IObjectSpace objectSpace)
        {
            DefaultRoleModification(objectSpace);
            CreateBusinessRuleCreatorRole(objectSpace);
            objectSpace.CommitChanges();
        }

        private static void DefaultRoleModification(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, XUserUpdater.DEFAULT_ROLE_NAME);
            XUserUpdater.AddReadNavigatePermissionsToTypes(objectSpace, result, DefaultReadNavigatePermissions);
        }

        private static void CreateBusinessRuleCreatorRole(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, BusinessRuleCreatorRoleName);
            result.Description = BusinessRuleCreatorRoleDescription;
            XUserUpdater.AddFullAccessToTypes(objectSpace, result, BusinessRuleCreatorFullPermissions);
        }
    }
}
