﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using System;
using System.Xml.Serialization;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects
{
    public class SerializedRule : ISerializeToXaf
    {
        #region Fields
        private string _Name;
        private string _RuleSetName;
        private int _SortOrder;
        private int _Number;
        private string _OutlineNumber;
        private string _RuleCriteria;
        private bool _Active;
        private int _ParentRuleSortOrder;
        private SerializedTacticSet _Response;
        private Session _Session;
        #endregion Fields

        #region Constructors
        public SerializedRule() { }
        #endregion Constructors

        #region Properties
        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        [XmlIgnore]
        public string RuleSetName
        {
            get { return _RuleSetName; }
            set { if (_RuleSetName != value) _RuleSetName = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { if (_Name != value) _Name = value; }
        }

        public bool Active
        {
            get { return _Active; }
            set { if (_Active != value) _Active = value; }
        }

        public int SortOrder
        {
            get { return _SortOrder; }
            set { if (_SortOrder != value) _SortOrder = value; }
        }

        public int Number
        {
            get { return _Number; }
            set { if (_Number != value) _Number = value; }
        }

        public string OutlineNumber
        {
            get { return _OutlineNumber; }
            set { if (_OutlineNumber != value) _OutlineNumber = value; }
        }

        public string RuleCriteria
        {
            get { return _RuleCriteria; }
            set { if (_RuleCriteria != value) _RuleCriteria = value; }
        }

        public int ParentRuleSortOrder
        {
            get { return _ParentRuleSortOrder; }
            set { if (_ParentRuleSortOrder != value) _ParentRuleSortOrder = value; }
        }

        public SerializedTacticSet Response
        {
            get { return _Response; }
            set { if (_Response != value) _Response = value; }
        }
        #endregion Properties

        #region ISerialize Implementation
        public virtual Type SerializedType
        {
            get { return typeof(BusinessRule); }
        }

        public virtual object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new GroupOperator(GroupOperatorType.And, new BinaryOperator("RuleSet.Name", RuleSetName), new BinaryOperator("OutlineNumber", OutlineNumber)));
                if (convertedResult == null)
                {
                    convertedResult = SerializedType.CreateInstance(Session);
                }
            }
            var result = convertedResult as BusinessRule;
            result.Name = Name;
            result.Number = Number;
            result.OutlineNumber = OutlineNumber;
            result.Active = this.Active;
            if (Response != null)
            {
                Response.Session = Session;
                result.TacticSet = (TacticSet)Response.ChangeType(Response.SerializedType, null);
            }
            if (result.RuleCriteria == null)
            {
                result.RuleCriteria = new XAF.Common.Utilities.FilterCriteria(Session);
            }
            result.RuleCriteria.Criterion = RuleCriteria;
            result.SortOrder = SortOrder;
            //if (ParentRuleOutlineNumber != null)
            //{
            //    ParentRuleOutlineNumber.Session = Session;
            //    convertedResult.ParentRule = (BusinessRule)ParentRuleOutlineNumber.ChangeType();
            //}
            return result;
        }
        #endregion ISerialize Implementation
    }
}
