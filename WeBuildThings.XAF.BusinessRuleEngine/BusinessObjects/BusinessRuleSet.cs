﻿using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.XAF.Common.Utilities;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    [DefaultClassOptions]
    public class BusinessRuleSet : BaseObject, ISerialize
    {
        #region Constants
        [Browsable(false)]
        public Type SerializedType { get { return typeof(SerializedRuleSet); } }
        #endregion Constants

        #region Fields
        private string _Name;
        private PersistedType _TargetObject;
        private TacticSet _DefaultTactic;
        private string _PropertyToGroupOn;
        private FilterCriteria _DefaultCriteria;
        private IEnumerable<BusinessRule> _MatchingRules;
        #endregion Fields

        #region Constructors
        public BusinessRuleSet(Session session)
            : base(session)
        { }

        internal BusinessRuleSet() : base() { }

        public override void AfterConstruction()
        {
            DefaultCriteria = new FilterCriteria(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public PersistedType TargetObject
        {
            get { return _TargetObject; }
            set { if (SetPropertyValue("TargetObject", ref _TargetObject, value) && !IsLoading) { OnTargetObjectChanged(); } }
        }

        public string PropertyToGroupOn
        {
            get { return _PropertyToGroupOn; }
            set { SetPropertyValue("PropertyToGroupOn", ref _PropertyToGroupOn, value); }
        }

        [Association("RuleSet-Rule", typeof(BusinessRule)), Aggregated]
        public XPCollection<BusinessRule> Rules
        {
            get { return GetCollection<BusinessRule>("Rules"); }
        }

        public TacticSet DefaultTactic
        {
            get { return _DefaultTactic; }
            set { SetPropertyValue("DefaultResponse", ref _DefaultTactic, value); }
        }

        [Aggregated]
        public FilterCriteria DefaultCriteria
        {
            get { return _DefaultCriteria; }
            set { SetPropertyValue("DefaultCriteria", ref _DefaultCriteria, value); }
        }

        [Association("BusinessRuleSet-ExecutionLog", typeof(ExecutionLog)), Aggregated]
        public XPCollection<ExecutionLog> ExecutionLog
        {
            get { return GetCollection<ExecutionLog>("ExecutionLog"); }
        }
        #endregion Properties

        #region Validation Methods
        public bool ValidateTargetType(bool exception, Type targetType)
        {
            bool result = true;
            if (TargetObject != null)
            {
                result = TargetObject.ValidateType(exception, targetType);
            }
            return result;
        }
        #endregion Validation Methods

        #region Event Methods
        private void OnTargetObjectChanged()
        {
            SetRuleTargetObject();
            SetDefaultCriteriaTargetObject();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetRuleTargetObject()
        {
            foreach (var rule in Rules)
            {
                rule.SetRuleCriteriaFilterObject();
            }
        }

        private void SetDefaultCriteriaTargetObject()
        {
            DefaultCriteria.FilterObject = TargetObject;
        }
        #endregion Set Methods

        public bool Match(IObjectSpace objectSpace, BaseObject targetObject, ExecutionLog executionLog)
        {
            return (GetMatchingRule(objectSpace, targetObject, executionLog) != null);
        }

        private IEnumerable<BusinessRule> MatchingRules
        {
            get
            {
                if (_MatchingRules == null || _MatchingRules.Count() == 0)
                {
                    _MatchingRules = Rules.Where(n => n.Active == true && n.TacticSet != null && (n.ChildRules == null || !n.ChildRules.Any())).OrderBy(n => n.SortOrder).ToList();
                }
                return _MatchingRules;
            }
        }

        public BusinessRule GetMatchingRule(IObjectSpace objectSpace, BaseObject targetObject, ExecutionLog executionLog)
        {
            BusinessRule result = null;
            foreach (BusinessRule rule in MatchingRules)
            {
                if (rule.CheckCriteria(objectSpace, targetObject))
                {
                    result = rule;
                    executionLog.AddMessage("For {0} best rule is: {1}", targetObject, rule);
                    break;
                }
            }
            return result;
        }

        public void Execute(IObjectSpace objectSpace, BaseObject targetObject)
        {
            var executionLog = CreateExecutionLog("Execute(objectSpace, targetObject");
            if (!string.IsNullOrWhiteSpace(PropertyToGroupOn))
            {
                var exception = new ArgumentException("This Rule Set has grouping enabled and the Execute method called cannot process a collection.");
                exception.Source = this.GetType().Name;
                executionLog.AddMessage(exception.Message);
                executionLog.Save();
                throw exception;
            }
            var rule = GetMatchingRule(objectSpace, targetObject, executionLog);
            if (rule != null)
            {
                rule.TacticSet.Execute(rule, targetObject, executionLog);
            }
            else
            {
                DefaultTactic.Execute(targetObject, executionLog);
            }
            executionLog.Save();
        }

        public void Execute(IObjectSpace objectSpace, XPCollection targetObjects)
        {
            var executionLog = CreateExecutionLog("Execute(objectSpace, targetObjects)");
            targetObjects.Filter = DefaultCriteria.GetOperator();
            if (!string.IsNullOrWhiteSpace(PropertyToGroupOn))
            {
                ExecuteRulesOnGroup(objectSpace, targetObjects, executionLog);
            }
            else
            {
                ExecuteRulesIndividually(objectSpace, targetObjects, executionLog);
            }
            executionLog.Save();
        }


        private ExecutionLog CreateExecutionLog(string message)
        {
            ExecutionLog executionLog = new ExecutionLog(Session);
            executionLog.BusinessRuleSet = this;
            executionLog.Description = string.Format("Biz Rule Set {0} started at {1} via {2}", Name, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"), message);
            return executionLog;
        }

        private void ExecuteRulesIndividually(IObjectSpace objectSpace, XPCollection targetObjects, ExecutionLog executionLog)
        {
            int i = 0;
            int max = targetObjects.Count;
            executionLog.AddMessage("{0} objects in collection for comparison", max);
            var list = targetObjects.ToHashSet();
            if (list == null || !list.Any() || (list.Count() == 1 && list.First() == null))
            {
                executionLog.AddMessage("no objects that can be converted to base object meet the specified criteria");
            }
            foreach (BaseObject value in list)
            {
                var rule = GetMatchingRule(objectSpace, value, executionLog);
                if (rule != null)
                {
                    rule.TacticSet.Execute(rule, value, executionLog);
                    value.Save();
                }
                i++;
            }
        }

        private void ExecuteRulesOnGroup(IObjectSpace objectSpace, XPCollection targetObjects, ExecutionLog executionLog)
        {
            var values = BuildGroupCollections(targetObjects);
            executionLog.AddMessage("{0} objects in collection for comparison", values.Count());
            foreach (var value in values)
            {
                var rule = GetMatchingRule(objectSpace, value.BaseObject, executionLog);
                if (rule != null)
                {
                    value.BestRule = rule;
                }
            }

            var collectionValues = values.GroupBy(n => n.GroupedValue, g => g.BestRule, (key, g) => new { GroupedValue = key, Rules = g.ToList() });
            var results = collectionValues.Where(n => n.Rules != null && n.Rules.Any());
            int i = 0;
            int max = results.Count();
            executionLog.AddMessage("Best rules identified, processing tactics");
            foreach (var item in results)
            {
                var rule = item.Rules.Where(n => n != null).OrderBy(n => n.SortOrder).FirstOrDefault();
                if (rule != null)
                {
                    var matchingItem = values.Where(n => n.GroupedValue == item.GroupedValue && n.BestRule == rule).FirstOrDefault();
                    rule.TacticSet.Execute(rule, matchingItem.BaseObject, executionLog);
                }
                else if (DefaultTactic != null)
                {
                    var sampleItem = values.Where(n => n.GroupedValue == item.GroupedValue).FirstOrDefault();
                    DefaultTactic.Execute(sampleItem.BaseObject, executionLog);
                }
                i++;
            }
            if (results == null)
            {
                var sampleItem = values.FirstOrDefault();
                DefaultTactic.Execute(sampleItem.BaseObject, executionLog);
            }
        }

        private IEnumerable<GroupItem> BuildGroupCollections(XPCollection targetObjects)
        {
            HashSet<GroupItem> result = new HashSet<GroupItem>();
            foreach (BaseObject baseObject in targetObjects)
            {
                result.Add(new GroupItem(PropertyToGroupOn) { BaseObject = baseObject });
            }
            return result;
        }

        private class GroupItem
        {

            private BaseObject _BaseObject;
            private string PropertyToGroupOn;

            public object GroupedValue { get; set; }

            public BaseObject BaseObject
            {
                get { return _BaseObject; }
                set { if (_BaseObject != value) { _BaseObject = value; OnBaseObjectChanged(); } }
            }
            public BusinessRule BestRule { get; set; }

            private void OnBaseObjectChanged()
            {
                GroupedValue = WeBuildThings.Common.Extensions.TypeExtensions.GetProperty(BaseObject, PropertyToGroupOn);
            }

            public GroupItem(string propertyToGroupOn)
            {
                PropertyToGroupOn = propertyToGroupOn;
            }
        }

        #region ISerialize Implementation
        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedRuleSet;
            result.Name = this.Name;
            result.PropertyToGroupOn = this.PropertyToGroupOn;
            if (this.TargetObject != null)
            {
                result.TargetObject = this.TargetObject.FullName;
            }
            if (DefaultTactic != null)
            {
                result.DefaultResponse = (SerializedTacticSet)this.DefaultTactic.ChangeType(DefaultTactic.SerializedType, null);
            }
            result.DefaultCriteria = this.DefaultCriteria.CriterionText;
            if (this.Rules != null && this.Rules.Count > 0)
            {
                var ruleList = Rules.OrderBy(n => n.SortOrder);
                foreach (var rule in ruleList)
                {
                    var serializedRule = (SerializedRule)rule.ChangeType(rule.SerializedType, null);
                    result.SerializedRules.Add(serializedRule);
                }
            }
            return result;
        }

        #endregion ISerialize Implementation
    }
}
