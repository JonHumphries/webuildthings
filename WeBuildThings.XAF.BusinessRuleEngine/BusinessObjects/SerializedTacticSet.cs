﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects
{
    public class SerializedTacticSet : ISerializeToXaf
    {
        #region Fields
        private string _Name;
        private HashSet<SerializedTactic> _Responses;
        private Session _Session;
        #endregion Fields

        #region Constructors
        public SerializedTacticSet()
        {
            Responses = new HashSet<SerializedTactic>();
        }
        #endregion Constructors

        #region Properties
        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        public string Name
        {
            get { return _Name; }
            set { if (_Name != value) _Name = value; }
        }

        public HashSet<SerializedTactic> Responses
        {
            get { return _Responses; }
            set { if (_Responses != value) _Responses = value; }
        }

        public Type SerializedType
        {
            get { return typeof(TacticSet); }
        }
        #endregion Properties

        #region ISerialize Implementation
        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            { 
            convertedResult = Session.FindObject(SerializedType, new BinaryOperator("Name", Name));
            if (convertedResult == null)
            {
                convertedResult = SerializedType.CreateInstance(Session);
            }
            }
            var result = convertedResult as TacticSet;
            result.Name = this.Name;
            foreach (var response in Responses)
            {
                response.Session = Session;
                result.Tactic.Add((Tactic)response.ChangeType(response.SerializedType, null));
            }
            return result;
        }
        #endregion ISerialize Implementation

    }
}
