﻿using DevExpress.Xpo;
using System;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class ExecutionLog :StandardTracking
    {
        #region Fields
        private BusinessRuleSet _BusinessRuleSet;
        private string _Description;
        #endregion Fields

        #region Constructors
        public ExecutionLog(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        [Association("BusinessRuleSet-ExecutionLog",typeof(BusinessRuleSet))]
        public BusinessRuleSet BusinessRuleSet
        {
            get { return _BusinessRuleSet; }
            set { SetPropertyValue("BusinessRuleSet", ref _BusinessRuleSet, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }
        #endregion Properties

        public void AddMessage(string message)
        {
            Description += string.Format("{2}{0} - {1}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff"), message, Environment.NewLine);
        }

        public void AddMessage(string message, params object[] p)
        {
            AddMessage(string.Format(message, p));
        }

    }
}
