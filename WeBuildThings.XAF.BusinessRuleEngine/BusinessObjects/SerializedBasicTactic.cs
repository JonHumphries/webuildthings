﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using WeBuildThings.Utilities.Extensions;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using WeBuildThings.XAF.Common.Reflection;


namespace WeBuildThings.XAF.BusinessRuleEngine
{

    public class SerializedBasicTactic : SerializedTactic
    {
        public string HardValue { get; set; }

        protected override Type GetSerializedType()
        {
            return typeof(BasicTactic); //Changed from SerializedBasicTactic
        }

        protected override object Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = type.CreateInstance(Session);
                }
            }
            var result = convertedResult as BasicTactic;
            result.Name = Name;
            result.HardValue = HardValue;
            result.TargetType = PersistedType.CreatePersistedType(Session, TargetType);
            result.PropertyName = PropertyName;
            return result;
        }
    }
}
