﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Utilities;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using WeBuildThings.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class BusinessRule : BaseObject, IBinaryTree, IOutlinedList, ISerialize
    {
        #region Fields
        private int _SortOrder;
        private string _Name;
        private int _Number;
        private string _OutlineNumber;
        private FilterCriteria _RuleCriteria;
        private BusinessRuleSet _RuleSet;
        private BusinessRule _ParentRule;
        private TacticSet _TacticSet;
        private bool _Active;
        #endregion Fields

        #region Constructors
        public BusinessRule(Session session)
            : base(session)
        { }

        internal BusinessRule() : base() { }

        public override void AfterConstruction()
        {
            RuleCriteria = new FilterCriteria(Session);
            Active = true;
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        //[ModelDefault("AllowEdit", "False")]
        //[Browsable(false)]
        public int Number
        {
            get { return _Number; }
            set { if (SetPropertyValue("Number", ref _Number, value) && !IsLoading) { OnNumberChanged(); } }
        }

        //[ModelDefault("AllowEdit", "False")]
        public int SortOrder
        {
            get { return _SortOrder; }
            set { SetPropertyValue("SortOrder", ref _SortOrder, value); }
        }

        //[ModelDefault("AllowEdit", "False")]
        public string OutlineNumber
        {
            get { return _OutlineNumber; }
            set { if (SetPropertyValue("OutlineNumber", ref _OutlineNumber, value) && !IsLoading) { OnOutlineNumberChanged(); } }
        }

        [ExpandObjectMembers(ExpandObjectMembers.Always)]
        public FilterCriteria RuleCriteria
        {
            get { return _RuleCriteria; }
            set { if (SetPropertyValue("RuleCriteria", ref _RuleCriteria, value) && !IsLoading) { OnRuleCriteriaChanged(); } }
        }

        [Association("RuleSet-Rule", typeof(BusinessRuleSet))]
        public BusinessRuleSet RuleSet
        {
            get { return _RuleSet; }
            set { if (SetPropertyValue("RuleSet", ref _RuleSet, value) && !IsLoading) { OnRuleSetChanged(); } }
        }

        [Association("RuleParent-RuleChildren", typeof(BusinessRule))]
        public BusinessRule ParentRule
        {
            get { return _ParentRule; }
            set { if (SetPropertyValue("ParentRule", ref _ParentRule, value) && !IsLoading) { OnParentRuleChanged(); } }
        }

        [Association("RuleParent-RuleChildren", typeof(BusinessRule)), Aggregated]
        public XPCollection<BusinessRule> ChildRules
        {
            get { return GetCollection<BusinessRule>("ChildRules"); }
        }

        public TacticSet TacticSet
        {
            get { return _TacticSet; }
            set { SetPropertyValue("TacticSet", ref _TacticSet, value); }
        }

        public bool Active
        {
            get { return _Active; }
            set { SetPropertyValue("Active", ref _Active, value); }
        }
        #endregion Properties

        #region IBinaryTree Implementation
        IBindingList ITreeNode.Children
        {
            get { return ChildRules; }
        }

        ITreeNode ITreeNode.Parent
        {
            get { return ParentRule; }
        }

        ITreeNode IBinaryTree.Parent
        {
            get { return ParentRule; }
            set { ParentRule = (BusinessRule)value; }
        }

        IOutlinedList IOutlinedList.Parent
        {
            get { return ParentRule; }
        }

        IEnumerable<IOutlinedList> IOutlinedList.Children
        {
            get { return ChildRules; }
        }

        IEnumerable<IBinaryTree> IBinaryTree.Children
        {
            get { return ChildRules; }
        }

        IEnumerable<IBinaryTree> IBinaryTree.FullTree
        {
            get { return RuleSet.Rules; }
        }

        IEnumerable<IOutlinedList> IOutlinedList.FullList
        {
            get
            {
                IEnumerable<IOutlinedList> result = null;
                if (RuleSet != null)
                {
                    result = RuleSet.Rules;
                }
                return result;
            }
        }
        #endregion IBinaryTree Implementation

        #region INumberedList Implementation

        public IEnumerable<INumberedList> List
        {
            get
            {
                IEnumerable<INumberedList> result = null;
                if (RuleSet != null)
                {
                    result = (ParentRule != null) ? ParentRule.ChildRules : RuleSet.Rules.Where(n => n.ParentRule == null);
                }
                return result;
            }
        }
        #endregion INumberedList Implementation

        #region Event Methods
        private void OnRuleSetChanged()
        {
            SetRuleCriteriaFilterObject();
        }

        private void OnRuleCriteriaChanged()
        {
            SetRuleCriteriaFilterObject();
        }

        private void OnNumberChanged()
        {
            SetOutlineNumber();
            this.SetSortOrder();
        }

        private void OnParentRuleChanged()
        {//changed from move down to reorder
            this.SetNumber();
            SetOutlineNumber();
            if (ParentRule != null && !ParentRule.ChildRules.Contains(this))
            {
                ParentRule.ChildRules.Add(this);
            }
        }
        
        private void OnOutlineNumberChanged()
        {
            SetChildOutlineNumber();
        }
        #endregion Event Methods

        #region Set Methods
        internal void SetRuleCriteriaFilterObject()
        {
            if (RuleCriteria != null && RuleSet != null)
            {
                RuleCriteria.FilterObject = RuleSet.TargetObject;
            }
        }

        public void SetOutlineNumber()
        {
            OutlineNumber = this.CalcOutlineNumber();
        }

        private void SetChildOutlineNumber()
        {
            foreach (var item in ChildRules)
            {
                item.SetOutlineNumber();
            }
        }
        #endregion Set Methods

        #region Public Methods
        /// <summary>
        /// Returns a single criteria operator representing the entire rule set of the branch
        /// </summary>
        public CriteriaOperator FullCriteria()
        {
            CriteriaOperator result = null;
            if (ParentRule != null)
            {
                var parentCriteria = ParentRule.FullCriteria();
                if (!ReferenceEquals(parentCriteria,null))
                {
                    result = new GroupOperator(GroupOperatorType.And, RuleCriteria.GetOperator(), ParentRule.FullCriteria());
                }
            }
            if (ReferenceEquals(result,null))
            {
                result = RuleCriteria.GetOperator();
            }
            return result;
        }

        /// <summary>
        /// Check to see if this object's oid is return based on the filter criteria.
        /// </summary>
        public bool CheckCriteria(IObjectSpace objectSpace, BaseObject testObject)
        {
            var criteria = FullCriteria();
            bool? result = false;
            if (!ReferenceEquals(criteria, null))
            {
                result = objectSpace.IsObjectFitForCriteria(testObject, FullCriteria());
            }
            return (result.HasValue && result == true);
        }
        #endregion Public Methods

        #region ISerialize Implementation
        [Browsable(false)]
        public Type SerializedType
        {
            get { return typeof(SerializedRule); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(SerializedType);
            }
            var result = convertedResult as SerializedRule;
            result.Name = this.Name;
            result.Active = this.Active;
            result.Number = this.Number;
            result.OutlineNumber = this.OutlineNumber;
            if (TacticSet != null)
            {
                result.Response = (SerializedTacticSet)this.TacticSet.ChangeType(TacticSet.SerializedType, null);
            }
            result.RuleCriteria = this.RuleCriteria.CriterionText;
            result.SortOrder = this.SortOrder;
            if (ParentRule != null)
            {
                result.ParentRuleSortOrder = this.ParentRule.SortOrder;
            }
            return result;
        }

        #endregion ISerialize Implementation
    }

}
