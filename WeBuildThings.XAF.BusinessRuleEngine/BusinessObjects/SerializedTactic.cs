﻿using DevExpress.Xpo;
using System;
using System.Xml.Serialization;
using WeBuildThings.Common.Serialization;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public abstract class SerializedTactic : ISerializeToXaf
    {
        public string TacticSet { get; set; }
        public string Name { get; set; }
        public string TargetType { get; set; }
        public string PropertyName { get; set; }
        [XmlIgnore]
        public Session Session { get; set; }

        protected abstract Type GetSerializedType();

        protected abstract object Convert(Type type, object convertedObject);

        public Type SerializedType
        {
            get { return GetSerializedType(); }
        }

        public object ChangeType(Type type, object convertedObject)
        {
            return Convert(type, convertedObject);
        }
    }
}
