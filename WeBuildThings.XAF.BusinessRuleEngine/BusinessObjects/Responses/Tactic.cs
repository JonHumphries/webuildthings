﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;
using System.ComponentModel;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public abstract class Tactic : BaseObject, ISerialize
    {
        #region Fields
        private string _Name;
        private PersistedType _TargetType;
        private string _PropertyName;
        #endregion Fields

        #region Constructors
        public Tactic(Session session) : base(session) { }

        internal Tactic() : base() { }
        #endregion Constructors

        #region Properties
        [Association("TacticSet-Tactic", typeof(TacticSet))]
        public XPCollection<TacticSet> TacticSet
        {
            get { return GetCollection<TacticSet>("TacticSet"); }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public PersistedType TargetType
        {
            get { return _TargetType; }
            set { SetPropertyValue("TargetType", ref _TargetType, value); }
        }

        public string PropertyName
        {
            get { return _PropertyName; }
            set { SetPropertyValue("PropertyName", ref _PropertyName, value); }
        }
        #endregion Properties

        public abstract void Execute(BaseObject targetObject);

        public abstract void Execute(BusinessRule rule, BaseObject targetObject);

        #region ISerialize Implementation
        protected abstract SerializedTactic Convert(Type type, object convertedResult);

        protected abstract Type GetSerializedType();
        
        [Browsable(false)]
        public virtual Type SerializedType
        {
            get { return GetSerializedType(); }
        }

        public virtual object ChangeType(Type type, Object convertedResult)
        {
            return Convert(type, convertedResult);
        }

        #endregion ISerialize Implementation
    }
}
