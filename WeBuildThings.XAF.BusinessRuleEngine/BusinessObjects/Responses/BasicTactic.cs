﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Extensions;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class BasicTactic : Tactic
    {
        #region Fields
        private string _HardValue;
        #endregion Fields

        #region Constructors
        public BasicTactic(Session session) : base(session) { }

        public BasicTactic() : base() { }
        #endregion Constructors

        #region Properties


        public string HardValue
        {
            get { return _HardValue; }
            set { SetPropertyValue("HardValue", ref _HardValue, value); }
        }

        #endregion Properties

        #region Public Methods
        public override void Execute(BaseObject targetObject)
        {
            dynamic target = targetObject;
            TypeExtensions.SetProperty(target, PropertyName, HardValue);
        }

        public override void Execute(BusinessRule rule, BaseObject targetObject)
        {
            dynamic target = targetObject;
            TypeExtensions.SetProperty(target, PropertyName, HardValue);
        }
        #endregion Public Methods

        #region ISerialize Implementation
        protected override SerializedTactic Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedBasicTactic;
            result.HardValue = this.HardValue;
            result.TargetType = this.TargetType.FullName;
            result.PropertyName = this.PropertyName;
            result.Name = this.Name;
            return result;
        }

        protected override Type GetSerializedType()
        {
            return typeof(SerializedBasicTactic);
        }
        #endregion ISerialize Implementation
    }
}
