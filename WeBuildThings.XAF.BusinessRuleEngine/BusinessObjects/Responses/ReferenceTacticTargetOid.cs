﻿using System;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects.Responses
{
    [DefaultClassOptions]
    public class ReferenceTacticTargetOid : BaseObject
    {
        public ReferenceTacticTargetOid(Session session)
            : base(session)
        {
        }

        public Guid TargetOid { get; set; }
    }
}