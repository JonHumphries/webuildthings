﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Extensions;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class AlternatePropertyTactic : Tactic
    {
        #region Fields
        private string _NewProperty;
        #endregion Fields

        #region Constructors
        public AlternatePropertyTactic(Session session) : base(session) { }

        public AlternatePropertyTactic() : base() { }
        #endregion Constructors

        #region Properties


        public string NewProperty
        {
            get { return _NewProperty; }
            set { SetPropertyValue("NewProperty", ref _NewProperty, value); }
        }

        #endregion Properties

        #region Public Methods
        public override void Execute(BaseObject targetObject)
        {
            dynamic target = targetObject;
            var newValue = TypeExtensions.GetProperty(target, NewProperty);
            TypeExtensions.SetProperty(target, PropertyName, newValue);
        }

        public override void Execute(BusinessRule rule, BaseObject targetObject)
        {
            dynamic target = targetObject;
            var newValue = TypeExtensions.GetProperty(target, NewProperty);
            TypeExtensions.SetProperty(target, PropertyName, newValue);
        }
        #endregion Public Methods

        #region ISerialize Implementation
        protected override SerializedTactic Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedAlternatePropertyTactic;
            result.NewProperty = this.NewProperty;
            result.TargetType = this.TargetType.FullName;
            result.PropertyName = this.PropertyName;
            result.Name = this.Name;
            return result;
        }

        protected override Type GetSerializedType()
        {
            return typeof(SerializedReferenceRuleResponse);
        }
        #endregion ISerialize Implementation
    }

   
}