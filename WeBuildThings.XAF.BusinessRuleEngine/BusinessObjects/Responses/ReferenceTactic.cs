﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Extensions;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects.Responses;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class ReferenceTactic : Tactic
    {
        #region Fields
        private PersistedType _SourceType;
        private Guid _TargetOid;
        private BindingList<ReferenceTacticTargetOid> _listReferenceTacticTargetOid;
        private ReferenceTacticTargetOid _referenceTacticTargetOids;

        #endregion Fields

        #region Constructors
        public ReferenceTactic(Session session)
            : base(session)
        {
        }

        internal ReferenceTactic() : base() { }
        #endregion Constructors

        #region Properties
        public PersistedType SourceType
        {
            get { return _SourceType; }
            set
            {
                if (IsLoading)
                {
                    SetPropertyValue("TargetValueType", ref _SourceType, value);
                }
                else if (ValidateTargetValueType(value))
                {
                    SetPropertyValue("TargetValueType", ref _SourceType, value);
                }
            }
        }

        public Guid TargetOid
        {
            get { return _TargetOid; }
            set
            {
                SetPropertyValue("TargetOid", ref _TargetOid, value);
            }
        }

        [NonPersistent, DataSourceProperty("ListReferenceTacticTargetOid")]
        [VisibleInListView(false), VisibleInDetailView(false), VisibleInLookupListView(false)]
        [ImmediatePostData]
        public ReferenceTacticTargetOid ReferenceTacticTargetOids
        {
            get
            {
                if (IsLoading) return _referenceTacticTargetOids;
                if (TargetOid != Guid.Empty)
                {
                    _referenceTacticTargetOids = new ReferenceTacticTargetOid(Session)
                    {
                        TargetOid = TargetOid
                    };
                }
                return _referenceTacticTargetOids;
            }
            set { if (SetPropertyValue("ReferenceTacticTargetOids", ref _referenceTacticTargetOids, value) && !IsLoading) { OnReferenceTacticTargetOidsChanged(value); } }
        }

        public IList<ReferenceTacticTargetOid> ListReferenceTacticTargetOid => _listReferenceTacticTargetOid;
        #endregion Properties

        #region Public Methods
        public override void Execute(BaseObject targetObject)
        {
            dynamic target = targetObject;
            var targetValue = targetObject.Session.GetObjectByKey(SourceType.ObjectType, TargetOid);
            if (targetValue == null)
            {
                throw new ArgumentNullException(string.Format("Unable to locate referrenced {0} with Oid {1}", SourceType.ObjectType, TargetOid));
            }
            TypeExtensions.SetProperty(target, PropertyName, targetValue);
        }

        public override void Execute(BusinessRule rule, BaseObject targetObject)
        {
            dynamic target = targetObject;
            var targetValue = targetObject.Session.GetObjectByKey(SourceType.ObjectType, TargetOid);
            if (targetValue == null)
            {
                throw new ArgumentNullException(string.Format("Unable to locate referrenced {0} with Oid {1}", SourceType.ObjectType, TargetOid));
            }
            TypeExtensions.SetProperty(target, PropertyName, targetValue);
        }

        /// <summary>
        /// Change source type get list Oid
        /// </summary>
        /// <param name="persistedType"></param>
        public void OnSourceTypeChanged(PersistedType persistedType)
        {
            if (_listReferenceTacticTargetOid == null)
                _listReferenceTacticTargetOid = new BindingList<ReferenceTacticTargetOid>();
            _listReferenceTacticTargetOid.RaiseListChangedEvents = false;
            _listReferenceTacticTargetOid.Clear();
            if (string.IsNullOrEmpty(persistedType.FullName)) return;
            var type = XafTypesInfo.Instance.FindTypeInfo(persistedType.FullName).Type;
            var objectSpace = XPObjectSpace.FindObjectSpaceByObject(SecuritySystem.CurrentUser);
            var listObjects = objectSpace.GetObjects(type);
            foreach (var item in listObjects)
            {
                _listReferenceTacticTargetOid.Add(new ReferenceTacticTargetOid(Session)
                {
                    TargetOid = ((BaseObject)item).Oid
                });
            }
            _listReferenceTacticTargetOid.RaiseListChangedEvents = true;
            _listReferenceTacticTargetOid.ResetBindings();
            //if (TargetOid != Guid.Empty)
            //    ReferenceTacticTargetOids = new ReferenceTacticTargetOid(Session)
            //    {
            //        TargetOid = TargetOid
            //    };
        }

        /// <summary>
        /// Clear TargetOid when change SourceType
        /// </summary>
        public void ClearTargetOid()
        {
            ReferenceTacticTargetOids = null;
        }

        private void OnReferenceTacticTargetOidsChanged(ReferenceTacticTargetOid value)
        {
            if (value != null)
            {
                TargetOid = value.TargetOid;
            }
            else
            {
                TargetOid = Guid.Empty;
                ReferenceTacticTargetOids = null;
            }
        }
        #endregion Public Methods

        #region Validation Methods
        private bool ValidateTargetValueType(PersistedType type)
        {
            return type.ValidateType(true, typeof(BaseObject));
        }

        private bool ValidateTargetOid(bool exception, Guid guidToTest)
        {
            string result = string.Empty;
            if (guidToTest == Guid.Empty)
            {
                result = string.Format("{0} is not a valid Oid", guidToTest);
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                var argumentException = new ArgumentException(result);
                argumentException.Source = typeof(ReferenceTactic).Assembly.FullName;
                throw argumentException;
            }
            return string.IsNullOrWhiteSpace(result);
        }
        #endregion Validation Methods

        #region ISerialize Implementation
        protected override SerializedTactic Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedReferenceRuleResponse;
            result.SourceType = this.SourceType.FullName;
            result.TargetOid = this.TargetOid;
            result.TargetType = this.TargetType.FullName;
            result.PropertyName = this.PropertyName;
            result.Name = this.Name;
            return result;
        }

        protected override Type GetSerializedType()
        {
            return typeof(SerializedReferenceRuleResponse);
        }
        #endregion ISerialize Implementation
    }
}
