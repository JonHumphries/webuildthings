﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects;
using System;
using System.ComponentModel;
using System.Linq;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    [DefaultClassOptions]
    public class TacticSet : BaseObject, ISerialize
    {
        #region Fields
        private string _Name;
        #endregion Fields

        #region Constructors
        public TacticSet(Session session) : base(session) { }

        internal TacticSet() : base() { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Association("TacticSet-Tactic", typeof(Tactic))]
        public XPCollection<Tactic> Tactic
        {
            get { return GetCollection<Tactic>("Tactic"); }
        }
        #endregion Properties

        public void Execute(BaseObject targetObject, ExecutionLog executionLog)
        {
            executionLog.AddMessage("Executing Tactic set {0} on {1}", Name, targetObject);
            foreach (Tactic response in Tactic)
            {
                response.Execute(targetObject);
            }
        }

        public void Execute(BusinessRule rule, BaseObject targetObject, ExecutionLog executionLog)
        {
            executionLog.AddMessage("Executing Tactic set {0} on {1}", Name, targetObject);
            foreach (Tactic tactic in Tactic)
            {
                tactic.Execute(rule, targetObject);
            }
        }

        #region ISerialize Implementation
        [Browsable(false)]
        public Type SerializedType
        {
            get { return typeof(SerializedTacticSet); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedTacticSet;
            result.Name = this.Name;
            if (this.Tactic != null && Tactic.Any())
            {
                foreach (var response in Tactic)
                {
                    var convertedResponse = (SerializedTactic)response.ChangeType(response.SerializedType, null);
                    result.Responses.Add(convertedResponse);
                }
            }
            return result;
        }

        #endregion ISerializeImplementation
    }
}
