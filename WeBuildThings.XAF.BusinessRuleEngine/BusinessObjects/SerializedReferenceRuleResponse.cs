﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using WeBuildThings.Utilities.Extensions;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class SerializedReferenceRuleResponse : SerializedTactic
    {
        public string SourceType { get; set; }
        public Guid TargetOid { get; set; }

        protected override Type GetSerializedType()
        {
            return typeof(ReferenceTactic); //changed from SerializedReferenceRuleResponse
        }

        protected override object Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = type.CreateInstance(Session);
                }
            }
            var result = convertedResult as ReferenceTactic;
            result.Name = Name;
            result.SourceType = PersistedType.CreatePersistedType(Session, SourceType);
            result.TargetOid = TargetOid;
            result.TargetType = PersistedType.CreatePersistedType(Session, TargetType);
            result.PropertyName = PropertyName;
            return result;
        }

    }
}
