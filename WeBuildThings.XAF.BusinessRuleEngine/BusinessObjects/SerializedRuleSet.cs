﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects
{
    [NonPersistent]
    public class SerializedRuleSet : ISerializeToXaf
    {
        #region Fields
        private string _Name;
        private Session _Session;
        private string _TargetObject;
        private SerializedTacticSet _DefaultResponse;
        private string _PropertyToGroupOn;
        private string _DefaultCriteria;
        private HashSet<SerializedRule> _SerializedRules;
        #endregion Fields

        #region Constructors
        public SerializedRuleSet()
        {
            SerializedRules = new HashSet<SerializedRule>();
        }
        #endregion Constructors

        #region Properties
        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        [Browsable(false)]
        public Type SerializedType
        {
            get { return typeof(BusinessRuleSet); }
        }

        public string Name
        {
            get { return _Name; }
            set { if (_Name != value) _Name = value; }
        }

        public string TargetObject
        {
            get { return _TargetObject; }
            set { if (_TargetObject != value) _TargetObject = value; }
        }

        public SerializedTacticSet DefaultResponse
        {
            get { return _DefaultResponse; }
            set { if (_DefaultResponse != value) _DefaultResponse = value; }
        }

        public string DefaultCriteria
        {
            get { return _DefaultCriteria; }
            set { if (_DefaultCriteria != value) _DefaultCriteria = value; }
        }

        public string PropertyToGroupOn
        {
            get { return _PropertyToGroupOn; }
            set { if (_PropertyToGroupOn != value) _PropertyToGroupOn = value; }
        }

        public HashSet<SerializedRule> SerializedRules
        {
            get { return _SerializedRules; }
            set { if (_SerializedRules != value) _SerializedRules = value; }
        }
        #endregion Properties

        #region ISerialize Implementation
        public virtual object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = type.CreateInstance(Session);
                }
            }
            var result = convertedResult as BusinessRuleSet;
            result.Name = this.Name;
            result.PropertyToGroupOn = this.PropertyToGroupOn;
            if (TargetObject != null)
            {
                if (result.TargetObject != null) result.TargetObject.Delete();
                result.TargetObject = PersistedType.CreatePersistedType(Session, this.TargetObject);
            }
            if (DefaultResponse != null)
            {
                if (result.DefaultTactic != null) result.DefaultTactic.Delete();
                this.DefaultResponse.Session = Session;
                result.DefaultTactic = (TacticSet)this.DefaultResponse.ChangeType(DefaultResponse.SerializedType, null);
            }
            if (result.DefaultCriteria == null)
            {
                result.DefaultCriteria = new XAF.Common.Utilities.FilterCriteria(Session);
            }
            result.DefaultCriteria.Criterion = DefaultCriteria;
            foreach (SerializedRule rule in this.SerializedRules)
            {
                rule.Session = Session;
                rule.RuleSetName = result.Name;
                var newRule = (BusinessRule)rule.ChangeType(rule.SerializedType, null);
                newRule.RuleSet = result;
                result.Rules.Add(newRule);
            }
            var childRules = this.SerializedRules.Where(n => n.ParentRuleSortOrder != 0);
            foreach (SerializedRule childRule in childRules)
            {
                var rule = result.Rules.Where(n => n.SortOrder == childRule.SortOrder).FirstOrDefault();
                if (rule != null)
                {
                    rule.ParentRule = result.Rules.Where(n => n.SortOrder == childRule.ParentRuleSortOrder).FirstOrDefault();
                }
            }
            return result;
        }

        #endregion ISerialize Implementation
    }
}
