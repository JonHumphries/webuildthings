﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using WeBuildThings.Utilities.Extensions;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.BusinessRuleEngine
{
    public class SerializedAlternatePropertyTactic : SerializedTactic
    {
        public string NewProperty { get; set; }

        protected override Type GetSerializedType()
        {
            return typeof(AlternatePropertyTactic); //changed from SerializedAlternatePropertyTactic
        }

        protected override object Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(GetSerializedType(), new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = GetSerializedType().CreateInstance(Session);
                }
            }
            var result = convertedResult as AlternatePropertyTactic;
            result.Name = Name;
            result.NewProperty = NewProperty;
            result.TargetType = PersistedType.CreatePersistedType(Session, TargetType);
            result.PropertyName = PropertyName;
            return result;
        }
    }
}
