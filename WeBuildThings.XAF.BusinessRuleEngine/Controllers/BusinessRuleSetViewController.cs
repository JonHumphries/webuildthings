﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;


namespace WeBuildThings.XAF.BusinessRuleEngine.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class BusinessRuleSetViewController : ViewController
    {
        public BusinessRuleSetViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        private void Execute_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var ruleSet = View.CurrentObject as BusinessRuleSet;
            if (ruleSet == null)
            {
                ruleSet = View.SelectedObjects[0] as BusinessRuleSet;
            }
            if (ruleSet != null)
            {
                using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)ObjectSpace).Session.ObjectLayer))
                {
                    using (var items = new XPCollection(uow, ruleSet.TargetObject.ObjectType, true))
                    {
                        ruleSet.Execute(ObjectSpace, items);
                        foreach (var item in items)
                        {
                            var bo = item as BaseObject;
                            if (bo != null)
                            {
                                bo.Save();
                            }
                        }
                    }
                    uow.CommitChanges();
                }
            }
        }
    }
}
