﻿namespace WeBuildThings.XAF.BusinessRuleEngine.Controllers
{
    partial class BusinessRuleSetViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RunRuleSet = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // RunRuleSet
            // 
            this.RunRuleSet.Caption = "Execute";
            this.RunRuleSet.Category = "RecordEdit";
            this.RunRuleSet.ConfirmationMessage = null;
            this.RunRuleSet.Id = "RunRuleSet";
            this.RunRuleSet.ImageName = "Action_Debug_Start";
            this.RunRuleSet.ToolTip = null;
            this.RunRuleSet.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Execute_Execute);
            // 
            // BusinessRuleSetViewController
            // 
            this.Actions.Add(this.RunRuleSet);
            this.TargetObjectType = typeof(WeBuildThings.XAF.BusinessRuleEngine.BusinessRuleSet);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction RunRuleSet;
    }
}
