﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Win.Core;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System;
using System.Linq;
using WeBuildThings.XAF.BusinessRuleEngine.BusinessObjects.Responses;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.BusinessRuleEngine.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class TacticViewController : ViewController
    {
        public TacticViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        private static string[] _ModelBrowserProperties = new string[] { "PropertyName", "TargetPropertyName" };

        protected override void OnActivated()
        {
            base.OnActivated();
            Console.WriteLine(Frame.Context);
        }

        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            DetailView detailView = View as DetailView;
            // Get StringPropertyEditor control and assign Click event so that we can display model browser.
            if (detailView != null)
            {
                foreach (StringPropertyEditor stringPropertyEditor in detailView.GetItems<StringPropertyEditor>())
                {
                    if (stringPropertyEditor.Control != null)
                    {
                        TextEdit textEdit = (TextEdit)stringPropertyEditor.Control;
                        if (textEdit != null && _ModelBrowserProperties.Contains(stringPropertyEditor.PropertyName))
                        {
                            textEdit.Click -= ObjectName_Click;
                            textEdit.Click += ObjectName_Click;
                            break;
                        }
                    }
                }
                
                var ctlSourceType = detailView.FindItem("SourceType") as LookupPropertyEditor;
                if (ctlSourceType == null) return;
                ctlSourceType.Control.EditValueChanging -= Control_EditValueChanging;
                ctlSourceType.Control.EditValueChanging += Control_EditValueChanging;
                ctlSourceType.Control.EditValueChanged -= Control_EditValueChanged;
                ctlSourceType.Control.EditValueChanged += Control_EditValueChanged;
            }
        }

        private void Control_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (e.NewValue == e.OldValue) return;
            if (View == null) return;
            var referenceTactic = View.CurrentObject as ReferenceTactic;
            referenceTactic?.ClearTargetOid();
        }

        private void Control_EditValueChanged(object sender, EventArgs e)
        {
            var persistedType = ((BaseEdit)sender).EditValue as PersistedType;
            //var session = ((XPObjectSpace)ObjectSpace).Session;
            if (View == null) return;
            var referenceTactic = View.CurrentObject as ReferenceTactic;
            if (persistedType != null)
                referenceTactic?.OnSourceTypeChanged(persistedType);
        }

        private void ObjectName_Click(object sender, EventArgs e)
        {
            Tactic ruleResponse = View.CurrentObject as Tactic;

            if (ruleResponse != null && ruleResponse.TargetType != null)
            {
                //string source = ()
                ITypeInfo typeInfo = XafTypesInfo.Instance.FindTypeInfo(ruleResponse.TargetType.FullName);
                ruleResponse.PropertyName = this.ShowModelBrowser(typeInfo);
            }
        }

        /// <summary>
        /// Opens up model browser to select DataSource member.
        /// </summary>
        private string ShowModelBrowser(ITypeInfo typeInfo)
        {
            string result = string.Empty;
            ModelBrowser modelBrowser = new ModelBrowser(typeInfo.Type, false);
            if (modelBrowser.ShowDialog())
            {
                result = modelBrowser.SelectedMember;
            }
            return result;
        }

        private void Execute_Execute(object sender, DevExpress.ExpressApp.Actions.SimpleActionExecuteEventArgs e)
        {

        }
    }
}
