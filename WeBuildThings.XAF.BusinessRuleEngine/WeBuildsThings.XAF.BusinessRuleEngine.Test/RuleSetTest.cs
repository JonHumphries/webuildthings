﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon;
using JonBuildsThings.DevXCommon.Reflection;
using JonBuildsThings.DevXCommon.Utilities;
using JonBuildsThings.Tests;
using JonBuildsThings.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace JonBuildsThings.HierarchicalRulesEngine.Test
{
    [TestClass]
    public class RuleSetTest
    {
        private Session Session { get { return XAFInitialization.Session; } }

        [TestMethod]
        public void RuleSetSingleRuleMatch()
        {
            string name = "HelloWorld";
            string connectionString = ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString;
            Session session = ConnectionHelper.CreateSession(connectionString, XAFInitialization.Assembly);
            IObjectSpace objectSpace = ConnectionHelper.CreateNewObjectSpace(connectionString);

            BusinessRuleSet ruleSet = new BusinessRuleSet(session);
            ruleSet.TargetObject = new PersistedType(session) { ObjectType = typeof(TestType) };

            BasicTactic response = new BasicTactic(session);
            response.Name = "Test Response";
            TacticSet responseSet = new TacticSet(session);
            responseSet.Tactic.Add(response);
            BusinessRule rule1 = new BusinessRule(session);
            rule1.RuleCriteria = new FilterCriteria(session);
            rule1.TacticSet = responseSet;
            GroupOperator criteria = new GroupOperator(new BinaryOperator("Name", name));
            rule1.RuleCriteria.Criterion = criteria.ToString();
            ruleSet.Rules.Add(rule1);

            TestType actualResult = new TestType(session);
            actualResult.Name = name;

            var result = ruleSet.Match(objectSpace, actualResult);
            var actualRule = ruleSet.GetMatchingRule(objectSpace, actualResult);

            Assert.IsTrue(result);
            Assert.AreEqual(rule1, actualRule);
        }

        [TestMethod]
        public void RuleSetMultiRuleMatch()
        {
            string name = "HelloWorld";

            string connectionString = ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString;
            Session session = ConnectionHelper.CreateSession(connectionString, XAFInitialization.Assembly);
            IObjectSpace objectSpace = ConnectionHelper.CreateNewObjectSpace(connectionString);

            BusinessRuleSet ruleSet = new BusinessRuleSet(session);
            ruleSet.TargetObject = new PersistedType(session) { ObjectType = typeof(TestType) };
            BasicTactic response = new BasicTactic(session);
            response.Name = "Test Response";
            TacticSet responseSet = new TacticSet(session);
            responseSet.Tactic.Add(response);
            BusinessRule rule1 = new BusinessRule(session);
            rule1.RuleCriteria = new FilterCriteria(session);
            rule1.TacticSet = responseSet;
            GroupOperator criteria = new GroupOperator(new BinaryOperator("Name", name));
            rule1.RuleCriteria.Criterion = criteria.ToString();
            ruleSet.Rules.Add(rule1);

            BusinessRule rule2 = new BusinessRule(session);
            rule2.RuleCriteria = new FilterCriteria(session);
            rule2.TacticSet = responseSet;
            GroupOperator criteria2 = new GroupOperator(new BinaryOperator("Name", name + "False"));
            rule2.RuleCriteria.Criterion = criteria2.ToString();
            ruleSet.Rules.Add(rule2);

            TestType actualResult = new TestType(session);
            actualResult.Name = name;

            var result = ruleSet.Match(objectSpace, actualResult);
            var actualRule = ruleSet.GetMatchingRule(objectSpace, actualResult);

            Assert.IsTrue(result);
            Assert.AreEqual(rule1, actualRule);
        }

        [TestMethod]
        public void RuleSetNestedMatch()
        {
            string name = "HelloWorld";
            string referenceTestTypeName = "NotHelloWorld";
            string connectionString = ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString;
            Session session = ConnectionHelper.CreateSession(connectionString, XAFInitialization.Assembly);
            IObjectSpace objectSpace = ConnectionHelper.CreateNewObjectSpace(connectionString);

            BusinessRuleSet ruleSet = new BusinessRuleSet(session);
            ruleSet.TargetObject = new PersistedType(session) { ObjectType = typeof(TestType) };
            BasicTactic response = new BasicTactic(session);
            response.Name = "Test Response";
            TacticSet responseSet = new TacticSet(session);
            responseSet.Tactic.Add(response);
            BusinessRule rule1 = new BusinessRule(session);
            rule1.RuleCriteria = new FilterCriteria(session);
            rule1.TacticSet = responseSet;
            GroupOperator criteria = new GroupOperator(new BinaryOperator("ReferenceType.Name", referenceTestTypeName));
            rule1.RuleCriteria.Criterion = criteria.ToString();
            ruleSet.Rules.Add(rule1);

            BusinessRule rule2 = new BusinessRule(session);
            rule2.RuleCriteria = new FilterCriteria(session);
            rule2.TacticSet = responseSet;
            GroupOperator criteria2 = new GroupOperator(new BinaryOperator("Name", name + "False"));
            rule2.RuleCriteria.Criterion = criteria2.ToString();
            ruleSet.Rules.Add(rule2);

            TestType actualResult = new TestType(session);
            actualResult.Name = name;
            TestType referenceType = new TestType(session);
            referenceType.Name = referenceTestTypeName;
            actualResult.ReferenceType = referenceType;

            var result = ruleSet.Match(objectSpace, actualResult);
            var actualRule = ruleSet.GetMatchingRule(objectSpace, actualResult);

            Assert.IsTrue(result);
            Assert.AreEqual(rule1, actualRule);
        }

        [TestMethod]
        public void RuleSetExecute()
        {
            string expectedResult = "PassedTheTest";
            string name = "HelloWorld";
            string connectionString = ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString;
            Session session = ConnectionHelper.CreateSession(connectionString, XAFInitialization.Assembly);
            IObjectSpace objectSpace = ConnectionHelper.CreateNewObjectSpace(connectionString);

            BusinessRuleSet ruleSet = new BusinessRuleSet(session);
            ruleSet.TargetObject = new PersistedType(session) { ObjectType = typeof(TestType) };
            BusinessRule rule1 = new BusinessRule(session);
            rule1.RuleCriteria = new FilterCriteria(session);
            GroupOperator criteria = new GroupOperator(new BinaryOperator("Name", name));
            rule1.RuleCriteria.Criterion = criteria.ToString();
            rule1.TacticSet = new TacticSet(session);
            var response = new BasicTactic(session);
            response.TargetType = new PersistedType(session);
            response.TargetType.ObjectType = typeof(TestType);
            response.PropertyName = "Name";
            response.HardValue = expectedResult;
            rule1.TacticSet.Tactic.Add(response);
            ruleSet.Rules.Add(rule1);

            BusinessRule rule2 = new BusinessRule(session);
            rule2.RuleCriteria = new FilterCriteria(session);
            GroupOperator criteria2 = new GroupOperator(new BinaryOperator("Name", name + "False"));
            rule2.RuleCriteria.Criterion = criteria2.ToString();
            ruleSet.Rules.Add(rule2);

            TestType actualResult = new TestType(session);
            actualResult.Name = name;

            ruleSet.Execute(objectSpace, actualResult);

            Assert.AreEqual(actualResult.Name, expectedResult);
        }

        [TestMethod]
        public void AlternatePropertyTest()
        {
            string expectedResult = "PassedTheTest";
            string name = "HelloWorld";
            string connectionString = ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString;
            Session session = ConnectionHelper.CreateSession(connectionString, XAFInitialization.Assembly);
            IObjectSpace objectSpace = ConnectionHelper.CreateNewObjectSpace(connectionString);

            BusinessRuleSet ruleSet = new BusinessRuleSet(session);
            ruleSet.TargetObject = new PersistedType(session) { ObjectType = typeof(TestType) };
            BusinessRule rule1 = new BusinessRule(session);
            rule1.RuleCriteria = new FilterCriteria(session);
            GroupOperator criteria = new GroupOperator(new BinaryOperator("Name", name));
            rule1.RuleCriteria.Criterion = criteria.ToString();
            rule1.TacticSet = new TacticSet(session);
            var response = new AlternatePropertyTactic(session);
            response.TargetType = new PersistedType(session);
            response.TargetType.ObjectType = typeof(TestType);
            response.PropertyName = "Name";
            response.NewProperty = "String2";
            rule1.TacticSet.Tactic.Add(response);
            ruleSet.Rules.Add(rule1);

            BusinessRule rule2 = new BusinessRule(session);
            rule2.RuleCriteria = new FilterCriteria(session);
            GroupOperator criteria2 = new GroupOperator(new BinaryOperator("Name", name + "False"));
            rule2.RuleCriteria.Criterion = criteria2.ToString();
            ruleSet.Rules.Add(rule2);

            TestType actualResult = new TestType(session);
            actualResult.Name = name;
            actualResult.String2 = expectedResult;

            Assert.AreNotEqual(expectedResult, actualResult.Name);
            ruleSet.Execute(objectSpace, actualResult);

            Assert.AreEqual(expectedResult, actualResult.Name);
        }
    }
}
