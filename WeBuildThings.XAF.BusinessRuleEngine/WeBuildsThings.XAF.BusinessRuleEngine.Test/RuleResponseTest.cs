﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JonBuildsThings.Utilities;
using DevExpress.Xpo;
using System.Configuration;
using DevExpress.Persistent.BaseImpl;
using System.Reflection;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon.Reflection;
using JonBuildsThings.HierarchicalRulesEngine;

namespace JonBuildsThings.Tests
{
    [TestClass]
    public class RuleResponseTest
    {
        private Session Session { get { return XAFInitialization.Session; } }

        [TestMethod]
        public void TestExecuteUsingNestedProperty()
        {
            string nameValue = "HelloWorld";
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "ReferenceType.Name";
            ruleResponse.HardValue = nameValue;
            TestType actualResult = new TestType(Session) { Name = "Main Object"};
            actualResult.ReferenceType = new TestType(Session) { Name = "Nested Object" };
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(nameValue, actualResult.ReferenceType.Name);
        }

        [TestMethod]
        public void TestExecuteUsingEnum()
        {
            string nameValue = "Status2";
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Enum";
            ruleResponse.HardValue = nameValue;
            TestType actualResult = new TestType(Session);
            actualResult.ReferenceType = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Enum.ToString(), nameValue);
        }


        [TestMethod]
        public void TestExecuteUsingString()
        {
            string nameValue = "HelloWorld";
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Name";
            ruleResponse.HardValue = nameValue;
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Name, nameValue);
        }

        [TestMethod]
        public void TestExecuteUsingDateTime()
        {
            DateTime dateValue = new DateTime(2015, 10, 25, 1, 30, 14);
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "DateTime";
            ruleResponse.HardValue = dateValue.ToString();
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.DateTime, dateValue);
        }


        [TestMethod]
        public void TestExecuteUsingBoolean()
        {
            bool expectedResult = true;
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Bool";
            ruleResponse.HardValue = expectedResult.ToString();
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Bool, expectedResult);
        }

        [TestMethod]
        public void TestExecuteUsingInt()
        {
            int expectedResult = 12345;
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Int";
            ruleResponse.HardValue = expectedResult.ToString();
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Int, expectedResult);
        }

        [TestMethod]
        public void TestExecuteUsingDecimal()
        {
            decimal expectedResult = 15.543m;
            
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Decimal";
            ruleResponse.HardValue = expectedResult.ToString();
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Decimal, expectedResult);
        }

        [TestMethod]
        public void TestExecuteUsingGuid()
        {
            Guid expectedResult = Guid.NewGuid();
        
            var ruleResponse = new BasicTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "Guid";
            ruleResponse.HardValue = expectedResult.ToString();
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.Guid, expectedResult);
        }

        [TestMethod]
        //Tests that we can set a property to a referenced object
        public void TestExecuteUsingReference()
        {
            TestType expectedResult = new TestType(Session);
            expectedResult.Name = "HelloWorld";
            expectedResult.Save();

            var ruleResponse = new ReferenceTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "ReferenceType";
            PersistedType sourceType = new PersistedType(Session) { ObjectType = expectedResult.GetType() };
            ruleResponse.SourceType = sourceType;
            ruleResponse.TargetOid = expectedResult.Oid;
            TestType actualResult = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(expectedResult, actualResult.ReferenceType);
        }

        [TestMethod]
        public void TestExecuteUsing1stdNestedReference()
        {

            TestType expectedResult = new TestType(Session);
            expectedResult.Name = "HelloWorld";
            expectedResult.Save();

            var ruleResponse = new ReferenceTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "ReferenceType.ReferenceType";
            PersistedType sourceType = new PersistedType(Session) { ObjectType = expectedResult.GetType() };
            ruleResponse.SourceType = sourceType;
            ruleResponse.TargetOid = expectedResult.Oid;
            TestType actualResult = new TestType(Session);
            actualResult.ReferenceType = new TestType(Session);
            actualResult.ReferenceType.ReferenceType = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.ReferenceType.ReferenceType, expectedResult);
        }

        [TestMethod]
        public void TestExecuteUsing2rdNestedReference()
        {
            TestType expectedResult = new TestType(Session);
            expectedResult.Name = "HelloWorld";
            expectedResult.Save();

            var ruleResponse = new ReferenceTactic(Session);
            ruleResponse.TargetType = new PersistedType(Session) { ObjectType = typeof(TestType) };
            ruleResponse.PropertyName = "ReferenceType.ReferenceType.ReferenceType";
            PersistedType sourceType = new PersistedType(Session) { ObjectType = expectedResult.GetType() };
            ruleResponse.SourceType = sourceType;
            ruleResponse.TargetOid = expectedResult.Oid;
            TestType actualResult = new TestType(Session);
            actualResult.ReferenceType = new TestType(Session);
            actualResult.ReferenceType.ReferenceType = new TestType(Session);
            ruleResponse.Execute(actualResult);
            Assert.AreEqual(actualResult.ReferenceType.ReferenceType.ReferenceType, expectedResult);
        }
    }
}
