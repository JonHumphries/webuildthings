﻿using DevExpress.Xpo;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon;
using JonBuildsThings.DevXCommon.BusinessObjects.Common;
using JonBuildsThings.DevXCommon.Utilities;
using JonBuildsThings.HierarchicalRulesEngine;
using JonBuildsThings.HierarchicalRulesEngine.Test;
using JonBuildsThings.Utilities;
using System.Configuration;
using System.Reflection;

namespace JonBuildsThings.Tests
{
    internal static class XAFInitialization
    {
        internal static Assembly[] Assembly = new Assembly[] { typeof(BusinessRuleSet).Assembly, typeof(RuleResponseTest).Assembly, typeof(TestType).Assembly };


        private static Session _Session;
        public static Session Session
        {
            get
            {
                if (_Session == null)
                {
                    _Session = ConnectionHelper.CreateSession(ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString, Assembly);
                }
                return _Session;
            }
        }

    }
}
