﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DevExpress.Xpo;
using JonBuildsThings.Utilities;
using System.Configuration;
using JonBuildsThings.HierarchicalRulesEngine;
using System.Reflection;
using JonBuildsThings.DevXCommon.Utilities;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon.BusinessObjects.Common;
using JonBuildsThings.WorkflowEngine;

namespace JonBuildsThings.Tests
{
    [TestClass]
    public class WorkflowTests
    {
        #region Test Types
        private class TestTask1 : Task
        {
            public TestTask1(Session session) : base(session) { }
        }

        private class TestTask2 : Task
        {

            public TestTask2(Session session) : base(session) {}
            
        }

        internal class TestWorkflow: Workflow
        {

            public override Task CreateFirstTask(Session session)
            {
                return new TestTask1(session);
            }

            public override void OnTaskCompleted(Session session, Type taskType, CompletionMethod completionMethod)
            {
                if (taskType == typeof(TestTask1) && completionMethod == CompletionMethod.Approve)
                {
                    Add(new TestTask2(session));
                }
                else
                {
                    Close(WorkStatus.Completed);
                }
            }

            public TestWorkflow(Session session) : base(session) {}
        }
        #endregion Test Types

        private Session Session { get { return XAFInitialization.Session; } }
        
        /// <summary>
        /// Confirms that we can add a Task of a different type to an open task.
        /// </summary>
        [TestMethod]
        public void AddNewTaskType()
        {
            Workflow testWorkflow = new TestWorkflow(Session);
            testWorkflow.Add(new TestTask2(Session));
            int actualResult = testWorkflow.Tasks.Where(n => n.GetType() == typeof(TestTask2)).Count();
            Assert.AreEqual(1, actualResult);
        }

        /// <summary>
        /// Confirms that we cannot add a second instance of an open task.
        /// </summary>
        [TestMethod]
        public void AddExistingTaskWhenOpen()
        {
            Workflow testWorkflow = new TestWorkflow(Session);
            testWorkflow.Add(new TestTask1(Session));
            int actualResult = testWorkflow.Tasks.Where(n => n.GetType() == typeof(TestTask1)).Count();
            Assert.AreEqual(1, actualResult);
        }

        /// <summary>
        /// Confirms that we can add a second instance of a task when the first instance is completed.
        /// </summary>
        [TestMethod]
        public void AddExistingTaskWhenComplete()
        {
            Workflow testWorkflow = new TestWorkflow(Session);
            testWorkflow.Tasks.FirstOrDefault().Status = WorkStatus.Completed;
            testWorkflow.Add(new TestTask1(Session));
            int actualResult = testWorkflow.Tasks.Where(n => n.GetType() == typeof(TestTask1)).Count();
            Assert.AreEqual(2, actualResult);
        }

    }
}
