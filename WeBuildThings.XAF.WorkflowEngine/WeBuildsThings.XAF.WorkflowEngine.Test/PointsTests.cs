﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JonBuildsThings.WorkflowEngine.Test
{
    [TestClass]
    public class PointsTests
    {
        [TestMethod]
        public void TaskSameDay()
        {
            var actualResult = PriorityHelper.CalculateTaskPriorityPoints(0, WorkStatus.Pending, WorkFlowPriority.Normal, DateTime.Now);
            Assert.AreEqual(5, actualResult);
        }
        
        [TestMethod]
        public void TaskOneDayOld()
        {
            var actualResult = PriorityHelper.CalculateTaskPriorityPoints(0, WorkStatus.Pending, WorkFlowPriority.Normal, DateTime.Now.AddDays(-1));
            Assert.AreEqual(6, actualResult);
        }

        [TestMethod]
        public void TaskHalfDayOld()
        {
            var actualResult = PriorityHelper.CalculateTaskPriorityPoints(0, WorkStatus.Pending, WorkFlowPriority.Normal, DateTime.Now.AddHours(-12));
            Assert.AreEqual(5, actualResult);
        }
    }
}
