﻿using DevExpress.Xpo;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon;
using JonBuildsThings.DevXCommon.BusinessObjects.Common;
using JonBuildsThings.DevXCommon.Utilities;
using JonBuildsThings.HierarchicalRulesEngine;
using JonBuildsThings.Utilities;
using JonBuildsThings.WorkflowEngine;
using JonBuildsThings.WorkflowEngine.Test;
using System.Configuration;
using System.Reflection;

namespace JonBuildsThings.Tests
{
    internal static class XAFInitialization
    {
        private static Assembly[] assembly = new Assembly[] { typeof(BusinessRuleSet).Assembly, typeof(Workflow).Assembly, typeof(SystemSettings).Assembly, typeof(UserDefinedWorkflowTest).Assembly, typeof(TestType).Assembly, typeof(JonBuildsThings.Tests.WorkflowTests.TestWorkflow).Assembly};

        private static Session _Session;
        public static Session Session
        {
            get
            {
                if (_Session == null)
                {
                    _Session = ConnectionHelper.CreateSession(ConfigurationManager.ConnectionStrings["TestConnectionString"].ConnectionString, assembly);
                }
                return _Session;
            }
        }

    }
}
