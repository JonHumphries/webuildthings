﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using JonBuildsThings.WorkflowEngine.UserDefinedWorkflows;
using System.Reflection;
using System.Linq;
using JonBuildsThings.Utilities;
using DevExpress.Xpo;
using System.Configuration;
using JonBuildsThings.HierarchicalRulesEngine;
using JBTTesting.Module.BusinessObjects;
using JonBuildsThings.DevXCommon.Reflection;
using JonBuildsThings.DevXCommon.Utilities;
using JonBuildsThings.DevXCommon;
using JonBuildsThings.WorkflowEngine;

namespace JonBuildsThings.Tests
{
    [TestClass]
    public class UserDefinedWorkflowTest
    {

        private Session Session { get { return XAFInitialization.Session; } }

        [TestMethod]
        public void UserDefinedWorkflowCreateFirstTask()
        {
            var userDefinedWorkflow = new UserDefinedWorkflow(Session);
            var workflowConfiguration = new WorkflowConfiguration(Session);
            var taskType = new PersistedType(Session) { ObjectType = typeof(Task1) };
            workflowConfiguration.FirstTaskType = taskType;
            var workflowConfigurationItem = new BasicWorkflowConfigurationItem(Session);
            
            var nextTaskType = new PersistedType(Session) { ObjectType = typeof(Task2) };
            workflowConfigurationItem.TaskType = taskType;
            workflowConfigurationItem.CompletionMethod = CompletionMethod.Approve;
            workflowConfigurationItem.NextTaskType = nextTaskType;
            workflowConfiguration.WorkflowConfigurationItem.Add(workflowConfigurationItem);
            userDefinedWorkflow.WorkflowConfiguration = workflowConfiguration;

            bool taskFound = userDefinedWorkflow.Tasks.Where(n => n.GetType() == typeof(Task1)).Any();
            bool commentFound = (userDefinedWorkflow.Comments.Count > 0);
            Assert.IsTrue(taskFound && !commentFound);
        }

        [TestMethod]
        public void UserDefinedWorkflowTaskApproved()
        {
            using (UnitOfWork uow = new UnitOfWork(Session.ObjectLayer))
            {
                var userDefinedWorkflow = new UserDefinedWorkflow(uow);
                var workflowConfiguration = new WorkflowConfiguration(uow);
                var taskType = new PersistedType(uow) { ObjectType = typeof(Task1) };
                workflowConfiguration.FirstTaskType = taskType;
                var workflowConfigurationItem = new BasicWorkflowConfigurationItem(uow);

                var nextTaskType = new PersistedType(uow) { ObjectType = typeof(Task2) };
                workflowConfigurationItem.TaskType = taskType;
                workflowConfigurationItem.CompletionMethod = CompletionMethod.Approve;
                workflowConfigurationItem.NextTaskType = nextTaskType;
                workflowConfiguration.WorkflowConfigurationItem.Add(workflowConfigurationItem);
                userDefinedWorkflow.WorkflowConfiguration = workflowConfiguration;

                userDefinedWorkflow.ExecuteNextTask(uow);
                bool taskFound = userDefinedWorkflow.Tasks.Where(n => n.GetType() == typeof(Task2)).Any();
                Assert.IsTrue(taskFound);
            }
        }
    }
}
