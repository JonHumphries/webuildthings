﻿namespace WeBuildThings.XAF.WorkflowEngine.Controllers
{
    partial class Task_Controller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Approve = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Reject = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Execute = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.CancelWorkflow = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ClaimTask = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Transfer = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // Approve
            // 
            this.Approve.Caption = "Approve";
            this.Approve.Category = "RecordEdit";
            this.Approve.ConfirmationMessage = null;
            this.Approve.Id = "Approve";
            this.Approve.ImageName = "Action_Validation_Validate";
            this.Approve.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.Approve.ToolTip = null;
            this.Approve.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Approve_Execute);
            // 
            // Reject
            // 
            this.Reject.Caption = "Reject";
            this.Reject.Category = "RecordEdit";
            this.Reject.ConfirmationMessage = null;
            this.Reject.Id = "Reject";
            this.Reject.ImageName = "BO_Attention";
            this.Reject.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.Reject.ToolTip = null;
            this.Reject.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Reject_Execute);
            // 
            // Execute
            // 
            this.Execute.Caption = "Execute";
            this.Execute.Category = "RecordEdit";
            this.Execute.ConfirmationMessage = null;
            this.Execute.Id = "Execute";
            this.Execute.ImageName = "Action_Debug_Start";
            this.Execute.ToolTip = null;
            this.Execute.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Execute_Execute);
            // 
            // CancelWorkflow
            // 
            this.CancelWorkflow.Caption = "Cancel Workflow";
            this.CancelWorkflow.Category = "RecordEdit";
            this.CancelWorkflow.ConfirmationMessage = "Are you sure you want to cancel the entire workflow?";
            this.CancelWorkflow.Id = "CancelWorkflow";
            this.CancelWorkflow.ImageName = "Action_CloseAllWindows";
            this.CancelWorkflow.ToolTip = null;
            this.CancelWorkflow.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.CancelWorkflow_Execute);
            // 
            // ClaimTask
            // 
            this.ClaimTask.Caption = "Claim Task";
            this.ClaimTask.Category = "RecordEdit";
            this.ClaimTask.ConfirmationMessage = null;
            this.ClaimTask.Id = "ClaimTask";
            this.ClaimTask.ImageName = "BO_Rules";
            this.ClaimTask.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.ClaimTask.ToolTip = null;
            this.ClaimTask.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Claim_Execute);
            // 
            // Transfer
            // 
            this.Transfer.AcceptButtonCaption = null;
            this.Transfer.CancelButtonCaption = null;
            this.Transfer.Caption = "Transfer";
            this.Transfer.Category = "RecordEdit";
            this.Transfer.ConfirmationMessage = null;
            this.Transfer.Id = "Transfer";
            this.Transfer.ImageName = "Action_Change_State";
            this.Transfer.ToolTip = null;
            this.Transfer.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.Transfer_CustomizePopupWindowParams);
            this.Transfer.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.Transfer_Execute);
            // 
            // Task_Controller
            // 
            this.Actions.Add(this.Approve);
            this.Actions.Add(this.Reject);
            this.Actions.Add(this.Execute);
            this.Actions.Add(this.CancelWorkflow);
            this.Actions.Add(this.ClaimTask);
            this.Actions.Add(this.Transfer);
            this.TargetObjectType = typeof(WeBuildThings.WorkflowEngine.Task);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction Approve;
        private DevExpress.ExpressApp.Actions.SimpleAction Reject;
        private DevExpress.ExpressApp.Actions.SimpleAction Execute;
        private DevExpress.ExpressApp.Actions.SimpleAction CancelWorkflow;
        private DevExpress.ExpressApp.Actions.SimpleAction ClaimTask;
        private DevExpress.ExpressApp.Actions.PopupWindowShowAction Transfer;
    }
}
