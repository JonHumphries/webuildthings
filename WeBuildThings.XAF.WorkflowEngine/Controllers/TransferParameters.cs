﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Common.Extensions;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.WorkflowEngine.Controllers
{
    [NonPersistent]
    [ModelDefault("Caption","Transfer Workflow")]
    public class TransferParameter: BaseObject
    {
        #region Fields
        private XPCollection<PersistedType> _Types;
        private PersistedType _Type;
        private XRole _NewRole;
        private XUser _NewUser;
        #endregion Fields

        #region Constructors
        public TransferParameter(Session session) : base(session) { }
        #endregion Consructors

        #region Properties

        [DataSourceProperty("Types")]
        public PersistedType Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        public XRole NewRole
        {
            get { return _NewRole; }
            set { SetPropertyValue("NewRole", ref _NewRole, value); }
        }

        public XUser NewUser
        {
            get { return _NewUser; }
            set { SetPropertyValue("NewUser", ref _NewUser, value); }
        }

        [Browsable(false)]
        public XPCollection<PersistedType> Types
        {
            get
            {
                if (_Types == null || !_Types.Any())
                {
                    _Types = new XPCollection<PersistedType>(Session, false);
                    var types = new XPCollection<PersistedType>(Session);
                    var max = types.Count;
                    for (int i = 0; i < max; i++)
                    {
                        if (types[i].ObjectType.ContainsType(typeof(WeBuildThings.WorkflowEngine.Task)) && !_Types.Where(n=> n.ObjectType == types[i].ObjectType).Any())
                        {
                            _Types.Add(types[i]);
                        }
                    }
                }
                return _Types;
            }
        }
        #endregion Properties

    }
}
