﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using System.Collections;
using System.Linq;
using WeBuildThings.WorkflowEngine;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.BusinessObjects.Common;

namespace WeBuildThings.XAF.WorkflowEngine.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class Task_Controller : ViewController
    {
        public Task_Controller()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            //TODO deactivate execute, claim task, cancel workflow
            base.OnActivated();
            this.CancelWorkflow.Active.SetItemValue("CancelWorkflow", false);
            this.Execute.Active.SetItemValue("Execute", true);
            this.ClaimTask.Active.SetItemValue("ClaimTask", true);
            // Perform various tasks depending on the target View.
        }

        private void Approve_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            int max = View.SelectedObjects.Count;
            if (View.SelectedObjects != null && max > 0)
            {
                for (int i = 0; i < max; i++)
                {
                    var task = View.SelectedObjects[i] as Task;
                    task.Approve();
                }
            }
        }

        private void Claim_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            int max = View.SelectedObjects.Count;
            if (View.SelectedObjects != null && max > 0)
            {
                for (int i = 0; i < max; i++)
                {
                    var task = View.SelectedObjects[i] as Task;
                    task.ClaimTask();
                }
            }
        }

        private void Reject_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            int max = View.SelectedObjects.Count;
            if (View.SelectedObjects != null && max > 0)
            {
                for (int i = 0; i < max; i++)
                {
                    var task = View.SelectedObjects[i] as Task;
                    task.Reject();
                }
            }
        }

        private void CancelWorkflow_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            int max = View.SelectedObjects.Count;
            if (View.SelectedObjects != null && max > 0)
            {
                for (int i = 0; i < max; i++)
                {
                    var task = View.SelectedObjects[i] as Task;
                    task.CancelWorkflow();
                }
            }

        }

        private void Execute_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            int max = View.SelectedObjects.Count;
            if (View.SelectedObjects != null && max > 0)
            {
                for (int i = 0; i < max; i++)
                {
                    var task = View.SelectedObjects[i] as Task;
                    if (task != null)
                    {
                        if (task.GetType().GetInterfaces().Contains(typeof(IObjectSpaceRequired)))
                        {
                            ((IObjectSpaceRequired)task).ObjectSpace = View.ObjectSpace;
                            task.Execute(((XPObjectSpace)View.ObjectSpace).Session);
                        }
                        else
                        {
                            task.Execute(task.Session);
                        }
                    }
                }
            }
            else
            {
                throw new ArgumentException("You must select a task to execute.");
            }
        }

        private void Transfer_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace(typeof(TransferParameter));
            var session = ((XPObjectSpace)objectSpace).Session;
            e.View = Application.CreateDetailView(objectSpace, new TransferParameter(session));
        }

        private void Transfer_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var selectedTasks = new ArrayList();
            var transferParameter = (TransferParameter)e.PopupWindowViewCurrentObject;
            if (e.SelectedObjects.Count > 0 && e.SelectedObjects[0] is XafDataViewRecord)
            {
                foreach (var selectedObject in e.SelectedObjects)
                {
                    selectedTasks.Add((Task)ObjectSpace.GetObject(selectedObject));
                }
            }
            else
            {
                selectedTasks = (ArrayList)e.SelectedObjects;
            }
            foreach (Task task in selectedTasks)
            {
                Task newTask = null;
                if (transferParameter.Type != null)
                {
                    newTask = TaskFactory.CreateTask(task.Session, transferParameter.Type.ObjectType, task.Workflow);
                }
                var referencedTask = (newTask != null) ? newTask : task;
                if (transferParameter.NewRole != null)
                {
                    var role = referencedTask.Session.GetObjectByKey(transferParameter.NewRole.ClassInfo, transferParameter.NewRole.Oid);
                    referencedTask.AssignedRole = role as XRole;
                }
                if (transferParameter.NewUser != null)
                {
                    referencedTask.AssignedUser = WorkflowUser.CreateWorkflowUser(referencedTask.Session, transferParameter.NewUser);
                }
                if (newTask != null)
                {
                    task.Status = WorkStatus.Completed;
                }
            }
            ObjectSpace.CommitChanges();
            ObjectSpace.Refresh();
        }
    }
}
