using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using WeBuildThings.WorkflowEngine;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class Scheduler_Controller : ViewController
    {
        public Scheduler_Controller()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ForceTrigger_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            if (View.SelectedObjects != null)
            {
                using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.ObjectLayer))
                {
                    foreach (Scheduler trigger in View.SelectedObjects)
                    {
                        trigger.CreateWorkflow();
                    }
                    uow.CommitChanges();
                }
            }
        }

    }
}
