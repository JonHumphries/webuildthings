﻿namespace WeBuildThings.Controllers
{
    partial class DefaultViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.NextTaskAction = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // NextTaskAction
            // 
            this.NextTaskAction.AcceptButtonCaption = null;
            this.NextTaskAction.CancelButtonCaption = null;
            this.NextTaskAction.Caption = "NextTask";
            this.NextTaskAction.ConfirmationMessage = null;
            this.NextTaskAction.Id = "NextTask";
            this.NextTaskAction.ImageName = "Action_Reload";
            this.NextTaskAction.ToolTip = null;
            this.NextTaskAction.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.NextTask_Execute);
            // 
            // DefaultViewController
            // 
            this.Actions.Add(this.NextTaskAction);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Root;

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction NextTaskAction;
    }
}
