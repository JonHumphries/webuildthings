﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Common;
using WeBuildThings.Exceptions;
using WeBuildThings.WorkflowEngine;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.WorkflowEngine;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DefaultViewController : ViewController
    {
        #region Fields
        private int _TaskCount = 0;
        private Timer _Timer;
        private int _TimerSeconds;
        private Guid _TaskOid;
        #endregion Fields

        #region Properties
        public int TaskCount
        {
            get { return _TaskCount; }
            set { if (_TaskCount != value) { _TaskCount = value; OnTaskCountChanged(); } }
        }

        private Session Session
        {
            get
            {
                Session result = null;
                var objectSpace = View.ObjectSpace as XPObjectSpace;
                if (objectSpace != null)
                {
                    result = objectSpace.Session;
                }
                return result;
            }
        }

        [Browsable(false)]
        public Timer Timer
        {
            get { return _Timer; }
            set { if (Timer != value) _Timer = value; }
        }

        [Browsable(false)]
        public int TimerSeconds
        {
            get { return _TimerSeconds; }
            set { if (TimerSeconds != value) { _TimerSeconds = value; OnTimerSecondsChanged(); } }
        }

        [Browsable(false)]
        public Guid TaskOid
        {
            get { return _TaskOid; }
            set { if (_TaskOid != value) { _TaskOid = value;} }
        }
        #endregion Properties

        public DefaultViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewType = ViewType.Any;
            this.NextTaskAction.Active.SetItemValue("NextTaskAction", true);
        }

        #region Event Methods
        protected override void OnActivated()
        {
            base.OnActivated();
            if (View.Id != "ChangePasswordOnLogonParameters_DetailView")
            {
                ObjectSpace.ObjectSaved -= OnObjectSpaceSaved;
                ObjectSpace.ObjectSaved += OnObjectSpaceSaved;
                var workflowSystemSettings = SystemSettings.GetInstance<WorkflowSystemSettings>(Session);
                if (workflowSystemSettings != null)
                {
                    TimerSeconds = workflowSystemSettings.NextTaskButtonRefreshRate;
                }
            }
        }

        protected override void OnViewControlsCreated()
        {
            if (View.Id != "ChangePasswordOnLogonParameters_DetailView")
            {
                SetNumberOfTasks();
                base.OnViewControlsCreated();
            }
            // Access and customize the target View control.
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        //void OnObjectSpaceRefreshing(object sender, System.ComponentModel.CancelEventArgs e)
        //{
        //    SetNumberOfTasks();
        //}

        private void OnObjectSpaceSaved(object sender, ObjectManipulatingEventArgs e)
        {
            SetNumberOfTasks();
        }

        private void OnTaskCountChanged()
        {
            SetMyTaskCaption();
        }

        private void OnTimerSecondsChanged()
        {
            SetTimer();
        }
        #endregion

        #region Set Methods
        private void SetTimer()
        {
            Timer = new Timer(0, 0, TimerSeconds);
        }

        private void SetMyTaskCaption()
        {
            string caption = string.Format("Next Task {0}{1}", Environment.NewLine, (TaskCount <= 5 ? TaskCount.ToString() : "5+"));
            if (this.Actions["NextTask"].Caption != caption)
            {
                this.Actions["NextTask"].Caption = caption;
                View.Refresh();
            }
        }

        private void SetNumberOfTasks()
        {
            if (Session != null)
            {
                int tasks = 0;
                if (Timer == null)
                {
                    SetTimer();
                }
                if (Timer.Check())
                {
                    using (UnitOfWork uow = new UnitOfWork(Session.ObjectLayer))
                    {
                        var currentUser = SecuritySystem.CurrentUser as XUser;
                        if (currentUser != null)
                        {
                            var workflowUser = WorkflowUser.GetWorkflowUserByUserID(uow, currentUser.Oid);
                            if (workflowUser == null)
                            {
                                var user = uow.FindObject<XUser>(new BinaryOperator("Oid", currentUser.Oid));
                                workflowUser = WorkflowUser.CreateWorkflowUser(uow, user);
                            }
                            tasks = workflowUser.TaskCount;
                        }
                    }
                    TaskCount = (tasks) > 5 ? 6 : tasks;
                    SetTimer();
                }
            }
        }

        #endregion Set Methods

        private void NextTask_Execute(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)View.ObjectSpace).Session.ObjectLayer))
            {
                XPCollection<Task> tasks = Task.FindTasks(uow, WorkflowUser.GetWorkflowUserByUserID(uow, SecuritySystem.CurrentUserId));
                if (SecuritySystem.CurrentUserId == null)
                {
                    throw new Exception("Unable to detect current user");
                }
                else
                {
                    List<Task> claimedTasks = tasks.Where(ct => ct.AssignedUser == WorkflowUser.GetWorkflowUserByUserID(uow, SecuritySystem.CurrentUserId.ToString()) && (ct.Status == WorkStatus.InProgress || ct.Status == WorkStatus.Pending)).OrderByDescending(ct => ct.PriorityPoints).ToList();
                    List<Task> unclaimedTasks = tasks.Where(ct => ct.AssignedUser == null).OrderByDescending(ct => ct.PriorityPoints).ToList();
                    if (tasks != null && tasks.Count > 0)
                    {
                        IObjectSpace objectSpace = Application.CreateObjectSpace();
                        TaskOid = (claimedTasks.Count > 0) ? claimedTasks[0].Oid : unclaimedTasks[0].Oid;
                        Task objWorkflowTask = objectSpace.GetObjectByKey<Task>(TaskOid);
                        objWorkflowTask.ClaimTask();
                        objWorkflowTask.Save();
                        e.View = Application.CreateDetailView(objectSpace, objWorkflowTask);
                        e.DialogController.Cancelling -= OnCancelAction_Clicked;
                        e.DialogController.Cancelling += OnCancelAction_Clicked;
                    }
                    else
                    {
                        throw new NoAvailableTasksException("You have no available tasks right now.");
                    }
                }
                uow.CommitChanges();
            }
        }

        private void OnCancelAction_Clicked(object sender, EventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            Task objWorkflowTask = objectSpace.GetObjectByKey<Task>(TaskOid);
            objWorkflowTask.ReleaseTask();
            objectSpace.CommitChanges();
        }
    }
}
