namespace WeBuildThings.Controllers
{
    partial class Scheduler_Controller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ForceTrigger = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ForceTrigger
            // 
            this.ForceTrigger.Caption = "Force Trigger";
            this.ForceTrigger.ConfirmationMessage = null;
            this.ForceTrigger.Id = "ForceTrigger";
            this.ForceTrigger.ImageName = "Action_Debug_Start";
            this.ForceTrigger.ToolTip = null;
            this.ForceTrigger.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ForceTrigger_Execute);
            // 
            // Scheduler_Controller
            // 
            this.Actions.Add(this.ForceTrigger);
            this.TargetObjectType = typeof(WeBuildThings.WorkflowEngine.Scheduler);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ForceTrigger;
    }
}
