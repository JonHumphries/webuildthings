﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.WorkflowEngine
{
    public class UpdateActiveWorkflowPointsTask: Task
    {
        public UpdateActiveWorkflowPointsTask(Session session) : base(session)
        { }

        public override void AfterConstruction()
        {
            Status = WorkStatus.PendingSystem;
            AutomatedTask = true;
            base.AfterConstruction();
        }

        public override void Execute(Session session)
        {
            using (var tasks = new XPCollection<Task>(session, new InOperator("Status",Workflow.OpenStatus)))
            {
                foreach (var task in tasks)
                {
                    task.SetPriorityPoints();
                    task.Save();
                }
            }
            base.Execute(session);
            Approve();
        }
    }

    public class UpdateInactiveWorkflowPointsTask: Task
    {
        public UpdateInactiveWorkflowPointsTask(Session session):base(session)
        {
        }

        public override void AfterConstruction()
        {
            Status = WorkStatus.PendingSystem;
            AutomatedTask = true;
            base.AfterConstruction();
        }

        public override void Execute(Session session)
        {
            using (var tasks = new XPCollection<Task>(session, new GroupOperator(GroupOperatorType.And, new BinaryOperator("PriorityPoints",0,BinaryOperatorType.NotEqual),new NotOperator(new InOperator("Status", Workflow.OpenStatus)))))
            {
                foreach (var task in tasks)
                {
                    task.PriorityPoints = 0;
                    task.Save();
                }
            }
            base.Execute(session);
            Approve();
        }
    }
}
