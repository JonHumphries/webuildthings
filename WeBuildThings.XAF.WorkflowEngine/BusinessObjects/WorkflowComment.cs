﻿using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.WorkflowEngine
{
    public class WorkflowComment :Comment
    {
        #region Const
        internal const string WorkflowCommentType = "Workflow Engine Comment";
        #endregion Const

        #region Fields
        private Workflow _Workflow;
        #endregion Fields

        #region Constructors
        public WorkflowComment(Session session):base(session)
        { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Type = LookupItem.CreateLookupItem(Session, Comment.CommentTypeListName, WorkflowCommentType, "Indicates this comment was created by the Workflow Engine");
        }
        #endregion Constructors

        #region Properties

        [Association("Task-WorkflowComments", typeof(WeBuildThings.WorkflowEngine.Task))]
        public XPCollection<WeBuildThings.WorkflowEngine.Task> Tasks
        {
            get { return GetCollection<WeBuildThings.WorkflowEngine.Task>("Tasks"); }
        }

        [Association("Workflow-WorkflowComments", typeof(Workflow))]
        public Workflow Workflow
        {
            get { return _Workflow; }
            set { SetPropertyValue("Workflow", ref _Workflow, value); }
        }
        #endregion Properties

    }
}
