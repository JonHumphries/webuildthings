﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.WorkflowEngine;

namespace WeBuildThings.WorkflowEngine
{
    [DefaultProperty("User")]
    public class WorkflowUser: BaseObject
    {
        #region Fields
        private int _TaskCount;
        private DateTime _NextRefreshTime;
        private int _RefreshInterval;
        private XUser _User;
        #endregion Fields

        #region Constructors
        public WorkflowUser(Session session):base(session)
        { }


        public static WorkflowUser CreateWorkflowUser(Session session, XUser user)
        {
            WorkflowUser result = null;
            if (user != null)
            {
                result = session.FindObject<WorkflowUser>(new BinaryOperator("User.Oid", user.Oid));
            }
            if (result == null)
            {
                result = new WorkflowUser(session) { User = user };
                result.Save();
            }
            return result;
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            RefreshInterval = SystemSettings.GetInstance<WorkflowSystemSettings>(Session).TaskCountRefreshInterval;
        }
        #endregion Constructors

        #region Properties
        public int TaskCount
        {
            get { if (NextRefreshTime < DateTime.Now) { SetTaskCount(); } return _TaskCount; }
        }

        [Browsable(false)]
        public DateTime NextRefreshTime
        {
            get { return _NextRefreshTime; }
            set { SetPropertyValue("NextRefreshTime", ref _NextRefreshTime, value); }
        }

        [Browsable(false)]
        public int RefreshInterval
        {
            get { return _RefreshInterval; }
            set { if (_RefreshInterval != value) _RefreshInterval = value; }
        }

        [ModelDefault("AllowEdit","False")]
        public XUser User
        {
            get { return _User; }
            set { SetPropertyValue("User", ref _User, value); }
        }
        #endregion Properties

        #region Set Methods
        private void SetTaskCount()
        {
            XPCollection<Task> tasks = Task.FindTasks(Session, GetWorkflowUserByUserID(Session, SecuritySystem.CurrentUserId));
            _TaskCount = (tasks == null) ? 0 : tasks.Count;
            TimeSpan timeSpan = new TimeSpan(0, 0, RefreshInterval);
            NextRefreshTime = CalcNextRefreshTime(timeSpan);
        }
        #endregion Set Methods

        #region Calc Methods
        public static DateTime CalcNextRefreshTime(TimeSpan refreshInterval)
        {
            return DateTime.Now + refreshInterval;
        }
        #endregion Calc Methods

        #region Public Methods
        public static WorkflowUser GetWorkflowUserByUserID(Session session, object securitySystemCurrentUserID)
        {
            WorkflowUser result = null;
            if (securitySystemCurrentUserID != null && !string.IsNullOrWhiteSpace(securitySystemCurrentUserID.ToString())) //TODO kind of a security violation here but service isn't storing itself as a user :(
            {
                Guid oid = new Guid(securitySystemCurrentUserID.ToString());
                var xuser = session.GetObjectByKey<XUser>(oid, true);
                result = session.FindObject<WorkflowUser>(new BinaryOperator("User", xuser));
                if (result == null)
                {
                    result = CreateWorkflowUser(session, session.GetObjectByKey<XUser>(oid));
                }
            }
            return result;

        }
        #endregion Public Methods
    }
}
