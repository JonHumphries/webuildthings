﻿using DevExpress.Xpo;
using System;

namespace WeBuildThings.XAF.WorkflowEngine.Interfaces
{
    public interface IXAFTask
    {
        string Name { get; set; }
        Guid Oid { get; }
        Session Session { get; }
    }
}
