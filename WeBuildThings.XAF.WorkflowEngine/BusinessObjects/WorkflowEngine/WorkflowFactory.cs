﻿using System;
using System.Reflection;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Utilities;

namespace WeBuildThings.WorkflowEngine
{
    public static class WorkflowFactory
    {

        public static Workflow CreateWorkflow<T>(Session session, Scheduler triggeredBy) where T : BaseObject
        {
            Workflow workflow = CreateWorkflow(session, typeof(T));
            workflow.TriggeredBy = triggeredBy;
            return workflow;
        }

        public static Workflow CreateWorkflow(Session session, Type type)
        {
            Type[] parameterTypes = new Type[] { typeof(Session) };
            ConstructorInfo constructorInfo = type.GetConstructor(parameterTypes);
            object instance = constructorInfo.Invoke(new object[] { session });
            Workflow workflow = instance as Workflow;
            return workflow;
        }
    }
}
