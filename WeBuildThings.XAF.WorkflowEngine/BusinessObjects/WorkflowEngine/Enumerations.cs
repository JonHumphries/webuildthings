﻿
namespace WeBuildThings.WorkflowEngine
{
    public enum CompletionMethod { Approve, Reject, Transfer, Cancel }
    public enum TriggerType { Recurring, Event };
    public enum WorkFlowPriority { Normal = 5, Low = 1, High = 10, Urgent = 50, Immediate = 100 };
    public enum WorkStatus { Pending, Cancelled, Completed, PendingSystem, InProgress, Error = -1 };
    public enum RecurrenceFrequency { Minutes, Hours, Days, Weeks, Months, Years };
        
}
