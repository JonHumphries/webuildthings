using System;
using System.Linq;
using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.Utilities;
using DevExpress.ExpressApp;

namespace WeBuildThings.WorkflowEngine
{
    public class WorkflowEngine : absWorkflowEngine
    {
        public WorkflowEngine(Session session, WorkflowEngineOptions workflowEngineOptions, IObjectSpace objectSpace = null)
            : base(session, workflowEngineOptions, objectSpace)
        {
        }

        public static absWorkflowEngine CreateWorkflowEngine(Session session, WorkflowEngineOptions workflowEngineOptions, IObjectSpace objectSpace = null)
        {
            if (workflowEngineOptions == null)
            {
                workflowEngineOptions = WorkflowEngineOptions.Default();
            }
            absWorkflowEngine workflowEngine = new WorkflowEngine(session, workflowEngineOptions, objectSpace);
            return AddWorkflowEngineOptions(session, workflowEngine, workflowEngineOptions);
        }

        private static absWorkflowEngine AddWorkflowEngineOptions(Session session, absWorkflowEngine workflowEngine, WorkflowEngineOptions workflowEngineOptions)
        {
            if (workflowEngineOptions.RunWorkflows)
            {
                workflowEngine = new RunWorkflowsVariant(session, workflowEngine);
            }
            if (workflowEngineOptions.RunTriggers)
            {
                workflowEngine = new RunTriggersVariant(session, workflowEngine);
            }
            return workflowEngine;
        }

        private class RunWorkflowsVariant : WorkflowEngineVariants
        {
            public RunWorkflowsVariant(Session session, absWorkflowEngine workflowEngine)
                : base(session, workflowEngine)
            {
                Echo.EchoMessage("Run Workflow Added");
            }

            /// <summary>
            /// Executes the next _Workflow in the queue
            /// </summary>
            public override void Execute(Session session)
            {
                if (session == null)
                {
                    throw new ArgumentNullException("WorkflowEngine:Execute: Session is null");
                }
                using (UnitOfWork uow = new UnitOfWork(session.ObjectLayer))
                {
                    GroupOperator groupOperator = new GroupOperator(GroupOperatorType.And);
                    groupOperator.Operands.Add(new InOperator("WorkflowStatus", new WorkStatus[] { WorkStatus.PendingSystem }));
                    using (XPCollection<Workflow> workflows = new XPCollection<Workflow>(uow, groupOperator))
                    {
                        if (workflows != null && workflows.Count > 0)
                        {

                            var workflow = workflows.OrderByDescending(n => n.PriorityPoints).FirstOrDefault();
                            if (workflow != null)
                            {
                                try
                                {
                                    Echo.EchoMessage(string.Format("On Workflow {1} Executing Task: {0} ", workflow.CurrentTask.Name, workflow.Name));
                                    workflow.WorkflowStatus = WorkStatus.InProgress;
                                    workflow.Save();
                                    uow.CommitChanges();
                                    workflow.ExecuteNextTask(uow);
                                    //Task.Factory.StartNew(workflow.ExecuteNextTask);  Need to tweak workflow so that it will create its own session within Execution and then execute the next task.
                                    uow.CommitChanges();
                                }
                                catch (Exception e)
                                {
                                    uow.DropChanges();
                                    Echo.EchoMessage(string.Format("Exception On Workflow {1} Executing Task: {0} ", workflow.CurrentTask.Name, workflow.Name));
                                    Echo.EchoMessage(e.Message);
                                    workflow.WorkflowStatus = WorkStatus.Error;
                                    var comment = new WorkflowComment(uow);
                                    comment.Subject = "Critical Failure processing this worklfow";
                                    comment.Text = string.Format("{0}{1}{2}{1}{3}", e.GetType().FullName, Environment.NewLine, e.Message, e.StackTrace);
                                    workflow.Comments.Add(comment);
                                    workflow.Save();
                                    uow.CommitChanges();
                                }
                            }
                            ThreadUtil.StallThread(WorkflowEngineOptions.MillisecondDelay);
                        }
                        else
                        {
                            ThreadUtil.StallThread(1000 * 60);  //if no tasks were found wait 1 minute before rechecking;
                        }
                    }
                }
                
                //each variant is in its own thread
                //_WorkflowEngine.Execute(session);
            }
        }

        private class RunTriggersVariant : WorkflowEngineVariants
        {
            public RunTriggersVariant(Session session, absWorkflowEngine workflowEngine)
                : base(session, workflowEngine)
            {
                Echo.EchoMessage("Run Triggers Added");
            }

            public override void Execute(Session session)
            {
                using (UnitOfWork uow = new UnitOfWork(Session.ObjectLayer))
                {
                    using (XPCollection<Scheduler> triggers = new XPCollection<Scheduler>(uow, new BinaryOperator("Active", true)))
                    {
                        Echo.EchoMessage("Checking Triggers");
                        foreach (Scheduler trigger in triggers)
                        {
                            trigger.Execute();
                            uow.CommitChanges();
                        }
                    }
                }
                ThreadUtil.StallThread(1000 * 60 * 5); //Check triggers every 5 minutes
                //each variant is in its own thread
                //_WorkflowEngine.Execute(session);
            }
        }
    }
}