using System;
using System.Threading;
using DevExpress.Xpo;
using WeBuildThings.Utilities;

namespace WeBuildThings.WorkflowEngine
{
    public abstract class WorkflowEngineVariants : absWorkflowEngine
    {
        protected readonly absWorkflowEngine _WorkflowEngine;
        protected bool RunEngine { get; set; }
        private Thread Worker { get; set; }

        public WorkflowEngineVariants(Session session, absWorkflowEngine workflowEngine)
            : base(session, workflowEngine.WorkflowEngineOptions)
        {
            if (workflowEngine == null)
            {
                throw new ArgumentNullException("Null Workflow Engine received by WorkflowEngineVariants");
            }
            RunEngine = false;
            _WorkflowEngine = workflowEngine;
        }

        public override Session Session
        {
            get { return _WorkflowEngine.Session; }
        }

        public override WorkflowEngineOptions WorkflowEngineOptions
        {
            get { return _WorkflowEngine.WorkflowEngineOptions; }
        }

        public override Echo Echo
        {
            get { return _WorkflowEngine.Echo; }
        }

        public override void Execute(Session session)
        {
            _WorkflowEngine.Execute(session);
            ThreadUtil.StallThread(WorkflowEngineOptions.MillisecondDelay);
        }

        public override void StartEngine()
        {
            RunEngine = true;
            Worker = new Thread(new ThreadStart(this.EngineRunning));
            Worker.Start();
            _WorkflowEngine.StartEngine();
        }

        public override void StopEngine()
        {
            RunEngine = false;
            _WorkflowEngine.StopEngine();
        }

        public void EngineRunning()
        {
            if (Session == null)
            {
                throw new InvalidOperationException("WorkflowEngineVariants.EngineRunning: Session is null and must be established.");
            }
            while (RunEngine)
            {
                Execute(Session);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (Worker != null)
            {
                Worker = null;
            }
            _WorkflowEngine.Dispose();
            base.Dispose(disposing);
        }

        public override string DefaultConnectionString()
        {
            return _WorkflowEngine.DefaultConnectionString();
        }
    }
}
