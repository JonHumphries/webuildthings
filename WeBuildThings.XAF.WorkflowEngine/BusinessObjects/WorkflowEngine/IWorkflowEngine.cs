using System;
using WeBuildThings.Utilities;

namespace WeBuildThings.WorkflowEngine.Interfaces
{
    public interface IWorkflowEngine : IDisposable
    {
        Echo Echo { get; set; }
        void StartEngine();
        void StopEngine();
    }
}
