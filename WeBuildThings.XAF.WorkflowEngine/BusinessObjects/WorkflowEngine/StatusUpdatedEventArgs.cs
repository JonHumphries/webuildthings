using System;

namespace WeBuildThings.WorkflowEngine
{
    public class StatusUpdatedEventArgs : EventArgs
    {
        public WorkStatus OldStatus { get; set; }
        public WorkStatus NewStatus { get; set; }

        public StatusUpdatedEventArgs(WorkStatus oldStatus, WorkStatus newStatus)
        {
            OldStatus = oldStatus;
            NewStatus = newStatus;
        }
    }
}
