using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Utilities;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.BusinessObjects.Common;

namespace WeBuildThings.WorkflowEngine
{

    [DefaultClassOptions]
    public abstract class Workflow : NonOptimisticBaseObject, IStandardTracking
    {
        #region Consts
        public const string FEATURE_OVERRIDE_POINTS = "Feature: Override Points";
        #endregion Consts

        public static WorkStatus[] OpenStatus = new WorkStatus[] { WorkStatus.Pending, WorkStatus.PendingSystem, WorkStatus.InProgress, WorkStatus.Error };
        public static WorkStatus[] ClosedStatus = new WorkStatus[] { WorkStatus.Cancelled, WorkStatus.Completed };

        #region Fields
        internal const string FirstTaskException = "First Task Exception";
        private WorkflowEngine _WorkflowEngine;
        private string _Name;
        private Scheduler _TriggeredBy;
        private DateTime _DateModified;
        private DateTime _DateCreated;
        private XUser _CreatedBy;
        private XUser _ModifiedBy;
        private int _PriorityPoints;
        private WorkStatus _WorkflowStatus;
        private WorkFlowPriority _Priority = WorkFlowPriority.Normal;
        private Task _CurrentTask;
        private Task _PreviousTask;
        #endregion

        #region Constructors
        public Workflow(Session session) : base(session) 
        {
            Comments.CollectionChanged -= InvokeCommentCollectionChanged;
            Comments.CollectionChanged += InvokeCommentCollectionChanged;
            Tasks.CollectionChanged -= InvokeTaskCollectionChanged;
            Tasks.CollectionChanged += InvokeTaskCollectionChanged;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            SetName();
            this.SetCreatedUserAndDate();
            try
            {
                Task task = CreateFirstTask(Session);
                task.Workflow = this;
                SetStatus();
            }
            catch (Exception e)
            {
                WorkflowComment comment = new WorkflowComment(Session);
                comment.Subject = "Error Creating First Task";
                comment.Text = e.Message;
                comment.Workflow = this;
                comment.Save();
            }
            SetPriorityPoints();
        }
        #endregion

        #region Properties
        [ModelDefault("AllowEdit", "false")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        public Scheduler TriggeredBy
        {
            get { return _TriggeredBy; }
            set { SetPropertyValue("TriggeredBy", ref _TriggeredBy, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        [Aggregated, Association("Workflow-Task", typeof(Task))]
        public XPCollection<Task> Tasks
        {
            get { return GetCollection<Task>("Tasks"); }
        }

        public WorkFlowPriority Priority
        {
            get { return _Priority; }
            set { if (SetPropertyValue("Priority", ref _Priority, value) && !IsLoading) { OnPriorityUpdated(); } }
        }

        public WorkStatus WorkflowStatus
        {
            get { return _WorkflowStatus; }
            set { if (SetPropertyValue("WorkflowStatus", ref _WorkflowStatus, value) && !IsLoading) { OnWorkflowStatusChanged(); } }
        }

        public int PriorityPoints
        {
            get { return _PriorityPoints; }
            set
            {
                if (IsLoading)
                {
                    SetPropertyValue("PriorityPoints", ref _PriorityPoints, value);
                }
                else if (SetPropertyValue("PriorityPoints", ref _PriorityPoints, value) && !IsLoading)
                {
                    OnPriorityPointsUpdated();
                }
            }
        }

        [ModelDefault("AllowEdit","False")]
        public Task CurrentTask
        {
            get { return _CurrentTask; }
            set { SetPropertyValue("CurrentTask", ref _CurrentTask, value); }
        }

        [ModelDefault("AllowEdit","False")]
        public Task PreviousTask
        {
            get { return _PreviousTask; }
            set { SetPropertyValue("PreviousTask", ref _PreviousTask, value);}
        }

        [Browsable(false)]
        [NonPersistent]
        public WorkflowEngine WorkflowEngine
        {
            get { return _WorkflowEngine; }
            set { if (_WorkflowEngine != value) _WorkflowEngine = value; }
        }

        [Association("Workflow-WorkflowComments", typeof(WorkflowComment))]
        public XPCollection<WorkflowComment> Comments
        {
            get { return GetCollection<WorkflowComment>("Comments"); }
        }
        #endregion Properties

        #region Events
        public event EventHandler PriorityUpdated;
        public event EventHandler PriorityPointsUpdated;
        public event EventHandler CommentCollectionChanged;
        public event EventHandler TaskCollectionChanged;
        #endregion

        #region Public Methods
        public void Add(Task task)
        {
            if (!Tasks.Where(n => n.GetType() == task.GetType() && OpenStatus.Contains(n.Status)).Any())
            {
                Tasks.Add(task);
            }
            SetCurrentTask();
        }
        #endregion Public Methods

        #region Event methods
        protected virtual void OnPriorityUpdated()
        {
            SetPriorityPoints();
            if (PriorityUpdated != null)
            {
                PriorityUpdated.Invoke(this, EventArgs.Empty);
            }
        }

        protected virtual void OnPriorityPointsUpdated()
        {
            SetTaskPriorityPoints();
            if (PriorityPointsUpdated != null)
            {
                PriorityPointsUpdated.Invoke(this, EventArgs.Empty);
            }
        }

        protected virtual void OnCommentCollectionChanged(Object sender, XPCollectionChangedEventArgs e) { }

        protected virtual void OnTaskCollectionChanged(Object sender, XPCollectionChangedEventArgs e) { SetCurrentTask(); }
        #endregion Event methods

        #region Invokation Methods
        private void InvokeCommentCollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            var handler = CommentCollectionChanged;
            if (handler != null)
            {
                handler.Invoke(sender, e);
            }
            OnCommentCollectionChanged(sender, e);
        }

        private void InvokeTaskCollectionChanged(object sender, XPCollectionChangedEventArgs e)
        {
            var handler = TaskCollectionChanged;
            if (handler != null)
            {
                handler.Invoke(sender, e);
            }
            OnTaskCollectionChanged(sender, e);
        }
        #endregion Invokation Methods

        #region Set Methods
        public virtual void SetPriorityPoints()
        {
            PriorityPoints = PriorityHelper.CalculateWorkflowPriorityPoints(WorkflowStatus, Priority, DateCreated);
        }

        public void SetStatus()
        {
            var currentTask = Tasks.OrderByDescending(n => n.DateCreated).FirstOrDefault();
            if (currentTask != null)
            {
                WorkflowStatus = currentTask.Status;
            }
        }

        public virtual void SetTaskPriorityPoints()
        {
            foreach (Task task in Tasks)
            {
                task.SetPriorityPoints();
            }
        }

        public virtual void SetTaskPriority()
        {
            foreach (Task task in Tasks)
            {
                if (!task.Closed)
                {
                    task.Priority = Priority;
                }
            }
        }

        protected virtual void SetTasksClosed()
        {
            foreach (Task task in Tasks.Where(n=> n != CurrentTask && OpenStatus.Contains(n.Status)))
            {
                task.AssignToCurrentUser();
                task.Status = WorkStatus.Completed;
            }
        }

        public void SetName()
        {
            Name = this.GetType().UserFriendlyName().Replace("Workflow", "").Trim();
        }
        #endregion Set Methods

        #region Workflow execution methods

        public abstract Task CreateFirstTask(Session session);

        public virtual void OnTaskCompleted(Session session, Task task, CompletionMethod completionMethod)
        {
        }

        public virtual void Close(WorkStatus workflowStatus)
        {
            if (workflowStatus == WorkStatus.Cancelled || workflowStatus == WorkStatus.Completed)
            {
                WorkflowStatus = workflowStatus;
                SetTasksClosed();
            }
            else
            {
                throw new NotSupportedException(string.Format("{0} is not a valid Work Status for the Workflow Close Method", workflowStatus));
            }
        }

        public virtual void ExecuteNextTask(UnitOfWork uow)
        {
            Task task = null;
            try
            {
                task = CurrentSystemTask();
                if (task != null)
                {
                    task.Status = WorkStatus.InProgress;
                    task.ClaimTask();
                    task.Save();
                    uow.CommitChanges();
                    if (task.GetType().GetInterfaces().Contains(typeof(IObjectSpaceRequired)))
                    {
                        ((IObjectSpaceRequired)task).ObjectSpace = WorkflowEngine.ObjectSpace;
                    }
                    try
                    {
                        task.Execute(uow);
                        task.Save();
                    }
                    catch (Exception exception)
                    {
                        task.Status = WorkStatus.Error;
                        task.NewComment("Error Executing Task", string.Format("{0}{1}{0}{2}", Environment.NewLine, exception.Message, exception.StackTrace));
                        this.WorkflowStatus = WorkStatus.Error;
                        task.AssignedRole = Session.FindObject<XRole>(new BinaryOperator("Name", "Developer"));
                        task.Save();
                    }
                }
            }
            catch (Exception exception)
            {
                task.Status = WorkStatus.Error;
                task.NewComment(FirstTaskException, string.Format("{0}{1}{0}{2}", Environment.NewLine, exception.Message, exception.StackTrace));
                this.WorkflowStatus = WorkStatus.Error;
                task.AssignedRole = Session.FindObject<XRole>(new BinaryOperator("Name", "Developer"));
                task.Save();
            }
            finally
            {
                Save();
            }
        }

        internal void SetCurrentTask()
        {
            CurrentTask = Tasks.Where(n => n.Status == WorkStatus.Pending || n.Status == WorkStatus.PendingSystem).OrderByDescending(n => n.PriorityPoints).FirstOrDefault();
        }

        public Task CurrentSystemTask()
        {
            return Tasks.Where(n => n.Status == WorkStatus.PendingSystem).OrderByDescending(n => n.PriorityPoints).FirstOrDefault();
        }

        protected virtual void OnWorkflowStatusChanged()
        {
            if (this.WorkflowStatus != WorkStatus.InProgress)
            {
                foreach (var task in Tasks.Where(n => !ClosedStatus.Contains(n.Status)))
                {
                    task.Status = WorkflowStatus;
                }
            }
        }

        protected override void OnSaving()
        {
            SetStatus();
            this.SetModifiedUserAndDate();
            base.OnSaving();
        }
        #endregion

        #region Validation Methods
        public static bool ValidateFeatureOverridePriorityPoints(bool throwException, XUser user)
        {
            bool result = user.ContainsRole(FEATURE_OVERRIDE_POINTS);
            if (throwException && !result)
            {
                throw new AccessViolationException("You do not have access to override priority points");
            }
            return result;
        }
        #endregion Validation Methods
    }
}