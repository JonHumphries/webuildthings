using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Utilities;
using WeBuildThings.WorkflowEngine.BusinessObjects;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.WorkflowEngine
{
    /// <summary>
    /// A task can be assigned to either a user or the system.
    /// System tasks also contain an Execute method which the implementation will call
    /// when the task is executed by the workflow engine.
    /// </summary>
    [DefaultClassOptions]
    [ImageName("BO_Task")]
    public class Task : NonOptimisticBaseObject, IStandardTracking
    {
        #region Fields
        private bool _Closed;
        private string _Name;
        private WorkFlowPriority _Priority = WorkFlowPriority.Normal;
        private int _PriorityPoints;
        private bool _AutomatedTask;
        private DateTime _CompleteTime;
        private DateTime _StartTime;
        private WorkStatus _Status;
        private WorkflowUser _AssignedUser;
        private Document _UserInstructions;
        private XRole _AssignedRole;
        private Workflow _Workflow;
        private XUser _ModifiedBy;
        private XUser _CreatedBy;
        private DateTime _DateCreated;
        private DateTime _DateModified;
        #endregion

        #region Constructors
        /// <summary>
        /// Base XAF constructor
        /// </summary>
        /// <param name="session">DevExpress XAF Session</param>
        public Task(Session session) : base(session) { }

        /// <summary>
        /// Parameterless constructor for serialization
        /// </summary>
        public Task() : base() { }

        /// <summary>
        /// Sets the name, initial status, priority points, and role. 
        /// May be overriden to initialize other values.
        /// </summary>
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            SetName();
            SetInitialStatus();
            this.SetCreatedUserAndDate();
            SetPriorityPoints();
            this.AssignedRole = TaskConfiguration.GetRoleForTask(Session, GetType());
        }

        #endregion

        #region Properties
        /// <summary>
        /// User friendly name for this task.  Will be set automatically based on the Type name
        /// </summary>
        [ModelDefault("AllowEdit", "False")]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        /// <summary>
        /// Quick boolean value, not visible to users, for filtering out closed tasks.
        /// </summary>
        [Browsable(false)]
        [ModelDefault("AllowEdit", "False")]
        public bool Closed
        {
            get { return _Closed; }
            set { if (SetPropertyValue("Closed", ref _Closed, value) && !IsLoading) { OnClosedChanged(); } }
        }

        /// <summary>
        /// Current assigned user of this task
        /// </summary>
        public WorkflowUser AssignedUser
        {
            get { return _AssignedUser; }
            set { SetPropertyValue("AssignedUser", ref _AssignedUser, value); }
        }

        /// <summary>
        /// Current assigned role for this task.
        /// Uses WeBuildThings.XAF.Common's XRole object
        /// </summary>
        public XRole AssignedRole
        {
            get { return _AssignedRole; }
            set { SetPropertyValue("AssignedRole", ref _AssignedRole, value); }
        }

        /// <summary>
        /// For linking user training materials.
        /// </summary>
        public Document UserInstructions
        {
            get { return _UserInstructions; }
            set { SetPropertyValue("UserInstructions", ref _UserInstructions, value); }
        }

        /// <summary>
        /// Not visible to users.  This should be set to True
        /// when the task is to be completed by the system.
        /// </summary>
        [Browsable(false)]
        public bool AutomatedTask
        {
            get { return _AutomatedTask; }
            set { if (SetPropertyValue("automatedType", ref _AutomatedTask, value) && !IsLoading) { OnAutomatedTaskChanged(); } }
        }

        /// <summary>
        /// Date the task was created
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("CreationDate", ref _DateCreated, value); }
        }

        /// <summary>
        /// Last modification date of the task
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("LastModifiedDate", ref _DateModified, value); }
        }

        /// <summary>
        /// Initial creator of the task.
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        /// <summary>
        /// Last user to modify the task.
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }

        /// <summary>
        /// Start time of the task (based on when the Claim Task method was executed).
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime StartTime
        {
            get { return _StartTime; }
            set { SetPropertyValue("StartTime", ref _StartTime, value); }
        }

        /// <summary>
        /// Date/Time of when the task was completed (based on when Approve, Reject or Transfer was executed.
        /// </summary>
        [ModelDefault("AllowEdit", "false")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime CompleteTime
        {
            get { return _CompleteTime; }
            set { SetPropertyValue("CompleteTime", ref _CompleteTime, value); }
        }

        /// <summary>
        /// Current status of the task
        /// </summary>
        public WorkStatus Status
        {
            get { return _Status; }
            set
            {
                StatusUpdatedEventArgs eventArgs = new StatusUpdatedEventArgs(_Status, value);
                if (SetPropertyValue("Status", ref _Status, value) && !IsLoading) { OnStatusChanged(eventArgs); }
            }
        }

        /// <summary>
        /// Current priority of the task
        /// </summary>
        public WorkFlowPriority Priority
        {
            get { return _Priority; }
            set { if (SetPropertyValue("Priority", ref _Priority, value) && !IsLoading) { OnPriorityUpdated(); } }
        }

        /// <summary>
        /// Number of points this task is currently worth.  
        /// Tasks with more points are completed before tasks with fewer points.
        /// </summary>
        public int PriorityPoints
        {
            get { return _PriorityPoints; }
            set { if (SetPropertyValue("priorityPriority", ref _PriorityPoints, value) && !IsLoading) { OnPriorityPointsUpdated(); } }
        }

        /// <summary>
        /// Tasks are associated to Workflows so that Workflows inherited by a 
        /// Different object can see the current _Status of the entire program.
        /// </summary>
        [Association("Workflow-Task", typeof(Workflow)), Aggregated]
        public Workflow Workflow
        {
            get { return _Workflow; }
            set { Workflow oldWorkflow = _Workflow; if (SetPropertyValue("Workflow", ref _Workflow, value) && !IsLoading) { OnWorkflowChanged(oldWorkflow); } }
        }

        /// <summary>
        /// Comments for this task. These should be added via the NewComment method which
        /// will ensure that the comment also exists on the workflow
        /// </summary>
        [Association("Task-WorkflowComments", typeof(WorkflowComment))]
        public XPCollection<WorkflowComment> Comments
        {
            get { return GetCollection<WorkflowComment>("Comments"); }
        }
        #endregion

        #region Events
        /// <summary>
        /// Fired when the priority of the task is changed.
        /// </summary>
        public event EventHandler PriorityUpdated;

        /// <summary>
        /// Fired when the priority points of the task is changed.
        /// </summary>
        public event EventHandler PriorityPointsUpdated;

        /// <summary>
        /// Fired when the status of the task is changed.
        /// </summary>
        public event EventHandler<StatusUpdatedEventArgs> StatusUpdated;
        #endregion

        #region Event Methods
        /// <summary>
        /// Overrideable - Fired when the workflow of this task is changed.
        /// Calls SetWorkflowComments, SetWorkflowStatus, SetWorkflowCurrentTask (old workflow) and SetworkflowCurrentTask(new workflow)
        /// </summary>
        /// <param name="oldWorkflow"></param>
        protected virtual void OnWorkflowChanged(Workflow oldWorkflow)
        {
            SetWorkflowComments();
            SetWorkflowStatus();
            SetWorkflowCurrentTask(oldWorkflow);
            SetWorkflowCurrentTask(Workflow);
        }

        private void OnAutomatedTaskChanged()
        {
            SetInitialStatus();
        }

        /// <summary>
        /// Overrideable - called when the status of this task is changed.
        /// Calls SetClosed, SetWorkflowStatus, Invokes Status Changed event, ands the status changed to comments
        /// </summary>
        /// <param name="eventArgs"></param>
        protected virtual void OnStatusChanged(StatusUpdatedEventArgs eventArgs)
        {
            SetClosed();
            SetWorkflowStatus();
            PostStatusUpdate(eventArgs.OldStatus.ToString(), eventArgs.NewStatus.ToString());
            EventHandler<StatusUpdatedEventArgs> handler = StatusUpdated;
            if (handler != null)
            {
                handler.Invoke(this, eventArgs);
            }
        }

        /// <summary>
        /// Overrideable - Called when the priority points is updated.
        /// Invokes the priority points updated event
        /// </summary>
        protected virtual void OnPriorityPointsUpdated()
        {
            if (PriorityPointsUpdated != null)
            {
                PriorityPointsUpdated.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Overrideable - Called with the priority is updated.
        /// Invokes the Priority updated event
        /// </summary>
        protected virtual void OnPriorityUpdated()
        {
            SetPriorityPoints();
            if (PriorityUpdated != null)
            {
                PriorityUpdated.Invoke(this, EventArgs.Empty);
            }
        }

        private void OnClosedChanged()
        {
            SetCompleteDate();
        }
        #endregion

        #region Set Methods

        private void SetWorkflowCurrentTask()
        {
            if (Workflow.CurrentTask != this)
            {
                Workflow.CurrentTask = this;
            }
        }

        private void SetWorkflowCurrentTask(Workflow workflow)
        {
            if (workflow != null)
            {
                workflow.SetCurrentTask();
            }
        }

        /// <summary>
        /// Uses the supplied session to find all tasks assigned to the user or user's roles
        /// and the status is Pending
        /// </summary>
        /// <param name="session">DevExpress XAF Session</param>
        /// <param name="workflowUser">User whose potential tasks are being returned</param>
        /// <returns>A collection of pending tasks</returns>
        public static XPCollection<Task> FindTasks(Session session, WorkflowUser workflowUser)
        {
            CriteriaOperator criteriaOperator = FindTaskFilterCriteria(workflowUser);
            XPCollection<Task> result = new XPCollection<Task>(session, criteriaOperator);
            return result;
        }

        /// <summary>
        /// Uses the supplied session to find all of a particular type of task that is either assigned to the user or user's roles
        /// and the status is Pending
        /// </summary>
        /// <param name="session">DevExpress XAF Session</param>
        /// <param name="workflowUser">User whose potential tasks are being returned</param>
        /// <param name="taskName">The name of the task</param>    
        /// <returns>A collection of pending tasks</returns>
        public static XPCollection<Task> FindTasks(Session session, WorkflowUser workflowUser, string taskName)
        {
            var criteriaOperator = new GroupOperator(GroupOperatorType.And, FindTaskFilterCriteria(workflowUser), new BinaryOperator("Name", taskName));
            XPCollection<Task> result = new XPCollection<Task>(session, criteriaOperator);
            return result;
        }

        public static CriteriaOperator FindTaskFilterCriteria(WorkflowUser workflowUser)
        {
            GroupOperator result = new GroupOperator(GroupOperatorType.And);
            if (workflowUser != null)
            {
                GroupOperator groupOperator = new GroupOperator(GroupOperatorType.Or, new BinaryOperator("AssignedUser.Oid", workflowUser.Oid), FindTaskFilterCriteriaBasedOnRoles(workflowUser));

                result.Operands.Add(groupOperator);
                result.Operands.Add(new InOperator("Status", new WorkStatus[1] { WorkStatus.Pending }));
            }
            return result;
        }

        private static CriteriaOperator FindTaskFilterCriteriaBasedOnRoles(WorkflowUser workflowUser)
        {
            CriteriaOperator result = null;
            if (workflowUser.User == null)
            {
                workflowUser.User = workflowUser.Session.GetObjectByKey<XUser>(((XUser)SecuritySystem.CurrentUser).Oid);
            }
            if (workflowUser.User != null)
            {
                result = new GroupOperator(GroupOperatorType.And, new NullOperator("AssignedUser"), new InOperator("AssignedRole.Oid", workflowUser.User.Roles.FlattenHierarchy<SecuritySystemRole>(p => true, (SecuritySystemRole n) => { return n.ChildRoles; }).Select(n => n.Oid).ToArray()));
            }
            return result;
        }

        /// <summary>
        /// Calculates the number of points this tasks is worth.  May be overriden to customize the calculation.
        /// </summary>
        public virtual void SetPriorityPoints()
        {
            int workflowPriorityPoints = (Workflow == null) ? 0 : Workflow.PriorityPoints;
            PriorityPoints = PriorityHelper.CalculateTaskPriorityPoints(workflowPriorityPoints, Status, Priority, DateCreated);
        }

        private void SetInitialStatus()
        {
            Status = (AutomatedTask) ? WorkStatus.PendingSystem : WorkStatus.Pending;
        }

        private void SetCompleteDate()
        {
            if (Closed && CompleteTime == DateTime.MinValue)
            {
                CompleteTime = DateTime.Now;
            }
        }

        /// <summary>
        /// Sets the name of the task based on the implementation's dataype with the word Task removed.
        /// </summary>
        public void SetName()
        {
            Name = this.GetType().UserFriendlyName().Replace("Task", "").Trim(); ;
        }

        private void SetWorkflowStatus()
        {
            if (Workflow != null)
            {
                Workflow.SetStatus();
            }
        }

        private void SetWorkflowComments()
        {
            if (Workflow != null)
            {
                foreach (var comment in Comments)
                {
                    comment.Workflow = Workflow;
                    comment.Save();
                }
            }
        }

        public virtual bool TaskClaimedValidation()
        {
            return AssignedUser == WorkflowUser.GetWorkflowUserByUserID(Session, SecuritySystem.CurrentUserId) ? true : false;
        }

        private void SetClosed()
        {
            Closed = (Status == WorkStatus.Cancelled || Status == WorkStatus.Completed);
        }

        protected virtual void PostStatusUpdate(string oldStatus, string newStatus)
        {
            if (Workflow != null)
            {
                NewComment(string.Format("Workflow: {0} Task: {1}", Workflow.Name, Name), string.Format("Status changed from {0} to {1}", oldStatus.SeparateCamelCase(), newStatus.SeparateCamelCase()));
            }
        }
        #endregion

        #region Task completion methods
        public virtual void CancelWorkflow()
        {
            this.Status = WorkStatus.Cancelled;
        }

        /// <summary>
        /// If the task can be claimed, this will claim and Save it.
        /// </summary>
        public virtual void ClaimTask()
        {
            if (!Closed && Status != WorkStatus.InProgress)
            {
                AssignToCurrentUser();
                Status = WorkStatus.InProgress;
                Save();
            }
            SetWorkflowCurrentTask();
        }

        ///<summary>
        ///If the task is claimed, this will return it to the queue.
        ///</summary>
        public virtual void ReleaseTask()
        {
            if (!Closed && AssignedUser != null)
            {
                AssignedUser = null;
                Status = WorkStatus.Pending;
                StartTime = DateTime.MinValue;
                Save();
            }
        }


        /// <summary>
        /// Assigns this task to the current User
        /// </summary>
        public void AssignToCurrentUser()
        {
            AssignedUser = WorkflowUser.GetWorkflowUserByUserID(Session, SecuritySystem.CurrentUserId);
            StartTime = DateTime.Now;
        }

        /// <summary>
        /// This method executes the Approve task logic
        /// </summary>
        public void Approve()
        {
            if (AssignedUser == null || AssignedUser != WorkflowUser.GetWorkflowUserByUserID(Session, SecuritySystem.CurrentUserId))
            {
                AssignToCurrentUser();
            }
            Status = WorkStatus.Completed;
            Workflow.OnTaskCompleted(Session, this, CompletionMethod.Approve);
        }

        /// <summary>
        /// This method executes the Reject task logic
        /// </summary>
        public void Reject()
        {
            if (AssignedUser == null || AssignedUser != WorkflowUser.GetWorkflowUserByUserID(Session, SecuritySystem.CurrentUserId))
            {
                ClaimTask();
            }
            Status = WorkStatus.Completed;
            Workflow.OnTaskCompleted(Session, this, CompletionMethod.Reject);
        }

        /// <summary>
        /// This method implements the Transfer Task logic
        /// </summary>
        public void TransferTask(WorkflowUser selectedUser, XRole selectedRole)
        {
            if (selectedRole == null && selectedUser == null)
                throw new Exception("Please select User or Role");
            else
            {
                AssignedUser = selectedUser;
                AssignedRole = selectedRole;
                Workflow.OnTaskCompleted(Session, this, CompletionMethod.Transfer);
            }
        }

        /// <summary>
        /// Creates a new comment with the given message and adds it to the task and _Workflow;
        /// </summary>
        public void NewComment(string subject, string message)
        {
            var comment = new WorkflowComment(Session);
            comment.Subject = subject;
            comment.Text = message;
            comment.Save();
            this.Add(comment);
        }

        /// <summary>
        /// Creates a new comment with the given message and adds it to the task and _Workflow;
        /// </summary>
        public void NewComment(string message)
        {
            NewComment(Name, message);
        }

        /// <summary>
        /// Adds a comment to both this task and its _Workflow
        /// </summary>
        public void Add(WorkflowComment comment)
        {
            this.Comments.Add(comment);
            if (this.Workflow != null)
            {
                Workflow.Comments.Add(comment);
            }
        }

        protected override void OnSaving()
        {
            this.SetModifiedUserAndDate();
            base.OnSaving();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Override this on system completed tasks with the work the system should be executing.
        /// </summary>
        /// <param name="session"></param>
        public virtual void Execute(Session session) { }
        #endregion Public Methods
    }
}