﻿using DevExpress.Xpo;
using System;
using System.Reflection;
using WeBuildThings.WorkflowEngine;

namespace WeBuildThings.XAF.WorkflowEngine
{
    public class TaskFactory
    {
        public static Task CreateTask<T>(Session session, Workflow workflow) where T : DevExpress.Persistent.BaseImpl.BaseObject
        {
            Task result = CreateTask(session, typeof(T), workflow);
            return result;
        }

        public static Task CreateTask(Session session, Type type, Workflow workflow)
        {
            Type[] parameterTypes = new Type[] { typeof(Session) };
            ConstructorInfo constructorInfo = type.GetConstructor(parameterTypes);
            object instance = constructorInfo.Invoke(new object[] { session });
            Task result = instance as Task;
            result.Workflow = workflow;
            return result;
        }
    }
}
