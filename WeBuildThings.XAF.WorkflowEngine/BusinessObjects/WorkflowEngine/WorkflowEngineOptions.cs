using DevExpress.Xpo;
using WeBuildThings.Utilities;

namespace WeBuildThings.WorkflowEngine
{
    public class WorkflowEngineOptions
    {
        public bool MultiThreadWorkflows { get; set; }
        public bool MultiThreadTriggers { get; set; }
        public bool RunTriggers { get; set; }
        public bool RunWorkflows { get; set; }
        public EchoOptions EchoOptions { get; set; }
        public int MillisecondDelay { get; set; }
        public bool EchoRetrieveQueueCalled { get; set; }
        public bool EchoExecuteCalled { get; set; }


        public WorkflowEngineOptions(EchoOptions echoOptions, bool multiThreadWorkflows, bool multiThreadTriggers, bool runTriggers, bool runWorkflows, int millisecondDelay, bool echoRetrieveQueueCalled, bool echoExecuteCalled)
        {
            EchoOptions = echoOptions;
            MultiThreadWorkflows = multiThreadWorkflows;
            MultiThreadTriggers = multiThreadTriggers;
            RunTriggers = runTriggers;
            RunWorkflows = runWorkflows;
            MillisecondDelay = MillisecondDelay;
            EchoRetrieveQueueCalled = echoRetrieveQueueCalled;
            EchoExecuteCalled = echoExecuteCalled;
        }

        public static WorkflowEngineOptions Default(Session session = null)
        {
            return new WorkflowEngineOptions(EchoOptions.Default(session), true, true, true, true, 1000, false, false);
        }
    }
}