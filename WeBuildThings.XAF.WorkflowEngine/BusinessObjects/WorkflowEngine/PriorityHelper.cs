using System;
using System.Linq;

namespace WeBuildThings.WorkflowEngine
{
    public class PriorityHelper
    {
        public static double PointsFromTime(DateTime creationDate)
        {
            TimeSpan timeSpan = DateTime.Now - creationDate;
            return (timeSpan.TotalHours / 24);
        }

        public static int CalculateWorkflowPriorityPoints(WorkStatus status, WorkFlowPriority priority, DateTime creationDate)
        {
            int result = 0;
            if (Workflow.OpenStatus.Contains(status))
            {
                result = (int)((double)priority + PriorityHelper.PointsFromTime(creationDate));
            }
            return result;
        }

        public static int CalculateTaskPriorityPoints(int workflowPriorityPoints, WorkStatus status, WorkFlowPriority priority, DateTime creationDate)
        {
            int result = 0;
            if (Workflow.OpenStatus.Contains(status))
            {
                result = (int)((double)priority + PriorityHelper.PointsFromTime(creationDate) + (double)workflowPriorityPoints);
            }
            return result;
        }
    }
}
