using System;
using DevExpress.Xpo;
using WeBuildThings.Utilities;
using WeBuildThings.WorkflowEngine.Interfaces;
using DevExpress.ExpressApp;

namespace WeBuildThings.WorkflowEngine
{
    public class absWorkflowEngine : IWorkflowEngine, IDisposable
    {
        #region Fields
        private bool _Disposed;
        private Session _Session;
        private Echo _Echo;
        private WorkflowEngineOptions _WorkflowEngineOptions;
        private IObjectSpace _ObjectSpace;
        #endregion Fields

        #region Properties
        public virtual Session Session
        {
            get { return _Session; }
            set { _Session = value; }
        }
        
        public IObjectSpace ObjectSpace
        {
            get { return _ObjectSpace; }
            set { if (_ObjectSpace != value) _ObjectSpace = value; }
        }

        public virtual WorkflowEngineOptions WorkflowEngineOptions 
        {
            get { return _WorkflowEngineOptions; }
            set { if (_WorkflowEngineOptions != value) _WorkflowEngineOptions = value; }
        }

        public virtual Echo Echo 
        {
            get { return _Echo; }
            set { if (_Echo != value) _Echo = value; }
        }

        public virtual string DefaultConnectionString()
        {
            throw new NotImplementedException();
        }
        #endregion Properties

        #region Constructors
        
        public absWorkflowEngine(Session session, WorkflowEngineOptions workflowEngineOptions, IObjectSpace objectSpace = null)
        {
            Session = session;
            WorkflowEngineOptions = workflowEngineOptions;
            ObjectSpace = objectSpace;
            Echo = new Echo(workflowEngineOptions.EchoOptions);
        }
        #endregion Constructors

        #region Public Methods
        public virtual void Execute(Session session)
        {
            if (WorkflowEngineOptions.EchoExecuteCalled) Echo.EchoMessage("Workflow Engine - Execute Called");
        }

        public virtual void StartEngine()
        {
            Echo.EchoMessage("Workflow Engine - Started Engine Called");
        }

        public virtual void StopEngine()
        {
            Echo.EchoMessage("Workflow Engine - Stop Engine Called");
        }
        #endregion Public Methods

        #region IDisposable Implementation
        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_Disposed)
            {
                if (disposing)
                {
                    if (Session != null)
                    {
                        Session.Dispose();
                        Session = null;
                    }
                    _Disposed = true;
                }
            }
        }
        #endregion IDisposable Implementation

    }
}
