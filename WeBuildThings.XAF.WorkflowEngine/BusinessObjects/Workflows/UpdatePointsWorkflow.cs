﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.WorkflowEngine
{
    public class UpdatePointsWorkflow: Workflow
    {

        public UpdatePointsWorkflow(Session session):base(session)
        { }

        public override Task CreateFirstTask(Session session)
        {
            return new UpdateActiveWorkflowPointsTask(session);
        }

        public override void OnTaskCompleted(Session session, Task task, CompletionMethod completionMethod)
        {
            var taskType = task.GetType();
            if (taskType == typeof(UpdateActiveWorkflowPointsTask) && completionMethod == CompletionMethod.Approve)
            {
                Add(new UpdateInactiveWorkflowPointsTask(session));
            }
            else if (taskType == typeof(UpdateInactiveWorkflowPointsTask) && completionMethod == CompletionMethod.Approve)
            {
                Close(WorkStatus.Completed);
            }
            else if (completionMethod == CompletionMethod.Cancel || completionMethod == CompletionMethod.Reject)
            {
                Close(WorkStatus.Cancelled);
            }
            else
            {
                throw new NotImplementedException(string.Format("No instructions for task {0} and completion method {1}", taskType.Name, completionMethod));
            }
            base.OnTaskCompleted(session, task, completionMethod);
        }
    }
}
