using System;
using System.Collections.Generic;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.WorkflowEngine
{
    public class TimerCriteria : BaseObject
    {
        #region Fields
        private string _Name;
        private string _Description;
        private DateTime _NextRunDateTime;
        private bool _Saturday = true;
        private bool _Friday = true;
        private bool _Thursday = true;
        private bool _Wednesday = true;
        private bool _Sunday = true;
        private bool _Tuesday = true;
        private bool _Monday = true;
        private int _Every = 1;
        private RecurrenceFrequency _Frequency;
        #endregion Fields

        #region Constructors
        public TimerCriteria(Session session)
            : base(session)
        {
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        [ModelDefault("AllowEdit", "False")]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public RecurrenceFrequency Frequency
        {
            get { return _Frequency; }
            set { if (SetPropertyValue("Frequency", ref _Frequency, value)) { SetNextRunDateTime(); } }
        }

        public int Every
        {
            get { return _Every; }
            set { if (SetPropertyValue("Every", ref _Every, value)) { SetNextRunDateTime(); } }
        }

        public bool Sunday
        {
            get { return _Sunday; }
            set { if (SetPropertyValue("Sunday", ref _Sunday, value)) { SetNextRunDateTime(); } }
        }

        public bool Monday
        {
            get { return _Monday; }
            set { if (SetPropertyValue("Monday", ref _Monday, value)) { SetNextRunDateTime(); } }
        }

        public bool Tuesday
        {
            get { return _Tuesday; }
            set { if (SetPropertyValue("Tuesday", ref _Tuesday, value)) { SetNextRunDateTime(); } }
        }

        public bool Wednesday
        {
            get { return _Wednesday; }
            set { if (SetPropertyValue("Wednesday", ref _Wednesday, value)) { SetNextRunDateTime(); } }
        }

        public bool Thursday
        {
            get { return _Thursday; }
            set { if (SetPropertyValue("Thursday", ref _Thursday, value)) { SetNextRunDateTime(); } }
        }

        public bool Friday
        {
            get { return _Friday; }
            set { if (SetPropertyValue("Friday", ref _Friday, value)) { SetNextRunDateTime(); } }
        }

        public bool Saturday
        {
            get { return _Saturday; }
            set { if (SetPropertyValue("Saturday", ref _Saturday, value)) { SetNextRunDateTime(); } }
        }

        [ModelDefault("EditMask", "MM/dd/yyyy hh:mm tt")]
        public DateTime NextRunDateTime
        {
            get
            {
                if (_NextRunDateTime == DateTime.MinValue) { _NextRunDateTime = DateTime.Now; }
                return _NextRunDateTime;
            }
            set { if (SetPropertyValue("NextRunDateTime", ref _NextRunDateTime, value)) { OnNextRunDateTimeChanged(); } }
        }

        public event EventHandler NextRunDateTimeChanged;
        #endregion Properties

        #region Business Logic Methods

        protected virtual void OnNextRunDateTimeChanged()
        {
            SetDescription();
            EventHandler handler = NextRunDateTimeChanged;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        public void SetNextRunDateTime()
        {
            DateTime tempDateTime = (NextRunDateTime < DateTime.Now) ? DateTime.Now : NextRunDateTime;
            if (Frequency == RecurrenceFrequency.Minutes)
            {
                tempDateTime += new TimeSpan(0, Every, 0);
            }
            else if (Frequency == RecurrenceFrequency.Hours)
            {
                tempDateTime += new TimeSpan(Every, 0, 0);
            }
            else if (Frequency == RecurrenceFrequency.Days)
            {
                tempDateTime += new TimeSpan(Every, 0, 0, 0);
            }
            else if (Frequency == RecurrenceFrequency.Years)
            {
                tempDateTime.AddYears(Every);
            }
            else if (Frequency == RecurrenceFrequency.Weeks)
            {
                int startDayOfWeek = (int)tempDateTime.DayOfWeek;
                int nextDay = 0;
                foreach (int day in DaysSet())
                {
                    if (startDayOfWeek <= day)
                    {
                        nextDay = day - startDayOfWeek;
                        break;
                    }
                }
                tempDateTime.AddDays(nextDay);
            }
            else
            {
                throw new NotImplementedException(string.Format("{0} has not been implemented in TimerCriteria.SetNextRunDateTime", Frequency));
            }
            NextRunDateTime = tempDateTime;
        }

        private List<int> DaysSet()
        {
            List<int> tempList = new List<int>();
            if (Sunday) tempList.Add(0);
            if (Monday) tempList.Add(1);
            if (Tuesday) tempList.Add(2);
            if (Wednesday) tempList.Add(3);
            if (Thursday) tempList.Add(4);
            if (Friday) tempList.Add(5);
            if (Saturday) tempList.Add(6);
            tempList.Sort();
            return tempList;
        }

        public void SetDescription()
        {
            Description = string.Format("Starting {0} and repeating {1} {2}", NextRunDateTime.ToString("f"), EveryFrequencyAsString(), DaysAsString(), Environment.NewLine);
        }

        private string EveryFrequencyAsString()
        {
            string myDescription = string.Empty;
            //EveryDay is covered by DaysAsString()
            if (Every == 1 && Frequency == RecurrenceFrequency.Minutes) myDescription = " every minute";
            else if (Every == 1 && Frequency == RecurrenceFrequency.Hours) myDescription = " every hour";
            else if (Every == 1 && Frequency == RecurrenceFrequency.Days) myDescription = " every";
            else if (Every == 1 && Frequency == RecurrenceFrequency.Months) myDescription = " every month";
            else if (Every == 1 && Frequency == RecurrenceFrequency.Years) myDescription = " every year";
            else if (Every == 3 && Frequency == RecurrenceFrequency.Months) myDescription = " every quarter";
            else
            {
                myDescription = string.Format(" every {0} {1}", Every, Frequency);
            }
            return myDescription;
        }

        private string DaysAsString()
        {
            string dayDescription = string.Empty;
            //Not Repeating
            if (!Sunday && !Monday && !Tuesday && !Wednesday && !Thursday && !Friday && !Saturday) dayDescription = string.Empty;
            //Repeating Every Day
            else if (Sunday && Monday && Tuesday && Wednesday && Thursday && Friday && Saturday) dayDescription = " every day";
            //Repeating Weekdays
            else if (Monday && Tuesday && Wednesday && Thursday && Friday && !Saturday && !Sunday) dayDescription = "weekdays";
            //Repeating Specific Days
            else
            {
                dayDescription = "on ";
                dayDescription += (Sunday) ? "Sunday, " : string.Empty;
                dayDescription += (Monday) ? "Monday, " : string.Empty;
                dayDescription += (Tuesday) ? "Tuesday, " : string.Empty;
                dayDescription += (Wednesday) ? "Wednesday, " : string.Empty;
                dayDescription += (Thursday) ? "Thursday, " : string.Empty;
                dayDescription += (Friday) ? "Friday, " : string.Empty;
                dayDescription += (Saturday) ? "Saturday, " : string.Empty;
                dayDescription = dayDescription.Substring(0, dayDescription.Length - 2);
            }
            return dayDescription;
        }

        public bool ExecuteCriteria()
        {
            return (NextRunDateTime <= DateTime.Now) ? true : false;
        }
        #endregion Business Logic Methods
    }
}