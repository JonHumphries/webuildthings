﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WeBuildThings.WorkflowEngine.BusinessObjects;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.WorkflowEngine.BusinessObjects
{
    [DefaultClassOptions]
    public class DocumentTaskConfiguration : Document
    {
        #region Constructors

        public DocumentTaskConfiguration(Session session)
            : base(session)
        {
        }

        #endregion

        #region Properties

        [Association("TaskConfiguration-Documents", typeof(TaskConfiguration))]
        public XPCollection<TaskConfiguration> TaskConfigurations => GetCollection<TaskConfiguration>("TaskConfigurations");

        #endregion
    }
}