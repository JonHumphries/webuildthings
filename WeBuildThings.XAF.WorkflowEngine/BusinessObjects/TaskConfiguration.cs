﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.XAF.BusinessRuleEngine;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.WorkflowEngine.BusinessObjects;

namespace WeBuildThings.WorkflowEngine.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Type")]
    public class TaskConfiguration : BaseObject
    {
        #region Fields
        private PersistedType _Type;
        private BusinessRuleSet _RuleSet;
        private XRole _AssignedRole;
        #endregion Fields
         
        #region Constructors
        public TaskConfiguration(Session session) : base (session)
        {

        }
        #endregion Constructors

        #region Properties
        public PersistedType Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        public BusinessRuleSet RuleSet
        {
            get { return _RuleSet; }
            set { SetPropertyValue("RuleSet", ref _RuleSet, value); }
        }

        public XRole AssignedRole
        {
            get { return _AssignedRole; }
            set { SetPropertyValue("AssignedRole", ref _AssignedRole, value); }
        }

        [Association("TaskConfiguration-Documents", typeof(DocumentTaskConfiguration))]
        public XPCollection<DocumentTaskConfiguration> Documents => GetCollection<DocumentTaskConfiguration>("Documents");

        #endregion Properties

        public static XRole GetRoleForTask(Session session, Type type)
        {
            XRole result = null;
            var taskConfiguration = session.FindObject<TaskConfiguration>(new BinaryOperator("Type.FullName", type.FullName));
            if (taskConfiguration != null)
            {
                result = taskConfiguration.AssignedRole;
            }
            return result;
        }
    }
}
