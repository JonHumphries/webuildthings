﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WeBuildThings.XAF.BusinessRuleEngine;
using WeBuildThings.WorkflowEngine.UserDefinedWorkflows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.WorkflowEngine.UserDefinedWorkflows
{
    public class AdvancedWorkflowConfigurationItem : WorkflowConfigurationItem
    {
        #region Fields
        private BusinessRuleSet _RuleSet;
        #endregion Fields

        #region Constructors
        public AdvancedWorkflowConfigurationItem(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            RuleSet = new BusinessRuleSet(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
        public BusinessRuleSet RuleSet
        {
            get { return _RuleSet; }
            set { if (SetPropertyValue("RuleSet", ref _RuleSet, value) && !IsLoading) { OnRuleSetChanged(); } }
        }
        #endregion Properties

        #region Public Methods
        public override void Execute(Session session, UserDefinedWorkflow workflow)
        {

        }
        #endregion Public Methods

        #region Validation Methods
        public static bool ValidateRuleSet(bool exception, BusinessRuleSet ruleSet)
        {
            Type targetType = typeof(WeBuildThings.WorkflowEngine.Task);
            return ruleSet.ValidateTargetType(exception, targetType);
        }
        #endregion Validation Methods

        #region Event Methods
        protected override void OnSaving()
        {
            if (ValidateRuleSet(true, RuleSet))
            {
                SetRuleSetName();
                base.OnSaving();
            }
        }

        private void OnRuleSetChanged()
        {
            SetRuleSetTaskType();
        }

        protected override void OnTaskTypeChanged()
        {
            SetRuleSetTaskType();
            base.OnTaskTypeChanged();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetRuleSetName()
        {
            if (RuleSet != null && string.IsNullOrWhiteSpace(RuleSet.Name) && TaskType != null)
            {
                RuleSet.Name = string.Format("{0} - {1}", TaskType.Name, CompletionMethod);
                if (RuleSet.Name.Length > 99)
                {
                    RuleSet.Name = RuleSet.Name.Substring(0, 99);
                }
            }
        }

        private void SetRuleSetTaskType()
        {
            if (RuleSet != null)
            {
                RuleSet.TargetObject = TaskType;
            }
        }

        #endregion Set Methods
    }
}
