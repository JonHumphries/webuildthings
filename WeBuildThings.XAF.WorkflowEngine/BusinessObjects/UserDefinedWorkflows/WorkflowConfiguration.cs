﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.WorkflowEngine.UserDefinedWorkflows
{

    //[DefaultClassOptions]
    public class WorkflowConfiguration : BaseObject
    {

        #region Fields
        private string _Name;
        private PersistedType _FirstTaskType;
        #endregion Fields

        #region Constructors
        public WorkflowConfiguration(Session session)
            : base(session)
        { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public PersistedType FirstTaskType
        {
            get { return _FirstTaskType; }
            set {SetPropertyValue("FirstTaskType", ref _FirstTaskType, value);}
        }

        [Association("WorkflowConfiguration-WorkflowConfigurationItem", typeof(WorkflowConfigurationItem)), Aggregated]
        public XPCollection<WorkflowConfigurationItem> WorkflowConfigurationItem
        {
            get { return GetCollection<WorkflowConfigurationItem>("WorkflowConfigurationItem"); }
        }
        #endregion Properties

        #region Event Methods

        public virtual bool OnTaskComplete(Session session, UserDefinedWorkflow workflow, Type taskType, CompletionMethod completionMethod)
        {
            bool result = false;
            var workflowConfigurationItem = WorkflowConfigurationItem.Where(n => n.TaskType.ObjectType == taskType && n.CompletionMethod == completionMethod).FirstOrDefault();
            if (workflowConfigurationItem != null)
            {
                workflowConfigurationItem.Execute(session, workflow);
            }
            return result;
        }
        #endregion Event Methods

    }
}
