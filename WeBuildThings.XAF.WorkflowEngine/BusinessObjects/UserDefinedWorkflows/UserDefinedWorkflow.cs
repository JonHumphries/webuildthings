﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Utilities.Extensions;
using System;
using System.Linq;

namespace WeBuildThings.WorkflowEngine.UserDefinedWorkflows
{
    //[DefaultClassOptions]
    public class UserDefinedWorkflow : Workflow
    {

        #region Fields
        private WorkflowConfiguration _WorkflowConfiguration;

        #endregion Fields

        #region Constructors
        private UserDefinedWorkflow(Session session)
            : base(session)
        { }

        #endregion Constructors

        #region Properties
        public WorkflowConfiguration WorkflowConfiguration
        {
            get { return _WorkflowConfiguration; }
            set { if (SetPropertyValue("WorkflowConfiguration", ref _WorkflowConfiguration, value) && !IsLoading) { OnWorkflowConfigurationChanged(); } }
        }
        #endregion Properties

        #region Event Methods
        private void OnWorkflowConfigurationChanged()
        {
            Tasks.Add(CreateFirstTask(Session));
            Session.Delete(Comments.Where(n => n.Subject == Workflow.FirstTaskException).FirstOrDefault());
        }

        public override void OnTaskCompleted(Session session, Task task, CompletionMethod completionMethod)
        {
            var taskType = task.GetType();
            if (WorkflowConfiguration.OnTaskComplete(session, this, taskType, completionMethod))
            {
                base.OnTaskCompleted(session, task, completionMethod);
            }
            else
            {
                throw new NotImplementedException(string.Format("UserDefinedWorkflow.OnTaskCompleted: {0} Does not contain instructions for when {1} is {2}", WorkflowConfiguration, taskType, completionMethod));
            }
        }

        #endregion Event Methods

        public override Task CreateFirstTask(Session session)
        {
            Type firstTaskType = WorkflowConfiguration.FirstTaskType.ObjectType;
            return firstTaskType.CreateInstance(session);
        }
    }

}
