﻿using DevExpress.Xpo;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.WorkflowEngine.UserDefinedWorkflows
{
    public class BasicWorkflowConfigurationItem : WorkflowConfigurationItem
    {
        #region Fields
        private PersistedType _NextTaskType;
        private bool _Close;
        private WorkStatus _CloseStatus;
        #endregion Fields

        #region Constructors
        public BasicWorkflowConfigurationItem(Session session)
            : base(session)
        { }
        #endregion Constructors

        #region Properties
        public bool Close
        {
            get { return _Close; }
            set { SetPropertyValue("Close", ref _Close, value); }
        }

        public PersistedType NextTaskType
        {
            get { return _NextTaskType; }
            set
            {
                if (IsLoading)
                {
                    SetPropertyValue("NextTaskType", ref _NextTaskType, value);
                }
                else if (ValidateTaskType(true, value))
                {
                    SetPropertyValue("NextTaskType", ref _NextTaskType, value);
                }
            }
        }

        public WorkStatus CloseStatus
        {
            get { return _CloseStatus; }
            set { SetPropertyValue("CloseStatus", ref _CloseStatus, value); }
        }
        #endregion Properties

        public override void Execute(Session session, UserDefinedWorkflow workflow)
        {
            if (Close)
            {
                workflow.Close(CloseStatus);
            }
            else
            {
                dynamic nextTask = NextTaskType.ObjectType.CreateInstance(Session);
                workflow.Tasks.Add((Task)nextTask);
            }
        }
    }
}
