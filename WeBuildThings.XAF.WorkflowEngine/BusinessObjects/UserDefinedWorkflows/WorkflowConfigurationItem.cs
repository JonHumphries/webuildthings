﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using WeBuildThings.Utilities.Extensions;
using System.Linq;
using System.Text;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.WorkflowEngine.UserDefinedWorkflows
{

    public abstract class WorkflowConfigurationItem : BaseObject
    {
        #region Fields
        private WorkflowConfiguration _WorkflowConfiguration;
        private PersistedType _TaskType;
        private CompletionMethod _CompletionMethod;
        #endregion Fields

        #region Constructors
        public WorkflowConfigurationItem(Session session)
            : base(session)
        { }
        #endregion Constructors

        #region Properties
        [Association("WorkflowConfiguration-WorkflowConfigurationItem", typeof(WorkflowConfiguration))]
        public WorkflowConfiguration WorkflowConfiguration
        {
            get { return _WorkflowConfiguration; }
            set { SetPropertyValue("WorkflowConfiguration", ref _WorkflowConfiguration, value); }
        }

        public PersistedType TaskType
        {
            get { return _TaskType; }
            set
            {
                if (IsLoading)
                {
                    SetPropertyValue("TaskType", ref _TaskType, value);
                }
                else if (ValidateTaskType(true, value) && SetPropertyValue("TaskType", ref _TaskType, value))
                {
                    OnTaskTypeChanged();
                }
            }
        }

        public CompletionMethod CompletionMethod
        {
            get { return _CompletionMethod; }
            set { SetPropertyValue("CompletionMethod", ref _CompletionMethod, value); }
        }
        #endregion Properties

        #region Validation Methods
        public static bool ValidateTaskType(bool exception, PersistedType classItem)
        {
            Type type = typeof(Task);
            return classItem.ValidateType(exception, type);
        }
        #endregion Validation Methods

        #region Public Methods
        public abstract void Execute(Session session, UserDefinedWorkflow workflow);
        
        #endregion Public Methods

        #region Event Methods
        protected virtual void OnTaskTypeChanged()
        {
        }
        #endregion Event Methods

    }
}
