﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.WorkflowEngine
{
    public class WorkflowSystemSettings : SystemSettings
    {
         #region Fields
        private int _TaskCountRefreshInterval;
        private int _NextTaskButtonRefreshRate;
        #endregion Fields

        #region Constructors
        public WorkflowSystemSettings(Session session) : base(session) { }

        public WorkflowSystemSettings() : base(Session.DefaultSession) { }
        #endregion Constructors

        #region Properties

        public int TaskCountRefreshInterval
        {
            get { return _TaskCountRefreshInterval; }
            set { SetPropertyValue("TaskCountRefreshInterval", ref _TaskCountRefreshInterval, value); }
        }

        [ToolTip("The frequency in seconds to refresh the Next Task button's")]
        public int NextTaskButtonRefreshRate
        {
            get { return _NextTaskButtonRefreshRate; }
            set { SetPropertyValue("NextTaskButtonRefreshRate", ref _NextTaskButtonRefreshRate, value); }
        }
        #endregion Properties
    }
}
