﻿using DevExpress.ExpressApp;
using System;
using WeBuildThings.WorkflowEngine;
using WeBuildThings.WorkflowEngine.BusinessObjects;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.DataMergeProcess.DatabaseUpdate;

namespace WeBuildThings.DatabaseUpdate
{
    public static class WorkflowEngineRoleUpdater
    {
        #region Constants
        public const string WorkflowCreatorRoleName = "Workflow Creator";
        public const string WorkflowCreatorRoleDescription = "This role grants full access to workflows and tasks.  Use this for users who are familiar with creating/modifying workflows, schedules, and tasks.";
        #endregion Constants

        #region Type Definitions
        private static Type[] DefaultFullNoDeletePermissions = new Type[] { typeof(Task), typeof(Workflow), typeof(WorkflowUser), typeof(WorkflowComment) };
        private static Type[] DefaultReadNavigatePermissions = new Type[] { typeof(UpdateActiveWorkflowPointsTask), typeof(Scheduler), typeof(TimerCriteria), typeof(UpdatePointsWorkflow), typeof(TaskConfiguration)};
        private static Type[] WorkflowCreatorFullPermissions = new Type[] { typeof(Task), typeof(Workflow), typeof(WorkflowUser), typeof(WorkflowComment), typeof(UpdateActiveWorkflowPointsTask), typeof(Scheduler), typeof(TimerCriteria), typeof(UpdatePointsWorkflow), typeof(TaskConfiguration)};
        #endregion Type Definitions

        public static void Update(IObjectSpace objectSpace)
        {
            DefaultRoleModification(objectSpace);
            WorkflowCreatorRole(objectSpace);
            WorkflowFeatureOverridePoints(objectSpace);
            objectSpace.CommitChanges();
        }

        private static void DefaultRoleModification(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, XUserUpdater.DEFAULT_ROLE_NAME);
            XUserUpdater.AddFullNoDeleteAccessToTypes(objectSpace, result, DefaultFullNoDeletePermissions);
            XUserUpdater.AddReadNavigatePermissionsToTypes(objectSpace, result, DefaultReadNavigatePermissions);
        }

        private static void WorkflowCreatorRole(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, WorkflowCreatorRoleName);
            result.Description = WorkflowCreatorRoleDescription;
            XUserUpdater.AddFullAccessToTypes(objectSpace, result, WorkflowCreatorFullPermissions);
        }

        private static void WorkflowFeatureOverridePoints(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, Workflow.FEATURE_OVERRIDE_POINTS);
            result.Description = "Allows the user to override the normal priority points calculation";
            result.Save();
        }
    }
}
