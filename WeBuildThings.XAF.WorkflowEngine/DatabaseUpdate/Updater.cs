﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using WeBuildThings.DatabaseUpdate;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.DatabaseUpdate;
using WeBuildThings.XAF.WorkflowEngine;

namespace WeBuildThings.WorkflowEngine.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater 
    {
        public string[] WorkflowCommentTypes = new string[] { WorkflowComment.WorkflowCommentType };

        public Updater(IObjectSpace objectSpace, Version currentDBVersion) : base(objectSpace, currentDBVersion) 
        { }

        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            UpdateWorkflowSystemSettings(ObjectSpace);
            WorkflowEngineUpdater.Update(ObjectSpace);
            WorkflowUserUpdater.Update(ObjectSpace);
            WorkflowEngineRoleUpdater.Update(ObjectSpace);
            LookupListUpdater.UpdateLookupList(ObjectSpace, Comment.CommentTypeListName, WorkflowCommentTypes);
            ObjectSpace.CommitChanges();
        }

        public static void UpdateWorkflowSystemSettings(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var systemSettings = SystemSettings.GetInstance<WorkflowSystemSettings>(uow);
                if (uow.IsNewObject(systemSettings))
                {
                    systemSettings.TaskCountRefreshInterval = 3;
                    systemSettings.NextTaskButtonRefreshRate = 60;
                }
                systemSettings.Save();
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
        }


        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
    }
}
