﻿using DevExpress.ExpressApp;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.DataMergeProcess.DatabaseUpdate;

namespace WeBuildThings.DatabaseUpdate
{
    public static class WorkflowEngineUpdater
    {
        public static void Update(IObjectSpace objectSpace)
        {
            XRole role = XUserUpdater.CreateRole(objectSpace, "Services");
            XUserUpdater.CreateUser(objectSpace, "WorkflowEngine", role);
            objectSpace.CommitChanges();
        }
    }
}
