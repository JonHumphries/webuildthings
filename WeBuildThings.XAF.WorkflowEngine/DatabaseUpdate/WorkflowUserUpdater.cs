﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.WorkflowEngine.DatabaseUpdate
{
    public static class WorkflowUserUpdater
    {
        public static void Update(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var XUsers = new XPCollection<XUser>(uow);
                foreach (XUser user in XUsers)
                {
                    var workflowUser = WorkflowUser.CreateWorkflowUser(uow, user);
                    workflowUser.Save();
                } 
                uow.CommitChanges();
            }
        }
    }
}
