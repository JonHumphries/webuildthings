﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using System.Drawing;
using WeBuildThings.Common.Windows;

namespace WeBuildThings.XAF.Common.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    /*
    public partial class PopupSize_ViewController : ViewController<DetailView>
    {
        public PopupSize_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            ((BaseXafPage)WebWindow.CurrentRequestPage).XafPopupWindowControl.CustomizePopupWindowSize += XafPopupWindowControl_CustomizePopupWindowSize;
        }

        void XafPopupWindowControl_CustomizePopupWindowSize(object sender, DevExpress.ExpressApp.Web.Controls.CustomizePopupWindowSizeEventArgs e)
        {
            var size = CurrentWindow.Size;
            var width = (size.Width < 800)? 800 : size.Width - 10;
            var height = (size.Height < 600) ? 600 : size.Height - 10;
            e.Size = new Size(size.Width -10, size.Height-10);
            e.Handled = true;
        }
    }
     */
}
