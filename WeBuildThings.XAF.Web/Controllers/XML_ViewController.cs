﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using WeBuildThings.Module.CustomPopUps;
using WeBuildThings.Utilities.Extensions;
using System;

namespace WeBuildThings.Web.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class XML_ViewController : ViewController
    {
        public XML_ViewController()
        {
            InitializeComponent();
            TargetViewType = ViewType.ListView;
            TargetObjectType = typeof(ISerialize);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ImportXML_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            FileUploadParameter fileParameter = e.PopupWindow.View.CurrentObject as FileUploadParameter;
            Session session = fileParameter.Session;
            fileParameter.File.Transfer();
            string filePath = fileParameter.File.FullPath;
            dynamic instance = /*(View.ObjectTypeInfo.Type == typeof(EDI)) ? typeof(DelimitedFileEDI).CreateInstance(session) :*/ View.ObjectTypeInfo.Type.CreateInstance(session);
            Type type = ((ISerialize)instance).SerializedType;
            var baseObject = instance as BaseObject;
            if (baseObject != null)
            {
                baseObject.Delete();
            }
            var convertedResults = SerializeToXAF.Import(session, filePath, type);
            foreach (var result in convertedResults)
            {
                result.Save();
            }
            View.Refresh();
        }

        private void ImportXML_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            string filePath = @"F:\Revemed\Payer Master\PayerMaster 03092016.xml";
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            FileUploadParameter fileUploadParameter = objectSpace.CreateObject<FileUploadParameter>();
            objectSpace.CommitChanges();
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                dynamic instance = /*(View.ObjectTypeInfo.Type == typeof(EDI)) ? typeof(DelimitedFileEDI).CreateInstance(uow) :*/ View.ObjectTypeInfo.Type.CreateInstance(uow);
                Type type = ((ISerialize)instance).SerializedType;
                var baseObject = instance as BaseObject;
                if (baseObject != null)
                {
                    baseObject.Delete();
                }
                var convertedResults = SerializeToXAF.Import(uow, filePath, type);
                foreach (var result in convertedResults)
                {
                    result.Save();
                }
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, fileUploadParameter);
        }
    }
}
