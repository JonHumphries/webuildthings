﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Xpo;
using WeBuildThings.XAF.Web;

namespace WeBuildThings.Web.Controllers
{
    public class SwitchToEditModeModificationController : ViewController<DetailView>
    {
        protected override void OnActivated()
        {
            base.OnActivated();
            if (View.ViewEditMode == ViewEditMode.View)
            {
                var objectSpace = View.ObjectSpace as XPObjectSpace;
                if (objectSpace != null)
                {
                    var webSystemSettings = WebSystemSettings.GetInstance<WebSystemSettings>(objectSpace.Session);
                    if (webSystemSettings.DefaultDetailViewMode == DetailViewMode.Edit)
                    {
                        View.ViewEditMode = ViewEditMode.Edit;
                    }
                }
            }
        }
    }
}
