﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Web
{
    public enum DetailViewMode { Read, Edit };

    public class WebSystemSettings : SystemSettings
    {
        #region Fields
        private DetailViewMode _DefaultDetailViewMode;
        #endregion Fields

        #region Constructors
        public WebSystemSettings(Session session) : base(session) { }

        public WebSystemSettings() : base(Session.DefaultSession) { }
        #endregion Constructors

        #region Properties
        [ToolTip("Sets the default view mode for detail view screens on the Web version of the system")]
        public DetailViewMode DefaultDetailViewMode
        {
            get { return _DefaultDetailViewMode; }
            set { SetPropertyValue("DefaultDetailViewMode", ref _DefaultDetailViewMode, value); }
        }

        #endregion Properties
    }
}
