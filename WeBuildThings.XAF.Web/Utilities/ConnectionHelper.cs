﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Security.ClientServer.Wcf;
using DevExpress.ExpressApp.Web;
using DevExpress.ExpressApp.Web.Templates;
using System;
using System.Configuration;
using System.ServiceModel;
using System.Web.SessionState;
using WeBuildThings.Utilities.Extensions;


namespace WeBuildThings.Web.Utilities
{
    public static class ConnectionHelper<T> where T : WebApplication, new()
    {
        const string NTierApplicationSetting = "NTierApplication";
        const string ApplicationNameSetting = "ApplicationName";
        const string NTierConnectionStringName = "NTierConnectionString";
        const string CSConnectionStringName = "CSConnectionString";
        const string UIStyleSettingName = "UiStyle";
        const string UI_STYLE_NEW = "NEW";
        const string UI_STYLE_OLD = "OLD";

        private static bool NTier;
        private static string ConnectionString;
        private static string ApplicationName;

        public static void WebApplicationStart()
        {
            ConfigMgrHelper.TestAppSetting(NTierApplicationSetting);
            NTier = (ConfigurationManager.AppSettings[NTierApplicationSetting] == "True");
            ConfigMgrHelper.TestAppSetting(ApplicationNameSetting);
            ApplicationName = ConfigurationManager.AppSettings[ApplicationNameSetting];
            string connectionStringName = string.Empty;
            if (NTier)
            {
                ConfigMgrHelper.ValidateConnectionStringName(NTierConnectionStringName);
                connectionStringName = NTierConnectionStringName;
            }
            else
            {
                ConfigMgrHelper.ValidateConnectionStringName(CSConnectionStringName);
                connectionStringName = CSConnectionStringName;
            }
            ConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;

        }

        public static void Session_Start(HttpSessionState session, Object sender, EventArgs e)
        {
            WebApplication.SetInstance(session, new T());
            WebApplication.Instance.ApplicationName = ApplicationName;
            if (!NTier)
            {
            WebApplication.Instance.ConnectionString = ConnectionString;

            if (ConfigurationManager.AppSettings[UIStyleSettingName] == null || ConfigurationManager.AppSettings[UIStyleSettingName].ToUpper() == UI_STYLE_NEW)
            {
                WebApplication.Instance.SwitchToNewStyle();
                DefaultVerticalTemplateContentNew.ClearSizeLimit();
            }
            else
            {
                WebApplication.Instance.SwitchToOldStyle();
            }
#if EASYTEST
            throw new NotImplementedException();
            if(ConfigurationManager.ConnectionStrings["EasyTestConnectionString"] != null) {
                WebApplication.Instance.ConnectionString = ConfigurationManager.ConnectionStrings["EasyTestConnectionString"].ConnectionString;
            }
#endif
            }
            else
            {
            WcfSecuredDataServerClient clientDataServer = new WcfSecuredDataServerClient(
                WcfDataServerHelper.CreateDefaultBinding(), new EndpointAddress(ConnectionString));
            session["DataServerClient"] = clientDataServer;
            ServerSecurityClient securityClient =
                new ServerSecurityClient(clientDataServer, new ClientInfoFactory());
            WebApplication.Instance.Security = securityClient;
            WebApplication.Instance.CreateCustomObjectSpaceProvider +=
                delegate(object sender2, CreateCustomObjectSpaceProviderEventArgs e2)
                {
                    e2.ObjectSpaceProvider =
                        new DataServerObjectSpaceProvider(clientDataServer, securityClient);
                };
            }

            WebApplication.Instance.Setup();
            WebApplication.Instance.Start();
        }

        public static void DatabaseMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e)
        {
            if (NTier)
            {
                string message = e.CompatibilityError.ToString();

                throw new InvalidOperationException(
                    "The application cannot connect to the specified database " +
                    "because the latter does not exist or its version is older " +
                    "than that of the application.");
            }
            else
            {
#if EASYTEST
                            e.Updater.Update();
                            e.Handled = true;
#else
                //if (System.Diagnostics.Debugger.IsAttached)
                //{
                e.Updater.Update();
                e.Handled = true;
                //}
                //else
                //{
                //    throw new InvalidOperationException(
                //        "The application cannot connect to the specified database, because the latter doesn't exist or its version is older than that of the application.\r\n" +
                //        "This error occurred  because the automatic database update was disabled when the application was started without debugging.\r\n" +
                //        "To avoid this error, you should either start the application under Visual Studio in debug mode, or modify the " +
                //        "source code of the 'DatabaseVersionMismatch' event handler to enable automatic database update, " +
                //        "or manually create a database using the 'DBUpdater' tool.\r\n" +
                //        "Anyway, refer to the 'Update Application and Database Versions' help topic at http://www.devexpress.com/Help/?document=ExpressApp/CustomDocument2795.htm " +
                //        "for more detailed information. If this doesn't help, please contact our Support Team at http://www.devexpress.com/Support/Center/");
                //}
#endif
            }
        }

    }
}
