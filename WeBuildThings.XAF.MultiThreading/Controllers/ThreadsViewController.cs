﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using WeBuildThings.MultiThreading;

namespace WeBuildThings.XAF.MultiThreading.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ThreadsViewController : ViewController
    {
        public ThreadsViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ShowThreads_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            var localThreads = objectSpace.CreateObject<LocalThreads>();
            localThreads.SetThreads(ThreadMonitor.Threads);
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, localThreads);
        }
    }
}
