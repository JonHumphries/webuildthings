﻿namespace WeBuildThings.XAF.MultiThreading.Controllers
{
    partial class ThreadsViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ShowThreads = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ShowThreads
            // 
            this.ShowThreads.AcceptButtonCaption = null;
            this.ShowThreads.CancelButtonCaption = null;
            this.ShowThreads.Caption = "Show Threads";
            this.ShowThreads.Category = "Tools";
            this.ShowThreads.ConfirmationMessage = null;
            this.ShowThreads.Id = "ShowThreads";
            this.ShowThreads.ToolTip = null;
            this.ShowThreads.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ShowThreads_CustomizePopupWindowParams);
            // 
            // ThreadsViewController
            // 
            this.Actions.Add(this.ShowThreads);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ShowThreads;
    }
}
