﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.Collections.Generic;
using System.ComponentModel;
using WeBuildThings.MultiThreading;

namespace WeBuildThings.XAF.MultiThreading.Controllers
{
    [NonPersistent]
    public class LocalThreads : BaseObject
    {
        #region Fields
        private BindingList<LocalThread> _Threads;
        #endregion Fields

        #region Constructors
        public LocalThreads(Session session) : base(session) { }

        private void InitializeThreads()
        {
            if (Threads == null) Threads = new BindingList<LocalThread>();
        }
        #endregion Constructors

        #region Properties
        public BindingList<LocalThread> Threads
        {
            get { return _Threads; }
            set { SetPropertyValue("Threads", ref _Threads, value); }
        }
        #endregion Properties

        #region Set Methods
        public void SetThreads(IEnumerable<MonitoredThread> threads)
        {
            InitializeThreads();
            foreach (var thread in threads)
            {
                Threads.Add(LocalThread.CreateLocalThread(Session, thread));
            }
        }
        #endregion Set Methods
    }

    [NonPersistent]
    public class LocalThread: BaseObject
    {
        #region Fields
        private int _Id;
        private string _Status;
        private string _Message;
        #endregion Fields

        #region Constructors
        public LocalThread(Session session) : base(session) { }

        public static LocalThread CreateLocalThread(Session session, MonitoredThread thread)
        {
            LocalThread result = new LocalThread(session);
            result.Id = thread.Id;
            if (thread.Task != null) result.Status = thread.Status.ToString();
            result.Message = thread.Message;
            return result;
        }
        #endregion Constructors

        #region Properties
        public int Id
        {
            get { return _Id; }
            set { SetPropertyValue("Id", ref _Id, value); }
        }

        public string Status
        {
            get { return _Status; }
            set { SetPropertyValue("Status", ref _Status, value); }
        }

        public string Message
        {
            get { return _Message; }
            set { SetPropertyValue("Message", ref _Message, value); }
        }
        #endregion Properties
    }
}
