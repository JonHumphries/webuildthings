﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Exceptions;

namespace WeBuildThings.Twilio.Test
{
    [TestClass]
    public class LookupTests
    {
        //Twilio
        string TwilioAccountSid = "AC7c4a90ea328d3af00bddac7288496492";
        string TwilioAuthToken = "5ede2c827eaa63a6eccd4566a24d8ed4";
        string Number = "+3102148677";
        string ExpectedNumberCallerName = "LAVIDA MEDICAL";
        string UnknownPhoneNumber = "7148720001";
        string CarrierTestNumber = "6503198930";
        string UnknownCarrierTestNumber = "1111111111";
        string Carrier = "Onvoy/3 - Sybase365";
        string BadNumber = "abc1234578";
        string NotFoundErrorCode = "20404";

        /*
         ///<summary>
         ///Setup account with Zang before running test
         ///<\summary>
         //Zang.io
        [TestMethod]
        public void ZangGetTest()
        {     
            string ZangAccountSid = "AC0788908451fced28b76c4bfbabdeb87e";
            string ZangAuthToken = "fedf75b4ecae4bf4885977b0d0586327";
            var actualResult = PhoneNumberVerificationAPI.ZangGet(Number, ZangAccountSid, ZangAuthToken);
            var expectedResult = Number.Replace("+", string.Empty);
            Assert.AreEqual(ExpectedNumberCallerName, actualResult.Name);
        }
         */


        [TestMethod]
        public void AccountSidMissing()
        {
            var lookup = new PhoneNumberVerificationAPI();
            lookup.AuthToken = TwilioAuthToken;
            try
            {
                lookup.LookupPhoneNumber(Number);
                Assert.Fail("Argument Exception should have occurred");
            }
            catch (Exception exception)
            {
                Assert.IsInstanceOfType(exception, typeof(RequiredPropertyException));
            }
        }

        [TestMethod]
        public void AuthTokenMissing()
        {
            var lookup = new PhoneNumberVerificationAPI();
            lookup.AccountSid = TwilioAccountSid;
            try
            {
                lookup.LookupPhoneNumber(Number);
                Assert.Fail("RequiredPropertyException should have occurred");
            }
            catch (Exception exception)
            {
                Assert.IsInstanceOfType(exception, typeof(RequiredPropertyException));
            }
        }

        [TestMethod]
        public void AuthTokenAndSIDMissing()
        {
            var lookup = new PhoneNumberVerificationAPI();
            try
            {
                lookup.LookupPhoneNumber(Number);
                Assert.Fail("RequiredPropertyException should have occurred");
            }
            catch (Exception exception)
            {
                Assert.IsInstanceOfType(exception, typeof(RequiredPropertyException));
            }
        }

        [TestMethod]
        public void NoPhoneNumberProvided()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            try
            {
                lookup.LookupPhoneNumber(string.Empty);
                Assert.Fail("InvalidPhoneNumberException should have occurred");
            }
            catch (Exception exception)
            {
                Assert.IsInstanceOfType(exception, typeof(InvalidPhoneNumberException));
            }
        }

        [TestMethod]
        public void FoundPhoneNumber()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            var actualResult = lookup.LookupPhoneNumber(Number);
            var expectedResult = Number.Replace("+", string.Empty);
            Assert.AreEqual(ExpectedNumberCallerName, actualResult.Name);
        }

        [TestMethod]
        public void PhoneNumberNotFound()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            var actualResult = lookup.LookupPhoneNumber(UnknownPhoneNumber);
            Assert.AreEqual(null, actualResult.Name);
        }

        [TestMethod]
        public void InvalidPhoneNumber()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            try
            {
                var actualResult = lookup.LookupCarrier(BadNumber);
                Assert.Fail("Should throw exception");
            }
            catch (Exception e)
            {
                Assert.IsInstanceOfType(e, typeof(InvalidPhoneNumberException));
            }
        }

        [TestMethod]
        public void LookupCarrier()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            var actualResult = lookup.LookupCarrier(CarrierTestNumber);
            Assert.AreEqual(Carrier, actualResult.Carrier.Name);
        }

        [TestMethod]
        public void CarrierNotFound()
        {
            var lookup = new PhoneNumberVerificationAPI(TwilioAccountSid, TwilioAuthToken);
            var actualResult = lookup.LookupCarrier(UnknownCarrierTestNumber);
            Assert.AreEqual("The requested resource /PhoneNumbers/1111111111 was not found", actualResult.ErrorCode.Message);
        }

    }
}
