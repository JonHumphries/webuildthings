﻿
namespace WeBuildThings.Twilio
{
    public class TwilioPhoneNumber: IPhoneNumber
    {

        public TwilioPhoneNumber() { }
        
        public string Name { get; set;}
        
        public string CallerType { get; set; }

        public CarrierInfo Carrier { get; set; }

        public string CountryCode { get; set; }

        public string NationalFormat { get; set; }

        public string PhoneNumber { get; set; }

        public ErrorCode ErrorCode { get; set; }
    }
}
