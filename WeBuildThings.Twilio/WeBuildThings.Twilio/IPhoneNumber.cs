﻿using System;
using WeBuildThings.Twilio;

namespace WeBuildThings.Twilio
{
    public interface IPhoneNumber
    {
        string Name { get; set; }
        string CallerType { get; set; }
        CarrierInfo Carrier { get; set; }
        string CountryCode { get; set; }
        string NationalFormat { get; set; }
        string PhoneNumber { get; set; }
        ErrorCode ErrorCode { get; set; }
    }

    public static class IPhoneNumberExtensions
    {
        public static string Formatless(this IPhoneNumber number)
        {
            return number.NationalFormat.Replace("(", string.Empty).Replace(")", string.Empty).Replace("-",string.Empty).Replace(" ", string.Empty).Trim();
        }
    }
}
