﻿using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Text;
using WeBuildThings.Exceptions;
using WeBuildThings.Validation;

namespace WeBuildThings.Twilio
{
    public enum LookupType { CallerName, Carrier };

    public class PhoneNumberVerificationAPI
    {
        #region Properties
        public string AccountSid { get; set; }
        public string AuthToken { get; set; }
        public LookupType LookupType { get; set; }
        #endregion Properties

        #region Constructors
        public PhoneNumberVerificationAPI() { }

        public PhoneNumberVerificationAPI(string accountSid, string authToken)
        {
            AccountSid = accountSid;
            AuthToken = authToken;
        }
        #endregion Constructors

        #region Public Methods
        public IPhoneNumber Execute(string phoneNumber)
        {
            IPhoneNumber result = null;
            switch (LookupType)
            {
                case Twilio.LookupType.CallerName:
                    result = LookupPhoneNumber(phoneNumber);
                    break;
                case Twilio.LookupType.Carrier:
                    result = LookupCarrier(phoneNumber);
                    break;
                default:
                    var exception = new NotImplementedException(string.Format("Execute has no instructions for LookupType {0}", LookupType));
                    exception.Source = this.GetType().FullName;
                    throw exception;
            }
            return result;
        }

        public static bool ValidateRequest(bool throwException, string accountSid, string authToken, string phoneNumber)
        {
            bool validAccount = ValidationRules.ValidateAccount<PhoneNumberVerificationAPI>(throwException, accountSid, authToken);
            bool validPhoneNumber = ValidationRules.ValidatePhoneNumber<PhoneNumberVerificationAPI>(throwException, phoneNumber);
            return validAccount && validPhoneNumber;
        }

        public IPhoneNumber LookupPhoneNumber(string phoneNumber)
        {
            IPhoneNumber result = null;
            if (ValidateRequest(true, AccountSid, AuthToken, phoneNumber))
            {
                LookupType = LookupType.CallerName;
                var request = PhoneNumberLookupUrl(phoneNumber);
                var response = AuthenticatedGet(request, AccountSid, AuthToken);
                result = ProcessResponse(response);
            }
            return result;
        }

        public IPhoneNumber LookupCarrier(string phoneNumber)
        {
            IPhoneNumber result = null;
            if (ValidateRequest(true, AccountSid, AuthToken, phoneNumber))
            {
                LookupType = LookupType.Carrier;
                var request = CarrierLookupUrl(phoneNumber);
                var response = AuthenticatedGet(request, AccountSid, AuthToken);
                result = ProcessResponse(response);
            }
            return result;
        }
        #endregion Public Methods

        #region Methods
        private CallerNameResponse ProcessResponse(IRestResponse response)
        {
            CallerNameResponse result = null;
            var jsonDeserializer = new JsonDeserializer();
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                result = new CallerNameResponse();
                result.ErrorCode = jsonDeserializer.Deserialize<ErrorCode>(response); ;
            }
            else
            {
                switch (LookupType)
                {
                    case Twilio.LookupType.CallerName:
                    case Twilio.LookupType.Carrier:
                        result = jsonDeserializer.Deserialize<CallerNameResponse>(response);
                        break;
                    default:
                        var exception = new NotImplementedException(string.Format("Process Response has no instructions for LookupType {0}", LookupType));
                        exception.Source = this.GetType().FullName;
                        throw exception;
                }
            }
            return result;
        }

        private static string PhoneNumberLookupUrl(string phoneNumber)
        {
            phoneNumber = phoneNumber.Replace("+", string.Empty);
            return string.Format("https://lookups.twilio.com/v1/PhoneNumbers/{0}/?Type=caller-name&CountryCode=US", phoneNumber);
        }

        private static string CarrierLookupUrl(string phoneNumber)
        {
            phoneNumber = phoneNumber.Replace("+", string.Empty);
            return string.Format("https://lookups.twilio.com/v1/PhoneNumbers/{0}?Type=carrier&CountryCode=US", phoneNumber);
        }
        #endregion Methods

        public static IRestResponse AuthenticatedGet(string requestUrl, string accountSid, string token)
        {
            IRestResponse result = null;
            var client = new RestClient(requestUrl);
            var request = new RestRequest(Method.GET);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Authorization", string.Format("Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(accountSid + ":" + token))));
            result = client.Execute(request);
            return result;
        }

        public static IPhoneNumber ZangGet(string phoneNumber, string accountSid, string authToken)
        {
            var client = new RestClient(string.Format("https://api.zang.io/v2/Accounts/{0}/CNAM?PhoneNumber={1}", accountSid, phoneNumber));
            var request = new RestRequest(Method.POST);
            client.Authenticator = new HttpBasicAuthenticator(accountSid, authToken);
            var result = client.Execute(request);
            return new CallerNameResponse();
        }

    }

    public class CallerNameResponse : IPhoneNumber
    {
        public CallerNameResponse() { }

        public InnerResponse CallerName { get; set; }
        public string CountryCode { get; set; }
        public string PhoneNumber { get; set; }
        public string NationalFormat { get; set; }
        public string Url { get; set; }

        public ErrorCode ErrorCode { get; set; }
        public CarrierInfo Carrier { get; set; }

        #region IPhoneNumber Implementation
        string IPhoneNumber.Name
        {
            get { return CallerName.CallerName; }
            set { CallerName.CallerName = value; }
        }

        string IPhoneNumber.CallerType
        {
            get { return CallerName.CallerType; }
            set { CallerName.CallerType = value; }
        }
        #endregion IPhoneNumber Implementation
    }

    public class InnerResponse
    {
        public string CallerName { get; set; }
        public string CallerType { get; set; }
        public ErrorCode ErrorCode { get; set; }
    }

    public class ErrorCode
    {
        public ErrorCode() { }

        public string Status { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public string MoreInfo { get; set; }
    }

    public class CarrierInfo
    {
        public CarrierInfo() { }

        public string MobileCountryCode { get; set; }
        public string MobileNetworkCode { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public ErrorCode ErrorCode { get; set; }
    }
}
