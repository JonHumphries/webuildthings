﻿using DevExpress.ExpressApp;
using System;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.DatabaseUpdate;
using WeBuildThings.XAF.DataMergeProcess.DatabaseUpdate;

namespace WeBuildThings.XAF.ElectronicDataInterchange.DatabaseUpdate
{
    public static class EDIUpdater
    {
        public const string EDICreatorRoleName = "EDI Creator";
        public const string EDICreatorRoleDescription = "This role grants full access to EDIs.  Use this for users who are familiar with creating/modifying EDIs.";
      
        static string[] lists = new string[] { ErrorCode.EDI_ERROR_CODES };
        static string[] DocumentTypes = new string[] { EDILog.RawDataDocumentType };
        private static Type[] EDICreatorFullPermissions = new Type[] { typeof(ColumnMap), typeof(DelimitedColumn), typeof(DelimitedFileEDI), typeof(EDI), typeof(EDILog), typeof(ErrorCode) };
        private static Type[] DefaultFullNoDeletePermissions = new Type[] { typeof(EDILog), typeof(ErrorCode) };
        private static Type[] DefaultReadNavigatePermissions = new Type[] { typeof(ColumnMap), typeof(DelimitedColumn), typeof(DelimitedFileEDI), typeof(EDI)};
        
        public static void Update(IObjectSpace objectSpace)
        {
            LookupListUpdater.CreateLookupLists(objectSpace, lists);
            LookupListUpdater.UpdateLookupList(objectSpace, Document.DocumentTypeListName, DocumentTypes);
            CreateEDICreatorRole(objectSpace);
            XUserUpdater.AddFullNoDeleteAccessToTypes(objectSpace, XUserUpdater.CreateRole(objectSpace, XUserUpdater.DEFAULT_ROLE_NAME), DefaultFullNoDeletePermissions);
            XUserUpdater.AddReadNavigatePermissionsToTypes(objectSpace, XUserUpdater.CreateRole(objectSpace, XUserUpdater.DEFAULT_ROLE_NAME), DefaultReadNavigatePermissions);
            objectSpace.CommitChanges();
        }

        /// <summary>
        /// Defines the default type parameters for the role responsible for creating new EDIs.
        /// </summary>
        /// <param name="objectSpace"></param>
        /// <returns></returns>
        private static XRole CreateEDICreatorRole(IObjectSpace objectSpace)
        {
            XRole result = XUserUpdater.CreateRole(objectSpace, EDICreatorRoleName);
            if (result != null)
            {
                result.Description = EDICreatorRoleDescription;
                XUserUpdater.AddFullAccessToTypes(objectSpace, result, EDICreatorFullPermissions);
            }
            return result;
        }
    }
}
