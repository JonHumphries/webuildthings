﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using System;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppUpdatingModuleUpdatertopic.aspx
    public class Updater : ModuleUpdater 
    {
        public const string DefaultEDISourceDirectory = @"C:\EDISourceFiles";

        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }

        public override void UpdateDatabaseAfterUpdateSchema() {
            base.UpdateDatabaseAfterUpdateSchema();
            EDIUpdater.Update(ObjectSpace);
            UpdateEDISystemSettings(ObjectSpace);
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            
        }

         public static void UpdateEDISystemSettings(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var systemSettings = SystemSettings.GetInstance<EDISystemSettings>(uow);
                if (uow.IsNewObject(systemSettings))
                {
                    systemSettings.EDISourceDirectory = DefaultEDISourceDirectory;
                }
                systemSettings.Save();
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
        }

    }
}
