﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Win.Core;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraEditors;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ExcelColumnViewController : ViewController
    {

        public ExcelColumnViewController()
        {
            InitializeComponent();
        }
        protected override void OnActivated()
        {
            base.OnActivated();
        }

        protected override void OnViewControlsCreated()
        {
            // Get StringPropertyEditor control and assign Click event so that we can display model browser.
            var excelColumnDetailView = View as DetailView;
            if (excelColumnDetailView != null)
            {
                AttachToClickEvent(excelColumnDetailView.GetItems<StringPropertyEditor>());
            }
            //var excelColumnListView = View as DevExpress.ExpressApp.ListView;
            //if (excelColumnListView != null)
            //{
            //    AttachToClickEvent(excelColumnListView.GetItems<StringPropertyEditor>());
            //}
        }

        private void AttachToClickEvent(IEnumerable<StringPropertyEditor> properties)
        {
            foreach (StringPropertyEditor stringPropertyEditor in properties)
            {
                if (stringPropertyEditor.Control != null)
                {
                    TextEdit textEdit = (TextEdit)stringPropertyEditor.Control;
                    if (textEdit != null && stringPropertyEditor.PropertyName == "PropertyName")
                    {
                        textEdit.Click -= ObjectName_Click;
                        textEdit.Click += ObjectName_Click;
                        break;
                    }
                }
            }
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ObjectName_Click(object sender, EventArgs e)
        {
            DelimitedColumn excelColumn = View.CurrentObject as DelimitedColumn;
            if (excelColumn != null)
            {
                if (excelColumn.ColumnMap == null)
                {
                    var exception = new ArgumentNullException("Column Map is not initialized.");
                    exception.Source = this.GetType().Name;
                    throw exception;
                }
                if (excelColumn.ColumnMap.TargetObject == null)
                {
                    var exception = new ArgumentNullException("Target Object must be set before the Property Name can be set.");
                    exception.Source = this.GetType().Name;
                    throw exception;
                }
                ITypeInfo typeInfo = XafTypesInfo.Instance.FindTypeInfo(excelColumn.ColumnMap.TargetObject.ObjectType.FullName);
                excelColumn.PropertyName = this.ShowModelBrowser(typeInfo);
            }
        }

        /// <summary>
        /// Opens up model browser to select DataSource member.
        /// </summary>
        private string ShowModelBrowser(ITypeInfo typeInfo)
        {
            string result = string.Empty;
            ModelBrowser modelBrowser = new ModelBrowser(typeInfo.Type,false);
            if (modelBrowser.ShowDialog())
            {
                result = modelBrowser.SelectedMember;
            }
            return result;
        }
    }
}
