﻿namespace WeBuildThings.XAF.ElectronicDataInterchange.Controllers
{
    partial class EDI_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        protected System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RunEDI = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // RunEDI
            // 
            this.RunEDI.AcceptButtonCaption = null;
            this.RunEDI.CancelButtonCaption = null;
            this.RunEDI.Caption = "Run EDI";
            this.RunEDI.Category = "RecordEdit";
            this.RunEDI.ConfirmationMessage = null;
            this.RunEDI.Id = "RunEDI";
            this.RunEDI.ToolTip = null;
            this.RunEDI.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.RunEDI_CustomizePopupWindowParams);
            this.RunEDI.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.RunEDI_Execute);
            // 
            // EDI_ViewController
            // 
            this.Actions.Add(this.RunEDI);
            this.TargetObjectType = typeof(WeBuildThings.XAF.ElectronicDataInterchange.EDI);
        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction RunEDI;

    }
}
