﻿using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.Controllers
{
    partial class ColumnMapViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.LoadHeaderRow = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // LoadHeaderRow
            // 
            this.LoadHeaderRow.AcceptButtonCaption = null;
            this.LoadHeaderRow.CancelButtonCaption = null;
            this.LoadHeaderRow.Caption = "Load Header Row";
            this.LoadHeaderRow.Category = "RecordEdit";
            this.LoadHeaderRow.ConfirmationMessage = "This will delete any existing columns.  Press Accept to continue.";
            this.LoadHeaderRow.Id = "LoadHeaderRow";
            this.LoadHeaderRow.ToolTip = null;
            this.LoadHeaderRow.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.LoadHeaderRow_CustomizePopupWindowParams);
            this.LoadHeaderRow.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.LoadHeaderRow_Execute);
            // 
            // ColumnMapViewController
            // 
            this.Actions.Add(this.LoadHeaderRow);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction LoadHeaderRow;
    }
}
