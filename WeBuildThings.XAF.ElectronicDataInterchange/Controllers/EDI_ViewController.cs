﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;

namespace WeBuildThings.XAF.ElectronicDataInterchange.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class EDI_ViewController : ViewController
    {
        public EDI_ViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        protected virtual void RunEDI_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {

            IObjectSpace objectSpace = Application.CreateObjectSpace();
            EDIParameter ediPopup = objectSpace.CreateObject<EDIParameter>();
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, ediPopup);

        }

        protected virtual void RunEDI_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var edi = View.CurrentObject as EDI;
            if (edi != null)
            {
                edi.StartExecutionAsync(Application);
            }
        }
    }
}
