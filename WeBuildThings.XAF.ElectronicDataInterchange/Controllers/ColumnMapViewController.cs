﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using WeBuildThings.Module.CustomPopUps;
using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ColumnMapViewController : ViewController
    {
        public ColumnMapViewController()
        {
            InitializeComponent();
            this.TargetObjectType = typeof(IColumnMap);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void LoadHeaderRow_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var columnMapInterface = View.SelectedObjects[0] as IColumnMap; 
            var columnMap = (columnMapInterface != null) ? columnMapInterface.ColumnMap : null;
            if (columnMap != null )
            {
                FileUploadParameter fileParameter = e.PopupWindow.View.CurrentObject as FileUploadParameter;
                columnMap.CreateDelimitedColumns(fileParameter.File.LocalPath);
                columnMap.HeaderRow = true;
            }
        }

        private void LoadHeaderRow_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            FileUploadParameter fileUploadParameter = objectSpace.CreateObject<FileUploadParameter>();
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, fileUploadParameter);
        }
    }
}
