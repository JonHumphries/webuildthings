﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using System;
using System.Diagnostics;
using WeBuildThings.Module.CustomPopUps;

namespace WeBuildThings.XAF.ElectronicDataInterchange.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class FileEDI_ViewController : EDI_ViewController
    {

        private DevExpress.ExpressApp.Actions.SimpleAction OpenFolder;

        public FileEDI_ViewController()
            : base()
        {

            this.OpenFolder = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // OpenFolder
            // 
            this.OpenFolder.Caption = "Open Folder";
            this.OpenFolder.Category = "RecordEdit";
            this.OpenFolder.ConfirmationMessage = null;
            this.OpenFolder.Id = "OpenFolder";
            this.OpenFolder.ImageName = "Action_Open";
            this.OpenFolder.TargetViewType = DevExpress.ExpressApp.ViewType.DetailView;
            this.OpenFolder.ToolTip = null;
            this.OpenFolder.TypeOfView = typeof(DevExpress.ExpressApp.DetailView);
            this.OpenFolder.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.OpenFolder_Execute);
            // 
            // FileEDI_ViewController
            // 
            this.Actions.Add(this.OpenFolder);

            // this.TargetObjectType = typeof(FileEDI);    
        }


        private void OpenFolder_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var fileEDI = View.SelectedObjects[0] as FileEDI;
            if (fileEDI != null)
            {
                Process.Start(fileEDI.FolderPath);
            }
        }
        protected override void RunEDI_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            var selectedEDI = View.CurrentObject as FileEDI;
            if (selectedEDI != null && selectedEDI.Direction == EDIDirection.Incoming)
            {
                IObjectSpace objectSpace = Application.CreateObjectSpace();
                FileUploadParameter fileUploadParameter = objectSpace.CreateObject<FileUploadParameter>();
                objectSpace.CommitChanges();
                e.View = Application.CreateDetailView(objectSpace, fileUploadParameter);
            }
            else
            {
                base.RunEDI_CustomizePopupWindowParams(sender, e);
            }
        }

        protected override void RunEDI_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var selectedEDI = View.CurrentObject as FileEDI;
            if (selectedEDI != null)
            {
                if (selectedEDI.Direction == EDIDirection.Incoming)
                {
                    FileUploadParameter fileParameter = e.PopupWindow.View.CurrentObject as FileUploadParameter;
                    selectedEDI.StartExecutionAsync(Application, fileParameter.File.LocalPath);
                }
                else
                {
                    selectedEDI.StartExecutionAsync(Application, null);
                }
            }
            else
            {
                base.RunEDI_Execute(sender, e);
            }
        }


    }
}
