﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.DevXCommon.Test
{
    [TestClass]
    public class InitializeFileAdapaterTests
    {
        [TestMethod]
        public void FileExtensionContainsPeriod()
        {
            FileInfo file = new FileInfo(@"C:\Testing\TestFile.txt");
            Assert.IsFalse(file.Extension.Replace(".","").Contains("."));
        }

        [TestMethod]
        public void FileExtensionParse()
        {
            FileInfo file = new FileInfo(@"C:\Testing\TestFile.txt");
            EDIFileExtension fileExtension = EDIFileExtension.ToEDIFileExtension(file.Extension);
            Assert.AreEqual(EDIFileExtension.TXT, fileExtension);
        }
    }
}
