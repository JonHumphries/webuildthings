﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common.PipeFileAdapter;
using WeBuildThings.Excel;
using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.DevXCommon.Test
{
    [TestClass]
    public class EDIFileExtensionTests
    {


        [TestMethod]
        public void NotEqualTest()
        {
            EDIFileExtension lhs = EDIFileExtension.XLS;
            EDIFileExtension rhs = EDIFileExtension.TXT;
            Assert.AreEqual(false, lhs == rhs);
        }

        [TestMethod]
        public void EqualTest()
        {
            EDIFileExtension lhs = EDIFileExtension.XLSX;
            EDIFileExtension rhs = EDIFileExtension.XLSX;
            Assert.AreEqual(true, lhs == rhs);
        }

        [TestMethod]
        public void ExcelInitialization()
        {
            var extension = EDIFileExtension.XLSX;
            var actualResult = FileFormatExtensions.InitializeFileAdapter(extension);
            Assert.AreEqual(typeof(ExcelAdapter), actualResult.GetType());
        }

        [TestMethod]
        public void PipeInitialization()
        {
            var extension = EDIFileExtension.TXT;
            var actualResult = FileFormatExtensions.InitializeFileAdapter(extension);
            Assert.AreEqual(typeof(PipeAdapter), actualResult.GetType());
        }

        [TestMethod]
        public void CSVInitialization()
        {
            var extension = EDIFileExtension.CSV;
            var actualResult = FileFormatExtensions.InitializeFileAdapter(extension);
            Assert.IsInstanceOfType(actualResult, typeof(PipeAdapter));
        }

    }
}
