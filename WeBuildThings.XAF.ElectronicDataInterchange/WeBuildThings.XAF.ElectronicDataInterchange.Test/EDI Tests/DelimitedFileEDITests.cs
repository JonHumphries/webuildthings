﻿using DevExpress.Xpo;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.ElectronicDataInterchange;
using WeBuildThings.XAF.ElectronicDataInterchange.Test;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.DevXCommon.Test
{
    [TestClass]
    public class DelimitedFileEDITests
    {
        private Session Session { get { return XAFInitialization.Session; } }

        public DelimitedFileEDITests()
        {
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }

        [TestMethod]
        public void TestDelimitedFileEDIDefaultValues()
        {
            DelimitedFileEDI edi = new DelimitedFileEDI(Session);
            Assert.IsTrue(edi.Active);
            Assert.AreEqual(EDIDirection.Incoming, edi.Direction);
            Assert.AreEqual(DocumentCatalog.DefaultDocumentCatalogName, edi.DocumentCatalog.Name);
            Assert.AreEqual(FileFormat.Excel2010, edi.FileFormat);
        }

    }
}
