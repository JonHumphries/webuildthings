﻿using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.PipeFileAdapter;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.ElectronicDataInterchange.Test
{
    [TestClass]
    public class FileEDITests
    {
        private string PipeTestFilePath = @"PipeFile.txt";
        private string CSVTestFilePath = @"CsvFile.csv";
        private string SamplesDirectory = "SampleFiles";

        public FileEDITests()
        {
            string pipeFile = "PipeFile.txt";
            string csvFile = "CsvFile.csv";
            string projectRoot = new DirectoryInfo("..\\..\\..\\..\\WeBuildThings.XAF.ElectronicDataInterchange\\WeBuildThings.XAF.ElectronicDataInterchange.Test").FullName;
            SamplesDirectory = Path.Combine(projectRoot, "SampleFiles");
            PipeTestFilePath = Path.Combine(SamplesDirectory, pipeFile);
            CSVTestFilePath = Path.Combine(SamplesDirectory, csvFile);
            
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }

        public Session Session { get { return XAFInitialization.Session; } }

        [TestMethod]
        public void InitializePipe()
        {
            using (IDelimitedFileAdapter fileAdapter = FileFormatExtensions.InitializeFileAdapter(PipeTestFilePath))
            {
                Assert.IsInstanceOfType(fileAdapter, typeof(PipeAdapter));
            }
        }

        [TestMethod]
        public void InitializeCSV()
        {
            using (IDelimitedFileAdapter fileAdapter = FileFormatExtensions.InitializeFileAdapter(CSVTestFilePath))
            {
                Assert.IsInstanceOfType(fileAdapter, typeof(PipeAdapter));
            }
        }

        [TestMethod]
        public void FinalFileNameTest()
        {
            string actualResult = FileEDI.CalcFileName("Test", FileFormat.Pipe, true, "2");
            string now = DateTime.Now.ToString("yyyyMMddHHmmss");
            Assert.AreEqual(string.Format("Test{0}2.txt",now), actualResult);
        }

      

        [TestMethod]
        public void FullPathTestWithTimeStamp()
        {
            FileEDI edi = new TESTEDI(Session);
            edi.FileFormat = FileFormat.Pipe;
            edi.FolderPath = @"C:\Documents";
            edi.FileName = "Test";
            edi.AddTimeStampToFileName = true;
            edi.TimeStampSuffix = "2";
            string now = DateTime.Now.ToString("yyyyMMddHHmmss");
            var actualResult = edi.FullPath;
            Assert.AreEqual(string.Format(@"C:\Documents\Test{0}2.txt", now), actualResult);
        }

        [TestMethod]
        public void FullPathTestNoTimeStamp()
        {
            FileEDI edi = new TESTEDI(Session);
            edi.FileFormat = FileFormat.CSV;
            edi.FolderPath = @"C:\Documents";
            edi.FileName = "Test";
            var actualResult = edi.FullPath;
            Assert.AreEqual((@"C:\Documents\Test.csv"), actualResult);
        }

        private class TESTEDI : FileEDI
        {
            public TESTEDI(Session session) : base(session) { }

            protected override void IncomingExecute(EDILog currentEDILog)
            {
                throw new NotImplementedException();
            }

            protected override void OutgoingExecute(EDILog currentEDILog)
            {
                throw new NotImplementedException();
            }

            protected override Type GetSerializedType()
            {
                throw new NotImplementedException();
            }

            protected override object Convert(Type type, object convertedResult)
            {
                throw new NotImplementedException();
            }
        }
    }
}
