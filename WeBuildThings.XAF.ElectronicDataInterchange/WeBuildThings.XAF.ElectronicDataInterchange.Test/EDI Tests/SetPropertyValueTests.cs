﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange.Test.EDI_Tests
{
    [TestClass]
    public class SetPropertyValueTests
    {
        [TestMethod]
        public void SetPropertyValueTest1()
        {
            string propertyValue = "12456";
            var instance = new SomeObject();
            string propertyName = "MyDateTime";
            WeBuildThings.Common.Extensions.TypeExtensions.SetProperty(instance, propertyName, propertyValue, DateFormat.DaysAsInt);
            Assert.AreEqual(new DateTime(1956, 1, 24), instance.MyDateTime);
        }

        [TestMethod]
        public void SetPropertyValueTest2()
        {
            string propertyValue = "12456";
            var instance = new SomeObject();
            string propertyName = "MyString";
            WeBuildThings.Common.Extensions.TypeExtensions.SetProperty(instance, propertyName, propertyValue, DateFormat.DaysAsInt);
            Assert.AreEqual("12456", instance.MyString);
        }

        private class SomeObject
        {
            public SomeObject() { }
            public DateTime MyDateTime { get; set; }
            public string MyString { get; set; }
        }
    }
}
