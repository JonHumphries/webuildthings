﻿using System.Reflection;
using WeBuildThings.DevXCommon.Test;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange.Test
{
    internal static class TestInitialization
    {
        internal static Assembly[] Assemblies = new Assembly[] { typeof(FileEDI).Assembly,  typeof(SystemSettings).Assembly, typeof(IdentityManager).Assembly, typeof(FileEDITests).Assembly };

    }
}
