﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    [NonPersistent]
    public class EDIParameter : BaseObject
    {
        public EDIParameter(Session session) : base(session) { }
    }
}
