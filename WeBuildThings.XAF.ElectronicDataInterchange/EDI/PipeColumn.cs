﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using JonBuildsThings.BusinessObjects.Common;
using JonBuildsThings.BusinessObjects.Reflection;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace JonBuildsThings.BusinessObjects.EDI
{
    public class PipeColumn :BaseObject, INumberedList
    {
        #region Fields
        private PipeMap _PipeMap;
        private int _Number;
        private string _PropertyName;
        private string _SampleValue;
        private NonPersistentPropertyName _NonPersistentPropertyName;
        #endregion Fields

        #region Constructors
		public PipeColumn(Session session):base(session)
        { }
        #endregion Constructors

        #region Properties
		[Association("PipeMap-Pipecolumn",typeof(PipeMap))]
		public PipeMap PipeMap
        {
            get { return _PipeMap; }
            set { SetPropertyValue("PipeMap", ref _PipeMap, value);}
        }

		[ModelDefault("AllowEdit","False")]
		public int Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        //[Browsable(false)]
        public string PropertyName
        {
            get { return _PropertyName; }
            set { if (SetPropertyValue("PropertyName", ref _PropertyName, value) && IsLoading) { NonPersistentPropertyName.PropertyName = value; } }
        }

        //[ImmediatePostData]
        //[ModelDefault("Caption","Property Name")]
        [Browsable(false)]
        public NonPersistentPropertyName NonPersistentPropertyName
        {
            get { return _NonPersistentPropertyName; }
            set { if (_NonPersistentPropertyName != value) _NonPersistentPropertyName = value; }
        }

        public string SampleValue
        {
            get { return _SampleValue; }
            set { SetPropertyValue("SampleValue", ref _SampleValue, value); }
        }
        #endregion Properties
    }
}
