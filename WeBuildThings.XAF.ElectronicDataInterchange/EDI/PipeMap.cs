﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using JonBuildsThings.BusinessObjects.Reflection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JonBuildsThings.BusinessObjects.EDI
{
    [DefaultClassOptions]
    public class PipeMap : BaseObject
    {
        #region Fields
        private string _Name;
        private PersistedType _TargetType;
        private bool _PrintColumnHeader;
        #endregion Fields

        #region Constructors
        public PipeMap(Session session)
            : base(session)
        { }

        public override void AfterConstruction()
        {
            PrintColumnHeader = true;
 	        base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public PersistedType TargetType
        {
            get { return _TargetType; }
            set { SetPropertyValue("TargetType", ref _TargetType, value); }
        }

        public bool PrintColumnHeader
        {
            get {return _PrintColumnHeader;}
            set {SetPropertyValue("PrintColumnHeader", ref _PrintColumnHeader, value);}
        }

        [Association("PipeMap-PipeColumn",typeof(PipeColumn)),Aggregated]
        public XPCollection<PipeColumn> PipeColumn
        {
            get { return GetCollection<PipeColumn>("PipeColumn"); }
        }
        #endregion Properties
    }
}
