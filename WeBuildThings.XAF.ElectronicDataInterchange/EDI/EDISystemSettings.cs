﻿using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class EDISystemSettings : SystemSettings
    {
        #region Fields
        private string _EDISourceDirectory;
        #endregion Fields

        #region Constructors
        public EDISystemSettings(Session session) : base(session) { }

        public EDISystemSettings() : base(Session.DefaultSession) { }
        #endregion Constructors

        #region Properties

        public string EDISourceDirectory
        {
            get { return _EDISourceDirectory; }
            set { SetPropertyValue("EDISourceDirectory", ref _EDISourceDirectory, value); }
        }
        #endregion Properties

    }
}
