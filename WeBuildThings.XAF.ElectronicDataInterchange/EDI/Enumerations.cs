﻿using System;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public enum EDILogStatus { New, InProcess, Error, InProcessWithErrors, Reprocessed, NoDataFound, Success, Cancelled };
    public enum EDIDirection { Incoming, Outgoing };
    public enum SequenceMethod { Letters, Number };
}
