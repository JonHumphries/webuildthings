﻿using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.Utilities;
using WeBuildThings.XAF.Common;
using WeBuildThings.Common.Extensions;
using DevExpress.Persistent.BaseImpl;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{

    public enum ErrorID
    {
        [Description("Failure processing row")]
        EDI001,

        [Description("No Data Found")]
        EDI002,

        [Description("Database connection failed.")]
        EDI003,

        [Description("Columns do not match expected format.")]
        EDI004,

        [Description("Process terminated early due to unhandled exception.")]
        EDI005,

        [Description("Locking Exception")]
        EDI006,

        [Description("Trading Partner Not Responding")]
        EDI007,

        [Description("Error creating directory")]
        EDI008
    };

    public class ErrorCode : BaseObject, IStandardTracking
    {
        public const string EDI_ERROR_CODES = "EDI Error Codes";

        #region Fields
        private string _Name;
        private string _Description;
        private ErrorID _Code;
        private DateTime _DateCreated;
        private XUser _CreatedBy;
        private DateTime _DateModified;
        private XUser _ModifiedBy;
        #endregion Fields

        #region Constructors
        public ErrorCode(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            //this.LookupList = LookupList.CreateLookupList(Session, EDI_ERROR_CODES);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public ErrorID Code
        {
            get { return _Code; }
            set { if (SetPropertyValue("Code", ref _Code, value) && !IsLoading) { OnCodeChanged(); } }
        }

        [Association("EDILog-ErrorCode", typeof(EDILog))]
        public XPCollection<EDILog> EDILog
        {
            get { return GetCollection<EDILog>("EDILog"); }
        }

        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }
        #endregion Properties

        #region Event Methods
        private void OnCodeChanged()
        {
            SetName();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetName()
        {
            string description = Code.GetEnumDescription();
            if (!string.IsNullOrWhiteSpace(description))
            {
                Name = description;
            }
        }
        #endregion Set Methods
    }
}
