﻿using System;
using System.IO;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.PipeFileAdapter;
using WeBuildThings.Excel;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public struct EDIFileExtension
    {
        #region Const
        private const string txt = "txt";
        private const string xls = "xls";
        private const string xlsx = "xlsx";
        private const string csv = "csv";
        private const string unknown = "unknown";
        #endregion Const

        #region Fields
        public string Name;
        public FileFormat FileFormat;
        public string Delimiter;
        #endregion Fields

        #region Constructors
        private EDIFileExtension(string name, FileFormat fileFormat, string delimiter)
        {
            Name = name;
            FileFormat = fileFormat;
            Delimiter = delimiter;
        }

        public static EDIFileExtension TXT
        {
            get { return new EDIFileExtension(txt, FileFormat.Pipe, '|'.ToString()); }
        }

        public static EDIFileExtension XLS
        {
            get { return new EDIFileExtension(xls, FileFormat.Excel1997, '\t'.ToString()); }
        }

        public static EDIFileExtension XLSX
        {
            get { return new EDIFileExtension(xlsx, FileFormat.Excel2010, '\t'.ToString()); }
        }

        public static EDIFileExtension CSV
        {
            get { return new EDIFileExtension(csv, FileFormat.CSV, ','.ToString()); }
        }

        public static EDIFileExtension Unknown
        {
            get { return new EDIFileExtension(unknown, FileFormat.Unknown, '\0'.ToString()); }
        }
        #endregion Constructors

        #region Public Methods

        public static EDIFileExtension ToEDIFileExtension(string extension)
        {
            EDIFileExtension result = new EDIFileExtension();
            extension = extension.Replace(".", "").Trim().ToLower();
            if (extension == txt)
            {
                result = TXT;
            }
            else if (extension == csv)
            {
                result = CSV;
            }
            else if (extension == xls)
            {
                result = XLS;
            }
            else if (extension == xlsx)
            {
                result = XLSX;
            }
            else
            {
                var exception = new ArgumentException(string.Format("Get EDI File Extension has no instructions for file extension {0}", extension));
                exception.Source = typeof(EDIFileExtension).FullName;
                throw exception;
            }
            return result;
        }

        #endregion Public Methods

        #region Operators
        public static bool operator !=(EDIFileExtension lhs, EDIFileExtension rhs)
        {
            return !(lhs == rhs);
        }

        public static bool operator ==(EDIFileExtension lhs, EDIFileExtension rhs)
        {
            bool result = false;
            if (Object.ReferenceEquals(lhs, null))
            {
                if (Object.ReferenceEquals(rhs, null))
                {
                    result = true;
                }
            }
            else
            {
                result = lhs.Equals(rhs);
            }
            return result;
        }
        #endregion Operators

        #region Overrides
        public override string ToString()
        {
            return Name;
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (!Object.ReferenceEquals(obj, null) && obj.GetType() == typeof(EDIFileExtension))
            {
                var rhs = (EDIFileExtension)obj;
                result = (this.Name == rhs.Name);
            }
            return result;
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
        #endregion Overrides
    }

    public enum FileFormat { Unknown, Excel2010, Pipe, Excel1997, CSV };

    public static class FileFormatExtensions
    {
        public static bool ValidateFileExists(bool exception, string fullPath)
        {
            string result = string.Empty;
            if (string.IsNullOrWhiteSpace(fullPath))
            {
                result = "You must supply a file path.";
            }
            else if (!WeBuildThings.Utilities.FileOperations.FileExists(fullPath))
            {
                result = string.Format("Unable to locate {0}", fullPath);
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                var e = new ArgumentNullException(result);
                e.Source = typeof(FileEDI).Name;
                throw e;
            }
            return string.IsNullOrWhiteSpace(result);
        }

        public static IDelimitedFileAdapter InitializeFileAdapter(string fullPath)
        {
            ValidateFileExists(true, fullPath);
            FileInfo file = new FileInfo(fullPath);
            EDIFileExtension fileExtension = EDIFileExtension.ToEDIFileExtension(file.Extension);
            var result = InitializeFileAdapter(fileExtension);
            result.File = new FileInfo(fullPath);
            return result;
        }

        public static IDelimitedFileAdapter InitializeFileAdapter(string fullPath, string delimiter)
        {
            ValidateFileExists(true, fullPath);
            FileInfo file = new FileInfo(fullPath);
            EDIFileExtension fileExtension = EDIFileExtension.ToEDIFileExtension(file.Extension);
            var result = InitializeFileAdapter(fileExtension);
            result.Delimiter = delimiter;
            result.File = new FileInfo(fullPath);
            return result;
        }

        public static IDelimitedFileAdapter InitializeFileAdapter(EDIFileExtension ediFileExtension)
        {
            IDelimitedFileAdapter result = null;
            if (ediFileExtension == EDIFileExtension.TXT)
            {
                result = new PipeAdapter();
                result.Delimiter = ediFileExtension.Delimiter;
            }
            else if (ediFileExtension == EDIFileExtension.CSV)
            {
                result = new CSVAdapter();
                result.Delimiter = ediFileExtension.Delimiter;
            }
            else if (ediFileExtension == EDIFileExtension.XLS || ediFileExtension == EDIFileExtension.XLSX)
            {
                result = new ExcelAdapter();
            }
            else
            {
                var exception = new ArgumentException(string.Format("Initialize File Adapter has no instructions for {0} files", ediFileExtension));
                exception.Source = typeof(FileEDI).FullName;
                throw exception;
            }
            return result;
        }

        public static EDIFileExtension ToEDIFileExtension(this FileFormat value)
        {
            EDIFileExtension result = EDIFileExtension.Unknown;
            if (value == FileFormat.CSV)
            {
                result = EDIFileExtension.CSV;
            }
            else if (value == FileFormat.Excel1997)
            {
                result = EDIFileExtension.XLS;
            }
            else if (value == FileFormat.Excel2010)
            {
                result = EDIFileExtension.XLSX;
            }
            else if (value == FileFormat.Pipe)
            {
                result = EDIFileExtension.TXT;
            }
            else if (value == FileFormat.Unknown)
            {
                result = EDIFileExtension.Unknown;
            }
            else
            {
                var exception = new NotImplementedException(string.Format("No instructions for converting {0} to an EDI File Extension", value));
                exception.Source = typeof(FileFormatExtensions).FullName;
                throw exception;
            }
            return result;
        }
    }
}
