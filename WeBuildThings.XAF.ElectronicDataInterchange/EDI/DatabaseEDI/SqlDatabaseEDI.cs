﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.Exceptions;
using WeBuildThings.MultiThreading;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class SqlDatabaseEDI : EDI
    {
        #region Fields
        private string _ConnectionString;
        private string _StoredProcedureName;
        private string _SqlQuery;
        private string _Parameters;
        private ColumnMap _ColumnMap;
        #endregion Fields

        #region Constructors
        public SqlDatabaseEDI(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            ColumnMap = new ColumnMap(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [Size(2000)]
        public string ConnectionString
        {
            get { return _ConnectionString; }
            set { SetPropertyValue("ConnectionString", ref _ConnectionString, value); }
        }

        [Size(1000)]
        public string StoredProcedureName
        {
            get { return _StoredProcedureName; }
            set { SetPropertyValue("StoredProcedureName", ref _StoredProcedureName, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string SqlQuery
        {
            get { return _SqlQuery; }
            set { SetPropertyValue("SqlQuery", ref _SqlQuery, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Parameters
        {
            get { return _Parameters; }
            set { SetPropertyValue("Parameters", ref _Parameters, value); }
        }

        public ColumnMap ColumnMap
        {
            get { return _ColumnMap; }
            set { var oldColumnMap = _ColumnMap; if (SetPropertyValue("ColumnMap", ref _ColumnMap, value) && !IsLoading) { OnColumnMapChanged(oldColumnMap); } }
        }
        #endregion Properties

        #region Incoming Methods

        private SqlCommand CreateCommand(SqlConnection connection)
        {
            SqlCommand result = null;
            if (!string.IsNullOrWhiteSpace(StoredProcedureName))
            {
                result = new SqlCommand(StoredProcedureName, connection);
                result.CommandType = System.Data.CommandType.StoredProcedure;
                var parameters = GetParameters();
                if (parameters != null && parameters.Any())
                {
                    result.Parameters.AddRange(parameters.ToArray());
                }
            }
            else if (!string.IsNullOrWhiteSpace(SqlQuery))
            {
                result = new SqlCommand();
                result.Connection = connection;
                result.CommandType = System.Data.CommandType.Text;
                result.CommandText = SqlQuery;
            }
            else
            {
                throw new RequiredPropertyException(this.GetType(), "Either a stored procedure or query must be set prior to running this edi.");
            }
            return result;
        }

        protected override void IncomingExecute(EDILog currentEDILog)
        {
            using (var connection = new SqlConnection(ConnectionString))
            {
                using (var command = CreateCommand(connection))
                {
                    try
                    {
                        connection.Open();
                        SqlDataReader reader = command.ExecuteReader();
                        if (!reader.HasRows) throw new ArgumentOutOfRangeException("No rows returned");
                        using (UnitOfWork uow = new UnitOfWork(currentEDILog.Session.ObjectLayer))
                        {
                            int i = 0;
                            var columnMap = uow.GetObjectByKey<ColumnMap>(ColumnMap.Oid);
                            while (reader.Read())
                            {
                                ThreadMonitor.AddMessage(TaskId, string.Format("Attempting to Add Row {0}", i));
                                try
                                {
                                    HandleRow(uow, reader, columnMap);
                                    uow.CommitChanges();
                                }
                                catch (Exception e)
                                {
                                    uow.DropChanges();
                                    string message = string.Format("Failure processing row {0}", i);
                                    AddErrorCode(currentEDILog, ErrorID.EDI001, message, e, true);
                                }
                                i++;
                            }
                        }
                    }
                    catch (SqlException e)
                    {
                        AddErrorCode(currentEDILog, ErrorID.EDI003, "Failure connecting to database", e, false);
                    }
                    catch (ArgumentOutOfRangeException e)
                    {
                        AddErrorCode(currentEDILog, ErrorID.EDI002, "Stored Procedure returned no data", e, false);
                    }
                    catch (Exception e)
                    {
                        AddErrorCode(currentEDILog, ErrorID.EDI005, "Process Failed", e, false);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        protected virtual void HandleRow(Session session, SqlDataReader record, ColumnMap columnMap)
        {
            Dictionary<string, string> row = new Dictionary<string, string>();
            for (int i = 0; i < record.FieldCount; i++)
            {
                string name = record.GetName(i);
                row.Add(name, record[name].ToString());
            }
            ColumnMap.CreateInstance(session, columnMap, row, Name);
        }

        protected virtual IEnumerable<SqlParameter> GetParameters()
        {
            IEnumerable<SqlParameter> result = null;
            if (!string.IsNullOrWhiteSpace(Parameters))
            {
                throw new NotImplementedException("Parameters are not supported yet.");
            }
            return result;
        }

        #endregion Incoming Methods

        #region Outgoing Methods

        protected override void OutgoingExecute(EDILog currentEDILog)
        {
            throw new NotImplementedException();
        }

        #endregion Outgoing Methods

        protected virtual void OnColumnMapChanged(ColumnMap oldColumnMap)
        {
            if (oldColumnMap != null && ColumnMap != null && string.IsNullOrWhiteSpace(oldColumnMap.Name) && !oldColumnMap.DelimitedColumn.Any())
            {
                Session.Delete(oldColumnMap);
            }
        }

        #region ISerializable Implementation

        protected override Type GetSerializedType()
        {
            throw new NotImplementedException();
        }

        protected override object Convert(Type type, object convertedResult)
        {
            throw new NotImplementedException();
        }
        #endregion ISerializable Implementation

        protected override void OnSaving()
        {
            if (!IsDeleted)
            {
                if (string.IsNullOrWhiteSpace(ColumnMap.Name)) ColumnMap.Name = this.Name;
                base.OnSaving();
            }
        }
    }
}
