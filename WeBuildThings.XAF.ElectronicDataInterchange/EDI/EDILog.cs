﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using WeBuildThings.Utilities;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class EDILog: BaseObject, IStandardTracking
    {
        #region Constants
        internal const string RawDataDocumentType = "Raw EDI Data";
        #endregion Constants

        #region Fields
        private EDI _EDI;
        private EDILogStatus _Status;
        private Document _RawData;
        private DateTime _DateCreated;
        private DateTime _DateModified;
        private XUser _CreatedBy;
        private XUser _ModifiedBy;
        #endregion Fields

        #region Constructors
        public EDILog(Session session)
            : base(session)
        { }

        internal EDILog() : base() { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.SetCreatedUserAndDate();
        }
        
        #endregion Constructors

        #region Properties
        [Association("EDI-Log",typeof(EDI))]
        public EDI EDI
        {
            get { return _EDI; }
            set { SetPropertyValue("EDI", ref _EDI, value); }
        }

        public EDILogStatus Status
        {
            get { return _Status; }
            set { SetPropertyValue("Status", ref _Status, value); }
        }

        public Document RawData
        {
            get { return _RawData; }
            set { SetPropertyValue("RawData", ref _RawData, value); }
        }

        [Association("EDILog-ErrorCode", typeof(ErrorCode))]
        public XPCollection<ErrorCode> ErrorCode
        {
            get { return GetCollection<ErrorCode>("ErrorCode"); }
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }
        #endregion Properties

        #region Override Methods
        protected override void OnSaving()
        {
            if (RawData != null)
            {
                RawData.Save();
            }
            this.SetModifiedUserAndDate();
            base.OnSaving();
        }
        #endregion Override Methods    
    }
}
