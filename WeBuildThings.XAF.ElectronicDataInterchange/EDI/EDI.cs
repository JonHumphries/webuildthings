﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Serialization;
using WeBuildThings.Exceptions;
using WeBuildThings.MultiThreading;
using WeBuildThings.Utilities;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    [DefaultClassOptions]
    public abstract class EDI : StandardTracking, ISerialize
    {
        #region Consts
        internal const int MAXIMUM_SEQUENTIAL_ERROR_COUNTS = 10;
        #endregion Consts

        #region Fields
        private string _Name;
        private CancellationToken _CancelEDI;
        private bool _Active;
        private EDIDirection _Direction;
        private DocumentCatalog _DocumentCatalog;
        private int _SequentialErrorCount;
        private int _TaskId;
        #endregion Fields

        #region Constructors
        public EDI(Session session)
            : base(session)
        { }

        internal EDI() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            var systemSettings = SystemSettings.GetInstance<EDISystemSettings>(Session);
            this.DocumentCatalog = DocumentCatalog.CreateDocumentCatalog(Session, DocumentCatalog.DefaultDocumentCatalogName);
            Active = true;
            Name = string.Format("{0} {1}", this.GetType().Name, Direction);
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { if (SetPropertyValue("Name", ref _Name, value) && !IsLoading) { OnNameChanged(); } }
        }

        public bool Active
        {
            get { return _Active; }
            set { SetPropertyValue("Active", ref _Active, value); }
        }

        [Browsable(false)]
        [NonPersistent]
        internal CancellationToken CancelEDI
        {
            get { return _CancelEDI; }
            set { if (_CancelEDI != value) _CancelEDI = value; }
        }

        public EDIDirection Direction
        {
            get { return _Direction; }
            set { if (SetPropertyValue("Direction", ref _Direction, value) && !IsLoading) { OnDirectionChanged(); } }
        }

        public DocumentCatalog DocumentCatalog
        {
            get { return _DocumentCatalog; }
            set { SetPropertyValue("DocumentCatalog", ref _DocumentCatalog, value); }
        }

        [Browsable(false)]
        [NonPersistent]
        public int SequentialErrorCount
        {
            get { return _SequentialErrorCount; }
            set { if (SequentialErrorCount != value) { _SequentialErrorCount = value; { OnSequentialErrorCountChanged(); } } }
        }

        [Association("EDI-Log", typeof(EDILog)), Aggregated]
        public XPCollection<EDILog> Log
        {
            get { return GetCollection<EDILog>("Log"); }
        }

        [Browsable(false)]
        public int TaskId
        {
            get { return _TaskId; }
            set { if (_TaskId != value) _TaskId = value; }
        }
        #endregion Properties

        #region Events
        public event EventHandler DirectionChanged;
        #endregion Events

        #region Event Methods
        protected virtual void OnNameChanged()
        { }

        protected virtual void OnDirectionChanged()
        {
            InvokeDirectionChanged();
        }

        protected virtual void OnSequentialErrorCountChanged()
        {
            if (SequentialErrorCount > MAXIMUM_SEQUENTIAL_ERROR_COUNTS)
            {
                throw new MaximumErrorCountException(string.Format("Maximum Sequential Error Count is set to {0} and number of sequential errors is {1}.  EDI Aborted.", MAXIMUM_SEQUENTIAL_ERROR_COUNTS, SequentialErrorCount));
            }
        }
        #endregion Event Methods

        #region Set Methods
        protected virtual void ResetSequentialErrorCount()
        {
            SequentialErrorCount = 0;
        }
        #endregion Set Methods

        #region Invoke Methods
        private void InvokeDirectionChanged()
        {
            var handler = DirectionChanged;
            if (handler != null) handler.Invoke(this, EventArgs.Empty);
        }
        #endregion Invoke Methods

        #region Public Methods
        public void StartExecutionAsync(XafApplication application, string filePath = null)
        {
            ValidateApplication(true, application);
            EDILog currentEDILog = CreateCurrentEDILog(filePath);
            TaskId = ThreadMonitor.Add(System.Threading.Tasks.Task.Factory.StartNew(() => ExecuteAsync(application, currentEDILog.Oid), CancelEDI, TaskCreationOptions.LongRunning, TaskScheduler.Default));
        }

        protected virtual void ExecuteAsync(XafApplication application, Guid ediLogOid)
        {
            using (IObjectSpace objectSpace = application.CreateObjectSpace())
            {
                EDILog ediLog = null;
                try
                {
                    ediLog = objectSpace.GetObjectByKey<EDILog>(ediLogOid);
                    Execute(ediLog);
                    EDI.FinalizeLog(ediLog);
                    objectSpace.Refresh();
                }
                catch (EDITimeoutException exception)
                {
                    AddErrorCode(ediLog, ErrorID.EDI003, string.Format("Connection to the database was interrupted. Failed Row Number {0}, failed Row ID {1}", exception.RowNumber, exception.RowId), exception.OriginalException, false);
                }
                catch (KeyNotFoundException exception)
                {
                    AddErrorCode(ediLog, ErrorID.EDI004, "Columns do not match expected format", exception, false);
                }
                catch (Exception exception)
                {
                    AddErrorCode(ediLog, ErrorID.EDI005, "An unhandled exception occurred processing this edi.", exception, false);
                }
                objectSpace.CommitChanges();
            }
        }

        /// <summary>
        /// Adds an error code to the current edi log passed.  Ensures session conflicts are avoided and sequential error count is updated.
        /// </summary>
        /// <param name="currentEdiLog">The edi log to be updated with an error code</param>
        /// <param name="code">The ErrorID of the code being added</param>
        /// <param name="message">The message to be added to the error code</param>
        /// <param name="exception">The exception that was thrown</param>
        /// <param name="canContinue">Should the edi continue or end early.</param>
        public static void AddErrorCode(EDILog currentEdiLog, ErrorID code, string message, Exception exception, bool canContinue)
        {
            using (UnitOfWork uow = new UnitOfWork(currentEdiLog.Session.ObjectLayer))
            {
                var ediLog = uow.GetObjectByKey<EDILog>(currentEdiLog.Oid);
                ErrorCode errorCode = new ErrorCode(uow);
                errorCode.Code = code;
                string name = EnumerationExtensions.GetEnumDescription(code);
                errorCode.Name = (string.IsNullOrWhiteSpace(name)) ? "EDI Error" : name;
                if (exception != null)
                {
                    errorCode.Description = string.Format("{4}{0}{2}-{1}{0}{3}", Environment.NewLine, exception.Message, exception.Source, exception.StackTrace, message);
                }
                else
                {
                    errorCode.Description = message;
                }
                ediLog.Status = (canContinue) ? EDILogStatus.InProcessWithErrors : EDILogStatus.Error;
                ediLog.ErrorCode.Add(errorCode);
                ediLog.Save();
                ediLog.EDI.SequentialErrorCount++;
                uow.CommitChanges();
            }
        }

        public void StartExecution(string filePath = null)
        {
            EDILog currentEDILog = null;
            try
            {
                currentEDILog = CreateCurrentEDILog(filePath);
                Execute(currentEDILog);
                FinalizeLog(currentEDILog);
            }
            catch (EDITimeoutException exception)
            {
                AddErrorCode(currentEDILog, ErrorID.EDI003, string.Format("Connection to the database was interrupted. Failed Row Number {0}, failed Row ID {1}", exception.RowNumber, exception.RowId), exception.OriginalException, false);
            }
            catch (KeyNotFoundException exception)
            {
                AddErrorCode(currentEDILog, ErrorID.EDI004, "Columns do not match expected format", exception, false);
            }
            catch (Exception exception)
            {
                AddErrorCode(currentEDILog, ErrorID.EDI005, "An unhandled exception occurred processing this edi.", exception, false);
            }
        }

        public static void FinalizeLog(EDILog currentEdiLog)
        {
            using (UnitOfWork uow = new UnitOfWork(currentEdiLog.Session.ObjectLayer))
            {
                var ediLog = uow.GetObjectByKey<EDILog>(currentEdiLog.Oid);
                if (ediLog.RawData != null && ediLog.RawData.DocumentType == null)
                {
                    ediLog.RawData.DocumentType = ediLog.Session.FindObject<LookupItem>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Name", EDILog.RawDataDocumentType), new BinaryOperator("LookupList.Name", Document.DocumentTypeListName)));
                }
                if (ediLog.ErrorCode != null && ediLog.ErrorCode.Any())
                {
                    ediLog.Status = EDILogStatus.Error;
                }
                else if (ediLog.Status != EDILogStatus.NoDataFound)
                {
                    ediLog.Status = EDILogStatus.Success;
                }
                ediLog.Save();
                uow.CommitChanges();
            }
        }

        private EDILog CreateCurrentEDILog(string filePath)
        {
            Guid oid;
            if (this.Oid == Guid.Empty)
            {
                throw new NewObjectException("An EDI must be saved before it can be executed");
            }
            using (UnitOfWork uow = new UnitOfWork(Session.ObjectLayer))
            {
                EDI edi = uow.GetObjectByKey<EDI>(this.Oid);
                EDILog ediLog = new EDILog(uow);
                if (Direction == EDIDirection.Incoming && !string.IsNullOrWhiteSpace(filePath))
                {
                    ediLog.RawData = new Document(uow);
                    ediLog.RawData.File = new XFile(uow);
                    ediLog.RawData.File.LocalPath = filePath;
                    ediLog.RawData.DocumentType = uow.FindObject<LookupItem>(new GroupOperator(GroupOperatorType.And, new BinaryOperator("Name", EDILog.RawDataDocumentType), new BinaryOperator("LookupList.Name", Document.DocumentTypeListName)));
                    ediLog.RawData.File.Transfer();
                }
                ediLog.EDI = edi;
                ediLog.Status = EDILogStatus.InProcess;
                ediLog.Save();
                uow.CommitChanges();
                oid = ediLog.Oid;
            }
            EDILog result = Session.GetObjectByKey<EDILog>(oid);
            return result;
        }

        protected virtual void Execute(EDILog currentEDILog)
        {
            if (Direction == EDIDirection.Incoming)
            {
                if (ValidateIncomingData(true, currentEDILog))
                {
                    IncomingExecute(currentEDILog);
                }
            }
            else if (Direction == EDIDirection.Outgoing)
            {
                OutgoingExecute(currentEDILog);
            }
            else
            {
                var exception = new ArgumentException(string.Format("No instructions for EDI Direction {0}", Direction));
                exception.Source = typeof(FileEDI).Name;
                throw exception;
            }
        }

        protected abstract void IncomingExecute(EDILog currentEDILog);

        protected abstract void OutgoingExecute(EDILog currentEDILog);
        #endregion Public Methods

        #region Outgoing EDI Methods
        /// <summary>
        /// Writes the row into the stream.
        /// </summary>
        protected static void WriteRowToStream(StreamWriter streamWriter, string[] row, string delimiter)
        {
            for (int i = 0; i < row.Count(); i++)
            {
                if (i + 1 != row.Count())
                {
                    streamWriter.Write(string.Format("{0}{1}", row[i], delimiter));
                }
                else
                {
                    //streamWriter.WriteLine(row[i]);
                    streamWriter.Write(string.Format("{0}{1}", row[i], Environment.NewLine));
                }
            }
        }
        #endregion Outgoing EDI Methods

        #region Validation Methods

        public static bool ValidateApplication(bool exception, XafApplication application)
        {
            string result = string.Empty;
            if (application == null)
            {
                result = "An xaf application reference is necessary to run an EDI asyncronously";
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                var e = new ArgumentNullException(result);
                e.Source = typeof(EDI).Name;
                throw e;
            }
            return !string.IsNullOrWhiteSpace(result);
        }

        public static bool ValidateSession(bool exception, Session session)
        {
            string result = string.Empty;
            if (session == null)
            {
                result = "An session is necessary to run an EDI synchronously.";
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                var e = new ArgumentNullException(result);
                e.Source = typeof(EDI).Name;
                throw e;
            }
            return string.IsNullOrWhiteSpace(result);
        }

        public virtual bool ValidateIncomingData(bool exception, EDILog currentEDILog)
        {
            return true;
        }

        #endregion Validation Methods

        #region Override Methods
        protected override void OnSaving()
        {
            this.SetModifiedUserAndDate();
            base.OnSaving();
        }
        #endregion Override Methods

        #region ISerialize Implementation
        protected abstract Type GetSerializedType();
        protected abstract object Convert(Type type, object convertedResult);

        [Browsable(false)]
        public Type SerializedType
        {
            get { return GetSerializedType(); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            return Convert(type, convertedResult);
        }
        #endregion ISerialize Implementation
    }
}
