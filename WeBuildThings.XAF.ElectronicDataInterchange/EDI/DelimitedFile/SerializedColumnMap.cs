﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.Utilities.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    [Browsable(false)]
    public class SerializedColumnMap: ISerializeToXaf
    {
        #region Fields
        private Session _Session;
        #endregion Fields

        #region Properties
        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        public SerializedColumnMap()
        {
            Columns = new List<SerializedDelimitedColumn>();
        }

        public string Name;
        public bool HeaderRow;
        public string TargetObject;
        public SequenceMethod SequenceMethod;
        public List<SerializedDelimitedColumn> Columns;
        #endregion Properties

        #region ISerializeImplementation
        public Type SerializedType
        {
            get { return typeof(ColumnMap); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = type.CreateInstance(Session);
                }   
            }
            var result = convertedResult as ColumnMap;
            result.HeaderRow = HeaderRow;
            result.Name = Name;
            result.SequenceMethod = SequenceMethod;
            result.TargetObject = PersistedType.CreatePersistedType(Session, TargetObject);
            foreach (var column in Columns)
            {
                column.Session = Session;
                column.ColumnMapName = result.Name;
                result.DelimitedColumn.Add((DelimitedColumn)column.ChangeType(column.SerializedType, null));
            }
            return result;
        }
        #endregion ISerializeImplementation
    }
}
