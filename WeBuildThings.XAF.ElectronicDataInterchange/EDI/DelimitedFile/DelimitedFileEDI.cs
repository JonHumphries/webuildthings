﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.IO;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Exceptions;
using WeBuildThings.MultiThreading;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Utilities;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class DelimitedFileEDI : FileEDI, IDisposable, IColumnMap
    {
        #region Fields
        private ColumnMap _ColumnMap;
        private FilterCriteria _OutgoingFileDataCriteria;
        private Type _SerializedType = typeof(SerializedDelimitedFileEDI);
        #endregion Fields

        #region Constructors
        public DelimitedFileEDI(Session session) : base(session) { }

        internal DelimitedFileEDI() : base() { }

        public override void AfterConstruction()
        {
            ColumnMap = new ColumnMap(Session);
            FileFormat = FileFormat.Excel2010;
            OutgoingFileDataCriteria = new FilterCriteria(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [ExpandObjectMembers(ExpandObjectMembers.Always), Aggregated]
        public ColumnMap ColumnMap
        {
            get { return _ColumnMap; }
            set
            {
                ColumnMap oldColumnMap = _ColumnMap;
                if (SetPropertyValue("ColumnMap", ref _ColumnMap, value)) { OnColumnMapChanged(oldColumnMap); }
            }
        }

        [ExpandObjectMembers(ExpandObjectMembers.InDetailView), Aggregated]
        public FilterCriteria OutgoingFileDataCriteria
        {
            get { return _OutgoingFileDataCriteria; }
            set { SetPropertyValue("OutgoingFilterDataCriteria", ref _OutgoingFileDataCriteria, value); }
        }
        #endregion Properties

        #region Event Methods

        protected override void OnSaving()
        {
            if (ValidateDataTypesMatch(this))
            {
                base.OnSaving();
            }
        }

        protected override void OnFileFormatChanged()
        {
            base.OnFileFormatChanged();
            SetColumnMapSequenceMethod();
        }

        private void OnColumnMapChanged(ColumnMap oldColumnMap)
        {
            SetColumnMapListener(oldColumnMap);
        }

        private void OnColumnMapTargetObjectChanged(object sender, EventArgs e)
        {
            SetOutgoingFileDataCriteriaFilterObject();
            SetName();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetName()
        {
            if (string.IsNullOrWhiteSpace(Name) && ColumnMap != null && ColumnMap.TargetObject != null)
            {
                Name = string.Format("{0} {1} EDI", ColumnMap.TargetObject.Name, Direction);
            }
        }

        private void SetColumnMapSequenceMethod()
        {
            if (ColumnMap != null)
            {
                ColumnMap.SequenceMethod = (FileFormat == FileFormat.Excel2010 || FileFormat == FileFormat.Excel1997) ? SequenceMethod.Letters : SequenceMethod.Number;
            }
        }

        private void SetColumnMapName()
        {
            if (ColumnMap != null)
            {
                ColumnMap.Name = this.Name;
            }
        }

        private void SetOutgoingFileDataCriteriaFilterObject()
        {
            if (ColumnMap != null && OutgoingFileDataCriteria != null)
            {
                OutgoingFileDataCriteria.FilterObject = ColumnMap.TargetObject;
            }
        }

        private void SetColumnMapListener(ColumnMap oldColumnMap)
        {
            if (oldColumnMap != null)
            {
                oldColumnMap.TargetObjectChanged -= OnColumnMapTargetObjectChanged;
            }
            if (ColumnMap != null)
            {
                ColumnMap.TargetObjectChanged -= OnColumnMapTargetObjectChanged;
                ColumnMap.TargetObjectChanged += OnColumnMapTargetObjectChanged;
            }
        }
        #endregion Set Methods

        #region Execution Methods

        protected override void IncomingExecute(EDILog currentEDILog)
        {
            try
            {
                using (IDelimitedFileAdapter fileAdapter = FileFormatExtensions.InitializeFileAdapter(currentEDILog.RawData.File.FullPath))
                {
                    using (UnitOfWork uow = new UnitOfWork(currentEDILog.Session.ObjectLayer))
                    {
                        var columnMap = uow.GetObjectByKey<ColumnMap>(ColumnMap.Oid);
                        int i = 0;
                        int max = fileAdapter.Count;
                        foreach (var row in fileAdapter.Rows)
                        {
                            try
                            {
                                ThreadMonitor.AddMessage(TaskId, string.Format("Attempting to Add Row {0} of {1}", i, max));
                                ColumnMap.CreateInstance(uow, columnMap, row, Name);
                                uow.CommitChanges();
                            }
                            catch (KeyNotFoundException keyError)
                            {
                                uow.DropChanges();
                                string message = string.Format("File Does not match expected format.  Could not find column header {0}{1}{2}", keyError.Data[ColumnMap.MISSING_KEY_ERROR_DATAKEY], Environment.NewLine, DictionaryToString(row));
                                AddErrorCode(currentEDILog, ErrorID.EDI005, message, keyError, false);
                            }
                            catch (Exception e)
                            {
                                uow.DropChanges();
                                AddErrorCode(currentEDILog, ErrorID.EDI001, string.Format("Failure Processing Row {0}", DictionaryToString(row)), e, true);
                            }
                            i++;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                AddErrorCode(currentEDILog, ErrorID.EDI005, "EDI ended early", e, false);
            }
        }

        private static string DictionaryToString(Dictionary<string, string> dictionary)
        {
            string result = string.Empty;
            foreach (var row in dictionary)
            {
                result += string.Format("{0}-{1} ", row.Key, row.Value);
            }
            return result;
        }

        protected override void OutgoingExecute(EDILog currentEDILog)
        {
            var delimitedFileEDI = currentEDILog.Session.GetObjectByKey<DelimitedFileEDI>(Oid);
            string fullPath = Path.Combine(FolderPath, FinalFileName);
            CriteriaOperator criteria = null;
            if (delimitedFileEDI.OutgoingFileDataCriteria != null)
            {
                criteria = delimitedFileEDI.OutgoingFileDataCriteria.GetOperator();
            }
            try
            {
                ColumnMap.CreateFile(currentEDILog.Session, delimitedFileEDI.ColumnMap, fullPath, FileFormat, criteria);
                if (currentEDILog.RawData == null) currentEDILog.RawData = new Document(currentEDILog.Session);
                if (currentEDILog.RawData.File == null) currentEDILog.RawData.File = new XFile(currentEDILog.Session);
                currentEDILog.RawData.File.LocalPath = fullPath;
                currentEDILog.RawData.File.SaveDirectory = currentEDILog.EDI.DocumentCatalog.CatalogDirectory;
            }
            catch (CriteriaYieldedNoResultsException)
            {
                currentEDILog.Status = EDILogStatus.NoDataFound;
            }
        }

        private static Document ExecuteOutgoing(UnitOfWork uow, ColumnMap excelMap, string fileName, FileFormat fileFormat, CriteriaOperator criteria)
        {
            return ColumnMap.CreateFile(uow, excelMap, fileName, fileFormat, criteria);
        }

        private static bool ValidateDataTypesMatch(DelimitedFileEDI excelEDI)
        {
            return (excelEDI.OutgoingFileDataCriteria != null && excelEDI.OutgoingFileDataCriteria.FilterObject == excelEDI.ColumnMap.TargetObject);
        }
        #endregion Execution Methods

        #region Override Methods
        protected override void OnNameChanged()
        {
            SetColumnMapName();
            base.OnNameChanged();
        }
        #endregion Override Methods

        #region ISerialize Implementation
        protected override Type GetSerializedType()
        {
            return _SerializedType;
        }

        protected override object Convert(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(SerializedType);
            }
            var result = convertedResult as SerializedDelimitedFileEDI;
            result.Active = Active;
            result.ColumnMap = (SerializedColumnMap)this.ColumnMap.ChangeType(ColumnMap.SerializedType, null);
            result.DeleteOnProcessed = DeleteOnProcessed;
            result.Direction = Direction;
            result.FileFormat = FileFormat;
            result.FileName = FileName;
            result.FolderPath = FolderPath;
            result.Name = Name;
            if (OutgoingFileDataCriteria != null)
            {
                result.OutgoingFileDataCriteria = OutgoingFileDataCriteria.CriterionText;
            }
            return result;
        }
        #endregion ISerialize Implementation

        #region IColumnMap Implementation
        ColumnMap IColumnMap.ColumnMap
        {
            get { return this.ColumnMap; }
        }
        #endregion IColumnMap Implementation

        #region IDisposable Implementation
        private bool Disposed;

        ~DelimitedFileEDI()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!Disposed)
            {
                if (disposing)
                {
                    if (ColumnMap != null)
                    {
                        ColumnMap.TargetObjectChanged -= OnColumnMapTargetObjectChanged;
                    }
                    Disposed = true;
                }
            }
        }
        #endregion IDisposable Implementation
    }
}
