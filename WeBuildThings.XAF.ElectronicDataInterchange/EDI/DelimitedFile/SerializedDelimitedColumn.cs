﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Xml.Serialization;
using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Serialization;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    [Browsable(false)]
    public class SerializedDelimitedColumn: ISerializeToXaf
    {
        private Session _Session;

        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        public int Number;
        [XmlIgnore]
        public string ColumnMapName;
        public string HeaderName;
        public string AltHeaderName;
        public string PropertyName;
        public string DateFormat;
        public string OverrideValue;
        public NullValueProcess NullValueProcess;

        #region ISerializeImplementation
        public Type SerializedType
        {
            get { return typeof(DelimitedColumn); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(SerializedType, new GroupOperator(GroupOperatorType.And, new BinaryOperator("ColumnMap.Name", ColumnMapName), new BinaryOperator("HeaderName", HeaderName)));
                if (convertedResult == null)
                {
                    convertedResult = SerializedType.CreateInstance(Session);
                }
            }
            var result = convertedResult as DelimitedColumn;
            result.SampleValueDisabled = true;
            result.AltHeaderName = AltHeaderName;
            result.HeaderName = HeaderName;
            result.NullValueProcess = NullValueProcess;
            result.OverrideValue = OverrideValue;
            if (!string.IsNullOrWhiteSpace(DateFormat))
            {
                result.DateFormat = (DateFormats)Enum.Parse(typeof(DateFormats), DateFormat);
            }
            result.Number = Number;
            result.PropertyName = PropertyName;
            return result;
        }
        #endregion ISerializeImplementation
    }
}
