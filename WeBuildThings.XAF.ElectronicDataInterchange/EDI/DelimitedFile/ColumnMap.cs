﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.Serialization;
using WeBuildThings.Excel;
using WeBuildThings.Exceptions;
using WeBuildThings.Utilities.Extensions;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class ColumnMap : BaseObject, ISerialize, IColumnMap
    {
        #region Consts
        private const int MAX_LOOPS = 20;
        internal const string MISSING_KEY_ERROR_DATAKEY = "Missing Key";
        #endregion Consts

        #region Fields
        private bool _HeaderRow;
        private string _Name;
        private PersistedType _TargetObject;
        private SequenceMethod _SequenceMethod;
        #endregion Fields

        #region Constructors
        public ColumnMap(Session session) : base(session) { }

        internal ColumnMap() : base() { }
        #endregion Constructors

        #region Properties
        public bool HeaderRow
        {
            get { return _HeaderRow; }
            set { SetPropertyValue("HeaderRow", ref _HeaderRow, value); }
        }

        public PersistedType TargetObject
        {
            get { return _TargetObject; }
            set { if (SetPropertyValue("TargetObject", ref _TargetObject, value) && !IsLoading) { OnTargetObjectChanged(); } }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public SequenceMethod SequenceMethod
        {
            get { return _SequenceMethod; }
            set { SetPropertyValue("SequenceMethod", ref _SequenceMethod, value); }
        }

        [Association("ColumnMap-DelimitedColumn", typeof(DelimitedColumn)), Aggregated]
        public XPCollection<DelimitedColumn> DelimitedColumn
        {
            get { return GetCollection<DelimitedColumn>("DelimitedColumn"); }
        }
        #endregion Properties

        #region Events
        public event EventHandler TargetObjectChanged;
        #endregion Events

        #region Set Methods
        /// <summary>
        /// Sets all of the column property names to empty string.
        /// </summary>
        public void ClearPropertyNames()
        {
            foreach (DelimitedColumn column in DelimitedColumn)
            {
                column.PropertyName = string.Empty;
            }
        }

        public void SetPropertyNames()
        {
            foreach (DelimitedColumn excelColumn in DelimitedColumn)
            {
                excelColumn.SetPropertyName();
            }
        }

        /// <summary>
        /// Looks within the supplied data for a value for one of the properties and sets it.
        /// </summary>
        /// <param name="rows"></param>
        public void SetSampleValue(IEnumerable<Dictionary<string, string>> rows)
        {
            if (rows == null) return;
            var sampleRow = rows.FirstOrDefault();

            foreach (var column in DelimitedColumn)
            {
                if (sampleRow != null)
                {
                    column.SampleValue = sampleRow[column.HeaderName];
                }
            }

            //Dictionary<string, string> sampleRow = null;
            //foreach (DelimitedColumn column in DelimitedColumn)
            //{
            //    int safety = 0;
            //    foreach (Dictionary<string, string> row in rows)
            //    {
            //        if (sampleRow == null || string.IsNullOrWhiteSpace(sampleRow[column.HeaderName]))
            //        {
            //            sampleRow = row;
            //        }
            //        if (!string.IsNullOrWhiteSpace(sampleRow[column.HeaderName]))
            //        {
            //            column.SampleValue = sampleRow[column.HeaderName];
            //            break;
            //        }
            //        safety++;
            //        if (safety > MAX_LOOPS)
            //        {
            //            break;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Deletes all Delimited Columns in the collection.
        /// </summary>
        public void DeleteAllColumns()
        {
            while (DelimitedColumn.Count > 0)
            {
                DelimitedColumn[0].Delete();
            }
        }

        #endregion Set Methods

        #region Map Creation Methods
        /// <summary>
        /// Creates a set of Delimited columns based on the file supplied.
        /// </summary>
        public void CreateDelimitedColumns(string fileName)
        {
            using (IDelimitedFileAdapter excelAdapter = FileFormatExtensions.InitializeFileAdapter(fileName))
            {
                CreateDelimitedColumns(excelAdapter.HeaderRow);
                SetSampleValue(excelAdapter.Rows);
            }
        }

        /// <summary>
        /// Creates a set of delimited columns based on the collection supplied.
        /// </summary>
        public void CreateDelimitedColumns(IEnumerable<string> headerRow)
        {
            DeleteAllColumns();
            foreach (string header in headerRow)
            {
                DelimitedColumn column = new DelimitedColumn(Session);
                column.ColumnMap = this;
                column.HeaderName = header;
            }
            DelimitedColumn[0].MoveDown();
        }
        #endregion Map Creation Methods

        #region File Import Methods


        /// <summary>
        /// Creates an instance of the object using data in the supplied row.
        /// If the new object implements the IDataTransaction interface, the execute method will be called.
        /// </summary>
        public static void CreateInstance(Session session, ColumnMap excelMap, Dictionary<string, string> rowValues, string name)
        {
            dynamic instance = excelMap.TargetObject.ObjectType.CreateInstance(session);
            var columns = excelMap.DelimitedColumn.Where(n => !string.IsNullOrWhiteSpace(n.PropertyName));
            foreach (var column in columns)
            {
                string propertyValue = string.Empty;
                if (!string.IsNullOrWhiteSpace(column.OverrideValue))
                {
                    propertyValue = column.OverrideValue;
                }
                else
                {
                    try
                    {
                        propertyValue = rowValues[column.HeaderName];
                    }
                    catch (KeyNotFoundException e)
                    {
                        e.Data.Add(MISSING_KEY_ERROR_DATAKEY, column.HeaderName);
                        throw e;
                    }
                }
                WeBuildThings.Common.Extensions.TypeExtensions.SetProperty(instance, column.PropertyName, propertyValue, DateFormat.DaysAsInt);
            }
            IDataTransaction dataTransaction = instance as IDataTransaction;
            if (dataTransaction != null)
            {
                dataTransaction.TransactionSource = name;
                dataTransaction.Execute();
            }
            ((BaseObject)instance).Save();
        }

        #endregion File Import Methods

        #region File Export Methods
        public static Document CreateFile(Session session, ColumnMap excelMap, string fileName, FileFormat fileFormat, CriteriaOperator criteria)
        {
            Document result = null;
            if (fileFormat == FileFormat.Pipe)
            {
                fileName = string.Format("{0}.{1}", fileName, fileFormat.ToEDIFileExtension());
                result = CreatePipeFile(session, excelMap, fileName, criteria);
            }
            else
            {
                throw new NotImplementedException(string.Format("ExcelMap.CreateFile: Has no instructions for file format {0}", fileFormat));
            }
            return result;
        }

        public static Document CreatePipeFile(Session session, ColumnMap columnMap, string fileName, CriteriaOperator criteria)
        {
            Document result = new Document(session);
            var columns = columnMap.DelimitedColumn.OrderBy(n => n.Number).ToList();
            using (var data = new XPCollection(session, columnMap.TargetObject.ObjectType, criteria))
            {
                if (data.Count > 0)
                {
                    using (StreamWriter sw = new StreamWriter(fileName))
                    {
                        WriteHeaderRow(columns, sw);
                        WriteDetails(columns, data, sw);
                    }
                }
                else
                {
                    throw new CriteriaYieldedNoResultsException(string.Format("No results found searching {0} with {1}", columnMap.TargetObject.Name, criteria.ToString()));
                }
            }
            result.File = XFile.CreateXFile(session, fileName, "Default");
            return result;
        }

        /// <summary>
        /// Writes the details of the data collection to the stream provided
        /// </summary>
        private static void WriteDetails(List<DelimitedColumn> columns, XPCollection data, StreamWriter streamWriter)
        {
            foreach (var dataRow in data)
            {
                for (int i = 0; i < columns.Count(); i++)
                {
                    var propertyName = columns[i].PropertyName;
                    var value = (string.IsNullOrWhiteSpace(propertyName)) ? string.Empty : WeBuildThings.Common.Extensions.TypeExtensions.GetProperty(dataRow, propertyName);
                    if (i + 1 != columns.Count())
                    {
                        DateFormat dateFormat = new DateFormat(columns[i].DateFormat);
                        var formattedValue = value.ToFormattedString(columns[i].NullValueProcess, dateFormat, -1);
                        streamWriter.Write(string.Format("{0}|", formattedValue));
                    }
                    else
                    {
                        streamWriter.WriteLine(value);
                    }
                }
            }
        }

        /// <summary>
        /// Writes the name of the columns provided as a header to the stream provided
        /// </summary>
        private static void WriteHeaderRow(List<DelimitedColumn> columns, StreamWriter streamWriter)
        {
            for (int i = 0; i < columns.Count(); i++)
            {
                if (i + 1 != columns.Count())
                {
                    streamWriter.Write(string.Format("{0}|", columns[i].HeaderName));
                }
                else
                {
                    streamWriter.WriteLine(columns[i].HeaderName);
                }
            }
        }
        #endregion File Export Methods

        #region Event Methods
        private void OnTargetObjectChanged()
        {
            InvokeTargetObjectChanged();
            ClearPropertyNames();
            SetPropertyNames();
        }
        #endregion Event Methods

        #region Invoke Methods
        private void InvokeTargetObjectChanged()
        {
            var handler = TargetObjectChanged;
            if (handler != null) handler.Invoke(this, EventArgs.Empty);
        }
        #endregion Invoke Methods

        #region Validation Methods
        public static bool ValidateOnSaving(bool exception, ColumnMap columnMap)
        {
            string result = string.Empty;
            if (!columnMap.IsDeleted && string.IsNullOrWhiteSpace(columnMap.Name))
            {
                result = "Name is a required field.";
            }
            if (exception && !string.IsNullOrWhiteSpace(result))
            {
                throw new RequiredPropertyException(typeof(ColumnMap), result);
            }
            return (string.IsNullOrWhiteSpace(result));
        }
        #endregion Validation Methods

        #region Override Method
        protected override void OnSaving()
        {
            if (ValidateOnSaving(true, this))
            {
                base.OnSaving();
            }
        }
        #endregion Override Method

        #region ISerializeImplementation
        [Browsable(false)]
        public Type SerializedType
        {
            get { return typeof(SerializedColumnMap); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedColumnMap;
            foreach (var item in DelimitedColumn.OrderBy(n => n.Number))
            {
                result.Columns.Add((SerializedDelimitedColumn)item.ChangeType(item.SerializedType, null));
            }
            result.HeaderRow = HeaderRow;
            result.Name = Name;
            result.TargetObject = this.TargetObject.FullName;
            return result;
        }
        #endregion ISerializeImplementation

        #region IColumnMap Implementation
        ColumnMap IColumnMap.ColumnMap
        {
            get { return this; }
        }
        #endregion IColumnMap Implementation
    }
}
