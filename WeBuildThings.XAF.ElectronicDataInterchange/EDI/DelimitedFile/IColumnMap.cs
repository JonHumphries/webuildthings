﻿
namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    interface IColumnMap
    {
        ColumnMap ColumnMap { get; }
    }
}
