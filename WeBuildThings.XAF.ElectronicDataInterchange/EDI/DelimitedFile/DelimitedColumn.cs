﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.Excel;
using WeBuildThings.Utilities.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Xml.Serialization;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public class DelimitedColumn : BaseObject, INumberedList, IPersistedProperty, ISerialize
    {
        #region Consts
        private const int MAX_SAMPLE_VALUE_SIZE = 10;
        #endregion Consts

        #region Fields
        private string _ColumnLetter;
        private object _SampleObject;
        private int _Number;
        private string _HeaderName;
        private NullValueProcess _NullValueProcess;
        private string _AltHeaderNames;
        private DateFormats _DateFormat;
        private ColumnMap _ColumnMap;
        private string _PropertyName;
        private string _SampleValue;
        private string _OverrideValue;
        private NonPersistentPropertyName _NonPersistentPropertyName;
        private bool _SampleValueDisabled;
        #endregion Fields

        #region Constructors
        public DelimitedColumn(Session session)
            : base(session)
        {
            NonPersistentPropertyName = new NonPersistentPropertyName(this, "PropertyName");
        }

        internal DelimitedColumn() : base() { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.SetNumber();
            SetColumnLetter();
        }
        #endregion Constructors

        #region Properties
        public bool SampleValueDisabled
        {
            get { return _SampleValueDisabled; }
            set { SetPropertyValue("SampleValueDisabled", ref _SampleValueDisabled, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public string ColumnLetter
        {
            get { return _ColumnLetter; }
            set { SetPropertyValue("ColumnLetter", ref _ColumnLetter, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public int Number
        {
            get { return _Number; }
            set { if (SetPropertyValue("Number", ref _Number, value) && !IsLoading) { OnNumberChanged(); } }
        }

        public string Column
        {
            get { return (ColumnMap.SequenceMethod == SequenceMethod.Letters) ? ColumnLetter : Number.ToString(); }
        }

        public string HeaderName
        {
            get { return _HeaderName; }
            set { if (SetPropertyValue("HeaderName", ref _HeaderName, value) && !IsLoading) { OnHeaderNameChanged(); } }
        }

        [Size(MAX_SAMPLE_VALUE_SIZE)]
        public string AltHeaderName
        {
            get { return _AltHeaderNames; }
            set { SetPropertyValue("AltHeaderName", ref _AltHeaderNames, value); }
        }

        //[Browsable(false)]
        public string PropertyName
        {
            get { return _PropertyName; }
            set { if (SetPropertyValue("PropertyName", ref _PropertyName, value) && !IsLoading) { OnPropertyNameChanged(); } }
        }

        public string OverrideValue
        {
            get { return _OverrideValue; }
            set { SetPropertyValue("OverrideValue", ref _OverrideValue, value); }
        }

        //[ImmediatePostData]
        //[ModelDefault("Caption","Property Name")]
        [Browsable(false)]
        [XmlIgnore]
        public NonPersistentPropertyName NonPersistentPropertyName
        {
            get { return _NonPersistentPropertyName; }
            set { if (_NonPersistentPropertyName != value) _NonPersistentPropertyName = value; }
        }

        [Size(250)]
        public string SampleValue
        {
            get { return _SampleValue; }
            set { SetPropertyValue("SampleValue", ref _SampleValue, value); }
        }

        public NullValueProcess NullValueProcess
        {
            get { return _NullValueProcess; }
            set { SetPropertyValue("NullValueProcess", ref _NullValueProcess, value); }
        }

        public DateFormats DateFormat
        {
            get { return _DateFormat; }
            set { SetPropertyValue("DateFormat", ref _DateFormat, value); }
        }

        [Association("ColumnMap-DelimitedColumn", typeof(ColumnMap))]
        public ColumnMap ColumnMap
        {
            get { return _ColumnMap; }
            set { if (SetPropertyValue("ColumnMap", ref _ColumnMap, value) && !IsLoading) { OnColumnMapChanged(); } }
        }

        [Browsable(false)]
        public IEnumerable<INumberedList> List
        {
            get { return (ColumnMap != null) ? ColumnMap.DelimitedColumn : null; }
        }
        #endregion Properties

        #region Event Methods
        private void OnNumberChanged()
        {
            SetColumnLetter();
        }

        private void OnColumnMapChanged()
        {
            this.SetNumber();
        }

        private void OnHeaderNameChanged()
        {
            SetPropertyName();
        }

        private void OnPropertyNameChanged()
        {
            NonPersistentPropertyName.PropertyName = PropertyName;
            SetSampleValue();
            SetHeaderName();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetColumnLetter()
        {
            ColumnLetter = ExcelExtensions.CalcExcelColumnName(Number);
        }

        private void SetHeaderName()
        {
            if (string.IsNullOrWhiteSpace(HeaderName))
            {
                HeaderName = PropertyName.Replace('.','_');
            }
        }

        private void SetSampleValue()
        {
            if (ColumnMap != null && ColumnMap.TargetObject != null && !SampleValueDisabled)
            {
                SetSampleObject();
                if (_SampleObject != null && !string.IsNullOrWhiteSpace(PropertyName))
                {
                    SampleValue = TypeExtensions.GetProperty(_SampleObject, PropertyName).ToString();
                    if (SampleValue.Length > MAX_SAMPLE_VALUE_SIZE)
                    {
                        SampleValue = SampleValue.Substring(0, MAX_SAMPLE_VALUE_SIZE);
                    }
                }
            }
        }

        private void SetSampleObject()
        {
            if (ColumnMap != null && ColumnMap.TargetObject != null && !SampleValueDisabled && !string.IsNullOrWhiteSpace(PropertyName))
            {
                CriteriaOperator criteria = new NotOperator(new NullOperator(PropertyName));
                _SampleObject = Session.FindObject(ColumnMap.TargetObject.ObjectType, criteria);   
            }
        }

        public void SetPropertyName()
        {
            if (ColumnMap != null && ColumnMap.TargetObject != null && !string.IsNullOrWhiteSpace(HeaderName))
            {
                var possibleNames = HeaderName.CreateFuzzyList();
                var properties = ColumnMap.TargetObject.ObjectType.GetProperties();
                var property = properties.Where(n => possibleNames.Contains(n.Name)).FirstOrDefault();
                if (property != null)
                {
                    PropertyName = property.Name;
                }
            }
        }
        #endregion Set Methods

        Type IPersistedProperty.GetType(string propertyName)
        {
            Type result = null;
            if (ColumnMap != null)
            {
                result = ColumnMap.TargetObject.ObjectType;
            }
            return result;
        }

        public void SetTargetProperty(string propertyName, string propertyValue)
        {
            this.PropertyName = propertyValue;
        }

        #region ISerializeImplementation
        [Browsable(false)]
        public Type SerializedType
        {
            get { return typeof(SerializedDelimitedColumn); }
        }

        public object ChangeType(Type type, object convertedResult)
        {
            if (convertedResult == null)
            {
                convertedResult = Activator.CreateInstance(type);
            }
            var result = convertedResult as SerializedDelimitedColumn;
            result.AltHeaderName = AltHeaderName;
            result.HeaderName = HeaderName;
            result.NullValueProcess = NullValueProcess;
            result.DateFormat = DateFormat.ToString();
            result.Number = Number;
            result.PropertyName = PropertyName;
            result.OverrideValue = OverrideValue;
            return result;
        }
        #endregion ISerializeImplementation
    }
}
