﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Xml.Serialization;
using WeBuildThings.Common.Serialization;
using WeBuildThings.XAF.Common.Utilities;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    [Browsable(false)]
    public class SerializedDelimitedFileEDI : ISerializeToXaf
    {

        private Session _Session;

        [XmlIgnore]
        public Session Session
        {
            get { return _Session; }
            set { if (_Session != value) _Session = value; }
        }

        public string Name;
        public bool Active;
        public EDIDirection Direction;
        public string FolderPath;
        public string FileName;
        public bool DeleteOnProcessed;
        public FileFormat FileFormat;
        public string OutgoingFileDataCriteria;
        public SerializedColumnMap ColumnMap;

        #region ISerializeImplementation
        public Type SerializedType
        {
            get { return typeof(DelimitedFileEDI); }
        }

        public object ChangeType(Type type, object convertedResult)
        {

            if (convertedResult == null)
            {
                convertedResult = Session.FindObject(type, new BinaryOperator("Name", Name));
                if (convertedResult == null)
                {
                    convertedResult = type.CreateInstance(Session);
                }
            }
            var result = convertedResult as DelimitedFileEDI;
            result.Active = Active;
            if (ColumnMap != null)
            {
                ColumnMap.Session = Session;
                if (result.ColumnMap != null) result.ColumnMap.Delete();
                result.ColumnMap = (ColumnMap)ColumnMap.ChangeType(ColumnMap.SerializedType, null);
            }
            result.DeleteOnProcessed = DeleteOnProcessed;
            result.Direction = Direction;
            result.FileFormat = FileFormat;
            result.FileName = FileName;
            result.FolderPath = FolderPath;
            result.Name = Name;
            if (result.OutgoingFileDataCriteria == null)
            {
                result.OutgoingFileDataCriteria = new FilterCriteria(Session);
            }
            result.OutgoingFileDataCriteria.Criterion = OutgoingFileDataCriteria;
            return result;
        }
        #endregion ISerializeImplementation
    }
}
