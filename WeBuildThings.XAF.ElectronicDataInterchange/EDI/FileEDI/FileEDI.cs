﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.PipeFileAdapter;
using WeBuildThings.Excel;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.ElectronicDataInterchange
{
    public abstract class FileEDI : EDI
    {
        #region Consts
        protected const int TimeStampFormat = 105;
        #endregion Consts

        #region Fields
        private string _FolderPath;
        private string _FileName;
        private string _FinalFileName;
        private string _FullPath;
        private string _TimeStampSuffix;
        private bool _DeleteOnProcessed;
        private FileFormat _FileFormat;
        private bool _AddTimeStampToFileName;
        #endregion Fields

        #region Constructors
        public FileEDI(Session session)
            : base(session)
        { }

        public FileEDI() : base() { }
        #endregion Constructors

        #region Properties
        public string FolderPath
        {
            get { return _FolderPath; }
            set { if (SetPropertyValue("FolderPath", ref _FolderPath, value) && !IsLoading) {OnFolderPathChanged(); }}
        }

        public string FileName
        {
            get { return _FileName; }
            set { if (SetPropertyValue("FileName", ref _FileName, value) && !IsLoading) { OnFileNameChanged(); } }
        }

        [ToolTip("Gets applied to the FileName after the time stamp, when the AddTimeStampToFileName feature is active")]
        public string TimeStampSuffix
        {
            get { return _TimeStampSuffix; }
            set { if (SetPropertyValue("TimeStampSuffix", ref _TimeStampSuffix, value) && !IsLoading) { OnTimeStampSuffixChanged(); } }
        }

        public bool DeleteOnProcessed
        {
            get { return _DeleteOnProcessed; }
            set { SetPropertyValue("DeleteOnProcessed", ref _DeleteOnProcessed, value); }
        }

        public FileFormat FileFormat
        {
            get { return _FileFormat; }
            set { if (SetPropertyValue("FileFormat", ref _FileFormat, value) && !IsLoading) { OnFileFormatChanged(); } }
        }

        public bool AddTimeStampToFileName
        {
            get { return _AddTimeStampToFileName; }
            set { if (SetPropertyValue("AddTimeStampToFileName", ref _AddTimeStampToFileName, value) && !IsLoading) { OnAddTimeStampToFileNameChanged(); } }
        }

        [NonPersistent]
        [Browsable(false)]
        public string FinalFileName
        {
            get { return _FinalFileName; }
            set { if (SetPropertyValue("FinalFileName", ref _FinalFileName, value) && !IsLoading) { OnFinalFileNameChanged(); } }
        }

        [NonPersistent]
        [Browsable(false)]
        public string FullPath
        {
            get { return _FullPath; }
            set { SetPropertyValue("FullPath", ref _FullPath, value); }
        }
        #endregion Properties

        #region Events
        public event EventHandler FileFormatChanged;
        #endregion Events

        #region Event Methods
        protected virtual void OnFinalFileNameChanged()
        {
            SetFullPath();
        }

        protected virtual void OnFileNameChanged()
        {
            SetFinalFileName();
        }

        protected virtual void OnFileFormatChanged()
        {
            InvokeFileFormatChanged();
            SetFinalFileName();
        }

        protected virtual void OnFolderPathChanged()
        {
            SetFullPath();
        }

        protected virtual void OnAddTimeStampToFileNameChanged()
        {
            SetFinalFileName();
        }

        protected virtual void OnTimeStampSuffixChanged()
        {
            SetFinalFileName();
        }

        protected override void OnLoaded()
        {
            base.OnLoaded();
            SetFinalFileName();
            SetFullPath();
        }
        #endregion Event Methods

        #region Set Methods
        protected virtual void SetFullPath()
        {
            if (!string.IsNullOrWhiteSpace(FolderPath) && !string.IsNullOrWhiteSpace(FinalFileName))
            {
                FullPath = Path.Combine(FolderPath, FinalFileName);
            }
        }

        protected virtual void SetFinalFileName()
        {
            if (!string.IsNullOrWhiteSpace(FileName) && FileFormat != FileFormat.Unknown)
            {
                FinalFileName = CalcFileName(FileName, FileFormat, AddTimeStampToFileName, TimeStampSuffix);
            }
        }
        #endregion Set Methods

        #region Public Methods
        /// <summary>
        /// Returns the value associated with the column with one of the specified names.
        /// Stops on first.
        /// </summary>
        public static string GetRowValue(Dictionary<string, string> row, List<string> akaList)
        {
            string result = string.Empty;
            foreach (string aka in akaList)
            {
                if (row.ContainsKey(aka))
                {
                    result = row[aka];
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Returns the value associated with the column with the specified name
        /// </summary>
        public static string GetRowValue(Dictionary<string, string> row, string columnName)
        {
            string result = string.Empty;
            if (row.ContainsKey(columnName))
            {
                result = row[columnName];
            }
            return result;
        }

        protected override void ExecuteAsync(DevExpress.ExpressApp.XafApplication application, Guid ediLogOid)
        {
            try
            {
                FileOperations.CreateNewDirectory(this.FolderPath);
            }
            catch (Exception e)
            {
                using (var objectSpace = application.CreateObjectSpace())
                {
                    var ediLog = objectSpace.GetObjectByKey<EDILog>(ediLogOid);
                    AddErrorCode(ediLog, ErrorID.EDI008, e.Message, e, true);
                    objectSpace.CommitChanges();
                }
            }
            base.ExecuteAsync(application, ediLogOid);
        }

        protected override void Execute(EDILog currentEDILog)
        {
            try
            {
                FileOperations.CreateNewDirectory(this.FolderPath);
            }
            catch (Exception e)
            {
                AddErrorCode(currentEDILog, ErrorID.EDI008, e.Message, e, true);
            }
            base.Execute(currentEDILog);
        }


        public static string CalcFileName(string fileName, FileFormat fileFormat, bool addTimeStampToFileName, string timeStampSuffix)
        {
            string result = fileName;
            EDIFileExtension fileExtension = fileFormat.ToEDIFileExtension();
            string timeStamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            result = (addTimeStampToFileName) ? string.Format(@"{0}{2}{3}.{1}", Path.GetFileNameWithoutExtension(fileName), fileExtension.Name, timeStamp, timeStampSuffix) : string.Format(@"{0}.{1}", Path.GetFileNameWithoutExtension(fileName), fileExtension.Name);
            return result;
        }

 
        #endregion Public Methods

        #region Invoke Methods

        private void InvokeFileFormatChanged()
        {
            var handler = FileFormatChanged;
            if (handler != null) handler.Invoke(this, EventArgs.Empty);
        }
        #endregion Invoke Methods

        #region Validation Methods

        

        public override bool ValidateIncomingData(bool exception, EDILog currentEDILog)
        {
            bool sessionResult = ValidateSession(exception, currentEDILog.Session);
            bool ediLogNullResult = !(currentEDILog == null || currentEDILog.RawData == null || currentEDILog.RawData.File == null);
            bool fileResult = FileFormatExtensions.ValidateFileExists(exception, currentEDILog.RawData.File.FullPath);
            if (exception && (!sessionResult || !ediLogNullResult || !fileResult))
            {
                var e = new ArgumentNullException("Unable to locate file name in current EDI log");
                e.Source = typeof(FileEDI).Name;
                throw e;
            }
            return (sessionResult && fileResult && ediLogNullResult);
        }

        #endregion Validation Methods

    }
}
