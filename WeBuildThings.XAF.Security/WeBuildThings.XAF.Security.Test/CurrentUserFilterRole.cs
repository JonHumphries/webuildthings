﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Security;

namespace WeBuildThings.XAF.Security.Test
{
    [TestClass]
    public class CurrentUserFilterRole
    {
        [TestMethod]
        public void CriteriaSyntax()
        {
            var expectedResult = CriteriaOperator.Parse("[Recruiter] Is Null Or [Recruiter.UserName] = 'Admin'");
            var actualResult = CurrentUserFilterCriteria.CalcCurrentUserFilterCriteriaOperator("Recruiter","Admin",true);
            Assert.AreEqual(expectedResult.ToString(), actualResult.ToString());
        }
    }
}
