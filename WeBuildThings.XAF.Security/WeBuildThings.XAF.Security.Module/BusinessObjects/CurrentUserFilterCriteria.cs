﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;
using DevExpress.ExpressApp.Security;

namespace WeBuildThings.XAF.Security
{
    public class CurrentUserFilterCriteria : XRole
    {
        #region Consts
        public const string FILTER_CRITERIA = "[{0}.UserName] == '{1}'";
        #endregion Consts

        #region Fields
        private PersistedType _TargetType;
        private string _PropertName;
        private bool _ShowEmpties;
        #endregion Fields

        #region Constructors
        public CurrentUserFilterCriteria(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public PersistedType TargetType
        {
            get { return _TargetType; }
            set { SetPropertyValue("TargetType", ref _TargetType, value); }
        }

        public string PropertyName
        {
            get { return _PropertName; }
            set { SetPropertyValue("PropertyName", ref _PropertName, value); }
        }

        public bool ShowEmpties
        {
            get { return _ShowEmpties; }
            set { SetPropertyValue("ShowEmpties", ref _ShowEmpties, value); }
        }
        #endregion Properties

        public CriteriaOperator CalcCurrentUserFilterCriteriaOperator()
        {
            return CalcCurrentUserFilterCriteriaOperator(PropertyName, ((XUser)SecuritySystem.CurrentUser).UserName, ShowEmpties);
        }

        public static CriteriaOperator CalcCurrentUserFilterCriteriaOperator(string propertyName, string userName, bool showEmpties)
        {
            if (showEmpties)
                return new GroupOperator(GroupOperatorType.Or, new NullOperator(propertyName), CriteriaOperator.Parse(string.Format(FILTER_CRITERIA, propertyName, userName)));
            else
                return CriteriaOperator.Parse(string.Format(FILTER_CRITERIA, propertyName, userName));
        }
    }
}
