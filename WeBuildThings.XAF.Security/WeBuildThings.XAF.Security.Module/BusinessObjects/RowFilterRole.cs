﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Utilities;

namespace WeBuildThings.XAF.Security
{
    public class RowFilterCriteria : XRole
    {
        #region Fields
        private FilterCriteria _FilterCriteria;
        #endregion Fields

        #region Constructors
        public RowFilterCriteria(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            FilterCriteria = new FilterCriteria(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [ExpandObjectMembers(ExpandObjectMembers.Always)]
        public FilterCriteria FilterCriteria
        {
            get { return _FilterCriteria; }
            set { SetPropertyValue("FilterCriteria", ref _FilterCriteria, value); }
        }
    
        #endregion Properties
    }
}
