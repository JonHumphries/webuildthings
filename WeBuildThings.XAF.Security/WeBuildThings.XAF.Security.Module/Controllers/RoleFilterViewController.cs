﻿using DevExpress.ExpressApp;
using System.Linq;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Security
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class RoleFilterViewController : ViewController
    {
        public RoleFilterViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            XUser user = ((XUser)SecuritySystem.CurrentUser);
            SetRowFilterCriteria(user);
            SetCurrentUserFilterCriteria(user);
        }

        private void SetCurrentUserFilterCriteria(XUser user)
        {
            var currentUserFilterCriterias = user.FlattenRoles().Where(n => n.GetType() == typeof(CurrentUserFilterCriteria));
            if (currentUserFilterCriterias != null && currentUserFilterCriterias.Any())
            {
                foreach (CurrentUserFilterCriteria restriction in currentUserFilterCriterias)
                {
                    if (restriction.TargetType.ObjectType.Name == View.ObjectTypeInfo.Name)
                    {
                        CollectionSourceBase cs = ((ListView)View).CollectionSource;
                        cs.Criteria["CriteriaOperator"] = restriction.CalcCurrentUserFilterCriteriaOperator();
                    }
                }
            }
        }

        private void SetRowFilterCriteria(XUser user)
        {
            var rowFilterRoles = user.FlattenRoles().Where(n => n.GetType() == typeof(RowFilterCriteria));
            if (rowFilterRoles != null && rowFilterRoles.Any())
            {
                foreach (RowFilterCriteria restriction in rowFilterRoles)
                {
                    if (restriction.FilterCriteria.ObjectType.Name == View.ObjectTypeInfo.Name)
                    {
                        CollectionSourceBase cs = ((ListView)View).CollectionSource;
                        cs.Criteria["CriteriaOperator"] = restriction.FilterCriteria.GetOperator();
                    }
                }
            }
        }
    }
}
