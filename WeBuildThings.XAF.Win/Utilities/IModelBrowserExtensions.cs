﻿
namespace WeBuildThings.Win.Utilities
{
    /// <summary>
    /// Allows user to select a property from a type using the DevExpress Model Browser
    /// </summary>
    //public class ModelBrowserControl : PopupBaseEdit
    //{
    //    #region Fields
    //    private WinPropertyEditor _PropertyEditor;
    //    private ModelBrowserPopup _ModelBrowserPopup;
    //    private IModelBrowser _ModelBrowserValue;
    //    #endregion Fields

    //    #region Properties
    //    internal WinPropertyEditor PropertyEditor
    //    {
    //        get { return _PropertyEditor; }
    //        set { if (_PropertyEditor != value) _PropertyEditor = value; }
    //    }

    //    public ModelBrowserPopup ModelBrowserPopup
    //    {
    //        get { return _ModelBrowserPopup; }
    //        set { if (_ModelBrowserPopup != value) _ModelBrowserPopup = value; }
    //    }

    //    public IModelBrowser ModelBrowserValue
    //    {
    //        get { return _ModelBrowserValue; }
    //        set { if (_ModelBrowserValue != value) _ModelBrowserValue = value; }
    //    }
    //    #endregion Properties

    //    #region Constructors
    //    public ModelBrowserControl(WinPropertyEditor propertyEditor)
    //        : base()
    //    {
    //        PropertyEditor = propertyEditor;
    //        //PropertyEditor.CurrentObjectChanged -= PropertyEditor_CurrentObjectChanged;
    //        //PropertyEditor.CurrentObjectChanged += PropertyEditor_CurrentObjectChanged;
    //    }
    //    #endregion Constructors

    //    #region Events
    //    public event EventHandler ValueChanged;
    //    #endregion Events

    //    #region Invoke Methods
    //    internal void RaiseValueChanged()
    //    {
    //        var handler = ValueChanged;
    //        if (handler != null) handler.Invoke(this, EventArgs.Empty);
    //    }
    //    #endregion Invoke Methods

    //    #region Override Methods
    //    protected override DevExpress.XtraEditors.Popup.PopupBaseForm CreatePopupForm()
    //    {
    //        var propertyValue = PropertyEditor.PropertyValue;
    //        if (propertyValue == null || ((IModelBrowser)propertyValue).Type == null)
    //        {
    //            string message = (EditValue == null) ? "Unable to locate object for control." : string.Format("Data type is missing {0}", ((IModelBrowser)propertyValue).TargetPropertyName);
    //            var argumentNullException = new ArgumentNullException(message);
    //            argumentNullException.Source = typeof(ModelBrowserControl).ToString();
    //            throw argumentNullException;
    //        }
    //        ModelBrowserPopup = new ModelBrowserPopup((IModelBrowser)propertyValue, this);
    //        ModelBrowserPopup.FormClosed -= modelBrowserPopup_FormClosed;
    //        ModelBrowserPopup.FormClosed += modelBrowserPopup_FormClosed;
    //        return ModelBrowserPopup;

    //    }


    //    //void PropertyEditor_CurrentObjectChanged(object sender, EventArgs e)
    //    //{
    //    //    ModelBrowserValue = PropertyEditor.PropertyValue as IModelBrowser;
    //    //}


    //    void modelBrowserPopup_FormClosed(object sender, EventArgs e)
    //    {
    //        BeginUpdate();
    //        var result = sender as ModelBrowserPopup;
    //        this.EditValue = result.ResultValue;
    //        //object oldValue = this.EditValue;
    //        //this.ModelBrowserValue = (IModelBrowser)result.ResultValue;
    //        //RaiseValueChanged();
    //        EndUpdate();
    //        //OnEditValueChanging(new DevExpress.XtraEditors.Controls.ChangingEventArgs(oldValue, result.ResultValue));
    //        //RaiseEditValueChanged();
    //    }

    //    protected override void Dispose(bool disposing)
    //    {
    //        //PropertyEditor.CurrentObjectChanged -= PropertyEditor_CurrentObjectChanged;
    //        ModelBrowserPopup.FormClosed -= modelBrowserPopup_FormClosed;
    //        base.Dispose(disposing);
    //    }
    //    #endregion Override Methods
    //}

    //public class ModelBrowserPopup : PopupBaseForm
    //{
    //    #region Fields
    //    private IModelBrowser _ResultValue;
    //    #endregion Fields

    //    public ModelBrowserPopup(IModelBrowser modelBrowserValue, PopupBaseEdit ownerEdit)
    //        : base(ownerEdit)
    //    {
    //        if (modelBrowserValue.Type == null)
    //        {
    //            ArgumentNullException argumentNullException = new ArgumentNullException("Type cannot be null");
    //            argumentNullException.Source = typeof(ModelBrowserPopup).ToString();
    //            throw argumentNullException;
    //        }
    //        _ResultValue = modelBrowserValue;
    //    }

    //    public override void ShowPopupForm()
    //    {
    //        string result = string.Empty;
    //        ModelBrowser modelBrowserWindow = new ModelBrowser(_ResultValue.Type);
    //        if (modelBrowserWindow.ShowDialog())
    //        {
    //            result = modelBrowserWindow.SelectedMember;
    //        }
    //        if (!string.IsNullOrWhiteSpace(result))
    //        {
    //            _ResultValue.PropertyName = result;
    //        }
    //        OnFormClosed(new FormClosedEventArgs(CloseReason.UserClosing));
    //    }

    //    protected override System.Drawing.Size CalcFormSizeCore()
    //    {
    //        return new System.Drawing.Size(1, 1);
    //    }

    //    public override object ResultValue
    //    {
    //        get { return _ResultValue; }
    //    }
    //}



}
