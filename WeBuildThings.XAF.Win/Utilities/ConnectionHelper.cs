﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Security.ClientServer.Wcf;
using DevExpress.ExpressApp.Win;
using WeBuildThings.Utilities.Extensions;
using System;
using System.Configuration;
using System.ServiceModel;
using System.Windows.Forms;

namespace WeBuildThings.Win.Utilities
{
    public static class ConnectionHelper<T> where T : WinApplication, new()
    {
        const string TRUE = "TRUE";
        const string NTierApplicationSetting = "NTierApplication";
        const string ApplicationNameSetting = "ApplicationName";
        const string NTierConnectionStringName = "NTierConnectionString";
        const string CSConnectionStringName = "CSConnectionString";

        private static bool NTier;
        private static string ConnectionString;
        private static string ApplicationName;

        public static void Start()
        {
            ConfigMgrHelper.TestAppSetting(NTierApplicationSetting);
            NTier = (ConfigurationManager.AppSettings[NTierApplicationSetting].ToUpper() == TRUE);
            ConfigMgrHelper.TestAppSetting(ApplicationNameSetting);
            ApplicationName = ConfigurationManager.AppSettings[ApplicationName];
            string connectionStringName = string.Empty;
            if (NTier)
            {
                ConfigMgrHelper.ValidateConnectionStringName(NTierConnectionStringName);
                connectionStringName = NTierConnectionStringName;
            }
            else
            {
                ConfigMgrHelper.ValidateConnectionStringName(CSConnectionStringName);
                connectionStringName = CSConnectionStringName;
            }
            ConnectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
            if (NTier)
            {
                NTierApplicationStart(ConnectionString, ApplicationName);
            }
            else
            {
                ClientServerStart(ConnectionString, ApplicationName);
            }
        }

        public static void ClientServerStart(string connectionString, string applicationName)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            EditModelPermission.AlwaysGranted = System.Diagnostics.Debugger.IsAttached;
            T winApplication = new T();
            winApplication.ConnectionString = connectionString;
            winApplication.ApplicationName = applicationName;
            try
            {
                DevExpress.Xpo.XpoDefault.TrackPropertiesModifications = true;
                winApplication.Setup();
                winApplication.Start();
            }
            catch (Exception e)
            {
                winApplication.HandleException(e);
            }
        }

        public static void NTierApplicationStart(string connectionString, string applicationName)
        {
            T winApplication = new T();
            winApplication.ConnectionString = connectionString;
            WcfSecuredDataServerClient clientDataServer = new WcfSecuredDataServerClient(WcfDataServerHelper.CreateDefaultBinding(), new EndpointAddress(winApplication.ConnectionString));
            try
            {
                ServerSecurityClient securityClient = new ServerSecurityClient(clientDataServer, new ClientInfoFactory());
                securityClient.IsSupportChangePassword = true;
                winApplication.ApplicationName = applicationName;
                winApplication.Security = securityClient;
                winApplication.CreateCustomObjectSpaceProvider += delegate(object sender, CreateCustomObjectSpaceProviderEventArgs e) { e.ObjectSpaceProvider = new DataServerObjectSpaceProvider(clientDataServer, securityClient); };
                DevExpress.Xpo.XpoDefault.TrackPropertiesModifications = true;
                winApplication.Setup();
                winApplication.Start();
            }
            catch (Exception e)
            {
                winApplication.HandleException(e);
            }
            finally
            {
                if (clientDataServer.State != CommunicationState.Faulted && clientDataServer.State != CommunicationState.Closed && clientDataServer.State != CommunicationState.Closing)
                {
                    clientDataServer.Close();
                }
            }
        }

        public static void DatabaseMismatch(object sender, DevExpress.ExpressApp.DatabaseVersionMismatchEventArgs e)
        {
            if (NTier)
            {
                string message = e.CompatibilityError.ToString();

                throw new InvalidOperationException(
                    "The application cannot connect to the specified database " +
                    "because the latter does not exist or its version is older " +
                    "than that of the application.");
            }
            else
            {
#if EASYTEST
                            e.Updater.Update();
                            e.Handled = true;
#else
                //if (System.Diagnostics.Debugger.IsAttached)
                //{
                    e.Updater.Update();
                    e.Handled = true;
                //}
                //else
                //{
                //    throw new InvalidOperationException(
                //        "The application cannot connect to the specified database, because the latter doesn't exist or its version is older than that of the application.\r\n" +
                //        "This error occurred  because the automatic database update was disabled when the application was started without debugging.\r\n" +
                //        "To avoid this error, you should either start the application under Visual Studio in debug mode, or modify the " +
                //        "source code of the 'DatabaseVersionMismatch' event handler to enable automatic database update, " +
                //        "or manually create a database using the 'DBUpdater' tool.\r\n" +
                //        "Anyway, refer to the 'Update Application and Database Versions' help topic at http://www.devexpress.com/Help/?document=ExpressApp/CustomDocument2795.htm " +
                //        "for more detailed information. If this doesn't help, please contact our Support Team at http://www.devexpress.com/Support/Center/");
                //}
#endif
            }
        }
    }
}
