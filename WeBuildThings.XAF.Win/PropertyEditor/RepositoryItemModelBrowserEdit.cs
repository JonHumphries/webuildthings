﻿using DevExpress.XtraEditors.Drawing;
using DevExpress.XtraEditors.Registrator;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraEditors.ViewInfo;
using JonBuildsThings.Win.Utilities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace JonBuildsThings.Win.PropertyEditor
{
    /// <summary>
    /// Stores Editor Specific properties?  Maybe I can use this to get the Type?
    /// </summary>
    /// <seealso cref="https://documentation.devexpress.com/#WindowsForms/CustomDocument4716"/>
    [UserRepositoryItem("RegisterModelBrowserEdit")]
    public class RepositoryItemModelBrowserEdit : RepositoryItemPopupContainerEdit
    {
        static RepositoryItemModelBrowserEdit() { RegisterModelBrowserEdit(); }

        public override string EditorTypeName
        {
            get { return ModelBrowserEditName; }
        }

        //The unique name for the custom editor 
        public const string ModelBrowserEditName = "ModelBrowserEdit";

        //Register the editor 
        public static void RegisterModelBrowserEdit()
        {
            //Icon representing the editor within a container editor's Designer 
            Image img = null;
            //try
            //{
            //    img = (Bitmap)Bitmap.FromStream(Assembly.GetExecutingAssembly().
            //      GetManifestResourceStream("DevExpress.CustomEditors.CustomEdit.bmp"));
            //}
            //catch
            //{
            //}
            EditorRegistrationInfo.Default.Editors.Add(new EditorClassInfo(ModelBrowserEditName, typeof(ModelBrowserControl), typeof(RepositoryItemModelBrowserEdit), typeof(PopupContainerEditViewInfo), new ButtonEditPainter(), true, img));
        }

        internal RepositoryItemModelBrowserEdit()
            : base()
        { 
        }

        public override void Assign(RepositoryItem item)
        {
            BeginUpdate();
            try
            {
                base.Assign(item);
            }
            finally
            {
                EndUpdate();
            }
        }
    }
}
