﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Core.ModelEditor;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid.Views.Grid;
using WeBuildThings.Utilities.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;

namespace WeBuildThings.Win.PropertyEditor
{
    [PropertyEditor(typeof(String), false)]
    public class ModelBrowserEditor : DXPropertyEditor
    {
        public ModelBrowserEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
        }
        protected override object CreateControlCore()
        {
            return new FieldPicker();
        }
        protected override RepositoryItem CreateRepositoryItem()
        {
            return new RepositoryFieldPicker();
        }
        protected override void SetupRepositoryItem(RepositoryItem item)
        {
            base.SetupRepositoryItem(item);
            ((RepositoryFieldPicker)item).QueryPopUp += ModelBrowserEditor_QueryPopUp;
        }
        void ModelBrowserEditor_QueryPopUp(object sender, CancelEventArgs e)
        {
            FieldPicker fieldPicker = (FieldPicker)sender;
            if (fieldPicker.DataBindings.Count == 1)
            {
                fieldPicker.Properties.ClassType = ((IModelBrowser)fieldPicker.DataBindings[0].DataSource).Type;
            }
        }
    }
    public class ModelBrowserEditorInitializationController : ViewController<ListView>
    {
        GridListEditor gridListEditor;
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            gridListEditor = View.Editor as GridListEditor;
            if (gridListEditor != null)
            {
                gridListEditor.GridView.ShownEditor -= GridView_ShownEditor;
                gridListEditor.GridView.ShownEditor += GridView_ShownEditor;
            }
        }
        void GridView_ShownEditor(object sender, EventArgs e)
        {
            GridView gridView = (GridView)sender;
            if (gridView.ActiveEditor is FieldPicker)
            {
                string propertyName = gridListEditor.FindColumnPropertyName(gridView.FocusedColumn);
                //IList<IMemberInfo> memberPath = View.ObjectTypeInfo.FindMember(propertyName).GetPath();
                //IMemberInfo modelBrowserMamber = memberPath[memberPath.Count - 2];
                IMemberInfo modelBrowserMamber = View.ObjectTypeInfo.FindMember(propertyName).GetPath().FirstOrDefault();
                ((FieldPicker)gridView.ActiveEditor).Properties.ClassType = ((IModelBrowser)modelBrowserMamber.GetValue(gridView.GetFocusedRow())).Type;
            }
        }
        protected override void OnDeactivated()
        {
            base.OnDeactivated();
            if (gridListEditor != null && gridListEditor.GridView != null)
            {
                gridListEditor.GridView.ShownEditor -= GridView_ShownEditor;
                gridListEditor = null;
            }
        }
    }
}