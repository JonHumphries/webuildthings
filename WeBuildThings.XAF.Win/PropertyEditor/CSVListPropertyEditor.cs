﻿/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp.Model;

namespace WeBuildThings.Win.PropertyEditor
{
    /// <see cref="http://blog.zerosharp.com/three-ways-to-store-a-list-of-currency-codes-in-xaf/"/>
    [PropertyEditor(typeof(String), false)]
    public class CSVListPropertyEditor<T>: SerializedListPropertyEditor<T>
        {
            public CSVListPropertyEditor(Type T, IModelMemberViewItem info)
                : base(T, info) { }

            protected override string GetDisplayText(T t)
            {
                return String.Format("{0}\t{1}", t);
            }

            protected override string GetValue(T t)
            {
                return t.ToString();
            }
        }
    }
    //[ModelDefault("PropertyEditorType", "Solution1.Module.Web.CurrencyListPropertyEditor")]
}
*/