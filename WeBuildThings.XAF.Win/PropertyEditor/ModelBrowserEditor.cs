﻿using DevExpress.ExpressApp.Model;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.XtraEditors.Controls;
using DevExpress.ExpressApp.Editors;
using DevExpress.XtraEditors.Repository;
using JonBuildsThings.Utilities.Interfaces;
using JonBuildsThings.Win.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DevExpress.XtraEditors;

namespace JonBuildsThings.Win.PropertyEditor
{
    [ToolboxItem(true)]
    [PropertyEditor(typeof(IModelBrowser), false)]
    public class ModelBrowserEditor : DXPropertyEditor, IInplaceEditSupport
    {
        private ModelBrowserControl ModelBrowser;

        static ModelBrowserEditor() { RepositoryItemModelBrowserEdit.RegisterModelBrowserEdit(); }

        protected override object CreateControlCore()
        {
           return new ModelBrowserControl(this);
        }

        private void OnControlClicked(object sender, EventArgs e)
        {
            ModelBrowser.ShowPopup();
        }

        public ModelBrowserEditor(Type objectType, IModelMemberViewItem info)
            : base(objectType, info)
        {
        }

        protected override void Dispose(bool disposing)
        {
            if (ModelBrowser != null)
            {
                ModelBrowser.Dispose();
                ModelBrowser = null;
            }
            base.Dispose(disposing);
        }

        RepositoryItem IInplaceEditSupport.CreateRepositoryItem()
        {
            return new RepositoryItemModelBrowserEdit();
        }
    }

}
