﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Win.SystemModule;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraPrinting;
using System;
using System.Linq;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Common.Serialization;
using WeBuildThings.Module.CustomPopUps;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class XML_ViewController : WinExportController
    {
        public XML_ViewController()
        {
            InitializeComponent();
            TargetViewType = ViewType.ListView;
            this.ExportActionItemsCreated += exportActionItemsCreated;
        }

        private void exportActionItemsCreated(object sender, EventArgs e)
        {
            if (View.ObjectTypeInfo.Type.ContainsInterface(typeof(ISerialize)))
            {
                var excelActionItem = this.ExportAction.Items.Find(ExportTarget.Xls);
                if (excelActionItem != null)
                {
                    ChoiceActionItem cai = new ChoiceActionItem("XML", "XML File", 999);
                    cai.ImageName = "XML_16x16";
                    this.ExportAction.Items.Insert(0, cai);
                }
            }
        }

        protected override void Export(ExportTarget exportTarget)
        {
            if ((int)exportTarget == 999)
            {
                if (!View.SelectedObjects[0].GetType().GetInterfaces().Where(n => n == typeof(ISerialize)).Any())
                {
                    throw new NotImplementedException(string.Format("{0} cannot be exported to XML format. Contact your System Support Representative if this is necessary functionality.", View.SelectedObjects[0].GetType()));
                }
                using (var form = new System.Windows.Forms.SaveFileDialog())
                {
                    form.Filter = "Xml Document| *.xml";
                    var dialogResult = form.ShowDialog();
                    if (dialogResult == System.Windows.Forms.DialogResult.OK)
                    {
                        var fileInfo = new System.IO.FileInfo(form.FileName);
                        string filePath = fileInfo.FullName.Replace(fileInfo.Extension, ".xml");
                        ((ISerialize)View.SelectedObjects[0]).Export(filePath, View.SelectedObjects.ToArray());
                    }
                }
            }
            else
            {
                base.Export(exportTarget);
            }
        }

        protected override void OnDeactivated()
        {
            ExportActionItemsCreated -= exportActionItemsCreated;
            base.OnDeactivated();
        }

        private void ImportXML_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            FileUploadParameter fileUploadParameter = objectSpace.CreateObject<FileUploadParameter>();
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, fileUploadParameter);
        }

        private void ImportXML_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            FileUploadParameter fileParameter = e.PopupWindow.View.CurrentObject as FileUploadParameter;
            Session session = fileParameter.Session;
            fileParameter.File.Transfer();
            string filePath = fileParameter.File.FullPath;
            Type type = null;
            if (View.ObjectTypeInfo.Type.IsAbstract)
            {
                type = typeof(SerializedCollection);
            }
            else
            {
                var instance = View.ObjectTypeInfo.Type.CreateInstance(session);
                type = ((ISerialize)instance).SerializedType;
                var baseObject = instance as BaseObject;
                if (baseObject != null)
                {
                    baseObject.Delete();
                }
            }
            var convertedResults = SerializeToXAF.Import(session, filePath, type);
            View.Refresh();
        }
    }
}
