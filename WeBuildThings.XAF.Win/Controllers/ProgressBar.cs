﻿using System;
using System.Windows.Forms;
using WeBuildThings.Common.Utilities;

namespace WeBuildThings.Win
{
    public partial class ProgressBar_Form : Form, IProgressBar
    {
        private bool _PercentView;
        private IMonitoredProcess _MonitoredProcess;

        public IMonitoredProcess MonitoredProcess
        {
            get { return _MonitoredProcess; }
            set { if (_MonitoredProcess != value) { _MonitoredProcess = value; } }
        }

        public bool PercentView 
        {
            get {return _PercentView;}
            set { if (_PercentView != value) { _PercentView = value; OnPercentViewChanged(); } }
        }
        
        public ProgressBar_Form()
        {
            InitializeComponent();
        }

        public void PerformStep()
        {
            this.ProgressBar_Control.PerformStep();
            System.Windows.Forms.Application.DoEvents();
        }

        public void SetMaximum(int maximum)
        {
            this.ProgressBar_Control.Properties.Maximum = maximum;
        }

        private void OnCancelButtonClicked(object sender, EventArgs e)
        {
            MonitoredProcess.Cancel();
        }

        public void OnPercentViewChanged()
        {
            this.ProgressBar_Control.Properties.PercentView = PercentView;
        }

    }
}
