﻿namespace WeBuildThings.Controllers
{
    partial class XML_ViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ImportXML = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // ImportXML
            // 
            this.ImportXML.AcceptButtonCaption = null;
            this.ImportXML.CancelButtonCaption = null;
            this.ImportXML.Caption = "Import XML";
            this.ImportXML.Category = "ObjectsCreation";
            this.ImportXML.ConfirmationMessage = null;
            this.ImportXML.Id = "ImportXML";
            this.ImportXML.ImageName = "XML";
            this.ImportXML.TargetObjectType = typeof(WeBuildThings.Common.Serialization.ISerialize);
            this.ImportXML.ToolTip = "Imports Records from an XML formatted file";
            this.ImportXML.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.ImportXML_CustomizePopupWindowParams);
            this.ImportXML.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.ImportXML_Execute);
            // 
            // XMLExportViewController
            // 
            this.Actions.Add(this.ImportXML);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction ImportXML;
    }
}
