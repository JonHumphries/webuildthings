﻿namespace WeBuildThings.Win
{
    partial class ProgressBar_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param _Name="disposing">true if managed resources should be _Disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tlp = new System.Windows.Forms.TableLayoutPanel();
            this.ProgressBar_Control = new DevExpress.XtraEditors.ProgressBarControl();
            this.Cancel_Button = new DevExpress.XtraEditors.SimpleButton();
            this.tlp.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar_Control.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // tlp
            // 
            this.tlp.ColumnCount = 2;
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.Controls.Add(this.ProgressBar_Control, 0, 0);
            this.tlp.Controls.Add(this.Cancel_Button, 0, 1);
            this.tlp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlp.Location = new System.Drawing.Point(0, 0);
            this.tlp.Name = "tlp";
            this.tlp.RowCount = 2;
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlp.Size = new System.Drawing.Size(124, 84);
            this.tlp.TabIndex = 0;
            // 
            // ProgressBar_Control
            // 
            this.tlp.SetColumnSpan(this.ProgressBar_Control, 2);
            this.ProgressBar_Control.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProgressBar_Control.Location = new System.Drawing.Point(3, 3);
            this.ProgressBar_Control.Name = "ProgressBar_Control";
            this.ProgressBar_Control.Properties.Step = 1;
            this.ProgressBar_Control.Size = new System.Drawing.Size(118, 36);
            this.ProgressBar_Control.TabIndex = 0;
            // 
            // Cancel_Button
            // 
            this.Cancel_Button.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel_Button.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cancel_Button.Location = new System.Drawing.Point(3, 45);
            this.Cancel_Button.Name = "Cancel_Button";
            this.Cancel_Button.Size = new System.Drawing.Size(56, 36);
            this.Cancel_Button.TabIndex = 1;
            this.Cancel_Button.Text = "Cancel";
            this.Cancel_Button.Click += new System.EventHandler(this.OnCancelButtonClicked);
            // 
            // ProgressBar_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Cancel_Button;
            this.ClientSize = new System.Drawing.Size(124, 84);
            this.ControlBox = false;
            this.Controls.Add(this.tlp);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(140, 100);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(140, 100);
            this.Name = "ProgressBar_Form";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.tlp.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ProgressBar_Control.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlp;
        private DevExpress.XtraEditors.ProgressBarControl ProgressBar_Control;
        private DevExpress.XtraEditors.SimpleButton Cancel_Button;
    }
}