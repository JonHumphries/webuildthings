﻿using DevExpress.ExpressApp;
using WeBuildThings.Common.Utilities;
using System.ComponentModel;

namespace WeBuildThings.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class ProgressBarViewController : ViewController
    {
        public IProgressBar ProgressBar { get; set; }
        public IMonitoredProcess MonitoredProcess { get; set; }
        public bool Continue { get; set; }

        public ProgressBarViewController()
        {
            InitializeComponent();
            TargetObjectType = typeof(IMonitoredProcess);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            var monitoredProcessDetailView = View as DetailView;
            if (monitoredProcessDetailView != null)
            {
                var currentObject = monitoredProcessDetailView.CurrentObject as IMonitoredProcess;
                currentObject.BackgroundWorker.DoWork += ProgressBarDoWork;
                MonitoredProcess = currentObject;
            }
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        public void Cancel()
        {
            ProgressBar.MonitoredProcess.Cancel();
        }

        public void ProgressBarDoWork(object sender, DoWorkEventArgs e)
        {
            var backgroundWorker = sender as BackgroundWorker;
            backgroundWorker.ProgressChanged += ProgressBarPerformStep;
            backgroundWorker.RunWorkerCompleted += ProgressBarProcessComplete;
            using (ProgressBar = new ProgressBar_Form())
            {
                ((ProgressBar_Form)ProgressBar).Show();
                ProgressBar.MonitoredProcess = MonitoredProcess;
                ProgressBar.SetMaximum(MonitoredProcess.Maximum);
            }
        }

        public void ProgressBarProcessComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            MonitoredProcess.BackgroundWorker.ProgressChanged -= ProgressBarPerformStep;
            MonitoredProcess.BackgroundWorker.RunWorkerCompleted -= ProgressBarProcessComplete;
            if (ProgressBar != null)
            {
                ProgressBar.Dispose();
            }
        }

        public void ProgressBarPerformStep(object sender, ProgressChangedEventArgs e)
        {
            ProgressBar.PerformStep();
        }
    }
}
