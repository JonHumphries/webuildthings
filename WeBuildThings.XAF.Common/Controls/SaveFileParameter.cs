﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.Controls
{
    [NonPersistent]
    public class SaveFileParameter : BaseObject
    {
        
        #region Fields
        private string _File;
        #endregion Fields

        public SaveFileParameter(Session session)
            : base(session)
        {}

        #region Properties
        
        public string File
        {
            get { return _File; }
            set { SetPropertyValue("File", ref _File, value); }
        }
        #endregion Properties
    }
}
