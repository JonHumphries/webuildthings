﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common.Controls
{
    [NonPersistent]
    public class SetPassword : BaseObject
    {

        #region Fields
        private string _NewPassword;
        private string _ConfirmPassword;
        #endregion Fields

        #region Constructors
        public SetPassword(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public string NewPassword
        {
            get { return _NewPassword; }
            set { SetPropertyValue("NewPassword", ref _NewPassword, value); }
        }

        public string ConfirmPassword
        {
            get { return _ConfirmPassword; }
            set { SetPropertyValue("ConfirmPassword", ref _ConfirmPassword, value); }
        }

        #endregion Properties
    }
}
