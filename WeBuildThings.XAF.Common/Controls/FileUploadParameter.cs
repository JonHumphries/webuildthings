﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.Module.CustomPopUps
{
    [NonPersistent]
    public class FileUploadParameter: BaseObject
    {

        #region Fields
        private XFile _File;
        #endregion Fields

        public FileUploadParameter(Session session ) : base (session)
        {}

        #region Properties
        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never)]
        public XFile File
        {
            get { return _File; }
            set { SetPropertyValue("File", ref _File, value); }
        }
        #endregion Properties
    }
}
