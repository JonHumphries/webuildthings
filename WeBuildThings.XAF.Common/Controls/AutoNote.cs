﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common.Controls
{
    [NonPersistent]
    public class AutoNote : BaseObject
    {
        #region Fields
        private CommentTemplate _Comment;
        private string _Subject;
        private string _Text;
        private LookupItem _Type;
        #endregion Fields

        #region Constructors
        public AutoNote(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        [ImmediatePostData]
        public CommentTemplate Comment
        {
            get { return _Comment; }
            set { if (SetPropertyValue("Comment", ref _Comment, value) && !IsLoading) { OnCommentChanged(); } }
        }

        public string Subject
        {
            get { return _Subject; }
            set { SetPropertyValue("Subject", ref _Subject, value);}
        }

        [Size(SizeAttribute.Unlimited)]
        public string Text
        {
            get { return _Text; }
            set { SetPropertyValue("Text", ref _Text, value); }
        }

        [DataSourceCriteria("[LookupList.Name] = 'CommentType' and [Active]")]
        public LookupItem Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        #endregion Properties

        #region Event Methods
        private void OnCommentChanged()
        {
            SetSubject();
            SetText();
            SetType();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetSubject()
        {
            Subject = Comment.Subject;
        }

        private void SetText()
        {
            Text = Comment.Text;
        }

        private void SetType()
        {
            Type = Comment.Type;
        }
        #endregion Set Methods

        #region Public Methods
        public virtual void CopyProperties(Comment comment)
        {
            comment.Subject = Subject;
            comment.Text = Text;
            if (Type != null)
            {
                var type = comment.Session.FindObject<LookupItem>(new BinaryOperator("Name", Type.Name));
                comment.Type = type;
            }
            //foreach (var document in Comment.Documents)
            //{
            //    Comment.Documents.Add(document);
            //}
        }
        #endregion Public Methods
    }
}
