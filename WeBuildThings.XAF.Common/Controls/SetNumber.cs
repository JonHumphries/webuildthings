﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common.Controls
{
    [NonPersistent]
    public class SetNumber : BaseObject
    {

        #region Fields
        private int _NewNumber;
        #endregion Fields

        #region Constructors
        public SetNumber(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public int NewNumber
        {
            get { return _NewNumber; }
            set { SetPropertyValue("NewNumber", ref _NewNumber, value); }
        }
        #endregion Properties
    }
}
