﻿using System;
using System.Collections.Generic;
using System.Linq;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.ExpressApp.Security;
using DevExpress.Xpo.Metadata;


namespace WeBuildThings.XAF.Common.Security
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class SecurityOperationsAttribute : Attribute
    {
        readonly string collectionNameCore;
        readonly string operationProviderPropertyCore;

        public SecurityOperationsAttribute(string collectionName, string operationProviderProperty)
        {
            collectionNameCore = collectionName;
            operationProviderPropertyCore = operationProviderProperty;
        }

        public string CollectionName
        {
            get { return collectionNameCore; }
        }

        public string OperationProviderProperty
        {
            get { return operationProviderPropertyCore; }
        }
    }
}
