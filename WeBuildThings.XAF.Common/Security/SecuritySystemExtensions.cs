﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Security
{
    public static class SecuritySystemExtensions
    {
        public static SecuritySystemUser GetUserByUserName(this SecuritySystemUser currentUser, Session session)
        {
            SecuritySystemUser result = null;
            if (currentUser != null)
            {
                result = session.FindObject<SecuritySystemUser>(new BinaryOperator("UserName", currentUser.UserName));
            }
            return result;
        }
    }
}
