﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.Security
{
    [Serializable]
    public class SecuritySystemException: Exception
    {
        public SecuritySystemException()
            : base()
        { }

        public SecuritySystemException(string message)
            : base(message)
        { }
    }
}
