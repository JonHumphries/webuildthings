﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common.DatabaseUpdate
{
    public static class SystemSettingsUpdater
    {

        #region Consts
        public static string DefaultDocumentCatalogDirectory = @"C:\Documents";
        #endregion Consts

        public static void Update(IObjectSpace objectSpace)
        {
            UpdateCommentSystemSettings(objectSpace);
            UpdateDocumentSystemSettings(objectSpace);
        }

        public static void UpdateCommentSystemSettings(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var systemSettings = SystemSettings.GetInstance<CommentSystemSettings>(uow);
                if (uow.IsNewObject(systemSettings))
                {
                    systemSettings.CommentEditPeriod = 24;
                }
                if (systemSettings.MaxWindowCaptionSize < 1)
                {
                    systemSettings.MaxWindowCaptionSize = 100;
                }
                systemSettings.Save();
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
        }



        public static void UpdateDocumentSystemSettings(IObjectSpace objectSpace)
        {
            using (UnitOfWork uow = new UnitOfWork(((XPObjectSpace)objectSpace).Session.ObjectLayer))
            {
                var systemSettings = SystemSettings.GetInstance<DocumentSystemSettings>(uow);
                if (uow.IsNewObject(systemSettings))
                {
                    systemSettings.DocumentCatalogDefaultDirectory = DefaultDocumentCatalogDirectory;
                    systemSettings.MaxFilesPerDirectoryDefault = 1000;

                }
                systemSettings.Save();
                uow.CommitChanges();
            }
            objectSpace.CommitChanges();
        }

    }
}
