using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Updating;
using System;

namespace WeBuildThings.XAF.Common.DatabaseUpdate {
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppUpdatingModuleUpdatertopic
    public class Updater : ModuleUpdater 
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion) {
        }

        public override void UpdateDatabaseAfterUpdateSchema() 
        {
            SystemSettingsUpdater.Update(ObjectSpace);
            DocumentCatalogUpdater.Update(ObjectSpace);
            XUserUpdater.Update(ObjectSpace);
            LookupListUpdater.Update(ObjectSpace);
            XTimeZoneUpdater.Update(ObjectSpace);
            AreaCodeUpdater.Update(ObjectSpace);
            ObjectSpace.CommitChanges();
            base.UpdateDatabaseAfterUpdateSchema();   
        }

        public override void UpdateDatabaseBeforeUpdateSchema() {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }
    }
}
