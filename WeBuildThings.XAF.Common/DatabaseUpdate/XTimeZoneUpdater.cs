﻿using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Data.Filtering;

namespace WeBuildThings.XAF.Common.DatabaseUpdate
{
    public static class XTimeZoneUpdater
    {

        public static void Update(IObjectSpace ObjectSpace)
        {
            foreach (var timezone in TimeZoneInfo.GetSystemTimeZones())
            {
                var xtimeZone = ObjectSpace.FindObject<XTimeZone>(new BinaryOperator("Id",timezone.Id));
                if (xtimeZone == null)
                {
                    var xtz = ObjectSpace.CreateObject<XTimeZone>();
                    xtz.Id = timezone.Id;
                    ObjectSpace.CommitChanges();
                }
            }
        }
    }
}
