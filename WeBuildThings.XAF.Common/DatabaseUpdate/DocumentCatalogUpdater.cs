﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Common.DatabaseUpdate
{
    public static class DocumentCatalogUpdater
    {

        public static void Update(IObjectSpace objectSpace)
        {

            DocumentCatalog documentCatalog = objectSpace.FindObject<DocumentCatalog>(new BinaryOperator("Name", "Default"));
            if (documentCatalog == null)
            {
                documentCatalog = objectSpace.CreateObject<DocumentCatalog>();
                documentCatalog.Name = "Default";
            }

            objectSpace.CommitChanges();
        }
    }
}
