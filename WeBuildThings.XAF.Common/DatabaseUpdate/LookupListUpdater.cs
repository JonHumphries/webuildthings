﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using System.Linq;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Common.DatabaseUpdate
{
    /// <summary>
    /// Adds the Lookup Lists and values from this library to the database;
    /// </summary>
    public static class LookupListUpdater
    {
        static string[] lists = new string[] { XAddress.AddressTypeListName, Document.DocumentTypeListName, Comment.CommentTypeListName };
        static string[] DocumentTypes = new string[] {Document.DefaultDocumentType};

        public static void Update(IObjectSpace objectSpace)
        {
            CreateLookupLists(objectSpace, lists);
            UpdateLookupList(objectSpace, Document.DocumentTypeListName, DocumentTypes);
            objectSpace.CommitChanges();
        }

        public static void CreateLookupLists(IObjectSpace objectSpace, params string[] listNames)
        {
            foreach (var name in listNames)
            {
                LookupList lookupList = objectSpace.FindObject<LookupList>(new BinaryOperator("Name", name));
                if (lookupList == null)
                {
                    lookupList = objectSpace.CreateObject<LookupList>();
                    lookupList.Name = name;
                }
            }
            objectSpace.CommitChanges();
        }

        public static void UpdateLookupList(IObjectSpace objectSpace, string listName, params string[] listValues)
        {
            LookupList lookupList = objectSpace.FindObject<LookupList>(new BinaryOperator("Name", listName));
            if (lookupList == null)
            {
                lookupList = objectSpace.CreateObject<LookupList>();
                lookupList.Name = listName;
            }
            if (lookupList != null)
            {
                foreach (string value in listValues)
                {
                    LookupItem lookupItem = lookupList.LookupItems.Where(n => n.Name == value).FirstOrDefault();
                    if (lookupItem == null)
                    {
                        lookupItem = objectSpace.CreateObject<LookupItem>();
                        lookupItem.Name = value;
                        lookupItem.LookupList = lookupList;
                    }
                }
            }
            objectSpace.CommitChanges();
        }
    }
}
