﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Extensions;
using System;
using System.ComponentModel;
using System.Reflection;

namespace WeBuildThings.XAF.Common.Reflection
{
    //[DefaultClassOptions]
    public class MethodItem : BaseObject
    {
        #region Fields
        private string _AssemblyQualifiedName;
        private string _TypeFullName;
        private string _MethodName;
        private string _Name;
        private MethodInfo _MethodInfo;
        #endregion Fields

        #region Constructors
        public MethodItem(Session session)
            : base(session)
        {
        }

        protected override void OnLoaded()
        {
            SetMethodInfo();
            base.OnLoaded();
        }

        public static MethodItem CreateMethodItem(Session session, MethodInfo methodInfo)
        {
            GroupOperator criteria = new GroupOperator(GroupOperatorType.And, new BinaryOperator("TypeFullName", methodInfo.DeclaringType.FullName), new BinaryOperator("MethodName", methodInfo.Name));
            MethodItem methodItem = session.FindObject<MethodItem>(criteria);
            if (methodItem == null)
            {
                methodItem = new MethodItem(session) { MethodInfo = methodInfo };
            }
            return methodItem;
        }
        #endregion Constructors

        #region Properties

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Browsable(false)]
        [Size(SizeAttribute.Unlimited)]
        public string AssemblyQualifiedName
        {
            get { return _AssemblyQualifiedName; }
            set { if (SetPropertyValue("AssemblyQualifiedName", ref _AssemblyQualifiedName, value) && !IsLoading) { OnAssemblyQualifiedNameChanged(); } }
        }

        [Browsable(false)]
        [Size(SizeAttribute.Unlimited)]
        public string MethodName
        {
            get { return _MethodName; }
            set { if (SetPropertyValue("MethodName", ref _MethodName, value) && !IsLoading) { OnMethodNameChanged(); } }
        }

        [Size(SizeAttribute.Unlimited)]
        public string TypeFullName
        {
            get { return _TypeFullName; }
            set { SetPropertyValue("TypeFullName", ref _TypeFullName, value);}
        }

        [NonPersistent]
        public MethodInfo MethodInfo
        {
            get { return _MethodInfo; }
            set { if (_MethodInfo != value) { _MethodInfo = value;  OnMethodInfoChanged(); } }
        }
        #endregion Properties

        #region Event Methods

        private void OnMethodInfoChanged()
        {
            SetAssemblyName();
            SetMethodName();
            SetTypeFullName();
        }

        private void OnAssemblyQualifiedNameChanged()
        {
            SetName();
        }

        private void OnMethodNameChanged()
        {
            SetMethodInfo();
        }
        #endregion Event Methods

        #region Set Methods

        private void SetMethodInfo()
        {
            if (!string.IsNullOrWhiteSpace(AssemblyQualifiedName) && !string.IsNullOrWhiteSpace(MethodName))
            {
                MethodInfo = Type.GetType(AssemblyQualifiedName).GetMethod(MethodName);
            }
        }

        private void SetTypeFullName()
        {
            TypeFullName = MethodInfo.DeclaringType.FullName;
        }

        private void SetMethodName()
        {
            if (MethodInfo != null)
            {
                MethodName = MethodInfo.Name;
            }
        }

        private void SetAssemblyName()
        {
            if (MethodInfo != null)
            {
                AssemblyQualifiedName = MethodInfo.DeclaringType.AssemblyQualifiedName;
            }
        }

        private void SetName()
        {
            if (MethodInfo != null)
            {
                Name = MethodInfo.Name.SeparateCamelCase().Replace("Reve Med", "ReveMed").TrimStart().TrimEnd();
            }
        }
        #endregion Set Methods
    }
}
