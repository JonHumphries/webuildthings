using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Editors;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.Common.Utilities
{
    public class FilterCriteria : StandardTracking
    {
        #region Fields
        private string _Name;
        private PersistedType _ClassType;
        #endregion Fields

        #region Constructors
        public FilterCriteria(Session session) : base(session) { }

        internal FilterCriteria() : base() { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public PersistedType FilterObject
        {
            get { return _ClassType; }
            set { if (SetPropertyValue("ClassType", ref _ClassType, value)) { OnFilterObjectChanged(); } }
        }

        [CriteriaOptions("ObjectType"), Size(-1), ImmediatePostData]
        public string Criterion
        {
            get { return GetPropertyValue<string>("Criterion"); }
            set { SetPropertyValue<string>("Criterion", value); }
        }

        [Browsable(false)]
        public Type ObjectType
        {
            get { if (FilterObject != null) { return FilterObject.ObjectType; } else { return null; } }
        }

        [Size(SizeAttribute.Unlimited)]
        public string CriterionText
        {
            get { return Criterion; }
        }
        #endregion Properties

        #region Events
        public event EventHandler FilterObjectChanged;
        #endregion Events

        #region Public Methods
        /// <summary>
        /// Returns the criterion text as a Criteria Operator
        /// </summary>
        public CriteriaOperator GetOperator()
        {
            return Session.ParseCriteria(this.Criterion);
        }

        /// <summary>
        /// Returns true if the object to check fits the criteria
        /// </summary>
        public static bool CheckCriteria(XPBaseObject objectToCheck, string criteria)
        {
            return ((objectToCheck != null) && ((String.IsNullOrWhiteSpace(criteria) || (bool)objectToCheck.Evaluate(criteria))));
        }
        #endregion Public Methods

        #region Event Methods
        protected virtual void OnFilterObjectChanged()
        {
            InvokeFilterObjectChanged();
        }

        #endregion Event Methods

        #region Invoke Methods
        private void InvokeFilterObjectChanged()
        {
            EventHandler handler = FilterObjectChanged;
            if (handler != null)
            {
                handler.Invoke(this, EventArgs.Empty);
            }
        }
        #endregion Invoke Methods
    }
}