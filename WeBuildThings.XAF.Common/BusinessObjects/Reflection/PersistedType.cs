using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.Common.Extensions;
using WeBuildThings.Validation;

namespace WeBuildThings.XAF.Common.Reflection
{
    [DefaultClassOptions]
    public class PersistedType : BaseObject
    {
        #region Fields
        private string _AssemblyQualifiedName;
        private string _FullName;
        private string _Name;
        private Type _ObjectType;
        #endregion Fields

        #region Constructors
        public PersistedType(Session session)
            : base(session)
        {
        }

        internal PersistedType() : base()
        { }

        public static PersistedType CreatePersistedType(Session session, Type type)
        {
            PersistedType classItem = session.FindObject<PersistedType>(new BinaryOperator("FullName", type.FullName));
            if (classItem == null) classItem = new PersistedType(session) { ObjectType = type};
            return classItem;
        }

        public static PersistedType CreatePersistedType(Session session, string fullName)
        {
            if (string.IsNullOrWhiteSpace(fullName))
            {
                var exception = new ArgumentNullException("Full name of the Target Type");
                exception.Source = typeof(PersistedType).Name;
                throw exception;
            }
            Type type = TypeExtensions.GetTypeFromMyAssemblies(fullName);
            PersistedType persistedType = session.FindObject<PersistedType>(PersistentCriteriaEvaluationBehavior.InTransaction, new BinaryOperator("FullName", fullName));
            if (persistedType == null) persistedType = new PersistedType(session) { ObjectType = type };
            return persistedType;

        }
        #endregion Constructors

        #region Properties

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string FullName
        {
            get { return _FullName; }
            set { SetPropertyValue("FullName", ref _FullName, value); }
        }

        [Browsable(false)]
        [Size(SizeAttribute.Unlimited)]
        public string AssemblyQualifiedName
        {
            get { return _AssemblyQualifiedName; }
            set { if (SetPropertyValue("AssemblyQualifiedName", ref _AssemblyQualifiedName, value) ) { OnAssemblyQualifiedNameChanged(); } }
        }

        [NonPersistent]
        public Type ObjectType
        {
            get { return _ObjectType; }
            set { if (_ObjectType != value) { _ObjectType = value; OnObjectTypeChanged(); } }
        }
        #endregion

        #region Event Methods
       
        private void OnAssemblyQualifiedNameChanged()
        {
            SetObjectType();
            if (!IsLoading)
            {
                SetFullName();
                SetName();
            }
        }

        protected virtual void OnObjectTypeChanged()
        {
            SetAssemblyQualifiedName();
            SetFullName();
            SetName();
        }

        #endregion Event Methods

        #region Set Methods
        private void SetObjectType()
        {
            if (!string.IsNullOrWhiteSpace(AssemblyQualifiedName))
            {
                ObjectType = Type.GetType(AssemblyQualifiedName);
            }
        }

        private void SetName()
        {
            if (ObjectType != null)
            {
                Name = ObjectType.Name.SeparateCamelCase().TrimStart().TrimEnd();
            }
        }

        private void SetFullName()
        {
            if (ObjectType != null)
            {
                FullName = ObjectType.FullName;
            }
        }

        private void SetAssemblyQualifiedName()
        {
            if (ObjectType != null)
            {
                AssemblyQualifiedName = ObjectType.AssemblyQualifiedName;
            }
        }


        #endregion Set Methods

        #region Validation Methods

        public bool ValidateType(bool exception, Type type)
        {
            return ValidateType(exception, this, type);
        }

        public static bool ValidateType(bool exception, PersistedType classItem, Type expectedType)
        {
            return ValidationRules.ValidateType(exception, classItem.ObjectType, expectedType);
        }

        #endregion Validation Methods
    }
}
