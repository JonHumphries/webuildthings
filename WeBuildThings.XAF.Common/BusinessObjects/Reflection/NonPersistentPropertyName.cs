﻿using DevExpress.ExpressApp.Model;
using WeBuildThings.Utilities.Interfaces;
using System;
using System.Xml.Serialization;

namespace WeBuildThings.XAF.Common.Reflection
{
    [DisplayProperty("PropertyName")]
    public class NonPersistentPropertyName : IModelBrowser
    {
        #region Fields
        private string _PropertyName;
        private IPersistedProperty _ParentObject;
        private string _TargetPropertyName;
        #endregion Fields

        #region Constructors
        public NonPersistentPropertyName(IPersistedProperty parent, string targetPropertyName)
        {
            ParentObject = parent;
            TargetPropertyName = targetPropertyName;
        }

        internal NonPersistentPropertyName()
        {
        }
        #endregion Constructors

        #region Properties
        [XmlIgnore]
        public IPersistedProperty ParentObject
        {
            get { return _ParentObject; }
            set { if (_ParentObject != value) _ParentObject = value; }
        }
        
        public string PropertyName
        {
            get { return _PropertyName; }
            set { if (_PropertyName != value) { _PropertyName = value; OnPropertyNameChanged(); } }
        }

        public Type Type
        {
            get { return ParentObject.GetType(TargetPropertyName); }
        }

        public string TargetPropertyName
        {
            get { return _TargetPropertyName; }
            set { if (_TargetPropertyName != value) _TargetPropertyName = value; }
        }
        #endregion Properties

        protected virtual void OnPropertyNameChanged()
        {
            SetPersistedPropertyName();
        }

        private void SetPersistedPropertyName()
        {
            ParentObject.SetTargetProperty(TargetPropertyName, PropertyName);
        }

        public override string ToString()
        {
            return PropertyName;
        }

    }

    
}
