﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeBuildThings.XAF.Common
{
    public class DocumentSystemSettings : SystemSettings
{
    #region Fields
    private string _DocumentCatalogDefaultDirectory;
    private int _MaxFilesPerDirectoryDefault;
    #endregion Fields

    #region Constructor
    public DocumentSystemSettings(Session session) : base(session) { }

    public DocumentSystemSettings() : base(Session.DefaultSession) { }
    #endregion Constructor

    #region Properties
    public string DocumentCatalogDefaultDirectory
    {
        get { return _DocumentCatalogDefaultDirectory; }
        set { SetPropertyValue("DocumentCatalogDefaultDirectory", ref _DocumentCatalogDefaultDirectory, value); }
    }

    public int MaxFilesPerDirectoryDefault
    {
        get { return _MaxFilesPerDirectoryDefault; }
        set { SetPropertyValue("MaxFilesPerDirectory", ref _MaxFilesPerDirectoryDefault, value); }
    }
    #endregion Properties

}
}
