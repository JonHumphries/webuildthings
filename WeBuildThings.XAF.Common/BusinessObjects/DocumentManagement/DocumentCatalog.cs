﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.IO;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    [DefaultProperty("DocumentCatalogName")]
    public class DocumentCatalog : BaseObject
    {
        #region Constants
        public const string DefaultDocumentCatalogName = "Default";
        #endregion Constants

        #region Fields
        private string _Name;
        private string _CatalogDirectory;
        private int _CurrentInstance;
        private int _MaxFilesPerInstance;
        private int _RoughCount;
        private DocumentSystemSettings _DocumentSystemSettings;
        private string _Note;
        #endregion

        #region Constructor
        public DocumentCatalog(Session session)
            : base(session)
        { }

        internal DocumentCatalog() : base() { }

        public override void AfterConstruction()
        {
            var settings = SystemSettings.GetInstance<DocumentSystemSettings>(Session);
            CatalogDirectory = settings.DocumentCatalogDefaultDirectory;
            MaxFilesPerInstance = settings.MaxFilesPerDirectoryDefault;
            base.AfterConstruction();
        }

        /// <summary>
        /// Get the document catalog specified or creates it.
        /// </summary>
        public static DocumentCatalog CreateDocumentCatalog(Session session, string name)
        {
            DocumentCatalog result = null;
            if (!string.IsNullOrWhiteSpace(name))
            {
                name = name.Trim();
                DocumentCatalog existingDocumentCatalog = session.FindObject<DocumentCatalog>(new BinaryOperator("Name", name));
                result = (existingDocumentCatalog == null) ? new DocumentCatalog(session) : existingDocumentCatalog;
                result.Name = name;
            }
            return result;
        }
        #endregion

        #region Properties

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("DocumentCatalogName", ref _Name, value); }
        }

        public string CatalogDirectory
        {
            get { return _CatalogDirectory; }
            set { SetPropertyValue("CategoryDirectory", ref _CatalogDirectory, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        public int CurrentInstance
        {
            get { return _CurrentInstance; }
            set { SetPropertyValue("CurrentSplit", ref _CurrentInstance, value); }
        }

        public int MaxFilesPerInstance
        {
            get { return _MaxFilesPerInstance; }
            set { SetPropertyValue("MaxFilesPerSplit", ref _MaxFilesPerInstance, value); }
        }

        [ModelDefault("AllowEdit", "false")]
        public int RoughCount
        {
            get { return _RoughCount; }
            set { SetPropertyValue("RoughCountInCurrentSplit", ref _RoughCount, value); }
        }

        public DocumentSystemSettings DocumentSystemSettings
        {
            get { return _DocumentSystemSettings; }
            set { SetPropertyValue("DocumentSystemSettings", ref _DocumentSystemSettings, value); }
        }

        public string Note
        {
            get { return _Note; }
            set { SetPropertyValue("Note", ref _Note, value); }
        }

        #endregion

        #region Override Methods

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!string.IsNullOrWhiteSpace(_Name))
            {
                try
                {
                    if (FileOperations.CreateNewDirectory(CurrentDirectoryPath))
                    {
                        RoughCount = 0;
                        CurrentInstance = 0;
                    }
                    FileOperations.CreateNewDirectory(CurrentFullPath);
                }
                catch (Exception e)
                {
                    Note = e.Message;
                }
            }
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
        }

        #endregion

        #region Methods

        private static string GetApplicationBaseDirectory(Session session)
        {
            return SystemSettings.GetInstance<DocumentSystemSettings>(session).DocumentCatalogDefaultDirectory;
        }

        private string CurrentDirectoryPath
        {
            get { return Path.Combine(GetApplicationBaseDirectory(Session), CatalogDirectory); }
        }

        private string CurrentFullPath
        {
            get { return Path.Combine(CurrentDirectoryPath, Convert.ToString(CurrentInstance)); }
        }

        public string GetCurrentDirectory()
        {
            RoughCount = FileOperations.GetNumberOfFilesInFolder(CurrentFullPath);
            if (RoughCount < MaxFilesPerInstance)
            {
                RoughCount++;
            }
            else
            {
                if (RoughCount >= MaxFilesPerInstance)
                {
                    CurrentInstance++;
                    while (!FileOperations.CreateNewDirectory(CurrentFullPath) && FileOperations.GetNumberOfFilesInFolder(CurrentFullPath) >= MaxFilesPerInstance)
                    {
                        CurrentInstance++;
                    }
                    RoughCount = FileOperations.GetNumberOfFilesInFolder(CurrentFullPath) + 1;
                }
            }
            return CurrentFullPath;
        }
        #endregion
    }
}