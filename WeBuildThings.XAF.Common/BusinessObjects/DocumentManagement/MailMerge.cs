﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using JonBuildsThings.BusinessObjects.Reflection;
using System.ComponentModel;

namespace JonBuildsThings.BusinessObjects.DocumentManagement
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class MailMerge: BaseObject
    {
        #region Fields

        private string _Name;
        private ClassItem _MailMergeSource;

        #endregion Fields

        #region Constructors

        public MailMerge(Session session) : base(session) { }

        #endregion

        #region Properties

        public ClassItem MailMergeSource
        {
            get { return _MailMergeSource; }
            set { SetPropertyValue("MailMergeSource", ref _MailMergeSource, value); }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }


        [Aggregated, Association("MailMerge-MailMergeItems", typeof(MailMergeItem))]
        public XPCollection<MailMergeItem> MailMergeItems
        {
            get { return GetCollection<MailMergeItem>("MailMergeItems"); }
        }

        #endregion Properties
    }
}
