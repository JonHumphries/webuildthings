﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.IO;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common
{
    [DefaultProperty("FileName")]
    public class XFile : BaseObject, IFileData, IDisposable
    {
        #region Fields

        private const int _ReadBytesSize = 0x1000;
        private bool disposed;
        private Guid _Number;
        private FileInfo _FileInfo;
        private string _LocalPath;
        private string _SaveDirectory;
        private string _FileName;
        private string _Extension;
        private int _Size;
        private string _FullPath;

        #endregion

        #region Constructor
        public XFile(Session session) : base(session) { }

        internal XFile() : base() { }

        public override void AfterConstruction()
        {
            Number = Guid.NewGuid();
            SaveDirectory = SystemSettings.GetInstance<DocumentSystemSettings>(Session).DocumentCatalogDefaultDirectory;
            base.AfterConstruction();
        }
        /// <summary>
        /// Creates XFile and prepares it for storage in the document catalog
        /// </summary>
        public static XFile CreateXFile(Session session, string fullPath, string documentCatalogName)
        {
            XFile xFile = new XFile(session);
            xFile.LocalPath = fullPath;
            xFile.Extension = Path.GetExtension(fullPath);
            DocumentCatalog documentCatalog = DocumentCatalog.CreateDocumentCatalog(session, documentCatalogName);
            if (documentCatalog != null)
            {
                xFile.SaveDirectory = documentCatalog.GetCurrentDirectory();
            }
            xFile.FileInfo = new FileInfo(fullPath);
            return xFile;
        }
        #endregion

        #region Properties
        [ModelDefault("AllowEdit", "False")]
        public Guid Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        [Size(1000)]
        public string SaveDirectory
        {
            get { return _SaveDirectory; }
            set { if (SetPropertyValue("SaveDirectory", ref _SaveDirectory, value) && !IsLoading) { OnSaveDirectoryChanged(); } }
        }

        [Browsable(false)]
        [NonPersistent]
        public string LocalPath
        {
            get { return _LocalPath; }
            set { if (SetPropertyValue("LocalFullPath", ref _LocalPath, value) && !IsLoading) { OnLocalFullPathChanged(); } }
        }

        [Size(260)]
        [ModelDefault("AllowEdit", "False")]
        public string FileName
        {
            get { return _FileName; }
            set { SetPropertyValue("FileName", ref _FileName, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public string Extension
        {
            get { return _Extension; }
            set { if (SetPropertyValue("Extension", ref _Extension, value) && !IsLoading) { OnExtensionChanged(); } }
        }

        [ModelDefault("AllowEdit", "False")]
        public int Size
        {
            get { return _Size; }
            set { SetPropertyValue("Size", ref _Size, value); }
        }

        [Browsable(false)]
        [NonPersistent]
        public FileInfo FileInfo
        {
            get { return _FileInfo; }
            set { if (_FileInfo != value) { _FileInfo = value; OnFileInfoChanged(); } }
        }

        [Size(1000)]
        public string FullPath
        {
            get { return _FullPath; }
            set
            {
                string oldFullPath = _FullPath;
                if (SetPropertyValue("FullPath", ref _FullPath, value) && !IsLoading) { OnFullPathChanged(oldFullPath); }
            }
        }
        #endregion

        #region Set Methods

        /// <summary>
        /// Set the FullPath for saving the _File to the Path
        /// </summary>
        private void SetFullPath()
        {
            if (!string.IsNullOrWhiteSpace(SaveDirectory) && !string.IsNullOrWhiteSpace(Extension))
            {
                FullPath = CalcFullPath(Number, SaveDirectory, Extension);
            }
        }

        private void SetFileInfo()
        {
            FileInfo = new FileInfo(FullPath);
        }

        private void SetSize()
        {
            Size = (int)CalcFileSize(LocalPath, FullPath);
        }

        private void SetExtension()
        {
            Extension = CalcFileExtension(LocalPath, FullPath);
            if (string.IsNullOrWhiteSpace(Extension) && !string.IsNullOrWhiteSpace(FileName))
            {
                Extension = Path.GetExtension(FileName);
            }
        }

        #endregion

        #region Calc Methods
        public static FileInfo FindFirstFile(params string[] paths)
        {
            FileInfo result = null;
            foreach (string item in paths)
            {
                if (FileOperations.FileExists(item))
                {
                    result = new FileInfo(item);
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// Used to find the size of the first file in the list of file paths
        /// </summary>
        public static long CalcFileSize(params string[] paths)
        {
            long result = 0;
            var fileInfo = FindFirstFile(paths);
            if (fileInfo != null) result = fileInfo.Length;
            return result;
        }

        /// <summary>
        /// Used to find the extension of the first file in the list of file paths.
        /// </summary>
        public static string CalcFileExtension(params string[] paths)
        {
            string result = string.Empty;
            var fileInfo = FindFirstFile(paths);
            if (fileInfo != null) result = fileInfo.Extension;
            return result;
        }
        #endregion Calc Methods

        #region Event Methods
        private void OnFullPathChanged(string oldFullPath)
        {
            FileOperations.Delete(oldFullPath);
            SetFileInfo();
        }

        private void OnLocalFullPathChanged()
        {
            SetExtension();
            if (FileOperations.FileExists(LocalPath)) FileName = new FileInfo(LocalPath).Name;
            SetFullPath();
        }

        private void OnFileInfoChanged()
        {
            SetSize();
            SetExtension();
        }

        private void OnExtensionChanged()
        {
            SetFullPath();
        }

        private void OnSaveDirectoryChanged()
        {
            SetFullPath();
        }

        #endregion Event Methods

        #region Override Methods

        protected override void OnDeleted()
        {
            FileOperations.Delete(FullPath);
            base.OnDeleted();
        }

        public void Transfer()
        {
            if (!string.IsNullOrWhiteSpace(LocalPath))
            {
                Transfer(LocalPath, FullPath);
            }
            else if (!System.IO.File.Exists(FullPath))
            {
                return;
            }
            else
            {
                var exception = new ArgumentNullException("No file path found");
                exception.Source = this.GetType().Name;
                throw exception;
            }
        }

        public static void Transfer(Stream stream, string destinationPath)
        {
            if (!string.IsNullOrWhiteSpace(destinationPath))
            {//If the Directory exists on the File server
                if (FileOperations.DirectoryExists(Path.GetDirectoryName(destinationPath)))
                {   //If a new _File was attached
                    //But first, if a _File with that _Name is already there, then delete it (i.e. an old version that we are updating);
                    FileOperations.Delete(destinationPath);
                    //New, may not work.
                    FileOperations.WriteStreamToFile(stream, destinationPath);
                }
                else
                {
                    throw new DirectoryNotFoundException(string.Format("Unable to Locate {0}", Path.GetDirectoryName(destinationPath)));
                }
            }
            else
            {
                throw new NotImplementedException("This instance of the XFile class was not implemented correctly. SaveDirectory and File Extension must be set before a transfer can complete.");
            }
        }

        public static void Transfer(string sourcePath, string destinationPath)
        {
            if (!string.IsNullOrWhiteSpace(sourcePath) && FileOperations.FileExists(sourcePath) && sourcePath != destinationPath)
            {
                FileInfo sourceFile = new FileInfo(sourcePath);
                if (!string.IsNullOrWhiteSpace(destinationPath))
                {//If the Directory exists on the File server
                    if (FileOperations.DirectoryExists(Path.GetDirectoryName(destinationPath)))
                    {   //If a new _File was attached
                        //But first, if a _File with that _Name is already there, then delete it (i.e. an old version that we are updating);
                        FileOperations.Delete(destinationPath);
                        //New, may not work.
                        using (Stream readStream = FileOperations.ReadStreamFromFile(sourcePath))
                        {
                            FileOperations.WriteStreamToFile(readStream, destinationPath);
                        }
                    }
                    else
                    {
                        throw new DirectoryNotFoundException(string.Format("Unable to Locate {0}", Path.GetDirectoryName(destinationPath)));
                    }
                }
                else
                {
                    throw new NotImplementedException("This instance of the XFile class was not implemented correctly. SaveDirectory and File Extension must be set before a transfer can complete.");
                }
            }
        }

        protected override void OnSaving()
        {
            if (!this.IsDeleted && !string.IsNullOrWhiteSpace(LocalPath))
            {
                Transfer();
            }
            base.OnSaving();
        }

        #endregion

        #region IFileData Methods
        /// <summary>
        /// Unsure what calls this Method but it fires when the
        /// class that implements this class as a property has a File
        /// dragged onto the ViewController.
        /// </summary>
        public void LoadFromStream(string fileName, Stream source)
        {
            FileName = fileName;
            SetSize();
            SetExtension();
            SetFullPath();
            if (source is FileStream)
            {
                LocalPath = ((FileStream)source).Name;
            }
            else
            {
                Transfer(source, FullPath);
            }
        }

        //TODO
        public void Clear()
        {
        }

        /// <summary>
        /// Part of the IFileData interface and
        /// fires when saving or opening a File.
        /// </summary>
        /// <param _Name="stream"></param>
        public void SaveToStream(Stream stream)
        {
            if (FileOperations.FileExists(FullPath))
            {
                using (Stream source = File.OpenRead(FullPath))
                {
                    byte[] buffer = new byte[_ReadBytesSize];
                    int read = 0;
                    while ((read = source.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        stream.Write(buffer, 0, read);
                    }
                }
            }
            else
            {
                throw new FileNotFoundException(string.Format("{0} is no longer in {1}", FileName, _LocalPath));
            }
        }

        public static string CalcFullPath(Guid number, string directory, string extension)
        {
            return string.Format("{0}\\{1}{2}", directory, number, extension);
        }
        #endregion

        #region IDisposable Implementation
        ~XFile()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                }
                disposed = true;
            }

        }
        #endregion IDisposable Implementation
    }
}