﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Xpo;
using System.Collections.Generic;
using System.Linq;

namespace JonBuildsThings.BusinessObjects.DocumentManagement
{

    [DefaultClassOptions]
    public class MailMergeItem : BaseObject
    {
        #region Fields
        private string _ColumnName;
        private MailMerge _MailMerge;
        private TypeFormatProvider _Format;
        private string _SampleValue;

        #endregion Fields

        #region Constructors

        public MailMergeItem(Session session) : base(session) { }

        #endregion Constructors

        #region Properties

        [ImmediatePostData, RuleRequiredField("RuleRequiredField for MailMergeItem.ColumnName", DefaultContexts.Save)]
        public string ColumnName
        {
            get { return _ColumnName; }
            set { SetPropertyValue("ColumnName", ref _ColumnName, value); }
        }

        [ImmediatePostData]
        

        [Association("MailMerge-MailMergeItems", typeof(MailMerge))]
        [ImmediatePostData]
        public MailMerge MailMerge
        {
            get { return _MailMerge; }
            set { SetPropertyValue("MailMerge", ref _MailMerge, value); }
        }

        [ImmediatePostData]


        public string SampleTag
        {
            get { return string.Format("{0}{1}{0}", "#", _ColumnName); }
        }

        [ModelDefault("AllowEdit", "False")]
        public string SampleValue
        {
            get { return _SampleValue; }
            set { SetPropertyValue("SampleValue", ref _SampleValue, value); }
        }

        #endregion

        #region Methods

        public void OnPropertyNameChanged()
        {
            if (MailMerge != null && MailMerge.MailMergeSource != null)
            {
                SetSampleValue();
            }
        }

        public void OnFormatChanged()
        {
            if (MailMerge != null && MailMerge.MailMergeSource != null)
            {
                SetSampleValue();
            }
        }

        /// <summary>
        /// Set sample value when ShowCurrency (or) Format changes.
        /// </summary>
        public void SetSampleValue()
        {
            if (this.MailMerge != null && this.MailMerge.MailMergeSource != null)
            {
                // TODO: Sample Value can be static instead of dynamic.
                XPCollection objCollection = new XPCollection(Session, this.MailMerge.MailMergeSource.ObjectType) { TopReturnedObjects = 1 }; // TopReturnedObjects: Gets the maximum number of objects retrieved by the collection from data store.
                BaseObject dataSourceObject = objCollection[0] as BaseObject;

                ITypeInfo currentObjectTypeInfo = XafTypesInfo.Instance.FindTypeInfo(dataSourceObject.GetType());
                this.SampleValue = GetMailMergeItemValue(dataSourceObject, currentObjectTypeInfo).ToString();
            }
        }

        /// <summary>
        /// Get the formatted value from dataSourceObject
        /// </summary>
        public object GetMailMergeItemValue<T>(T dataSourceObject, ITypeInfo currentObjectTypeInfo) where T : BaseObject
        {
            object propertyValue = string.Empty;
            IMemberInfo mergeMemberInfo = currentObjectTypeInfo.FindMember(this.PropertyName);
            if (mergeMemberInfo != null)
            {
                propertyValue = mergeMemberInfo.GetValue(dataSourceObject) ?? string.Empty;
                propertyValue = TypeFormatProvider.GetFormattedValue(propertyValue, mergeMemberInfo.MemberType, this.Format);
            }
            return propertyValue;
        }


        #endregion
    }
}