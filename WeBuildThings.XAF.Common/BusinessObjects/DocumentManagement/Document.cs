﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using WeBuildThings.Utilities.Extensions;
using WeBuildThings.Common.Extensions;
using System.ComponentModel;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    [ImageName("BO_Attachment")]
    [DefaultProperty("File")]
    public class Document : BaseObject
    {
        #region Constants
        public const string DocumentTypeListName = "Document Type";
        public const string DefaultDocumentType = "General Document";
        #endregion Constants

        #region Fields
        //TODO instead create a link to the document catalog?
        private bool disposed;
        private const string GENERAL_DOCUMENT_CATALOG = "Default";
        private LookupItem _DocumentType;
        private XFile _File;
        private string _ShortDescription;
        #endregion Fields

        #region Constructors
        public Document(Session session) : base(session) { }

        internal Document() : base() { }

        public override void AfterConstruction()
        {
            //File = new XFile(Session);
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [DataSourceCriteria("[LookupList.Name] = 'Document Type' and [Active]")]
        public LookupItem DocumentType
        {
            get { return _DocumentType; }
            set { SetPropertyValue("DocumentType", ref _DocumentType, value); }
        }

        [Aggregated, ExpandObjectMembers(ExpandObjectMembers.Never)]
        public XFile File
        {
            get { return _File; }
            set { XFile oldFile = _File; if (SetPropertyValue("File", ref _File, value) && !IsLoading) { OnFileChanged(oldFile); } }
        }

        public string ShortDescription
        {
            get { return _ShortDescription; }
            set { SetPropertyValue("ShortDescription", ref _ShortDescription, value); }
        }

        [Association("Comment-Documents", typeof(Comment))]
        public XPCollection<Comment> Comments
        {
            get { return GetCollection<Comment>("Comments"); }
        }
        #endregion Properties

        #region Event Methods
        protected virtual void OnFileChanged(XFile oldFile)
        {
            DocumentCatalog documentCatalog = DocumentCatalog.CreateDocumentCatalog(Session, GENERAL_DOCUMENT_CATALOG);
            File.SaveDirectory = documentCatalog.GetCurrentDirectory();
        }

        protected override void OnSaving()
        {
            if (!this.IsDeleted && File != null && !System.IO.File.Exists(File.FullPath))
            {
                File.Transfer();
            }
            base.OnSaving();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetFile(string fileFullName)
        {
            if (!string.IsNullOrWhiteSpace(fileFullName))
            {
                File = XFile.CreateXFile(Session, fileFullName, GENERAL_DOCUMENT_CATALOG);
            }
        }
        #endregion Set Methods

        public static Document CreateCopy(Session session, string localFilePath, Type type)
        {
            if (!type.ContainsType(typeof(Document))) throw new ArgumentException("type must inherit Document");
            Document result = type.CreateInstance(session);
            result.File = new XFile(session) {LocalPath = localFilePath};
            result.File.Transfer();
            return result;
        }

        public static Document CreateDocument(Session session, string filePath, Type type)
        {
            if (!type.ContainsType(typeof(Document))) throw new ArgumentException("type must inherit Document");
            Document result = type.CreateInstance(session);
            result.File = new XFile(session) { FullPath = filePath };
            result.File.Transfer();
            return result;
        }

        public static Document CreateDocument(Session session, string filePath)
        {
            Document result = new Document(session);
            result.File = new XFile(session) { FullPath = filePath };
            result.File.Transfer();
            return result;
        }

        public static Document CreateDocument(UnitOfWork uow, string filePath)
        {
            Document result = new Document(uow);
            result.File = new XFile(uow) { FullPath = filePath };
            result.File.Transfer();
            return result;
        }

        #region IDisposable Implementation
        ~Document()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    if (File != null)
                    {
                        File.Dispose();
                        File = null;
                    }
                }
                disposed = true;
            }

        }
        #endregion IDisposable Implementation
    }

}
