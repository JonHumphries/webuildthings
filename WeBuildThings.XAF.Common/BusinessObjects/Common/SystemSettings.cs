﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    public abstract class SystemSettings : BaseObject
    {
        #region Fields
        private string _SettingType;
        #endregion Fields

        #region Constructor
        public SystemSettings(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            SettingType = this.GetType().Name;
            base.AfterConstruction();
        }
        #endregion Constructor

        #region Properties
        [ModelDefault("AllowEdit","False")]
        public string SettingType
        {
            get { return _SettingType; }
            set { SetPropertyValue("SettingType", ref _SettingType, value); }
        }

        #endregion Properties

        public static T GetInstance<T>(IObjectSpace objectSpace) where T : BaseObject, new()
        {
            T result = null;
            if (objectSpace != null)
            {
                result = objectSpace.FindObject<T>(null);
                if (result == null)
                {
                    result = (T)Activator.CreateInstance(typeof(T), objectSpace);
                }
            }
            return result;
        }

        public static T GetInstance<T>(Session session) where T : BaseObject, new()
        {
            T result = null;
            if (session != null)
            {
                result = session.FindObject<T>(null);
                if (result == null)
                {
                    result = (T)Activator.CreateInstance(typeof(T), session);
                }
            }
            return result;
        }
    }
}
