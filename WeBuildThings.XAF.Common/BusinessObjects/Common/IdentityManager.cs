﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    [ImageName("Security")]
    public class IdentityManager : BaseObject
    {

        #region Fields
        private string _ClassName;
        private int _PreviousIdentityValue;
        #endregion Fields

        #region Constructors
        public IdentityManager(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public string ClassName
        {
            get { return _ClassName; }
            set { SetPropertyValue("ClassName", ref _ClassName, value); }
        }

        public int PreviousIdentityValue
        {
            get { return _PreviousIdentityValue; }
            set { SetPropertyValue("PreviousIdentityValue", ref _PreviousIdentityValue, value); }
        }
        #endregion Properties

        #region Public Methods
        public static int NextIdentityValue(string className, Session session)
        {
            int result = 0;
            IdentityManager identityManager = session.FindObject<IdentityManager>(new BinaryOperator("ClassName", className));
            if (identityManager == null)
            {
                identityManager = new IdentityManager(session);
                identityManager.ClassName = className;
                identityManager.PreviousIdentityValue = 0;
            }
            identityManager.PreviousIdentityValue++;
            result = identityManager.PreviousIdentityValue;
            identityManager.Save();
            return result;
        }
        #endregion Public Methods
    }

    public interface IIdentity
    {
        int Number { get; set; }
        Session Session { get; }
        XPClassInfo ClassInfo { get; }

    }

    public static class IIdentityExtensions
    {
        public static void SetIdentity(this IIdentity value)
        {
            if (value.Number == 0)
            {
                value.Number = IdentityManager.NextIdentityValue(value.ClassInfo.TableName, value.Session);
            }
        }
    }
}
