﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Linq;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class LookupList : BaseObject
    {
        #region Fields
        private string _Name;
        #endregion Fields

        #region Constructors
        public LookupList(Session session)
            : base(session)
        {
        }

        internal LookupList() : base() { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        public static LookupList CreateLookupList(Session session, string name)
        {
            var existingLookupList = new XPCollection<LookupList>(session, new BinaryOperator("Name", name)).FirstOrDefault();
            LookupList lookupList = (existingLookupList == null)? new LookupList(session) : existingLookupList;
            lookupList.Name = name;
            return lookupList;
        }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Association("LookupList-LookupItem", typeof(LookupItem)), Aggregated]
        public XPCollection<LookupItem> LookupItems
        {
            get { return GetCollection<LookupItem>("LookupItems"); }
        }
        #endregion Properties

    }


}
