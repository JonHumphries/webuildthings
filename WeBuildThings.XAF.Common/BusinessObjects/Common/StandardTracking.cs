﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Xml.Serialization;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common
{
    [NonPersistent]
    public class StandardTracking : BaseObject, IStandardTracking
    {
        #region Fields
        private DateTime _DateCreated;
        private XUser _CreatedBy;
        private DateTime _DateModified;
        private XUser _ModifiedBy;
        #endregion Fields

        #region Constructors
        public StandardTracking() : base() { }

        public StandardTracking(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            this.SetCreatedUserAndDate();
            base.AfterConstruction();
        }
        #endregion Constructors

        #region Properties
        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        [XmlIgnore]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [XmlIgnore]
        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        [XmlIgnore]
        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [XmlIgnore]
        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }
        #endregion Properties

        #region Event Methods
        protected override void OnSaving()
        {
            base.OnSaving();
            this.SetModifiedUserAndDate();
        }
        #endregion Event Methods

    }
}
