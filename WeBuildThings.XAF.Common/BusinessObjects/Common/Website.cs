﻿using DevExpress.Xpo;
using System.ComponentModel;

namespace WeBuildThings.XAF.Common
{
    public class Website : StandardTracking
    {
        #region Fields
        private string _URL;
        private string _Name;
        private XOrganization _Organization;
        private XPerson _Person;
        #endregion Fields

        #region Constructors
        public Website(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        [Size(255)]
        public string URL
        {
            get { return _URL; }
            set { SetPropertyValue("URL", ref _URL, value); }
        }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Association("Organization-Website", typeof(XOrganization))]
        [Browsable(false)]
        public XOrganization Organization
        {
            get { return _Organization; }
            set { SetPropertyValue("Organization", ref _Organization, value); }
        }

        [Association("Person-Website", typeof(XPerson))]
        [Browsable(false)]
        public XPerson Person
        {
            get { return _Person; }
            set { SetPropertyValue("Person", ref _Person, value); }
        }
        #endregion Properties
    }
}
