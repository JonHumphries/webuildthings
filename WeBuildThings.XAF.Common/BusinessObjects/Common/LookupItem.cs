﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Validation;

namespace WeBuildThings.XAF.Common
{

    [DefaultProperty("Name")]
    public class LookupItem : BaseObject
    {
        #region Fields
        private string _Name;
        private string _Description;
        private bool _Active;
        private LookupList _LookupList;
        #endregion Fields

        #region Constructors
        public LookupItem(Session session)
            : base(session)
        {
        }

        internal LookupItem()
            : base()
        {

        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            Active = true;
            // Place your initialization code here (http://documentation.devexpress.com/#Xaf/CustomDocument2834).
        }

        public static LookupItem CreateLookupItem(Session session, string lookupList, string name, string description)
        {
            LookupItem lookupItem = null;
            if (!string.IsNullOrWhiteSpace(name))
            {
                var existingLookupList = LookupList.CreateLookupList(session, lookupList);
                var existingLookupItem = existingLookupList.LookupItems.Where(n => n.Name == name).FirstOrDefault();
                lookupItem = (existingLookupItem == null) ? new LookupItem(session) : existingLookupItem;
                lookupItem.LookupList = existingLookupList;
                lookupItem.Name = name;
                if (string.IsNullOrWhiteSpace(lookupItem.Description) && !string.IsNullOrWhiteSpace(description))
                {
                    lookupItem.Description = description;
                }
            }
            return lookupItem;
        }
        #endregion Constructors

        #region Properties
        //[Size(100)]
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public bool Active
        {
            get { return _Active; }
            set { SetPropertyValue("Active", ref _Active, value); }
        }

        [Association("LookupList-LookupItem", typeof(LookupList))]
        public LookupList LookupList
        {
            get { return _LookupList; }
            set { SetPropertyValue("LookupList", ref _LookupList, value); }
        }
        #endregion Properties

        #region Validation Methods
        public static bool ValidateSaving(bool throwException, LookupItem lookupItem)
        {
            return true; // ValidationRules.ValidateRequiredField(throwException, lookupItem.LookupList, "LookupList");
        }
        #endregion Validation Methods

        #region Override Methods
        protected override void OnSaving()
        {
            if (!IsDeleted && ValidateSaving(true, this))
            {
                base.OnSaving();
            }
        }
        #endregion Override Methods
    }


}
