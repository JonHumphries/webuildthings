﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{

    public class XAddress : Address
    {
        #region Consts
        internal const string AddressTypeListName = "Address Type";
        #endregion Consts

        #region Fields
        private string _Attention;
        private string _Street2;
        private string _Description;
        private LookupItem _AddressType;
        #endregion Fields

        #region Constructors
        public XAddress(Session session) : base(session) { }

        public static XAddress CreateAddress(Session session, string attention, string description, string street1, string street2, string city, string state, string zip)
        {
            XAddress result = null;
            if (!string.IsNullOrWhiteSpace(attention) || !string.IsNullOrWhiteSpace(street1) || !string.IsNullOrWhiteSpace(street2) || !string.IsNullOrWhiteSpace(city) || !string.IsNullOrWhiteSpace(state) || !string.IsNullOrWhiteSpace(zip))
            {
                GroupOperator criteria = new GroupOperator(GroupOperatorType.And);
                if (!string.IsNullOrWhiteSpace(street1))
                {
                    criteria.Operands.Add(new BinaryOperator("Street", street1));
                }
                if (!string.IsNullOrWhiteSpace(street2))
                {
                    criteria.Operands.Add(new BinaryOperator("Street2", street2));
                }
                if (!string.IsNullOrWhiteSpace(city))
                {
                    criteria.Operands.Add(new BinaryOperator("City", city));
                }
                if (!string.IsNullOrWhiteSpace(state))
                {
                    criteria.Operands.Add(new BinaryOperator("StateProvince", state));
                }
                if (!string.IsNullOrWhiteSpace(zip))
                {
                    criteria.Operands.Add(new BinaryOperator("ZipPostal", zip));
                }
                if (criteria.Operands.Count > 0)
                {
                    result = session.FindObject<XAddress>(criteria);
                }

                if (result == null)
                {
                    result = new XAddress(session);
                }
                if (string.IsNullOrWhiteSpace(result.Attention) && !string.IsNullOrWhiteSpace(attention))
                {
                    result.Attention = attention;
                }
                if (string.IsNullOrWhiteSpace(result.Description) && !string.IsNullOrWhiteSpace(description))
                {
                    result.Description = description;
                }
                if (string.IsNullOrWhiteSpace(result.Street) && !string.IsNullOrWhiteSpace(street1))
                {
                    result.Street = street1;
                }
                if (string.IsNullOrWhiteSpace(result.Street2) && !string.IsNullOrWhiteSpace(street2))
                {
                    result.Street2 = street2;
                }
                if (string.IsNullOrWhiteSpace(result.City) && !string.IsNullOrWhiteSpace(city))
                {
                    result.City = city;
                }
                if (string.IsNullOrWhiteSpace(result.StateProvince) && !string.IsNullOrWhiteSpace(state))
                {
                    result.StateProvince = state;
                }
                if (string.IsNullOrWhiteSpace(result.ZipPostal) && !string.IsNullOrWhiteSpace(zip))
                {
                    result.ZipPostal = zip;
                }
            }
            return result;
        }
        #endregion Constructors

        #region Properties
        public string Attention
        {
            get { return _Attention; }
            set { SetPropertyValue("Attention", ref _Attention, value); }
        }

        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public string Street2
        {
            get { return _Street2; }
            set { SetPropertyValue("Street2", ref _Street2, value); }
        }

        [DataSourceCriteria("[LookupList.Name] = 'Address Type' and [Active]")]
        public LookupItem AddressType
        {
            get { return _AddressType; }
            set { SetPropertyValue("AddressType", ref _AddressType, value); }
        }

        [Association("Party-Addresses", typeof(XParty))]
        public XPCollection<XParty> Parties
        {
            get { return GetCollection<XParty>("Parties"); }
        }
        #endregion Properties
    }

    public static class AddressExtensions
    {
        public static Address UpdateAddress(this Address result, string street, string city, string state, string zip)
        {
            if (!string.IsNullOrWhiteSpace(street))
            {
                result.Street = street;
            }
            if (!string.IsNullOrWhiteSpace(city))
            {
                result.City = city;
            }
            if (!string.IsNullOrWhiteSpace(state))
            {
                result.StateProvince = state;
            }
            if (!string.IsNullOrWhiteSpace(zip))
            {
                result.ZipPostal = zip;
            }
            return result;
        }
    }
}
