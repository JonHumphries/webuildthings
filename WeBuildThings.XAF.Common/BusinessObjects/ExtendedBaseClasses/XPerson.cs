using DevExpress.Persistent.Base;
using DevExpress.Persistent.Base.General;
using DevExpress.Xpo;
using System;
using System.Linq;
using System.ComponentModel;
using WeBuildThings.Common.Enumerations;

namespace WeBuildThings.XAF.Common
{
    [DefaultProperty("FullName")]
    [ImageName("BO_Person")]
    public abstract class XPerson : XParty, IPerson
    {
        #region Fields
        private Gender _Gender;
        private DateTime _Birthday;
        private string _Title;
        private string _FirstName;
        private string _MiddleName;
        private string _LastName;
        #endregion Fields

        #region Constructors
        public XPerson(Session session) : base(session) { }

        public XPerson() : base(Session.DefaultSession) { }

        #endregion Constructors

        #region Properties
        public Gender Gender
        {
            get { return _Gender; }
            set { SetPropertyValue("Gender", ref _Gender, value); }
        }

        public DateTime Birthday
        {
            get { return _Birthday; }
            set { SetPropertyValue("Birthday", ref _Birthday, value); }
        }

        public string Title
        {
            get { return _Title; }
            set { SetPropertyValue("Title", ref _Title, value); }
        }

        public string FirstName
        {
            get { return _FirstName; }
            set { SetPropertyValue("FirstName", ref _FirstName, value); }
        }

        public string LastName
        {
            get { return _LastName; }
            set { SetPropertyValue("LastName", ref _LastName, value); }
        }

        public string MiddleName
        {
            get { return _MiddleName; }
            set { SetPropertyValue("MiddleName", ref _MiddleName, value); }
        }

        [Association("Organization-Person", typeof(XOrganization))]
        public XPCollection<XOrganization> Organizations
        {
            get { return GetCollection<XOrganization>("Organizations"); }
        }

        [Association("Person-Website", typeof(Website)), Aggregated]
        public XPCollection<Website> Websites
        {
            get { return GetCollection<Website>("Websites"); }
        }

        [Association("XEvent-XPerson", typeof(XEvent))]
        public XPCollection<XEvent> Events
        {
            get { return GetCollection<XEvent>("Events"); }
        }
        #endregion Fields

        #region Event Methods
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            if (propertyName == "FirstName" || propertyName == "LastName")
            {
                SetFullName(string.Format("{0} {1}", FirstName, LastName));
            }
            base.OnChanged(propertyName, oldValue, newValue);
        }
        #endregion Event Methods

        #region Set Methods
        public void SetFullName(string fullName)
        {
            FullName = fullName;
        }
        #endregion Set Methods

        #region IMergeable Implmentation
        public override void Merge(WeBuildThings.Common.Interfaces.IMergeable source)
        {
            base.Merge(source);
            var convertedSource = source as XPerson;
            if (convertedSource != null)
            {
                if (Gender == Gender.Unknown && convertedSource.Gender != Gender.Unknown)
                {
                    Gender = convertedSource.Gender;
                }
                if (Birthday == DateTime.MinValue && convertedSource.Birthday != DateTime.MinValue)
                {
                    Birthday = convertedSource.Birthday;
                }
                if (string.IsNullOrWhiteSpace(Title))
                {
                    Title = convertedSource.Title;
                }
                if (string.IsNullOrWhiteSpace(FirstName))
                {
                    FirstName = convertedSource.FirstName;
                }
                if (string.IsNullOrWhiteSpace(LastName))
                {
                    LastName = convertedSource.LastName;
                }
                if (string.IsNullOrWhiteSpace(MiddleName))
                {
                    MiddleName = convertedSource.MiddleName;
                }
                foreach (var organization in convertedSource.Organizations)
                {
                    if (!Organizations.Contains(organization))
                    {
                        Organizations.Add(organization);
                    }
                }
                foreach (var webSite in convertedSource.Websites)
                {
                    if (!Websites.Contains(webSite))
                    {
                        Websites.Add(webSite);
                    }
                }
                foreach (var csEvent in convertedSource.Events)
                {
                    if (!Events.Contains(csEvent))
                    {
                        Events.Add(csEvent);
                    }
                }
            }
        }
        #endregion IMergeable Implementation
    }

}