﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    public class XCountry : Country
    {

        private int _CallingCode;

        public XCountry(Session session) : base(session) { }

        public int CallingCode
        {
            get { return _CallingCode; }
            set { SetPropertyValue("CallingCode", ref _CallingCode, value); }
        }


    }
}
