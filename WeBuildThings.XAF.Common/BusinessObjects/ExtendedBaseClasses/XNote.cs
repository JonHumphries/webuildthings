﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    public class XNote : Note
    {

        public XNote(Session session)
            : base(session)
        { }

        internal XNote() : base(Session.DefaultSession) { }
    }
}
