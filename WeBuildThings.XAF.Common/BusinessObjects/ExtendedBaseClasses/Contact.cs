﻿using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    public class Contact : XPerson
    {
        #region Constructors
        public Contact(Session session) : base(session) {}
        #endregion Constructors

    }
}
