using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Linq;

namespace WeBuildThings.XAF.Common
{
    [DefaultProperty("FullName")]
    [ImageName("BO_Organization")]
    public class XOrganization : XParty
    {
        #region Fields
        private XPhoneNumber _FaxNumber;
        private string _Description;
        private string _Website;
        #endregion Fields

        #region Constructors
        public XOrganization(Session session)
            : base(session)
        {
            // This constructor is used when an object is loaded from a persistent storage.
            // Do not place any code here.
        }

        public XOrganization() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }

        public static PhoneNumber CreatePhoneNumber(XOrganization organization, string number)
        {
            PhoneNumber phoneNumber = null;
            if (organization != null && !string.IsNullOrWhiteSpace(number))
            {
                var existingPhoneNumber = new XPCollection<PhoneNumber>(organization.Session, new BinaryOperator("Number", number)).FirstOrDefault();
                phoneNumber = (existingPhoneNumber == null) ? new PhoneNumber(organization.Session) : existingPhoneNumber;
                phoneNumber.Number = number;
                organization.PhoneNumbers.Add(phoneNumber);
            }
            return phoneNumber;
        }
        #endregion Constructors

        #region Properties
        [DataSourceProperty("PhoneNumbers")]
        public XPhoneNumber FaxNumber
        {
            get { return _FaxNumber; }
            set { SetPropertyValue("FaxNumber", ref _FaxNumber, value); }
        }

        [Size(SizeAttribute.Unlimited)]
        public string Description 
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }
        
        [Size(255)]
        public string WebSite 
        {
            get { return _Website; }
            set { SetPropertyValue("Website", ref _Website, value); }
        }
        
        [Association("Organization-Person", typeof(XPerson))]
        public XPCollection<XPerson> Contacts
        {
            get { return GetCollection<XPerson>("Contacts"); }
        }

        [Association("Organization-Website", typeof(Website)),Aggregated]
        public XPCollection<Website> Websites
        {
            get { return GetCollection<Website>("Websites"); }
        }
        #endregion Properties

        #region IMergeable Implementation
        public override void Merge(WeBuildThings.Common.Interfaces.IMergeable source)
        {
            base.Merge(source);
            var convertedSource = source as XOrganization;
            if (convertedSource != null)
            {
                if (!PhoneNumbers.Contains(convertedSource.FaxNumber))
                {
                    PhoneNumbers.Add(convertedSource.FaxNumber);
                }
            }
            if (string.IsNullOrWhiteSpace(Description))
            {
                Description = convertedSource.Description;
            }
            if (string.IsNullOrWhiteSpace(WebSite))
            {
                WebSite = convertedSource.WebSite;
            }
            foreach (var contact in convertedSource.Contacts)
            {
                if (!Contacts.Contains(contact))
                {
                    Contacts.Add(contact);
                }
            }
            foreach (var webSite in convertedSource.Websites)
            {
                if (!Websites.Contains(webSite))
                {
                    Websites.Add(webSite);
                }
            }
        }
        #endregion IMergeable Implementation
    }

}