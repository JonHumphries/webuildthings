﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    public class XEvent: Event
    {
        #region Constructors
        public XEvent(Session session) : base(session) { }
        #endregion Constructors

        [Association("XEvent-XPerson",typeof(XPerson))]
        public XPCollection<XPerson> Attendees
        {
            get { return GetCollection<XPerson>("Attendees"); }
        }
    }
}
