﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.Validation;

namespace WeBuildThings.XAF.Common
{
    [DefaultProperty("Number")]
    public class AreaCode : BaseObject
    {
        #region Fields
        private string _Description;
        private string _OverlayArea;
        private int _Number;
        private string _TimeZone;
        #endregion Fields

        #region Constructors
        public AreaCode(Session session) : base(session) { }

        public AreaCode() : base() { }

        public static AreaCode CreateAreaCode(IObjectSpace objectSpace, int number, string description, string overlay, string timezone)
        {
            var result = objectSpace.FindObject<AreaCode>(new BinaryOperator("Number", number));
            if (result == null)
            {
                result = objectSpace.CreateObject<AreaCode>();
                result.Number = number;
            }
            if (!string.IsNullOrWhiteSpace(description)) result.Description = description;
            if (!string.IsNullOrWhiteSpace(timezone)) result.TimeZone = timezone;
            if (!string.IsNullOrWhiteSpace(overlay)) result.OverlayArea = overlay;
            return result;
        }

        public static AreaCode CreateAreaCode(Session session, int number, string description, string overlay, string timezone)
        {
            AreaCode result = null;
            if (ValidationRules.ValidateAreaCode<AreaCode>(false, number.ToString()))
            {
                result = session.FindObject<AreaCode>(new BinaryOperator("Number", number));
                if (result == null)
                {
                    result = new AreaCode(session);
                    result.Number = number;
                }
                if (!string.IsNullOrWhiteSpace(description)) result.Description = description;
                if (!string.IsNullOrWhiteSpace(timezone)) result.TimeZone = timezone;
                if (!string.IsNullOrWhiteSpace(overlay)) result.OverlayArea = overlay;
            }
            return result;
        }

        public static AreaCode CreateAreaCode(Session session, string number, string description, string overlay, string timezone)
        {
            return CreateAreaCode(session, Convert.ToInt32(number), description, overlay, timezone);
        }
        #endregion Constructors

        #region Properties
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }

        public int Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public string OverlayArea
        {
            get { return _OverlayArea; }
            set { SetPropertyValue("OverlayArea", ref _OverlayArea, value); }
        }

        public string TimeZone
        {
            get { return _TimeZone; }
            set { SetPropertyValue("TimeZone", ref _TimeZone, value); }
        }
        #endregion Properties
    }
}
