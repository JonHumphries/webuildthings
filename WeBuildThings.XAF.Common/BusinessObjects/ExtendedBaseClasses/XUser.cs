﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.XAF.Common
{
    public class XUser : SecuritySystemUser
    {

        #region Fields
        private string _FirstName;
        private string _LastName;
        private string _EmailAddress;
        #endregion Fields

        #region Constructors
        public XUser(Session session)
            : base(session)
        { }

        internal XUser() : base(Session.DefaultSession) { }
        #endregion Constructors

        #region Properties
        public string FirstName
        {
            get { return _FirstName; }
            set { SetPropertyValue("FirstName", ref _FirstName, value); }
        }

        public string LastName
        {
            get { return _LastName; }
            set { SetPropertyValue("LastName", ref _LastName, value); }
        }

        public string EmailAddress
        {
            get { return _EmailAddress; }
            set { SetPropertyValue("EmailAddress", ref _EmailAddress, value); }
        }
        #endregion Properties

        #region Public methods
        public static XUser GetUserByUserName(Session Session, string userName)
        {
            return Session.FindObject<XUser>(new BinaryOperator("UserName", userName));
        }

        public IEnumerable<XRole> FlattenRoles()
        {
            foreach (XRole role in GetXRoles())
            {
                foreach (var childRole in role.FlattenChildRoles())
                {
                    yield return childRole;
                }
                yield return role;
            }
        }

        public IEnumerable<XRole> GetXRoles()
        {
            foreach (var role in this.Roles)
            {
                if (role is XRole)
                {
                    yield return ((XRole)role);
                }
            }
        }

        public bool ContainsRole(string role)
        {
            return Roles.Where(n => n is XRole && ((XRole)n).ContainsRole(role)).Any();
        }
        #endregion Public Methods

        #region Override Methods
        public override string ToString()
        {
            return UserName;
        }
        #endregion Override Methods
    }
}
