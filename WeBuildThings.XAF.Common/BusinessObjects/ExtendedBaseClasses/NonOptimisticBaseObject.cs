﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    /// <summary>
    /// Non Optimistic base object to address concurrency issues in workflow engine
    /// </summary>
    /// <seealso cref="https://www.devexpress.com/Support/Center/Question/Details/DQ2973"/>
    [NonPersistent, MemberDesignTimeVisibility(false), OptimisticLocking(false)]
    public abstract class NonOptimisticBaseObject : BaseObject
    {
        public NonOptimisticBaseObject(Session session)
            : base(session)
        { }

        public NonOptimisticBaseObject() : this(Session.DefaultSession) { }

    }
}
