﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;

namespace WeBuildThings.XAF.Common
{
    [DisplayProperty("Id")]
    public class XTimeZone : BaseObject
    {
        #region Fields
        private string _Id;
        #endregion Fields

        #region Constructors
        public XTimeZone(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public string Id
        {
            get { return _Id; }
            set { SetPropertyValue("Id", ref _Id, value); }
        }
        #endregion Properties

        #region Public Methods
        public static XTimeZone GetTimeZone(Session session, string id)
        {
            return session.FindObject<XTimeZone>(new BinaryOperator("Id", id));
        }

        public TimeZoneInfo ToTimeZoneInfo()
        {
            return TimeZoneInfo.FindSystemTimeZoneById(Id);
        }
        #endregion Public Methods

        public override string ToString()
        {
            return Id;
        }
    }
}
