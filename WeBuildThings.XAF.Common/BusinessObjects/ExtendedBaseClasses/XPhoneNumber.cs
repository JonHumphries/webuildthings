﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.Utilities;
using WeBuildThings.Validation;

namespace WeBuildThings.XAF.Common
{
    [DefaultProperty("Number")]
    public class XPhoneNumber : PhoneNumber, IStandardTracking
    {
        #region Consts
        internal const string PhoneTypeListName = "Phone Type";
        #endregion Consts

        #region Fields
        private string _Description;
        private string _ErrorCode;
        private string _Carrier;
        private string _CarrierType;
        private DateTime _ValidationDate;
        private AreaCode _AreaCode;
        private XCountry _Country;
        private LookupItem _Type;
        private string _FullNumber;
        private string _COCode;
        private string _LineNumber;
        private XUser _CreatedBy;
        private DateTime _DateCreated;
        private XUser _ModifiedBy;
        private DateTime _DateModified;
        #endregion Fields

        #region Constructors
        public XPhoneNumber(Session session) : base(session) { }

        public XPhoneNumber() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            this.SetCreatedUserAndDate();
            base.AfterConstruction();
        }

        public static XPhoneNumber CreateXPhoneNumber(Session session, string number)
        {
            number = CleanNumber(number);
            XPhoneNumber result = null;
            if (!string.IsNullOrWhiteSpace(number))
            {
                result = session.FindObject<XPhoneNumber>(new BinaryOperator("Number", number));
                if (result == null)
                {
                    result = new XPhoneNumber(session);
                    result.Number = number;
                }
            }
            return result;
        }

        public static XPhoneNumber CreateXPhoneNumber(DevExpress.Persistent.BaseImpl.PhoneNumber phoneNumber)
        {
            XPhoneNumber result = new XPhoneNumber(phoneNumber.Session);
            result.Number = phoneNumber.Number;
            result.Description = phoneNumber.PhoneType;
            return result;
        }

        #endregion Constructors

        #region Properties
        public string Description
        {
            get { return _Description; }
            set { if (SetPropertyValue("Description", ref _Description, value) && !IsLoading) { OnDescriptionChanged(); } }
        }

        public AreaCode AreaCode
        {
            get { return _AreaCode; }
            set { if (SetPropertyValue("AreaCode", ref _AreaCode, value) && !IsLoading) { OnAreaCodeChanged(); } }
        }

        public string COCode
        {
            get { return _COCode; }
            set { if (SetPropertyValue("COCode", ref _COCode, value) && !IsLoading) { OnCOCodeChanged(); } }
        }

        public string LineNumber
        {
            get { return _LineNumber; }
            set { if (SetPropertyValue("LineNumber", ref _LineNumber, value) && !IsLoading) { OnLineNumberChanged(); } }
        }

        public XCountry Country
        {
            get { return _Country; }
            set { SetPropertyValue("Country", ref _Country, value); }
        }

        [ModelDefault("AllowEdit","False")]
        public string FullNumber
        {
            get { return _FullNumber; }
            set { SetPropertyValue("FullNumber", ref _FullNumber, value); }
        }

        public string ErrorCode
        {
            get { return _ErrorCode; }
            set { SetPropertyValue("ErrorCode", ref _ErrorCode, value); }
        }

        public DateTime ValidationDate
        {
            get { return _ValidationDate; }
            set { SetPropertyValue("ValidationDate", ref _ValidationDate, value); }
        }

        public string Carrier
        {
            get { return _Carrier; }
            set { SetPropertyValue("Carrier", ref _Carrier, value); }
        }

        public string CarrierType
        {
            get { return _CarrierType; }
            set { SetPropertyValue("CarrierType", ref _CarrierType, value); }
        }

        [DataSourceCriteria("[LookupList.Name] = 'Phone Type' and [Active]")]
        public LookupItem Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }

        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }
        #endregion Properties

        #region Events
        public event EventHandler DescriptionChanged;
        #endregion Events;

        #region Invoke Methods
        private void InvokeDescriptionChanged()
        {
            var handler = DescriptionChanged;
            if (handler != null) handler.Invoke(this, EventArgs.Empty);
        }
        #endregion Invoke Methods

        #region Event Methods
        private void OnDescriptionChanged()
        {
            InvokeDescriptionChanged();
        }

        private void OnNumberChanged()
        {
            Number = CleanNumber(Number);
            SetAreaCode();
            SetCOCode();
            SetLineNumber();
            SetFullNumber();
        }

        private void OnAreaCodeChanged()
        {
            SetFullNumber();
        }

        private void OnCOCodeChanged()
        {
            SetFullNumber();
        }

        private void OnLineNumberChanged()
        {
            SetFullNumber();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetAreaCode()
        {
            AreaCode = CalcAreaCode(Session, Number);
        }

        private void SetCOCode()
        {
            COCode = CalcCOCode(Number);
        }

        private void SetLineNumber()
        {
            LineNumber = CalcLineNumber(Number);
        }

        private void SetFullNumber()
        {
            if (ValidationRules.ValidatePhoneNumber<XPhoneNumber>(false, Number) && AreaCode != null && !string.IsNullOrWhiteSpace(COCode) && !string.IsNullOrWhiteSpace(LineNumber))
            {
                FullNumber = string.Format("({0}){1}-{2}", AreaCode.Number, COCode, LineNumber);
            }
        }
        #endregion Set Methods

        #region Calc Methods
        public static string CleanNumber(string number)
        {
            string result = number;
            if (!string.IsNullOrWhiteSpace(number))
            {
                result = number.Trim().Replace("-", "").Replace("(", "").Replace(")", "").Replace("+", "").Replace(" ", "");
            }
            return result;
        }

        public static AreaCode CalcAreaCode(Session session, string number)
        {
            
            AreaCode result = null;
            if (ValidationRules.ValidatePhoneNumber<XPhoneNumber>(false, number))
            {
                switch (number.Length)
                {
                    case 10:
                        result = AreaCode.CreateAreaCode(session, number.Substring(0, 3), "", "", "");
                        break;
                    case 11:
                        result = AreaCode.CreateAreaCode(session, number.Substring(1, 3), "", "", "");
                        break;
                }
            }
            return result;
        }

        public static string CalcCOCode(string number)
        {
            string result = string.Empty;
            if (ValidationRules.ValidatePhoneNumber<XPhoneNumber>(false, number))
            {
                switch (number.Length)
                {
                    case 10:
                        result = number.Substring(3, 3);
                        break;
                    case 11:
                        result = number.Substring(4, 3);
                        break;
                }
            }
            return result;
        }

        public static string CalcLineNumber(string number)
        {
            string result = null;
            if (ValidationRules.ValidatePhoneNumber<XPhoneNumber>(false, number))
            {
                result = number.Substring(number.Length - 4, 4);
            }
            return result;
        }
        #endregion Calc Methods

        #region Override Methods

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            if (propertyName == "Number" && !IsLoading && oldValue != newValue)
            {
                OnNumberChanged();
            }
            base.OnChanged(propertyName, oldValue, newValue);
        }

        protected override void OnSaving()
        {
            this.SetModifiedUserAndDate();
            base.OnSaving();
        }

        public override string ToString()
        {
            return Number;
        }
        #endregion Override Methods
    }




}
