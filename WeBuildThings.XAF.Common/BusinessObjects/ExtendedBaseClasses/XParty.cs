﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Linq;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common
{
    public class XParty : Party, IStandardTracking, IMergeable
    {
        #region Fields
        private string _FullName;
        private string _Email;
        private XPhoneNumber _PrimaryPhoneNumber;
        private XAddress _PhysicalAddress;
        private XAddress _MailingAddress;
        private DateTime _DateCreated;
        private XUser _CreatedBy;
        private DateTime _DateModified;
        private XUser _ModifiedBy;
        #endregion Fields

        #region Constructors
        public XParty(Session session)
            : base(session)
        { }

        internal XParty() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.SetCreatedUserAndDate();
        }
        #endregion Constructors

        #region Properties
        public string FullName
        {
            get { return _FullName; }
            set { string oldFullName = _FullName; if (SetPropertyValue("FullName", ref _FullName, value) && !IsLoading) { OnFullNameChanged(oldFullName); } }
        }

        public override string DisplayName
        {
            get { return FullName; }
        }

        [ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
        public XPhoneNumber PrimaryPhoneNumber
        {
            get { return _PrimaryPhoneNumber; }
            set { XPhoneNumber oldPhoneNumber = _PrimaryPhoneNumber; if (SetPropertyValue("PrimaryPhoneNumber", ref _PrimaryPhoneNumber, value) && !IsLoading) { OnPrimaryPhoneNumberChanged(oldPhoneNumber); } }
        }

        [ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
        public XAddress PhysicalAddress
        {
            get { return _PhysicalAddress; }
            set { SetPropertyValue("PhysicalAddress", ref _PhysicalAddress, value); }
        }

        [ExpandObjectMembers(ExpandObjectMembers.InDetailView)]
        public XAddress MailingAddress
        {
            get { return _MailingAddress; }
            set { SetPropertyValue("MailingAddress", ref _MailingAddress, value); }
        }

        [Size(255)]
        public string Email
        {
            get { return _Email; }
            set { SetPropertyValue("Email", ref _Email, value); }
        }

        [Association("Party-Addresses", typeof(XAddress))]
        public XPCollection<XAddress> Addresses
        {
            get { return GetCollection<XAddress>("Addresses"); }
        }

        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }
        #endregion Properties

        #region Event Methods
        protected virtual void OnFullNameChanged(string oldFullName) { }

        protected virtual void OnPrimaryPhoneNumberChanged(XPhoneNumber oldPhoneNumber)
        {
            if (oldPhoneNumber != null && !PhoneNumbers.Contains(oldPhoneNumber))
            {
                PhoneNumbers.Add(oldPhoneNumber);
            }
            if (PrimaryPhoneNumber != null && PhoneNumbers.Contains(PrimaryPhoneNumber))
            {
                PhoneNumbers.Remove(PrimaryPhoneNumber);
            }
        }

        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            if (propertyName == "Address1")
            {
                OnAddress1Changed();
            }
            base.OnChanged(propertyName, oldValue, newValue);
        }

        protected virtual void OnAddress1Changed() { }

        #endregion Event Methods

        #region Validation Methods
        public static bool ValidateSaving(bool throwException, XParty party)
        {
            string message = string.Empty;

            return string.IsNullOrEmpty(message);
        }
        #endregion Validation Methods

        #region Override Methods
        protected override void OnSaving()
        {
            if (!IsDeleted && ValidateSaving(true, this))
            {
                this.SetModifiedUserAndDate();
                base.OnSaving();
            }
        }
        #endregion Override Methods

        #region IMergeable Implementation
        public virtual void Merge(IMergeable source)
        {
            var convertedSource = source as XParty;
            if (convertedSource != null)
            {
                foreach (var address in convertedSource.Addresses)
                {
                    if (!Addresses.Contains(address))
                    {
                        Addresses.Add(address);
                    }
                }
                if (!Addresses.Contains(convertedSource.PhysicalAddress))
                {
                    Addresses.Add(convertedSource.PhysicalAddress);
                }
                if (!Addresses.Contains(convertedSource.MailingAddress))
                {
                    Addresses.Add(convertedSource.MailingAddress);
                }
                foreach (var phoneNumber in convertedSource.PhoneNumbers)
                {
                    if (!PhoneNumbers.Contains(phoneNumber))
                    {
                        PhoneNumbers.Add(phoneNumber);
                    }
                }
                if (!PhoneNumbers.Contains(convertedSource.PrimaryPhoneNumber))
                {
                    PhoneNumbers.Add(convertedSource.PrimaryPhoneNumber);
                }
                if (string.IsNullOrWhiteSpace(Email))
                {
                    Email = convertedSource.Email;
                }
                if (Photo == null)
                {
                    Photo = convertedSource.Photo;
                }
            }
        }
        #endregion IMergeable Implementation
    }
}
