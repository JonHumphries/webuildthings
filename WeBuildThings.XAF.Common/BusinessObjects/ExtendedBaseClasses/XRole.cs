﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Xpo;
using System.Collections.Generic;

namespace WeBuildThings.XAF.Common
{
    public class XRole:SecuritySystemRole
    {
        #region Fields
        private string _Description;
        #endregion Fields

        #region Constructors
        public XRole(Session session) : base(session) { }

        internal XRole() : base(Session.DefaultSession) { }

        public static XRole CreateRole(Session session, string roleName)
        {
            XRole result = null;
            if (!string.IsNullOrWhiteSpace(roleName))
            {
                var existingRole = session.FindObject<XRole>(new BinaryOperator("Name", roleName));
                result = (existingRole == null) ? new XRole(session) : existingRole;
                result.Name = roleName;
            }
            return result;
        }
        #endregion Constructors

        #region Properties
        [Size(SizeAttribute.Unlimited)]
        public string Description
        {
            get { return _Description; }
            set { SetPropertyValue("Description", ref _Description, value); }
        }
        #endregion Properties

        #region Override Methods
        public override string ToString()
        {
            return this.Name;
        }
        #endregion Override Methods

        #region Public methods
        public bool ContainsRole(string roleName)
        {
            bool result = (Name == roleName);
            if (!result)
            {
                foreach (var childRole in ChildRoles)
                {
                    result = ((XRole)childRole).ContainsRole(roleName);
                    if (result) break;
                }
            }
            return result;
        }

        public IEnumerable<XRole> FlattenChildRoles()
        {
            HashSet<XRole> result = new HashSet<XRole>();
            result.Add(this);
            foreach (XRole role in ChildRoles)
            {
                foreach (var childRole in role.FlattenChildRoles())
                {
                    result.Add(childRole);
                }
            }
            return result;
        }
        #endregion Public Methods
    }
}
