﻿using DevExpress.ExpressApp.Model;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.ComponentModel;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    public class Comment : XNote, IStandardTracking
    {
        public const string CommentTypeListName = "CommentType";

        #region Fields
        private XUser _CreatedBy;
        private DateTime _DateCreated;
        private XUser _ModifiedBy;
        private DateTime _DateModified;
        private string _Subject;
        private LookupItem _Type;
        private bool _ValidateCommentPeriod;
        #endregion Fields

        #region Constructors
        public Comment(Session session) : base(session) { Initialize(); }

        internal Comment() : base() { Initialize(); }

        protected virtual void Initialize()
        {
            ValidateCommentPeriod = true;
            CommentModified = false;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            this.SetCreatedUserAndDate();
        }

        #endregion Constructors

        #region Properties
        [Size(255)]
        public string Subject
        {
            get { return _Subject; }
            set { SetPropertyValue("Subject", ref _Subject, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public XUser CreatedBy
        {
            get { return _CreatedBy; }
            set { SetPropertyValue("CreatedBy", ref _CreatedBy, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateCreated
        {
            get { return _DateCreated; }
            set { SetPropertyValue("DateCreated", ref _DateCreated, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        public XUser ModifiedBy
        {
            get { return _ModifiedBy; }
            set { SetPropertyValue("ModifiedBy", ref _ModifiedBy, value); }
        }

        [ModelDefault("AllowEdit", "False")]
        [ModelDefault("DisplayFormat", "g")]
        public DateTime DateModified
        {
            get { return _DateModified; }
            set { SetPropertyValue("DateModified", ref _DateModified, value); }
        }

        [DataSourceCriteria("[LookupList.Name] = 'CommentType' and [Active]")]
        public LookupItem Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        [Association("Comment-Documents", typeof(Document))]
        public XPCollection<Document> Documents
        {
            get { return GetCollection<Document>("Documents"); }
        }

        [Browsable(false)]
        protected bool ValidateCommentPeriod
        {
            get {return _ValidateCommentPeriod;}
            set {if (_ValidateCommentPeriod != value) _ValidateCommentPeriod = value;}
        }

        private bool CommentModified { get; set; }
        #endregion Properties
        
        #region Overrides
        protected override void OnChanged(string propertyName, object oldValue, object newValue)
        {
            if (propertyName == "Subject" || propertyName == "Text") CommentModified = true;
            base.OnChanged(propertyName, oldValue, newValue);
        }

        protected override void OnSaving()
        {
            this.SetModifiedUserAndDate();
            var user = this.GetCurrentUser();
            int editPeriod = SystemSettings.GetInstance<CommentSystemSettings>(Session).CommentEditPeriod;
            if (CreatedBy == null) CreatedBy = user;
            if (!IsDeleted && ValidateComment(ValidateCommentPeriod, true, this, editPeriod, user))
            {
                base.OnSaving();
            }
        }
        #endregion

        #region Validation Methods
        public static bool ValidateComment(bool validateCommentPeriod, bool exception, Comment comment, int editPeriod, XUser user)
        {
            bool result = true;
            if (!comment.IsDeleted && validateCommentPeriod)
            {
                if (user == null)
                {
                    //TODO
                    //throw new SecuritySystemException("Comment.ValidateComment: User not logged in.");
                }
                else
                {
                    DateTime now = DateTime.Now.ToUniversalTime();
                    DateTime endOfEditPeriod = comment.DateCreated.ToUniversalTime().AddHours(editPeriod);
                    string createdByUserName = (comment.CreatedBy == null) ? string.Empty : comment.CreatedBy.UserName;
                    result = (comment.Session.IsNewObject(comment) || //its a new object so it doesn't matter then pass
                                !comment.CommentModified || //if we didn't modify the subject or text then pass
                                (endOfEditPeriod > now && createdByUserName == user.UserName) // true if it is the same user and we are within the comment edit period.
                              );
                    if (!result && exception)
                    {
                        Exception e = new Exception("Cannot edit the comment after initial creation.");
                        e.Source = "Comment.ValidateComment";
                        e.Data.Add("Exception", exception.ToString());
                        e.Data.Add("Date", comment.DateCreated.ToString());
                        e.Data.Add("Author", (comment.Author == null) ? "Null" : comment.Author.ToString());
                        e.Data.Add("User", (user == null) ? "Null" : user.UserName.ToString());
                        e.Data.Add("Edit Period", editPeriod.ToString());
                        throw e;
                    }
                }
            }
            return result;
        }
        #endregion Validation Methods
    }
}