﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System.ComponentModel;

namespace WeBuildThings.XAF.Common
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class CommentTemplate : Comment
    {
        #region Fields
        private string _Name;
        #endregion Fields

        #region Constructors

        public CommentTemplate(Session session)
            : base(session)
        { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }
        #endregion Properties

        #region Override Methods
        protected override void Initialize()
        {
            base.Initialize();
            ValidateCommentPeriod = false;
        }
        #endregion Override Methods

    }
}
