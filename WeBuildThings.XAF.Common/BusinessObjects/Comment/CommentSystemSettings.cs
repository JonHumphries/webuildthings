﻿using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common
{
    public class CommentSystemSettings : SystemSettings
    {
        #region Fields
        private int _CommentEditPeriod;
        private int _MaxWindowCaptionSize;
        #endregion Fields

        #region Constructors
        public CommentSystemSettings(Session session) : base(session) { }

        public CommentSystemSettings() : base(Session.DefaultSession) { }
        #endregion Constructors

        #region Properties
        public int CommentEditPeriod
        {
            get { return _CommentEditPeriod; }
            set { SetPropertyValue("CommentEditPeriod", ref _CommentEditPeriod, value); }
        }

        public int MaxWindowCaptionSize
        {
            get { return _MaxWindowCaptionSize; }
            set { SetPropertyValue("MaxWindowCaptionSize", ref _MaxWindowCaptionSize, value); }
        }
        #endregion Properties
    }
}
