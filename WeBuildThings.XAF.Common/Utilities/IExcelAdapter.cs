using System;
using System.Collections.Generic;
using System.IO;

namespace AETNAEnrollmentEDI
{
    public interface IExcelAdapter : IDisposable
    {
        FileInfo File { get; set; }
        List<Dictionary<string, string>> Rows { get; }
        void AddColumn(string columnName);
        string GetValue(string columnName, int rowNumber);
        void SetValue(string columnName, int rowNumber, string value);
        int Count { get; }
        string[] ListSheets();
    }

    public enum ExcelAdapterType { OLE, Raw };
}
