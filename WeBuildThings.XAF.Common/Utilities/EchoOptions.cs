using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Messaging;

namespace WeBuildThings.Utilities
{
    public class EchoOptions : BaseObject, IMessagingOptions
    {
        #region Fields

        private bool _EchoOn;
        private bool _MessageErrors;
        private bool _ThrowErrors;
        private bool _UpdateDatabase;
        private bool _UpdateConsole;
        private bool _UpdateDiagnostic;

        #endregion Fields

        #region Constructors

        public EchoOptions(Session session)
            : base(session)
        {
        }

        public EchoOptions()
        {
        }

        #endregion

        #region Properties

        public bool UpdateConsole
        {
            get { return _UpdateConsole; }
            set { SetPropertyValue("UpdateConsole", ref _UpdateConsole, value); }
        }

        public bool UpdateDatabase
        {
            get { return _UpdateDatabase; }
            set { SetPropertyValue("UpdateDatabase", ref _UpdateDatabase, value); }
        }

        public bool UpdateDiagnostic
        {
            get { return _UpdateDiagnostic; }
            set { SetPropertyValue("UpdateDiagnostic", ref _UpdateDiagnostic, value); }
        }

        public bool ThrowErrors
        {
            get { return _ThrowErrors; }
            set { SetPropertyValue("ThrowErrors", ref _ThrowErrors, value); }
        }

        public bool MessageErrors
        {
            get { return _MessageErrors; }
            set { SetPropertyValue("MessageErrors", ref _MessageErrors, value); }
        }

        public bool EchoOn
        {
            get { return _EchoOn; }
            set { SetPropertyValue("EchoOn", ref _EchoOn, value); }
        }

        #endregion

        #region Methods

        public void SetEchoOptions(bool updateConsole, bool updateDatabase, bool updateDiagnostic, bool echoOn, bool throwErrors, bool echoErrorMessages)
        {
            UpdateConsole = updateConsole;
            UpdateDatabase = updateDatabase;
            UpdateDiagnostic = updateDiagnostic;
            EchoOn = echoOn;
            ThrowErrors = throwErrors;
            MessageErrors = echoErrorMessages;
        }

        public static EchoOptions Default(Session session)
        {
            EchoOptions echoOptions = new EchoOptions(session);
            echoOptions.SetEchoOptions(true, false, true, true, true, true);
            return echoOptions;
        }

        public static EchoOptions Default()
        {
            EchoOptions echoOptions = new EchoOptions();
            echoOptions.SetEchoOptions(true, false, true, true, true, true);
            return echoOptions;
        }

        #endregion
    }
}
