﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.Collections.Generic;

namespace WeBuildThings.Utilities.Extensions
{
    public static class XPCollectionExtensions
    {
        public static HashSet<BaseObject> ToHashSet(this XPCollection value)
        {
            HashSet<BaseObject> result = null;
            if (value != null && value.Session != null)
            {
                result = new HashSet<BaseObject>();
                foreach (var item in value)
                {
                    result.Add(item as BaseObject);
                }
            }
            return result;
        }
    }
}
