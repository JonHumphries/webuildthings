﻿using DevExpress.Xpo;
using System;
using System.ComponentModel;
using System.Reflection;

namespace WeBuildThings.Utilities.Extensions
{
    public static class TypeExtensions
    {

        public static dynamic CreateInstance(this Type type, Session session)
        {
            // create an object of the type
            //Type type = Type.GetType(typeName);
            object[] parameters = new object[] {session};
            return Activator.CreateInstance(type,  parameters);
        }

    }
}
