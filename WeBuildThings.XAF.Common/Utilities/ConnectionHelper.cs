﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.ClientServer;
using DevExpress.ExpressApp.Security.ClientServer.Wcf;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using DevExpress.Xpo.DB;
using DevExpress.Xpo.Metadata;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.ServiceModel;
using WeBuildThings.Utilities.Extensions;

namespace WeBuildThings.XAF.Common
{
    public static class ConnectionHelper
    {
        #region Constants
        private const string NTierConnectionStringName = "NTierConnectionString";
        private const string DirectConnectionStringName = "ConnectionString";
        private const string TRUE = "TRUE";
        private const string NTierApplicationSetting = "NTierApplication";
        #endregion Constants

        #region Fields
        static string ConnectionString;
        static Assembly[] Assemblies;
        private readonly static object lockObject = new object();
        static volatile IDataLayer _DataLayer;
        #endregion Fields

        #region Properties
        static IDataLayer DataLayer
        {
            get
            {
                if (_DataLayer == null)
                {
                    lock (lockObject)
                    {
                        if (_DataLayer == null)
                        {
                            _DataLayer = CreateDataLayer(ConnectionString, Assemblies);
                        }
                    }
                }
                return _DataLayer;
            }
        }
        #endregion Properties

        #region Creation Methods
        private static XPDictionary CreateXPDictionary(params Assembly[] assemblies)
        {
            XPDictionary result = new ReflectionDictionary();
            result.CollectClassInfos(assemblies);
            return result;
        }

        private static IDataLayer CreateDataLayer(string connectionString,params Assembly[] assemblies)
        {
            XpoDefault.Session = null;
            IDataStore dataStore = XpoDefault.GetConnectionProvider(connectionString, AutoCreateOption.SchemaAlreadyExists);
            return CreateThreadSafeDataLayer(dataStore, assemblies);
        }

        private static IDataLayer CreateDataLayer(params Assembly[] assemblies)
        {
            XpoDefault.Session = null;
            IDataStore dataStore = XpoDefault.GetConnectionProvider(AutoCreateOption.SchemaAlreadyExists);
            return CreateThreadSafeDataLayer(dataStore, assemblies);
        }

        private static IDataLayer CreateThreadSafeDataLayer(IDataStore dataStore, params Assembly[] assemblies)
        {
            XPDictionary dict = null;
            dict = new ReflectionDictionary();
            dict.GetDataStoreSchema(assemblies);
            IDataLayer dl = new ThreadSafeDataLayer(dict, dataStore);
            return dl;
        }
        #endregion Creation Methods

        #region Public Methods
        public static IObjectSpace CreateObjectSpace()
        {
            string connectionString = ConfigurationManager.ConnectionStrings[DirectConnectionStringName].ConnectionString;
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                var exception = new ArgumentNullException("Connection String must be set to generate a new object space");
                exception.Source = typeof(ConnectionHelper).ToString();
                throw exception;
            }
            return CreateNewObjectSpace(connectionString);
        }

        /// <summary>
        /// Returns a session but will not have any assemblies loaded.
        /// </summary>
        public static Session CreateSession(string connectionString)
        {
            ConnectionString = connectionString;
            Assemblies = null;
            return new Session(DataLayer);
        }

        /// <summary>
        /// Returns a session based on the assemblies supplied
        /// </summary>
        //public static Session CreateSession(string connectionString, params Assembly[] assemblies)
        //{
        //    ConnectionString = connectionString;
        //    Assemblies = assemblies;
        //    return new Session(DataLayer);
        //}

        /// <summary>
        /// Returns a session based on the assemblies supplied
        /// </summary>
        public static Session CreateSession(string connectionString, Assembly[] assemblies)
        {
            ConnectionString = connectionString;
            Assemblies = assemblies;
            return new Session(DataLayer);
        }

        /// <summary>
        /// Creates an object layer, connected to an XAF Application Server, using XAF standard authentication methods.
        /// </summary>
        /// <param name="userName">Application user name</param>
        /// <param name="password">Application password</param>
        /// <param name="assemblies">The assemblies that will be in the object layer</param>
        public static SerializableObjectLayerClient StandardAuthenticationConnection(string userName, string password, params Assembly[] assemblies)
        {
            ConfigMgrHelper.TestAppSetting(NTierApplicationSetting);
            bool NTier = (ConfigurationManager.AppSettings[NTierApplicationSetting].ToUpper() == TRUE);
            if (NTier)
            {
                XPDictionary dictionary = CreateXPDictionary(assemblies);
                ClientInfo statelessClientInfo = new ClientInfo(Guid.Empty, Guid.Empty, new AuthenticationStandardLogonParameters(userName, password));
                ConfigMgrHelper.ValidateConnectionStringName(NTierConnectionStringName);
                ConnectionString = ConfigurationManager.ConnectionStrings[NTierConnectionStringName].ConnectionString;
                WcfSecuredDataServerClient clientDataServer = new WcfSecuredDataServerClient(WcfDataServerHelper.CreateDefaultBinding(), new EndpointAddress(ConnectionString));
                SecuredSerializableObjectLayerClient securedObjectLayerClient = new SecuredSerializableObjectLayerClient(statelessClientInfo, clientDataServer);
                return new SerializableObjectLayerClient(securedObjectLayerClient, dictionary);
            }
            else
            {
                throw new NotImplementedException("This connection method is for connecting to a XAF Application Server only");
            }
        }

        /// <summary>
        /// Creates an Object Space based on the connection string.
        /// </summary>
        /// <see cref="http://blog.paraoffice.at/how-to-use-dependency-injection-in-xaf-unitte"/>
        public static IObjectSpace CreateNewObjectSpace(string connectionString)
        {
            if (string.IsNullOrWhiteSpace(connectionString))
            {
                throw new ArgumentNullException("Create New Object Space requires a valid connection string");
            }
            IDbConnection connection = new SqlConnection(connectionString);
            IObjectSpaceProvider ObjectSpaceProvider = new XPObjectSpaceProvider(connectionString, connection);
            var OS = ObjectSpaceProvider.CreateObjectSpace();
            return OS;
        }
        #endregion Public Methods
    }
}
