using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;

namespace AETNAEnrollmentEDI
{
    public class OleAdapter : IExcelAdapter
    {
        #region Fields
        private FileInfo file;
        private string connectionString;
        private OleDbConnection connection;
        private OleDbDataAdapter adapter;
        private string selectSQL;
        private string updateSQL;
        private string insertSQL;
        private bool disposed;
        private DataSet DataSet;
        #endregion

        #region Properties

        public List<Dictionary<string, string>> Rows
        {
            get { return DataSet.Tables[0].Rows.ToIExcelRow(); }
        }

        public string SelectSQL
        {
            get { return selectSQL; }
            set { if (selectSQL != value) { selectSQL = value; OnSelectSQLChanged(); } }
        }

        public string UpdateSQL
        {
            get { return updateSQL; }
            set { if (updateSQL != value) { updateSQL = value; OnUpdateSQLChanged(); } }
        }

        public string InsertSQL
        {
            get { return insertSQL; }
            set { if (insertSQL != value) { insertSQL = value; OnInsertSQLChanged(); } }
        }

        public OleDbDataAdapter Adapter
        {
            get { return adapter; }
            set { if (adapter != value) adapter = value; }
        }

        public FileInfo File
        {
            get { return file; }
            set { if (file != value) { file = value; OnFileChanged(); } }
        }

        public OleDbConnection Connection
        {
            get { return connection; }
            set { if (connection != value) { connection = value; OnConnectionChanged(); } }
        }

        public string ConnectionString
        {
            get { return connectionString; }
            set { if (connectionString != value) { connectionString = value; OnConnectionStringChanged(); } }
        }

        public int Count
        {
            get { return Rows.Count; }
        }
        #endregion

        #region Constructors

        public OleAdapter(FileInfo file)
        {
            File = file;
        }

        public OleAdapter()
        {
        }
        #endregion

        #region Public Methods
        public string GetValue(string columnName, int rowNumber)
        {
            throw new NotImplementedException("Get Value is not implemented on OleAdapter yet");
        }

        /*TODO build paramters dynamically
 *     adapter.UpdateCommand.Parameters.Add("@StreetAddressLine2", OleDbType.Char, 255).SourceColumn = "STREET ADDRESS LINE 2";
            adapter.UpdateCommand.Parameters.Add("@DEPSSN", OleDbType.Char, 255).SourceColumn = "Dep SSN";
        */
        public void AddColumn(string columnName)
        {
            throw new NotImplementedException("The OleAdapter does not support Adding Columns");
        }
        #endregion

        #region Event Methods
        private void OnSelectSQLChanged()
        {
            SetAdapter();
        }

        private void OnUpdateSQLChanged()
        {
            SetAdapter();
        }

        private void OnInsertSQLChanged()
        {
            SetAdapter();
        }

        private void OnConnectionChanged()
        {
            SetAdapter();
        }

        private void OnFileChanged()
        {
            SetConnectionString();
        }

        private void OnConnectionStringChanged()
        {
            SetConnection();
        }
        #endregion

        #region Set Methods
        private void SetAdapter()
        {
            AdapterDispose();
            if (!string.IsNullOrEmpty(SelectSQL) && Connection != null)
            {
                Adapter = new OleDbDataAdapter(SelectSQL, Connection);
                if (!string.IsNullOrEmpty(UpdateSQL))
                {
                    Adapter.UpdateCommand = new OleDbCommand(UpdateSQL, Connection);
                }
                if (!string.IsNullOrEmpty(InsertSQL))
                {
                    Adapter.InsertCommand = new OleDbCommand(InsertSQL, Connection);
                }
                DataSetDispose();
                DataSet = new DataSet();
                Adapter.Fill(DataSet);
            }
        }


        private void SetConnection()
        {
            Connection = new OleDbConnection(ConnectionString);
        }

        public void SetValue(string columnName, int rowNumber, string value)
        {
            throw new NotImplementedException("OleAdapter has not and may not implemented SetValue.");
        }

        private void SetConnectionString()
        {
            if (File.FullName.EndsWith("xls"))
            {
                ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", File.FullName);
            }
            else if (File.FullName.EndsWith("csv"))
            {
                ConnectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=\"text;HDR=Yes;FMT=CSVDelimited\"", File.FullName);
            }
            else if (File.FullName.EndsWith("xlsx"))
            {
                ConnectionString = File.FullName.EndsWith("xlsx") ? string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0}; Extended Properties=Excel 12.0;", File.FullName) : null;
            }
            else
            {
                throw new InvalidCastException(string.Format("{0} is not a supported filetype", File.FullName));
            }
        }


        public string[] ListSheets()
        {
            Workbook workbook = null;
            try
            {
                Application ExcelInstance = new Application() { DisplayAlerts = false };
                workbook = ExcelInstance.Workbooks.Open(File.FullName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
                string[] result = new string[ExcelInstance.Worksheets.Count];
                int i = 0;
                foreach (Microsoft.Office.Interop.Excel.Worksheet worksheet in ExcelInstance.Worksheets)
                {
                    result[i] = worksheet.Name;
                    i++;
                }
                return result;
            }
            finally
            {
                if (workbook != null)
                {
                    Marshal.ReleaseComObject(workbook);
                    workbook = null;
                }
            }
        }
        #endregion

        #region Dispose
        ~OleAdapter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    AdapterDispose();
                    DataSetDispose();
                    disposed = true;
                }
            }
        }

        private void AdapterDispose()
        {
            if (adapter != null)
            {
                adapter.Dispose();
                adapter = null;
            }
        }

        private void DataSetDispose()
        {
            if (DataSet != null)
            {
                DataSet.Dispose();
                DataSet = null;
            }
        }
        #endregion

    }
}
