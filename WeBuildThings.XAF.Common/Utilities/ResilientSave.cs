﻿using DevExpress.ExpressApp;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Common.Utilities
{
    /// <summary>
    /// The Save methods will make 10 attempts spaced 10 seconds apart
    /// to commit the save to the database if a timeout exception occurs.
    /// </summary>
    public static class ResilientSave
    {
        private const int attempts = 10;
        private const int sleepDuration = 10000;
        private const int TIMEOUT_EXPIRED = -2;
        private static int[] exceptionNumbers = new int[] { TIMEOUT_EXPIRED };

        private static int HandleException(SqlException exception, int attempt)
        {
            if (exceptionNumbers.Contains(exception.Number))
            {
                attempt++;
                if (attempt == attempts)
                {
                    throw exception;
                }
                else
                {
                    ThreadUtil.StallThread(sleepDuration);
                }
            }
            else
            {
                throw exception;
            }
            return attempt;
        }

        /// <summary>
        /// Process a resilient save on a session transaction
        /// </summary>
        public static void Save(Session session)
        {
            int attempt = 0;
            while (attempt < attempts)
            {
                try
                {
                    session.CommitTransaction();
                    attempt = attempts;
                }
                catch (SqlException exception)
                {
                    attempt = HandleException(exception, attempt);
                }
            }
        }

        /// <summary>
        /// Process a resilient save on an object space
        /// </summary>
        public static void Save(IObjectSpace objectSpace)
        {
            int attempt = 0;
            while (attempt < attempts)
            {
                try
                {
                    objectSpace.CommitChanges();
                    attempt = attempts;
                }
                catch (SqlException exception)
                {
                    attempt = HandleException(exception, attempt);
                }
            }
        }

        /// <summary>
        /// Process a resilient save on a unit of work
        /// </summary>
        public static void Save(UnitOfWork uow)
        {
            int attempt = 0;
            while (attempt < attempts)
            {
                try
                {
                    uow.CommitChanges();
                    attempt = attempts;
                }
                catch (SqlException exception)
                {
                    attempt = HandleException(exception, attempt);
                }
            }
        }
    }
}
