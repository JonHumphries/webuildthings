﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using WeBuildThings.Common.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeBuildThings.Common.Extensions;

namespace WeBuildThings.Common.Serialization
{
    public interface ISerializeToXaf : ISerialize
    {
        DevExpress.Xpo.Session Session { get; set; }
    }

    public static class SerializeToXAF
    {
        /// <summary>
        /// TODO Make this generic
        /// </summary>
        /// <param name="session"></param>
        /// <param name="filePath"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public static BaseObject[] Import(Session session, string filePath, Type type)
        {
            if (!type.ContainsInterface(typeof(ISerializeToXaf)) && type != typeof(SerializedCollection))
            {
                var exception = new ArgumentException(string.Format("{0} cannot be imported by this method because it does not implement the {1} interface.", type.Name, typeof(ISerializeToXaf)));
                exception.Source = typeof(SerializeToXAF).FullName;
                throw exception;
            }
            HashSet<BaseObject> result = null;
            using (UnitOfWork uow = new UnitOfWork(session.ObjectLayer))
            {
                XMLHelper xmlHelper = new XMLHelper();
                xmlHelper.ExceptionOnFailure = false;
                var obj = xmlHelper.ImportFromXML(filePath, type);
                var xafObject = obj as ISerializeToXaf;
                var serializedCollection = obj as SerializedCollection;
                if (xafObject == null && serializedCollection != null)
                {
                    type = Type.GetType(serializedCollection.TypeFullName);
                    if (type == null)
                    {
                        throw new ArgumentNullException(string.Format("Unable to locate type {0}", serializedCollection.TypeFullName));
                    }
                    obj = xmlHelper.ImportFromXML(filePath, type);
                    xafObject = obj as ISerializeToXaf;
                    serializedCollection = obj as SerializedCollection;
                }
                if (xafObject != null)
                {
                    xafObject.Session = uow;
                    var convertedObject = xafObject.ChangeType(xafObject.SerializedType, null);
                    ((BaseObject)convertedObject).Save();
                    result = new HashSet<BaseObject>() { (BaseObject)convertedObject };
                    uow.CommitChanges();
                }
                else if (serializedCollection != null &&  serializedCollection.SerializedItems[0].GetType().ContainsInterface(typeof(ISerializeToXaf)))
                {

                    result = new HashSet<BaseObject>();
                    foreach (var item in serializedCollection.SerializedItems)
                    {
                        xafObject = item as ISerializeToXaf;
                        xafObject.Session = uow;
                        var convertedObject = (BaseObject)xafObject.ChangeType(xafObject.SerializedType, null);
                        ((BaseObject)convertedObject).Save();
                        result.Add(convertedObject);
                        uow.CommitChanges();
                    }
                }
                else
                {
                    throw new NotImplementedException(string.Format("No instructions for converting {0} objects to the XAF framework", obj.GetType()));
                }
            }
            return result.ToArray();
        }
    }
}
