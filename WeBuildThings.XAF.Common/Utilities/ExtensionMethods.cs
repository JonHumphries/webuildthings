using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System;

namespace AETNAEnrollmentEDI
{
    public static class ExtensionMethods
    {
        //TODO this is pretty slow, and it is misleading
        //it results in a copy of the sheet in memory which takes a while to create and
        //any manipulation of hte values doesn't get copied to the sheet.
        //needs to be more of a reference to the actual sheet.
        public static List<Dictionary<string, string>> ToIExcelRow(this Range value)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            List<string> headerRow = new List<string>();
            for (int columnNumber = 1; columnNumber < value.Rows.Cells.Count; columnNumber++)
            {
                Range rng = value.Rows.Cells[1, columnNumber];
                if (rng.Value2 != null)
                {
                    headerRow.Add(rng.Value2.ToString().Trim());
                }
                else
                {
                    break;
                }
            }
            for (int rowNumber = 2; rowNumber < value.Rows.Count; rowNumber++)
            {
                Dictionary<string, string> resultRow = new Dictionary<string, string>();
                for (int columnNumber = 0; columnNumber < headerRow.Count; columnNumber++)
                {
                    Range rng = value.Rows.Cells[rowNumber, columnNumber + 1];
                    if (rng.Value2 != null)
                    {
                        resultRow.Add(headerRow[columnNumber], rng.Value2.ToString().Trim());
                    }
                }
                result.Add(resultRow);
            }
            return result;
        }

        public static List<Dictionary<string, string>> ToIExcelRow(this DataRowCollection value)
        {
            List<Dictionary<string, string>> result = new List<Dictionary<string, string>>();
            List<string> headerRow = new List<string>();
            for (int columnNumber = 0; columnNumber < value[0].Table.Columns.Count; columnNumber++)
            {
                headerRow.Add(value[0].Table.Columns[columnNumber].ToString().Trim());
            }
            for (int rowNumber = 1; rowNumber < value.Count; rowNumber++)
            {
                Dictionary<string, string> resultRow = new Dictionary<string, string>();
                for (int columnNumber = 0; columnNumber < value[rowNumber].Table.Columns.Count; columnNumber++)
                {
                    resultRow.Add(headerRow[columnNumber], value[rowNumber][columnNumber].ToString().Trim());
                }
                result.Add(resultRow);
            }
            return result;
        }

        /// <summary>
        /// Removes Accents and other non standard characters from the input string
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/249087/how-do-i-remove-diacritics-accents-from-a-string-in-net?lq=1"/>
        /// <param name="text">The string to have Diacritic Characters removed from</param>
        /// <returns>Diacritic free string</returns>
        public static string ConvertDiacritics(this string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }
            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }

        /// <summary>
        /// This removes numbers, punctuation, etc... from a string
        /// It leaves spaces.
        /// </summary>
        /// <param name="value">The string to be modified</param>
        /// <returns>The modified string</returns>
        public static string RemoveNonLetters(this string value)
        {
            return new string(value.ToCharArray().Where(c => c.Equals(' ') || char.IsLetter(c)).ToArray());
        }

        /// <summary>
        /// Allows conversion from int datatype as well as string and Date
        /// Convert.ToDateTime cannot handle ints by default.
        /// </summary>
        /// <seealso cref="http://stackoverflow.com/questions/727466/how-do-i-convert-an-excel-serial-date-number-to-a-net-datetime"/>
        public static DateTime ToDateTime(this string value)
        {
            DateTime result;
            int i = 0;
            if (int.TryParse(value, out i))
            {
                if (i > 59) i -= 1; //Excel/Lotus 2/29/1900 bug   
                result = new DateTime(1899, 12, 31).AddDays(i);
            }
            else
            {
                result = Convert.ToDateTime(value);
            }
            return result;
        }
    }
}
