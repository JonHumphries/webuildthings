﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Microsoft.Office.Interop.Excel;

namespace AETNAEnrollmentEDI
{
    public class ExcelAdapter : IExcelAdapter
    {
        #region Fields
        private Application excelInstance;
        private Workbook workbook;
        private Worksheet worksheet;
        private FileInfo file;
        private bool disposed;
        private Dictionary<string, string> headerRow;
        public bool ThrowExceptionOnError = true;
        #endregion

        #region Properties
        public Application ExcelInstance
        {
            get { return excelInstance; }
            set { if (excelInstance != value) { excelInstance = value; OnExcelInstanceChanged(); } }
        }
        public FileInfo File
        {
            get { return file; }
            set { if (file != value) { file = value; OnFileChanged(); } }
        }

        public Workbook Workbook
        {
            get { return workbook; }
            set { if (workbook != value) { workbook = value; OnWorkbookChanged(); } }
        }

        public Worksheet Worksheet
        {
            get { return worksheet; }
            set { if (worksheet != value) { worksheet = value; OnWorksheetChanged(); } }
        }

        public Dictionary<string, string> HeaderRow
        {
            get { return headerRow; }
            set { if (headerRow != value) headerRow = value; }
        }

        public List<Dictionary<string, string>> Rows
        {
            get { return Worksheet.UsedRange.ToIExcelRow(); }
        }

        public int Count
        {
            get { return Worksheet.UsedRange.Rows.Count; }
        }
        #endregion

        #region Constructors
        public ExcelAdapter(FileInfo file)
        {
            File = file;
        }

        public ExcelAdapter()
        {
        }
        #endregion

        #region Private Methods
        private void OnFileChanged()
        {
            SetExcelInstance();
        }

        private void OnExcelInstanceChanged()
        {
            SetWorkbook();
        }

        private void OnWorkbookChanged()
        {
            SetWorksheet();
        }

        private void OnWorksheetChanged()
        {
            SetHeaderRow();
        }

        private void SetHeaderRow()
        {
            if (Worksheet != null)
            {
                HeaderRow = new Dictionary<string, string>();
                for (int columnNumber = 1; columnNumber < Worksheet.UsedRange.Rows.Cells.Count; columnNumber++)
                {
                    Range rng = Worksheet.UsedRange.Rows.Cells[1, columnNumber];
                    if (rng.Value2 != null)
                    {
                        HeaderRow.Add(rng.Value2.ToString().Trim(), GetExcelColumnName(rng.Column));
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }

        private void SetExcelInstance()
        {
            ExcelInstanceDispose();
            ExcelInstance = new Application() { DisplayAlerts = false };
        }

        private void SetWorkbook()
        {
            WorkbookDispose();
            try
            {
                Workbook = ExcelInstance.Workbooks.Open(File.FullName, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            }
            catch (Exception e)
            {
                Console.WriteLine(string.Format("{0} has been removed", File.FullName));
            }
        }

        private void SetWorksheet()
        {
            WorksheetDispose();
            if (Workbook != null)
            {
                Worksheet = (Worksheet)Workbook.Worksheets.get_Item(1);
            }
        }

        private bool ValidateRowNumber(int rowNumber)
        {
            bool result = true;
            if (rowNumber < 1)
            {
                if (ThrowExceptionOnError)
                {
                    throw new ArgumentException("rowNumber must be greater than zero", "rowNumber");
                }
                else
                {
                    result = false;
                }
            }
            return result;
        }
        #endregion

        #region Public Methods
        public void AddColumn(string columnName)
        {
            int nextColumnNumber = HeaderRow.Count + 1;
            SetValue(nextColumnNumber, 1, columnName);
            SetHeaderRow();
        }

        public string GetValue(string columnName, int rowNumber)
        {
            string address = GetAddress(columnName, rowNumber);
            var result = Worksheet.get_Range(address, address).Value2;
            return (result == null) ? string.Empty : result.ToString();
        }

        public void SetValue(int columnNumber, int rowNumber, string value)
        {
            if (ValidateRowNumber(rowNumber))
            {
                string address = string.Format("{0}{1}", GetExcelColumnName(columnNumber), rowNumber);
                Worksheet.get_Range(address, address).Value = value;
            }
        }

        public void SetValue(string columnName, int rowNumber, string value)
        {
            string address = GetAddress(columnName, rowNumber);
            Worksheet.get_Range(address, address).Value = value;
        }

        public string GetAddress(string columnName, int rowNumber)
        {
            return (ValidateRowNumber(rowNumber)) ? string.Format("{0}{1}", HeaderRow[columnName], rowNumber) : string.Empty;
        }

        public void DisplayCellValue(string address)
        {
            Console.WriteLine(string.Format("Cell {0} has a value of {1}", address, Worksheet.get_Range(address, address).Value2));
        }

        public void DisplayHeaderRow()
        {
            foreach (KeyValuePair<string, string> row in HeaderRow)
            {
                Console.WriteLine(string.Format("Name - {0} : Letters - {1}", row.Key, row.Value));
            }
        }

        public static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;
            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo) + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }
            return columnName;
        }

        public string[] ListSheets()
        {
            string[] result = new string[this.ExcelInstance.Worksheets.Count];
            for (int i = 0; i < result.Length; i++)
            {
                result[i] = ExcelInstance.Worksheets[i].displayWorksheet.Name;
            }
            return result;
        }
        #endregion

        #region Dispose Methods
        ~ExcelAdapter()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    WorksheetDispose();
                    WorkbookDispose();
                    ExcelInstanceDispose();
                    disposed = true;
                }
            }
        }

        private void ExcelInstanceDispose()
        {
            if (ExcelInstance != null)
            {
                ExcelInstance.Quit();
                Marshal.ReleaseComObject(ExcelInstance);
            }
        }

        private void WorksheetDispose()
        {
            if (Worksheet != null)
            {
                Marshal.ReleaseComObject(Worksheet);
                Worksheet = null;
            }
        }

        private void WorkbookDispose()
        {
            if (Workbook != null)
            {
                object misValue = System.Reflection.Missing.Value;
                Workbook.Close(true, misValue, misValue);
                Marshal.ReleaseComObject(Workbook);
                Workbook = null;
            }
        }
        #endregion
    }

}
