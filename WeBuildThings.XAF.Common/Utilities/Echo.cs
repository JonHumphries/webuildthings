using System;
using System.Diagnostics;

namespace WeBuildThings.Utilities
{
    public class Echo
    {
        public EchoOptions Options { get; set; }
        public INote Note { get; set; }

        public Echo(EchoOptions options, INote note)
        {
            Note = note;
            Options = options;
        }

        public Echo(EchoOptions options)
        {
            Options = options;
        }

        public void EchoError(Exception error)
        {
            if (Options.EchoOn)
            {
                if (Options.MessageErrors)
                {
                    EchoMessage(error.Message);
                }
                if (Options.ThrowErrors)
                {
                    throw error;
                }
            }
        }

        public void EchoMessage(string message)
        {
            if (Options.EchoOn)
            {
                if (Options.UpdateConsole)
                {
                    Console.WriteLine(message);
                }
                if (Options.UpdateDiagnostic)
                {
                    Debug.WriteLine(message);
                }
                if (Options.UpdateDatabase)
                {
                    throw new NotImplementedException("Commands to Update Database are not Set");
                }
                if (Note != null)
                {
                    Note.Note += message;
                }
            }
        }
    }
}
