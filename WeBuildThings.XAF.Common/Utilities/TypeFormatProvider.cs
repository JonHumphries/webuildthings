﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;

namespace AETNAEnrollmentEDI
{
    public class TypeFormatProvider
    {
        public TypeFormatProvider(string defaultFormat, CultureInfo defaultCultureInfo)
        {
            this.Format = defaultFormat;
            this.CultureInfo = defaultCultureInfo;
        }
        public TypeFormatProvider(string format)
        {
            this.Format = format;
        }
        public TypeFormatProvider()
        {
        }

        public string Format { get; set; }
        public CultureInfo CultureInfo { get; set; }

        public static void TestMethod()
        {
            //Get the TypeFormatProvider
            TypeFormatProvider typeFormatProvider = new TypeFormatProvider("C2", CultureInfo.GetCultureInfo("en-US"));

            //Get the specific Culture Info 
            CultureInfo cultureInfo = CultureInfo.GetCultureInfo("ja-JP");

            decimal d = 120;
            Console.WriteLine("Original value: " + d);

            // Option 1
            string result = d.ToString(typeFormatProvider.Getformat(cultureInfo));
            Console.WriteLine("Formatted value: " + result);

            //Option 2
            result = typeFormatProvider.GetformatValue(d);
            Console.WriteLine("Formatted value: " + result);

            //Option 3
            result = TypeFormatProvider.GetFormattedValue(d, typeFormatProvider);
            Console.WriteLine("Formatted value: " + result);

            Console.ReadLine();
        }

        public object Getformat(CultureInfo cultureInfo)
        {
            this.CultureInfo = cultureInfo;
            return this;
        }

        /// <summary>
        /// Get the Formatted value -- Option 2
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetformatValue(object value)
        {
            string result = Convert.ToString(value);
            dynamic dynamicValue = value;
            if (value.GetType() == typeof(string))
            {
                result = dynamicValue.ToString(CultureInfo);
            }
            else
            {
                result = dynamicValue.ToString(Format, CultureInfo);
            }
            return result;
        }

        /// <summary>
        /// Get the Formatted value -- Option 3
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetFormattedValue(object value, TypeFormatProvider typeFormat)
        {
            string result = Convert.ToString(value);
            dynamic dynamicValue = value;
            if (value.GetType() == typeof(string))
            {
                result = dynamicValue.ToString(typeFormat.CultureInfo);
            }
            else
            {
                result = dynamicValue.ToString(typeFormat.Format, typeFormat.CultureInfo);
            }
            return result;
        }
    }

    static class TypeFormatProviderExtension
    {
        /// <summary>
        /// Get the Formated value -- Option 1
        /// </summary>
        /// <param name="value"></param>
        /// <param name="typeFormatProvider"></param>
        /// <returns></returns>
        public static string ToString(this object value, object typeFormatProvider)
        {
            string result = Convert.ToString(value);

            dynamic dynamicValue = value;
            if (value.GetType() == typeof(string))
            {
                result = dynamicValue.ToString(((TypeFormatProvider)typeFormatProvider).CultureInfo);
            }
            else
            {
                result = dynamicValue.ToString(((TypeFormatProvider)typeFormatProvider).Format, ((TypeFormatProvider)typeFormatProvider).CultureInfo);
            }

            return result;
        }
    }
}
