﻿using System;

namespace WeBuildThings.Utilities.Interfaces
{
    public interface IPersistedProperty
    {
        Type GetType(string propertyName);
        void SetTargetProperty(string propertyName, string propertyValue);
    }
}
