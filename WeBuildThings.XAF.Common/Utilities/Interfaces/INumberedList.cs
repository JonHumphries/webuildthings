﻿using System.Collections.Generic;
using System.Linq;

namespace WeBuildThings.BusinessObjects.Common
{
    public interface INumberedList
    {
        int Number { get; set; }
        IEnumerable<INumberedList> List { get; }
    }

    public static class OrderListExtension
    {
        public static void MoveUp(this INumberedList numberedList)
        {
            int newNumber = (numberedList.Number - 1 > 0) ? numberedList.Number - 1 : numberedList.GetNextNumber();
            var items = numberedList.List.Where(n => n.Number == newNumber);
            foreach (var item in items)
            {
                item.Number++;
            }
            numberedList.Number = newNumber;
        }

        public static void MoveDown(this INumberedList numberedList)
        {
            int newNumber = (numberedList.Number + 1 > numberedList.GetNextNumber()) ? 1 : numberedList.Number + 1;
            var items = numberedList.List.Where(n => n.Number == newNumber);
            foreach (var item in items)
            {
                item.Number--;
            }
            numberedList.Number = newNumber;
        }

        public static void ReOrder(this INumberedList numberedList)
        {
            if (numberedList.List != null)
            {
                var list = numberedList.List.OrderBy(n => n.Number).ToList();
                int newNumber = 1;
                foreach (INumberedList listItem in list)
                {
                    listItem.Number = newNumber;
                    newNumber++;
                }
            }
        }

        public static void SetNumber(this INumberedList numberedList)
        {
            numberedList.Number = numberedList.GetNextNumber();
        }

        /// <summary>
        /// Calculates the next number value in the list.
        /// Handles whether the item has been added yet or not.
        /// </summary>
        private static int GetNextNumber(this INumberedList numberedList)
        {
            int result = 0;
            if (numberedList.List != null)
            {
                result = numberedList.List.Where(n => n != numberedList).Count() + 1;
            }
            return result;
        }

    }
}
