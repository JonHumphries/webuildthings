﻿using DevExpress.Persistent.Base.General;
using WeBuildThings.BusinessObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.Utilities.Interfaces
{
    public interface IBinaryTree: ITreeNode
    {
        new ITreeNode Parent { get; set; }
        new IEnumerable<IBinaryTree> Children { get; }
        IEnumerable<IBinaryTree> FullTree { get; }
    }

    public static class IBinaryTreeExtension
    {
        public static void Promote(this IBinaryTree currentObject)
        {
            if (currentObject.Parent != null)
            {
                currentObject.Parent = currentObject.Parent.Parent;
            }
        }

        public static void Demote(this IBinaryTree currentObject)
        {
            if (currentObject.Children != null && currentObject.Children.Any())
            {
                var outlinedList = currentObject as IOutlinedList;
                IBinaryTree newParent = (outlinedList != null) ? (IBinaryTree)outlinedList.Children.OrderBy(n => n.SortOrder).FirstOrDefault() : currentObject.Children.FirstOrDefault();
                foreach (var child in currentObject.Children)
                {
                    child.Parent = currentObject.Parent;
                }
                currentObject.Parent = newParent;
            }
            else if (currentObject.FullTree != null)
            {
                IBinaryTree newParent = null;
                var iNumberedList = currentObject as INumberedList;
                if (iNumberedList != null)
                {
                    newParent = (IBinaryTree)iNumberedList.List.Where(n => n.Number == iNumberedList.Number - 1).FirstOrDefault();
                }
                if (newParent == null)
                {
                    newParent = currentObject.FullTree.Where(n => n != currentObject && n.Parent == null).FirstOrDefault();
                }
                currentObject.Parent = newParent;
            }
        }
    }
}
