namespace WeBuildThings.Utilities
{
    public interface INote
    {
        string Note { get; set; }
    }
}
