﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Security;

namespace WeBuildThings.Utilities
{
    public interface IStandardTracking
    {
        DateTime DateCreated { get; set; }
        XUser CreatedBy { get; set; }
        DateTime DateModified { get; set; }
        XUser ModifiedBy { get; set; }
    }

    public static class StandardTrackingExtension
    {
        /// <summary>
        /// Used to set the Created User and Date for given object
        /// </summary>
        /// <param _Name="tracking"></param>
        public static void SetCreatedUserAndDate(this IStandardTracking tracking)
        {
            tracking.DateCreated = DateTime.Now.ToUniversalTime();
            tracking.CreatedBy = tracking.GetCurrentUser();
        }

        /// <summary>
        /// Used to set the Modified User and Date for given object
        /// </summary>
        public static void SetModifiedUserAndDate(this IStandardTracking tracking)
        {
            if (tracking.DateCreated == DateTime.MinValue)
            {
                tracking.DateCreated = DateTime.Now;
            }
            var xoUser = tracking.GetCurrentUser();
            tracking.CreatedBy = tracking.CreatedBy ?? xoUser;
            tracking.ModifiedBy = xoUser;
            tracking.DateModified = DateTime.Now.ToUniversalTime();
        }

        /// <summary>
        /// Get the username of the current user
        /// </summary>
        public static XUser GetCurrentUser(this IStandardTracking tracking)
        {
            Session session = ((BaseObject)tracking).Session;
            var result = XUser.GetUserByUserName(session, SecuritySystem.CurrentUserName);
            if (result == null)
            {
                var ssr = ((SecuritySystemUser)SecuritySystem.CurrentUser).GetUserByUserName(session);
                if (ssr != null)
                {
                    throw new NotImplementedException("The WeBuildThings libraries expect that users are created using the XUser class not Security System User");
                }
            }
            return result;
        }
    }
}
