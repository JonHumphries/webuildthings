﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace WeBuildThings.Common.Utilities
{
    public interface IMonitoredProcess
    {

        int Maximum { get; }
        void Cancel();
        BackgroundWorker BackgroundWorker { get; }
    }

    public interface IProgressBar: IDisposable
    {
        IMonitoredProcess MonitoredProcess { get; set; }
        void SetMaximum(int maximum);
        void PerformStep();
        bool PercentView { get; set; }
    }
}
