﻿using System.Collections.Generic;
using System.Linq;
using WeBuildThings.BusinessObjects.Common;

namespace WeBuildThings.Utilities.Interfaces
{
    public interface IOutlinedList : INumberedList
    {
        string OutlineNumber { get; set; }
        int SortOrder { get; set; }
        IOutlinedList Parent { get; }
        IEnumerable<IOutlinedList> Children { get; }
        IEnumerable<IOutlinedList> FullList { get; }
    }

    public static class IOutlinedListExtensions
    {
        public static void SetSortOrder(this IOutlinedList outlinedList)
        {
            if (outlinedList.FullList != null)
            {
                if (outlinedList.Parent != null)
                {
                    var maxChild = outlinedList.Parent.Children.Where(n => n.Number == outlinedList.Number - 1).FirstOrDefault();
                    if (maxChild != null)
                    {
                        outlinedList.SortOrder = maxChild.MaxSortOrder() + 1;
                    }
                    else
                    {
                        outlinedList.SortOrder = outlinedList.Parent.SortOrder + outlinedList.Number;
                    }
                }
                else
                {
                    var previousItem = outlinedList.FullList.Where(n => n.Number == outlinedList.Number - 1).FirstOrDefault();
                    if (previousItem != null)
                    {
                       outlinedList.SortOrder = previousItem.MaxSortOrder() + 1;
                    }
                    else
                    {
                        outlinedList.SortOrder = outlinedList.Number;
                    }
                }
                var fullList = outlinedList.FullList.Where(n => n != outlinedList && n.SortOrder >= outlinedList.SortOrder).OrderBy(n => n.SortOrder).ToList();
                int i = outlinedList.SortOrder + 1;
                foreach (var item in fullList)
                {
                    item.SortOrder = i;
                    i++;
                }
                fullList = outlinedList.FullList.Where(n => n != outlinedList && n.SortOrder < outlinedList.SortOrder).OrderByDescending(n => n.SortOrder).ToList();
                i = outlinedList.SortOrder - 1;
                foreach (var item in fullList)
                {
                    item.SortOrder = i;
                    i--;
                }
                fullList = outlinedList.FullList.OrderBy(n => n.SortOrder).ToList();
                i = 1;
                foreach (var item in fullList)
                {
                    item.SortOrder = i;
                    i++;
                }
            }
        }

        private static int MaxSortOrder(this IOutlinedList outlinedList)
        {
            int result = 0;
            if(outlinedList.Children.Any())
            {
                int childResult = 0;
                foreach(var item in outlinedList.Children)
                {
                    int temp = item.MaxSortOrder();
                    if (temp > childResult)
                    {
                        childResult = temp;
                    }
                }
                result = childResult;
            }
            else
            {
                result = outlinedList.SortOrder;
            }
            return result;
        }

        private static void SetSortOrderRecursively(this IOutlinedList outlinedList)
        {
            if (outlinedList.Children.Any())
            {
                var list = outlinedList.Children.OrderBy(n => n.Number).ToList();
                int i = 1;
                for (i = 1; i < list.Count(); i++)
                {
                    var child = list[i];
                    child.SortOrder = child.Parent.SortOrder + i;
                    child.SetSortOrderRecursively();
                }
            }
        }

        public static void SetOutlineNumber(this IOutlinedList outlinedList)
        {
            outlinedList.OutlineNumber = outlinedList.CalcOutlineNumber();
        }

        public static string CalcOutlineNumber(this IOutlinedList outlinedList)
        {
            string parentOutlineNumber = (outlinedList.Parent == null) ? string.Empty : outlinedList.Parent.OutlineNumber + ".";
            return string.Format("{0}{1}", parentOutlineNumber, outlinedList.Number);
        }
    }
}
