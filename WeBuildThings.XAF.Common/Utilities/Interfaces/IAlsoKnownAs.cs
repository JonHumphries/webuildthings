﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.Collections.Generic;
using WeBuildThings.Common.Interfaces;

namespace WeBuildThings.Utilities
{
    [NonPersistent]
    public abstract class AKA : BaseObject
    {

        #region Fields
        private string _Name;
        #endregion Fields

        #region Constructors
        public AKA(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }
        #endregion Properties

        #region Public Methods
        /// <summary>
        /// Searches the persisted list of alternate spellings for a match.
        /// </summary>
        public static T FuzzySearch<T>(Session session, string matchTo, bool learnMatches) where T: BaseObject, IAlsoKnownAs
        {
            T result = null;
            var list = new XPCollection<T>(session);
            result = list.FuzzySearch<T>(matchTo, learnMatches);
            return result;
        }
        #endregion Public Methods
    }


}
