﻿using System;

namespace WeBuildThings.Utilities.Interfaces
{

    public interface IModelBrowser
    {
        string TargetPropertyName { get; }
        string PropertyName { get; set; }
        Type Type { get; }
    }
}
