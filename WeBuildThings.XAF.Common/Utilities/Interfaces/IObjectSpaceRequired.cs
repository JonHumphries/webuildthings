﻿using DevExpress.ExpressApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeBuildThings.XAF.Common.BusinessObjects.Common
{
    public interface IObjectSpaceRequired
    {
        IObjectSpace ObjectSpace { get; set; }
    }
}
