﻿using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WeBuildThings.Utilities.Interfaces;
using WeBuildThings.XAF.TestSupport;


namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class IBinaryTreeTests
    {
        public Session Session
        {
            get { return XAFInitialization.Session; }
        }

        public IBinaryTreeTests()
        {
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();

        }

        [TestMethod]
        public void IBinaryTree_PromoteTest()
        {
            var parent = new TestPromoteDemote(Session) { Name = "Parent" };
            var child = new TestPromoteDemote(Session) { Name = "Child" };
            parent.Children.Add(child);
            Assert.AreEqual(child.Parent, parent);
            Assert.AreEqual(parent.Children[0], child);
            child.Promote();
            Assert.IsNull(child.Parent);
            Assert.IsFalse(parent.Children.Any());
        }

        [TestMethod]
        public void IBinaryTree_DemoteTest()
        {
            var TestSet = new TestBinaryTreeSet(Session);
            var parent = new TestPromoteDemote(Session) { Name = "Parent" };
            var child = new TestPromoteDemote(Session) { Name = "Child" };
            TestSet.TestBinaryTrees.Add(parent);
            TestSet.TestBinaryTrees.Add(child);
            Assert.IsNull(child.Parent);
            Assert.IsFalse(parent.Children.Any());
            child.Demote();
            Assert.AreEqual(child.Parent, parent);
            Assert.AreEqual(parent.Children[0], child);
        }


        [TestMethod]
        public void IBinaryTree_PromoteWithMultiParentTest()
        {
            var parent = new TestPromoteDemote(Session) { Name = "Parent" };
            var parent2 = new TestPromoteDemote(Session) { Name = "Parent2" };
            var parent3 = new TestPromoteDemote(Session) { Name = "Parent3" };
            var child = new TestPromoteDemote(Session) { Name = "Child" };
            parent.Children.Add(child);
            Assert.AreEqual(child.Parent, parent);
            Assert.AreEqual(parent.Children[0], child);
            child.Promote();
            Assert.IsNull(child.Parent);
            Assert.IsFalse(parent.Children.Any());
        }

        [TestMethod]
        public void IBinaryTree_DemoteWithMultiParentTest()
        {
            var TestSet = new TestBinaryTreeSet(Session);
            var parent = new TestPromoteDemote(Session) { Name = "Parent" };
            var parent2 = new TestPromoteDemote(Session) { Name = "Parent2" };
            var parent3 = new TestPromoteDemote(Session) { Name = "Parent3" };
            var child = new TestPromoteDemote(Session) { Name = "Child" };
            TestSet.TestBinaryTrees.Add(parent);
            TestSet.TestBinaryTrees.Add(parent2);
            TestSet.TestBinaryTrees.Add(parent3);
            TestSet.TestBinaryTrees.Add(child);
            Assert.IsNull(child.Parent);
            Assert.IsFalse(parent.Children.Any());
            Assert.AreEqual(3, parent3.Number);
            Assert.AreEqual(4, child.Number);
            child.Demote();
            Assert.AreEqual(child.Parent, parent3);
            Assert.AreEqual(parent3.Children[0], child);
        }
    }

}
