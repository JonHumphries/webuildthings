﻿using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class INumberedListTests
    {

        public INumberedListTests()
        {
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }


        public Session Session
        {
            get { return XAFInitialization.Session; }
        }

        [TestMethod]
        public void AddToList()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual(1, item1.Number);
            testSet.TestBinaryTrees.Add(item2);
            Assert.AreEqual(2, item2.Number);
        }

        [TestMethod]
        public void MoveUp()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            var item3 = new TestPromoteDemote(Session) { Name = "Item3" };
            testSet.TestBinaryTrees.Add(item1);
            testSet.TestBinaryTrees.Add(item2);
            testSet.TestBinaryTrees.Add(item3);
            Assert.AreEqual(1, item1.Number);
            Assert.AreEqual(2, item2.Number);
            Assert.AreEqual(3, item3.Number);
            item2.MoveUp();
            Assert.AreEqual(1, item2.Number);
            Assert.AreEqual(2, item1.Number);
            Assert.AreEqual(3, item3.Number);
        }

        [TestMethod]
        public void MoveDown()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            var item3 = new TestPromoteDemote(Session) { Name = "Item3" };
            testSet.TestBinaryTrees.Add(item1);
            testSet.TestBinaryTrees.Add(item2);
            testSet.TestBinaryTrees.Add(item3);
            Assert.AreEqual(1, item1.Number);
            Assert.AreEqual(2, item2.Number);
            Assert.AreEqual(3, item3.Number);
            item1.MoveDown();
            Assert.AreEqual(1, item2.Number);
            Assert.AreEqual(2, item1.Number);
            Assert.AreEqual(3, item3.Number);
        }

        [TestMethod]
        public void ReOrder()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            var item3 = new TestPromoteDemote(Session) { Name = "Item3" };
            testSet.TestBinaryTrees.Add(item1);
            item1.Number = 1;
            testSet.TestBinaryTrees.Add(item2);
            item2.Number = 1;
            testSet.TestBinaryTrees.Add(item3);
            item3.Number = 1;
            Assert.AreEqual(1, item1.Number);
            Assert.AreEqual(1, item2.Number);
            Assert.AreEqual(1, item3.Number);
            item1.ReOrder();
            int sum = testSet.TestBinaryTrees.Sum(n => n.Number);
            Assert.AreEqual(6, sum);
        }
    }
}
