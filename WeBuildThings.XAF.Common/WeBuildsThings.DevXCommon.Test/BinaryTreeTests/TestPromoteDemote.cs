﻿using DevExpress.Persistent.Base.General;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using WeBuildThings.BusinessObjects.Common;
using WeBuildThings.Utilities.Interfaces;


namespace WeBuildThings.XAF.Test
{

    public sealed class TestBinaryTreeSet : BaseObject
    {
        public TestBinaryTreeSet(Session session) : base(session) { }

        [Association("TestBinaryTreeSet-TestBinaryTree", typeof(TestPromoteDemote)), Aggregated]
        public XPCollection<TestPromoteDemote> TestBinaryTrees
        {
            get { return GetCollection<TestPromoteDemote>("TestBinaryTrees"); }
        }
    }

    public sealed class TestPromoteDemote : BaseObject, IBinaryTree, IOutlinedList
    {
        #region Fields
        private TestPromoteDemote _Parent;
        private string _Name;
        private TestBinaryTreeSet _TestSet;
        private int _Number;
        private int _SortOrder;
        private string _OutlineNumber;
        #endregion Fields

        public TestPromoteDemote(Session session) : base(session) { }

        public string Name
        {
            get { return _Name; }
            set { if (_Name != value) _Name = value; }
        }

        public int Number
        {
            get { return _Number; }
            set { if (SetPropertyValue("Number", ref _Number, value) && !IsLoading) { OnNumberChanged(); } }
        }

        public int SortOrder
        {
            get { return _SortOrder; }
            set { SetPropertyValue("SortOrder", ref _SortOrder, value); }
        }

        public string OutlineNumber
        {
            get { return _OutlineNumber; }
            set { SetPropertyValue("OutlineNumber", ref _OutlineNumber, value); }
        }

        [Association("TestBinaryTreeSet-TestBinaryTree", typeof(TestPromoteDemote))]
        public TestBinaryTreeSet TestSet
        {
            get { return _TestSet; }
            set { if (SetPropertyValue("TestSet", ref _TestSet, value) && !IsLoading) { OnTestSetChanged(); } }
        }

        [Association("TBTParent-TBTChildren", typeof(TestPromoteDemote))]
        public TestPromoteDemote Parent
        {
            get { return _Parent; }
            set { if (SetPropertyValue("Parent", ref _Parent, value) && !IsLoading) { OnParentChanged(); } }
        }

        [Association("TBTParent-TBTChildren", typeof(TestPromoteDemote))]
        public XPCollection<TestPromoteDemote> Children
        {
            get { return GetCollection<TestPromoteDemote>("Children"); }
        }

        IBindingList ITreeNode.Children { get { return (IBindingList)Children; } }


        ITreeNode ITreeNode.Parent
        {
            get { return (ITreeNode)Parent; }
        }

        ITreeNode IBinaryTree.Parent
        {
            get { return (ITreeNode)Parent; }
            set { Parent = (TestPromoteDemote)value; }
        }

        IEnumerable<IBinaryTree> IBinaryTree.Children
        {
            get { return Children; }
        }

        IEnumerable<IBinaryTree> IBinaryTree.FullTree
        {
            get { return (TestSet == null) ? null : TestSet.TestBinaryTrees; }
        }

        #region INumberedList Implementation
        IEnumerable<INumberedList> INumberedList.List
        {
            get
            {
                IEnumerable<INumberedList> result = null;
                if (Parent != null)
                {
                    result = Parent.Children;
                }
                else if (TestSet != null)
                {
                    result = TestSet.TestBinaryTrees;
                }
                return result;
            }
        }

        void OnTestSetChanged()
        {
            this.SetNumber();
            this.SetOutlineNumber();
            this.SetSortOrder();
        }
        #endregion INumberedList Implementation

        #region IOutlinedList Implementation

        IOutlinedList IOutlinedList.Parent
        {
            get { return Parent; }
        }

        IEnumerable<IOutlinedList> IOutlinedList.Children
        {
            get { return Children; }
        }

        public IEnumerable<IOutlinedList> FullList
        {
            get { return (TestSet == null) ? null : TestSet.TestBinaryTrees; }
        }

        void OnNumberChanged()
        {
            this.SetOutlineNumber();
            this.SetSortOrder();
        }

        void OnParentChanged()
        {
            if (Parent != null)
            {
                TestSet = Parent.TestSet;
            }
            this.SetNumber();
            this.SetSortOrder();
        }
        #endregion IOutlinedList Implementation

    }
}
