﻿using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.Utilities.Interfaces;
using WeBuildThings.XAF.TestSupport;
using System.Linq;

namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class IOutlinedListTests
    {
        public Session Session
        {
            get { return WeBuildThings.XAF.TestSupport.XAFInitialization.Session; }
        }

        public IOutlinedListTests()
        {

            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }

        [TestMethod]
        public void OutlineNumber_AddToList()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual("1", item1.OutlineNumber);
            testSet.TestBinaryTrees.Add(item2);
            Assert.AreEqual("2", item2.OutlineNumber);
        }
        
        [TestMethod]
        public void OutlineNumber_AddChildToList()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual("1", item1.OutlineNumber);
            testSet.TestBinaryTrees.Add(item2);
            item2.Demote();
            Assert.AreEqual("1.1", item2.OutlineNumber);
        }

        [TestMethod]
        public void OutlineNumber_PromoteChildFromList()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual("1", item1.OutlineNumber);
            testSet.TestBinaryTrees.Add(item2);
            item2.Demote();
            Assert.AreEqual("1.1", item2.OutlineNumber);
            item2.Promote();
            Assert.AreEqual("2", item2.OutlineNumber);
        }

        [TestMethod]
        public void SortOrder_AddItem()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual(1, item1.SortOrder);
            testSet.TestBinaryTrees.Add(item2);
            Assert.AreEqual(2, item2.SortOrder);
        }

        [TestMethod]
        public void SortOrder_AddChildrenItem()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            var item3 = new TestPromoteDemote(Session) { Name = "Item3" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual(1, item1.SortOrder);
            testSet.TestBinaryTrees.Add(item2);
            Assert.AreEqual(2, item2.SortOrder);
            item1.Children.Add(item3);
            Assert.AreEqual(2, item3.SortOrder);
            Assert.AreEqual(3, item2.SortOrder);
        }

        [TestMethod]
        public void SortOrder_Promote()
        {
            var testSet = new TestBinaryTreeSet(Session);
            var item1 = new TestPromoteDemote(Session) { Name = "Item1" };
            var item2 = new TestPromoteDemote(Session) { Name = "Item2" };
            var item3 = new TestPromoteDemote(Session) { Name = "Item3" };
            testSet.TestBinaryTrees.Add(item1);
            Assert.AreEqual(1, item1.SortOrder);
            testSet.TestBinaryTrees.Add(item2);
            Assert.AreEqual(2, item2.SortOrder);
            item1.Children.Add(item3);
            Assert.AreEqual(2, item3.SortOrder);
            Assert.AreEqual(3, item2.SortOrder);
            item3.Promote();
            Assert.AreEqual(3, item3.SortOrder);
            Assert.AreEqual(2, item2.SortOrder);
        }
    }
}
