﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using System;

namespace WeBuildThings.XAF.Test
{
    /// <summary>
    /// https://www.devexpress.com/Support/Center/Question/Details/Q490798
    /// </summary>
    public class TestSecurityStrategyBase : ISecurityStrategyBase
    {
        public object User { get; set; }

        public void Logon(IObjectSpace objectSpace)
        {
            throw new NotImplementedException();
        }
        public void Logoff()
        {
            throw new NotImplementedException();
        }
        public void ClearSecuredLogonParameters()
        {
            throw new NotImplementedException();
        }
        public void ReloadPermissions()
        {
            throw new NotImplementedException();
        }
        public Type GetModuleType()
        {
            throw new NotImplementedException();
        }
        public System.Collections.Generic.IList<Type> GetBusinessClasses()
        {
            throw new NotImplementedException();
        }
        public bool IsAuthenticated
        {
            get { throw new NotImplementedException(); }
        }
        public Type UserType
        {
            get { throw new NotImplementedException(); }
        }
        public string UserName
        {
            get { throw new NotImplementedException(); }
        }
        public object UserId
        {
            get { throw new NotImplementedException(); }
        }
        public object LogonParameters
        {
            get { throw new NotImplementedException(); }
        }
        public bool NeedLogonParameters
        {
            get { throw new NotImplementedException(); }
        }
        public bool IsLogoffEnabled
        {
            get { throw new NotImplementedException(); }
        }


        public IObjectSpace LogonObjectSpace
        {
            get { throw new NotImplementedException(); }
        }
    }
    
}
