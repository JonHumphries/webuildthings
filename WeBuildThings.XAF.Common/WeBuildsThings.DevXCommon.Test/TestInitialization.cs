﻿using System.Reflection;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Testing;

namespace WeBuildThings.XAF.Test
{
    internal class TestInitialization
    {
        internal static Assembly[] Assemblies = new Assembly[] { typeof(StandardTracking).Assembly, typeof(TestType).Assembly, typeof(TestBinaryTreeSet).Assembly, typeof(TestPromoteDemote).Assembly};
    }
}
