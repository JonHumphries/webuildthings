﻿using DevExpress.Data.Filtering;
using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Testing;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class IdentityManagerTests
    {
        #region Fields
        private TestType _TestType1;
        private TestType _TestType2;
        #endregion Fields

        public IdentityManagerTests()
        {
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }



        public Session Session
        {
            get { return XAFInitialization.Session; }
        }

        public TestType TestType1
        {
            get
            {
                if (_TestType1 == null)
                {
                    _TestType1 = new TestType(Session);
                    _TestType1.SetIdentity();
                }
                return _TestType1;
            }
        }

        public TestType TestType2
        {
            get
            {
                if (_TestType2 == null)
                {
                    _TestType2 = new TestType(Session);
                    _TestType2.SetIdentity();
                }
                return _TestType2;
            }
        }

        [TestMethod]
        public void NewIdentityTest()
        {
            var identityManager = Session.FindObject<IdentityManager>(new BinaryOperator("ClassName", TestType1.ClassInfo.TableName));
            if (identityManager != null)
            {
                identityManager.Delete();
            }
            try
            {
                Assert.AreEqual(1, TestType1.Number);
            }
            finally
            {
                var mgrToCleanup = Session.FindObject<IdentityManager>(new BinaryOperator("ClassName", TestType1.ClassInfo.TableName));
                if (mgrToCleanup != null)
                {
                    mgrToCleanup.Delete();
                }
            }
        }


        [TestMethod]
        public void IdentityOrderingTest()
        {
            var identityManager = Session.FindObject<IdentityManager>(new BinaryOperator("ClassName", TestType1.ClassInfo.TableName));
            if (identityManager != null)
            {
                identityManager.Delete();
                _TestType1 = null;
            }
            try
            {
                Assert.AreEqual(1, TestType1.Number);
                Assert.AreEqual(2, TestType2.Number);
            }
            finally
            {
                TestType1.Delete();
                var mgrToCleanup = Session.FindObject<IdentityManager>(new BinaryOperator("ClassName", TestType1.ClassInfo.TableName));
                if (mgrToCleanup != null)
                {
                    mgrToCleanup.Delete();
                }
            }
        }

    }
}
