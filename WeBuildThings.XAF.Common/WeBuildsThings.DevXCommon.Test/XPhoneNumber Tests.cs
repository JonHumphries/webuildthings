﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WeBuildThings.XAF.Common;
using DevExpress.Xpo;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class XPhoneNumberTests
    {

        public Session Session = XAFInitialization.Session;

        [TestMethod]
        public void CreatePhoneNumber()
        {
            string expectedResult = "8883092221";
            var phoneNumber = XPhoneNumber.CreateXPhoneNumber(Session, expectedResult);
            Assert.AreEqual(expectedResult, phoneNumber.Number);
            Assert.AreEqual(expectedResult, phoneNumber.FullNumber.Replace("(", "").Replace(")", "").Replace("-", ""));
        }

        [TestMethod]
        public void InvalidPhoneNumber()
        {
            string expectedResult = null;
            var actualResult = XPhoneNumber.CreateXPhoneNumber(Session, "TE");
            Assert.AreEqual(expectedResult, actualResult.FullNumber);
            Assert.AreEqual("TE", actualResult.Number);
        }
    }
}
