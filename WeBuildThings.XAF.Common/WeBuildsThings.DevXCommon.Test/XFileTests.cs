﻿using DevExpress.Xpo;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.IO;
using System.Linq;
using WeBuildThings.Utilities;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.DatabaseUpdate;
using WeBuildThings.XAF.TestSupport;

namespace WeBuildThings.XAF.Test
{
    [TestClass]
    public class XFileTests
    {
        #region Fields
        private string FilePath;
        private string DestinationPath;
        private string SaveDirectory;
        #endregion Fields

        #region Properties
        public Session Session { get { return XAFInitialization.Session; } }
        #endregion Properties

        public XFileTests()
        {
            string filePath = "TestFile.txt";
            string destinationPath = "TransferredFile.txt";
            string projectRoot = new DirectoryInfo("..\\..\\..\\..\\WeBuildThings.XAF.ElectronicDataInterchange\\WeBuildThings.XAF.ElectronicDataInterchange.Test").FullName;
            SaveDirectory = Path.Combine(projectRoot, "SampleFiles");
            FilePath = Path.Combine(SaveDirectory, filePath);
            DestinationPath = Path.Combine(SaveDirectory, destinationPath);
            XAFInitialization.Assemblies = TestInitialization.Assemblies.ToList();
        }


        /// <summary>
        /// Verifies that forcing a manual transfer to a specific location works as expected.
        /// </summary>
        [TestMethod]
        public void ManuallyTransferToRepository()
        {
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                if (File.Exists(DestinationPath))
                {
                    File.Delete(DestinationPath);
                }
                XFile xFile = new XFile(Session);
                xFile.LocalPath = FilePath;
                xFile.FullPath = DestinationPath;
                xFile.Transfer();
                Assert.IsTrue(File.Exists(DestinationPath));
            }
            finally
            {
                File.Delete(FilePath);
                File.Delete(DestinationPath);
            }
        }

        /// <summary>
        /// Verifies that the FileName is set correctly when LocalPath is set
        /// </summary>
        [TestMethod]
        public void TestFileName()
        {
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                XFile xFile = new XFile(Session);
                xFile.LocalPath = FilePath;
                string expectedFileName = new FileInfo(FilePath).Name;
                Assert.AreEqual(expectedFileName, xFile.FileName);
            }
            finally
            {
                File.Delete(FilePath);
            }
        }

        /// <summary>
        /// Verifies that the Size is set correctly when LocalPath is set
        /// </summary>
        [TestMethod]
        public void TestSize()
        {
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                XFile xFile = new XFile(Session);
                xFile.LocalPath = FilePath;
                int expectedSize = (int)new FileInfo(FilePath).Length;
                Assert.AreEqual(expectedSize, xFile.Size);
            }
            finally
            {
                File.Delete(FilePath);
            }
        }

        /// <summary>
        /// Verifies that the extension is set correctly when LocalPath is set
        /// </summary>
        [TestMethod]
        public void TestExtension()
        {
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                XFile xFile = new XFile(Session);
                xFile.LocalPath = FilePath;
                string expectedExtension = new FileInfo(FilePath).Extension;
                Assert.AreEqual(expectedExtension, xFile.Extension);
            }
            finally
            {
                File.Delete(FilePath);
            }
        }

        /// <summary>
        /// Verifies that the system can select it's own destination as expected
        /// </summary>
        [TestMethod]
        public void AutoTransferToRepository()
        {
            string destinationPath = string.Empty;
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                FileOperations.Delete(DestinationPath);
                XFile xFile = new XFile(Session);
                xFile.SaveDirectory = SaveDirectory;
                xFile.LocalPath = FilePath;
                xFile.Transfer();
                destinationPath = xFile.FullPath;
                Assert.IsTrue(File.Exists(destinationPath));
            }
            finally
            {
                File.Delete(FilePath);
                File.Delete(destinationPath);
            }
        }

        /// <summary>
        /// Verifies that directory numbers increment as expected
        /// </summary>
        [TestMethod]
        public void DocumentCatalogDirectory()
        {
            string expectedCurrentInstance = Path.Combine(SystemSettingsUpdater.DefaultDocumentCatalogDirectory, "0");
            DocumentCatalog documentCatalog = new DocumentCatalog(Session);
            Assert.AreEqual(expectedCurrentInstance, documentCatalog.GetCurrentDirectory());
        }

        /// <summary>
        /// Verifies that Raw data is properly attached to a Document
        /// </summary>
        [TestMethod]
        public void ManualDocumentWithXFile()
        {
            Document document = null;
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                if (File.Exists(DestinationPath))
                {
                    File.Delete(DestinationPath);
                }
                document = new Document(Session);
                if (document.File == null) document.File = new XFile(Session);
                document.File.LocalPath = FilePath;
                document.File.FullPath = DestinationPath;
                document.File.Transfer();
                Assert.IsTrue(File.Exists(DestinationPath));
            }
            finally
            {
                if (document != null && document.File != null)
                {
                    document.File.Delete();
                }
                if (document != null)
                {
                    document.Delete();
                }
                File.Delete(FilePath);
                File.Delete(DestinationPath);
            }
        }

        /// <summary>
        /// Verifies that Raw data is properly attached to a Document
        /// </summary>
        [TestMethod]
        public void AutoDocumentWithXFile()
        {
            Document document = null;
            try
            {
                using (Stream file = File.Create(FilePath))
                {
                    file.Close();
                }
                if (File.Exists(DestinationPath))
                {
                    File.Delete(DestinationPath);
                }
                document = new Document(Session);
                if (document.File == null) document.File = new XFile(Session);
                document.File.LocalPath = FilePath;
                document.File.Transfer();
                Assert.IsTrue(File.Exists(document.File.FullPath));
            }
            finally
            {
                if (document != null && document.File != null)
                {
                    document.File.Delete();
                }
                if (document != null)
                {
                    document.Delete();
                }
                File.Delete(FilePath);
                File.Delete(DestinationPath);
            }
        }
    }
}
