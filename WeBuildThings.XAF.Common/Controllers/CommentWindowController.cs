﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Xpo;
using System;

namespace WeBuildThings.XAF.Common.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppWindowControllertopic.aspx.
    public partial class CommentWindowController : WindowController
    {

        private WindowTemplateController controller;
        private int MaxWindowCaptionSize = 100;
        
        public CommentWindowController()
        {
            TargetWindowType = WindowType.Child;
            Activated += CustomizeWindowController_Activated;
            InitializeComponent();
        }

        protected override void OnActivated()
        {
            base.OnActivated();
            SetDefaultWindowCaption();
        }

        private void SetDefaultWindowCaption()
        {
            if (Frame != null && Frame.View != null)
            {
                var session = ((XPObjectSpace)this.Frame.View.ObjectSpace).Session;
                var systemSettings = SystemSettings.GetInstance<CommentSystemSettings>(session);
                MaxWindowCaptionSize = systemSettings.MaxWindowCaptionSize;
            }
        }

        void CustomizeWindowController_Activated(object sender, EventArgs e)
        {
            controller = Frame.GetController<WindowTemplateController>();
            controller.CustomizeWindowCaption += controller_CustomizeWindowCaption;
        }

        void controller_CustomizeWindowCaption(object sender, CustomizeWindowCaptionEventArgs e)
        {
            //if (Frame != null && Frame.View != null && Frame.View)
            if (e.WindowCaption.Text.Length > MaxWindowCaptionSize)
            {
                e.WindowCaption.Text = e.WindowCaption.Text.Substring(0, MaxWindowCaptionSize);
            }
        }
    }
}
