﻿namespace WeBuildThings.Controllers
{
    partial class INumberedListViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.MoveUp = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.MoveDown = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // MoveUp
            // 
            this.MoveUp.Caption = "Move Up";
            this.MoveUp.ConfirmationMessage = null;
            this.MoveUp.Id = "MoveUp";
            this.MoveUp.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.MoveUp.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.MoveUp.ToolTip = null;
            this.MoveUp.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.MoveUp.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MoveUp_Execute);
            // 
            // MoveDown
            // 
            this.MoveDown.Caption = "Move Down";
            this.MoveDown.ConfirmationMessage = null;
            this.MoveDown.Id = "MoveDown";
            this.MoveDown.SelectionDependencyType = DevExpress.ExpressApp.Actions.SelectionDependencyType.RequireSingleObject;
            this.MoveDown.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.MoveDown.ToolTip = null;
            this.MoveDown.TypeOfView = typeof(DevExpress.ExpressApp.ListView);
            this.MoveDown.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.MoveDown_Execute);
            // 
            // INumberedListViewController
            // 
            this.Actions.Add(this.MoveUp);
            this.Actions.Add(this.MoveDown);
            this.TargetViewNesting = DevExpress.ExpressApp.Nesting.Nested;
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction MoveUp;
        private DevExpress.ExpressApp.Actions.SimpleAction MoveDown;
    }
}
