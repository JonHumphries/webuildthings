﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using WeBuildThings.XAF.Common.Controls;
using WeBuildThings.XAF.Common.BusinessObjects.Common;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;

namespace WeBuildThings.XAF.Common.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CommentViewController : ViewController
    {
        public CommentViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }


        protected Comment Comment {get; set;}

        protected virtual Comment CreateComment(Session session)
        {
            return new Comment(session);
        }
        
        protected virtual void AutoNote_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Session session = ((XPObjectSpace)View.ObjectSpace).Session;
            Comment = CreateComment(session);
            AutoNote autoNote = e.PopupWindow.View.CurrentObject as AutoNote;
            autoNote.CopyProperties(Comment);
        }

        private void AutoNote_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            AutoNote autoNote = objectSpace.CreateObject<AutoNote>();
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, autoNote);
        }
    }
}
