﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.Extensions;

using WeBuildThings.XAF.Common.Controls;
using DevExpress.Data.Filtering;

namespace WeBuildThings.XAF.Common.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IMergableViewController : ViewController
    {
        public IMergableViewController()
        {
            InitializeComponent();
            this.TargetObjectType = typeof(IMergeable);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        private BaseObject Target { get; set; }

        private void Merge_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            if (View.SelectedObjects != null && View.SelectedObjects.Count > 0)
            {
                IObjectSpace objectSpace = Application.CreateObjectSpace();
                Target = (BaseObject)View.SelectedObjects[0];
                string listViewId = Application.FindListViewId(Target.GetType());
                var collectionSourceBase = Application.CreateCollectionSource(objectSpace, Target.GetType(), listViewId);
                e.View = Application.CreateListView(listViewId,
                    collectionSourceBase,
                    true);
            }
            else
            {
                var exception = new ArgumentNullException("Select a record to merge with.");
                exception.Source = this.GetType().Name;
                throw exception;
            }
        }

        private void Merge_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            var sources = e.PopupWindowView.SelectedObjects;
            if (Target != null && sources != null && sources.Count > 0)
            {
                var session = ((BaseObject)sources[0]).Session;
                var target = session.GetObjectByKey(Target.ClassInfo, Target.Oid);
                ((IMergeable)target).Merge(sources);
                foreach (var item in sources)
                {
                    session.Delete(item);
                }
                ((BaseObject)target).Save();
                e.PopupWindowView.ObjectSpace.CommitChanges();
                View.ObjectSpace.Refresh();
            }
            else
            {
                var exception = new ArgumentNullException("Either Source or Targets are missing");
                exception.Source = this.GetType().Name;
                throw exception;
            }
        }

    }
}
