﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using WeBuildThings.Utilities.Interfaces;
using WeBuildThings.BusinessObjects.Common;

namespace WeBuildThings.XAF.BusinessRuleEngine.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IBinaryTreeViewController : ViewController
    {
        public IBinaryTreeViewController()
        {
            InitializeComponent();
            TargetViewType = ViewType.ListView;
            TargetObjectType = typeof(IBinaryTree);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void Promote_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IBinaryTree currentObject in View.SelectedObjects)
            {
                currentObject.Promote();
            }
        }

        private void Demote_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            foreach (IBinaryTree currentObject in View.SelectedObjects)
            {
                currentObject.Demote();
            }
        }

        private void Sort_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            INumberedList currentObject = View.SelectedObjects[0] as INumberedList;
            if (currentObject != null)
            {
                currentObject.ReOrder();
            }
        }
    }
}
