﻿namespace WeBuildThings.XAF.Common.Controllers
{
    partial class IMergableViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Merge = new DevExpress.ExpressApp.Actions.PopupWindowShowAction();
            // 
            // Merge
            // 
            this.Merge.AcceptButtonCaption = "";
            this.Merge.CancelButtonCaption = null;
            this.Merge.Caption = "Merge";
            this.Merge.Category = "RecordEdit";
            this.Merge.ConfirmationMessage = null;
            this.Merge.Id = "Merge";
            this.Merge.ImageName = "Action_CloneMerge_Merge_Object";
            this.Merge.ToolTip = null;
            this.Merge.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.Merge_CustomizePopupWindowParams);
            this.Merge.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.Merge_Execute);
            // 
            // IMergableViewController
            // 
            this.Actions.Add(this.Merge);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        protected DevExpress.ExpressApp.Actions.PopupWindowShowAction Merge;

    }
}
