﻿namespace WeBuildThings.XAF.BusinessRuleEngine.Controllers
{
    partial class IBinaryTreeViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Promote = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Demote = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.Sort = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // Promote
            // 
            this.Promote.Caption = "Promote";
            this.Promote.ConfirmationMessage = null;
            this.Promote.Id = "Promote";
            this.Promote.ToolTip = null;
            this.Promote.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Promote_Execute);
            // 
            // Demote
            // 
            this.Demote.Caption = "Demote";
            this.Demote.ConfirmationMessage = null;
            this.Demote.Id = "Demote";
            this.Demote.ToolTip = null;
            this.Demote.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Demote_Execute);
            // 
            // Sort
            // 
            this.Sort.Caption = "Sort";
            this.Sort.ConfirmationMessage = null;
            this.Sort.Id = "Sort";
            this.Sort.ToolTip = null;
            this.Sort.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.Sort_Execute);
            // 
            // IBinaryTreeViewController
            // 
            this.Actions.Add(this.Promote);
            this.Actions.Add(this.Demote);
            this.Actions.Add(this.Sort);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction Promote;
        private DevExpress.ExpressApp.Actions.SimpleAction Demote;
        private DevExpress.ExpressApp.Actions.SimpleAction Sort;
    }
}
