﻿using DevExpress.Data;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.XtraGrid;
//using DevExpress.XtraGrid.Views.Grid;
using WeBuildThings.BusinessObjects.Common;

namespace WeBuildThings.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class INumberedListViewController : ViewController
    {
        public INumberedListViewController()
        {
            InitializeComponent();
            RegisterActions(components);
            TargetViewType = ViewType.ListView;
            TargetObjectType = typeof(INumberedList);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }

        protected override void OnViewControlsCreated()
        {
            //base.OnViewControlsCreated();
            //                GridListEditor listEditor = ((ListView)View).Editor as GridListEditor;
            //    if (listEditor != null)
            //    {
            //        GridView gridView = listEditor.GridView;
            //        if (gridView != null && gridView.Columns["Number"] != null)
            //        {
            //            gridView.ClearSorting();
            //            gridView.Columns["Number"].SortOrder = ColumnSortOrder.Ascending;
            //        }
            //    }
        }

        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void MoveDown_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            INumberedList numberedList = (INumberedList)View.CurrentObject;
            numberedList.MoveDown();
        }

        private void MoveUp_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            INumberedList numberedList = (INumberedList)View.CurrentObject;
            numberedList.MoveUp();
        }

        private void ReOrder_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            INumberedList numberedList = (INumberedList)View.CurrentObject;
            numberedList.ReOrder();
        }
    }
}
