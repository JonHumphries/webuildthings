﻿namespace WeBuildThings.XAF.Common.Controllers
{
    partial class IDataTransactionViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ExecuteTransactions = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // ExecuteTransactions
            // 
            this.ExecuteTransactions.Caption = "Execute";
            this.ExecuteTransactions.ConfirmationMessage = null;
            this.ExecuteTransactions.Id = "ExecuteTransactions";
            this.ExecuteTransactions.ToolTip = null;
            this.ExecuteTransactions.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ExecuteClaimTransaction_Execute);
            // 
            // IDataTransactionViewController
            // 
            this.Actions.Add(this.ExecuteTransactions);
            this.TypeOfView = typeof(DevExpress.ExpressApp.View);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction ExecuteTransactions;


    }
}
