﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.Common.Interfaces;
using WeBuildThings.Common.Extensions;
using WeBuildThings.XAF.Common.Controls;
using DevExpress.Data.Filtering;

namespace WeBuildThings.XAF.Common.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class IDataTransactionViewController : ViewController
    {
        public IDataTransactionViewController()
        {
            InitializeComponent();
            this.TargetObjectType = typeof(IDataTransaction);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }

        private void ExecuteClaimTransaction_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var list = new List<IDataTransaction>();
            foreach (IDataTransaction item in View.SelectedObjects)
            {
                list.Add(item);
            }
            foreach (var item in list.OrderBy(n => n.DateCreated))
            {
                item.Execute();
            }
        }
        

    }
}
