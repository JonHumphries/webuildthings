﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using System.Linq;
using WeBuildThings.DataMergeProcess;
using WeBuildThings.DataMergeProcess.Extensions;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.DataMergeProcess.Controls
{
    [NonPersistent]
    public class AutoNote : BaseObject
    {
        #region Fields
        private CommentTemplate _Comment;
        private string _Subject;
        private string _Text;
        private LookupItem _Type;
        #endregion Fields

        #region Constructors
        public AutoNote(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        [ImmediatePostData]
        public CommentTemplate Comment
        {
            get { return _Comment; }
            set { if (SetPropertyValue("Comment", ref _Comment, value) && !IsLoading) { OnCommentChanged(); } }
        }

        public string Subject
        {
            get { return _Subject; }
            set { SetPropertyValue("Subject", ref _Subject, value);}
        }

        [Size(SizeAttribute.Unlimited)]
        public string Text
        {
            get { return _Text; }
            set { SetPropertyValue("Text", ref _Text, value); }
        }

        [DataSourceCriteria("[LookupList.Name] = 'CommentType' and [Active]")]
        public LookupItem Type
        {
            get { return _Type; }
            set { SetPropertyValue("Type", ref _Type, value); }
        }

        #endregion Properties

        #region Event Methods
        private void OnCommentChanged()
        {
            SetSubject();
            SetText();
            SetType();
        }
        #endregion Event Methods

        #region Set Methods
        private void SetSubject()
        {
            Subject = Comment.Subject;
        }

        private void SetText()
        {
            Text = Comment.Text;
        }

        public void PopularData(object target)
        {
            var title = Subject;
            var body = Text;

            foreach (var item in Comment.DataMerge?.DataMergeItems ?? Enumerable.Empty<DataMergeItem>())
            {
                if (title.Contains(item.Key) || body.Contains(item.Key))
                {
                    item.SetStringValue(target);
                    title = body.Replace(item.Key, item.Value);
                    body = body.Replace(item.Key, item.Value);
                }
            }

            Subject = title;
            Text = body;
        }

        private void SetType()
        {
            Type = Comment.Type;
        }
        #endregion Set Methods

        #region Public Methods
        public virtual void CopyProperties(Comment comment)
        {
            comment.Subject = Subject;
            comment.Text = Text;
            if (Type != null)
            {
                var type = comment.Session.FindObject<LookupItem>(new BinaryOperator("Name", Type.Name));
                comment.Type = type;
            }
            //foreach (var document in Comment.Documents)
            //{
            //    Comment.Documents.Add(document);
            //}
        }
        #endregion Public Methods
    }
}
