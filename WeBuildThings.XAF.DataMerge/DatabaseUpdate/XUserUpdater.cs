﻿using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using DevExpress.Persistent.BaseImpl;
using System;
using System.Collections.Generic;
using System.Linq;
using WeBuildThings.DataMergeProcess;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.Common.Reflection;
using WeBuildThings.XAF.Common.Utilities;

namespace WeBuildThings.XAF.DataMergeProcess.DatabaseUpdate
{
    public static class XUserUpdater
    {
        #region Constants
        public const string DEFAULT_ROLE_NAME = "Default Read Navigate";

        public const string DefaultRoleDescription = "This role grants basic read/write/delete/navigate access to the system.  Use this for basic level users.";
        #endregion Constants

        #region Type Definitions
        private static Type[] DefaultFullPermissions = new Type[] { typeof(StandardTracking), typeof(FilterCriteria) };
        private static Type[] DefaultFullNoDeletePermissions = new Type[] { typeof(Comment), typeof(Document), typeof(XFile) };
        private static Type[] DefaultReadNavigatePermissions = new Type[] { typeof(XPhoneNumber), typeof(PhoneNumber), typeof(XAddress), typeof(Address), typeof(Party), typeof(XParty), typeof(XPerson), typeof(XOrganization), typeof(CommentTemplate), typeof(LookupItem), typeof(LookupList), typeof(DocumentCatalog), typeof(PersistedType), typeof(XRole) };
        private static Type[] DefaultReadNoNavigatePermissions = new Type[] { typeof(SystemSettings), typeof(MethodItem), typeof(Role), typeof(User), typeof(XUser) };

        #endregion Type Definitions

        public static void Update(IObjectSpace objectSpace)
        {
            RemoveDuplicateTypePermissions(objectSpace);
            XUser sampleUser = objectSpace.FindObject<XUser>(new BinaryOperator("UserName", "User"));
            if (sampleUser == null)
            {
                sampleUser = objectSpace.CreateObject<XUser>();
                sampleUser.UserName = "User";
                sampleUser.SetPassword("");
            }
            XRole defaultRole = CreateDefaultRole(objectSpace);
            sampleUser.Roles.Add(defaultRole);

            XUser userAdmin = objectSpace.FindObject<XUser>(new BinaryOperator("UserName", "Admin"));
            if (userAdmin == null)
            {
                userAdmin = objectSpace.CreateObject<XUser>();
                userAdmin.UserName = "Admin";
                // Set a password if the standard authentication Type is used
                userAdmin.SetPassword("");
            }
            // If a role with the Administrators fullName doesn't exist in the database, create this role
            string[] roles = new string[] { "Administrator", "Developer"};
            var newRoles = CreateRole(objectSpace, roles);
            foreach (var role in newRoles)
            {
                userAdmin.Roles.Add(role);
            }
            objectSpace.CommitChanges();
        }

        public static void RemoveDuplicateTypePermissions(IObjectSpace objectSpace)
        {
            var roles = objectSpace.GetObjects<XRole>();
            var existingTypes = new List<SecuritySystemTypePermissionObject>();
            var typesToDelete = new List<SecuritySystemTypePermissionObject>();
            foreach (var role in roles)
            {
                foreach (var typePermission in role.TypePermissions)
                {
                    if (existingTypes.Where(n => compareTypes(n, typePermission)).Any())
                    {
                        typesToDelete.Add(typePermission);
                    }
                    else //New type permission
                    {
                        existingTypes.Add(typePermission);
                    }
                }
            }
            objectSpace.Delete(typesToDelete);

        }

        private static bool compareTypes(SecuritySystemTypePermissionObject lhs, SecuritySystemTypePermissionObject rhs)
        {
            return (lhs.TargetType == rhs.TargetType
                && lhs.AllowCreate == rhs.AllowCreate
                && lhs.AllowDelete == rhs.AllowDelete
                && lhs.AllowNavigate == rhs.AllowNavigate
                && lhs.AllowRead == rhs.AllowRead
                && lhs.AllowWrite == rhs.AllowWrite);
        }


        /// <summary>
        /// Creates and returns a new user with a given role
        /// </summary>
        public static XUser CreateUser(IObjectSpace objectSpace, string userName, XRole role)
        {
            XUser result = objectSpace.FindObject<XUser>(new BinaryOperator("UserName", userName));
            if (result == null)
            {
                result = objectSpace.CreateObject<XUser>();
                result.UserName = userName;
                result.SetPassword("");
                result.Roles.Add(role);
            }
            return result;
        }

        /// <summary>
        /// Creates a set of roles from a collection.
        /// </summary>
        public static XRole[] CreateRole(IObjectSpace objectSpace, string[] roles)
        {
            XRole[] result = new XRole[roles.Length];
            for (int i = 0; i < roles.Length; i++)
            {
                result[i] = CreateRole(objectSpace, roles[i]);
            }
            return result;
        }

        /// <summary>
        /// Creates a role with no permissions.
        /// </summary>
        public static XRole CreateRole(IObjectSpace objectSpace, string role)
        {
            XRole result = objectSpace.FindObject<XRole>(new BinaryOperator("Name", role));
            if (result == null)
            {
                result = objectSpace.CreateObject<XRole>();
                result.Name = role;
                if (role == "Administrator") result.IsAdministrative = true;
            }
            return result;
        }

        /// <summary>
        /// Adds full access for a collection of types to a given role.
        /// </summary>
        public static void AddFullAccessToTypes(IObjectSpace objectSpace, XRole role, params Type[] types)
        {
            foreach (Type type in types)
            {
                if (!role.TypePermissions.Where(n => n.TargetType == type).Any())
                {
                    SecuritySystemTypePermissionObject typePermission = objectSpace.CreateObject<SecuritySystemTypePermissionObject>();
                    typePermission.TargetType = type;
                    typePermission.AllowCreate = true;
                    typePermission.AllowDelete = true;
                    typePermission.AllowNavigate = true;
                    typePermission.AllowRead = true;
                    typePermission.AllowWrite = true;
                    role.TypePermissions.Add(typePermission);
                }
            }
        }

        /// <summary>
        /// Adds full access except for deleting for a collection of types to a given role.
        /// </summary>
        public static void AddFullNoDeleteAccessToTypes(IObjectSpace objectSpace, XRole role, params Type[] types)
        {
            foreach (Type type in types)
            {
                if (!role.TypePermissions.Where(n => n.TargetType == type).Any())
                {
                    SecuritySystemTypePermissionObject typePermission = objectSpace.CreateObject<SecuritySystemTypePermissionObject>();
                    typePermission.TargetType = type;
                    typePermission.AllowCreate = true;
                    typePermission.AllowDelete = false;
                    typePermission.AllowNavigate = true;
                    typePermission.AllowRead = true;
                    typePermission.AllowWrite = true;
                    role.TypePermissions.Add(typePermission);
                }
            }
        }

        /// <summary>
        /// Adds read and navigate access for a collection of types to a given role.
        /// </summary>
        public static void AddReadNavigatePermissionsToTypes(IObjectSpace objectSpace, XRole role, params Type[] types)
        {
            foreach (Type type in types)
            {
                if (!role.TypePermissions.Where(n => n.TargetType == type).Any())
                {
                    SecuritySystemTypePermissionObject typePermission = objectSpace.CreateObject<SecuritySystemTypePermissionObject>();
                    typePermission.TargetType = type;
                    typePermission.AllowCreate = false;
                    typePermission.AllowDelete = false;
                    typePermission.AllowNavigate = true;
                    typePermission.AllowRead = true;
                    typePermission.AllowWrite = false;
                    role.TypePermissions.Add(typePermission);
                }
            }
        }

        /// <summary>
        /// Adds read access for a collection of types to a given role.
        /// </summary>
        public static void AddReadNoNavigateAccessToTypes(IObjectSpace objectSpace, XRole role, params Type[] types)
        {
            foreach (Type type in types)
            {
                if (!role.TypePermissions.Where(n => n.TargetType == type).Any())
                {
                    SecuritySystemTypePermissionObject typePermission = objectSpace.CreateObject<SecuritySystemTypePermissionObject>();
                    typePermission.TargetType = type;
                    typePermission.AllowCreate = false;
                    typePermission.AllowDelete = false;
                    typePermission.AllowNavigate = false;
                    typePermission.AllowRead = true;
                    typePermission.AllowWrite = false;
                    role.TypePermissions.Add(typePermission);
                }
            }
        }

        /// <summary>
        /// Defines the default type parameters for the Default role.
        /// </summary>
        private static XRole CreateDefaultRole(IObjectSpace objectSpace)
        {
            XRole defaultRole = CreateRole(objectSpace, DEFAULT_ROLE_NAME);
            if (defaultRole != null)
            {
                defaultRole.Description = DefaultRoleDescription;
                defaultRole.AddObjectAccessPermission<XUser>("[Oid] = CurrentUserId()", SecurityOperations.ReadOnlyAccess);
                defaultRole.AddMemberAccessPermission<XUser>("ChangePasswordOnFirstLogon", SecurityOperations.Write);
                defaultRole.AddMemberAccessPermission<XUser>("StoredPassword", SecurityOperations.Write);
                defaultRole.SetTypePermissionsRecursively<XRole>(SecurityOperations.Read, SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifference>(SecurityOperations.ReadWriteAccess, SecuritySystemModifier.Allow);
                defaultRole.SetTypePermissionsRecursively<ModelDifferenceAspect>(SecurityOperations.ReadWriteAccess, SecuritySystemModifier.Allow);
                AddFullAccessToTypes(objectSpace, defaultRole, DefaultFullPermissions);
                AddFullNoDeleteAccessToTypes(objectSpace, defaultRole, DefaultFullNoDeletePermissions);
                AddReadNavigatePermissionsToTypes(objectSpace, defaultRole, DefaultReadNavigatePermissions);
                AddReadNoNavigateAccessToTypes(objectSpace, defaultRole, DefaultReadNoNavigatePermissions);
            }
            return defaultRole;
        }


    }
}
