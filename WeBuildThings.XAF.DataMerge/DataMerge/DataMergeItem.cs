﻿using DevExpress.Xpo;
using System.ComponentModel;
using WeBuildThings.Common;
using WeBuildThings.Common.Extensions;
using WeBuildThings.DataMergeProcess.Extensions;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.DataMergeProcess
{
    public class DataMergeItem : StandardTracking, IDataMergeItem
    {
        #region Fields
        private DataMerge _DataMerge;
        private string _Key;
        private string _Value;
        private string _PropertyName;
        private NullValueProcess _NullValueProcess;
        private DateFormats _DateFormat;
        private int _StringLength;
        #endregion Fields

        #region Constructors
        public DataMergeItem(Session session) : base(session) { }

        public DataMergeItem() : base(Session.DefaultSession) { }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            StringLength = -1;
        }
        #endregion Constructors

        #region Properties
        public string Key
        {
            get { return _Key; }
            set { SetPropertyValue("Key", ref _Key, value); }
        }

        [Browsable(false)]
        public string Value
        {
            get { return _Value; }
            set { SetPropertyValue("Value", ref _Value, value); }
        }

        public string PropertyName
        {
            get { return _PropertyName; }
            set { SetPropertyValue("PropertyName", ref _PropertyName, value); }
        }

        public NullValueProcess NullValueProcess
        {
            get { return _NullValueProcess; }
            set { SetPropertyValue("NullValueProcess", ref _NullValueProcess, value); }
        }

        public DateFormats DateFormat
        {
            get { return _DateFormat; }
            set { SetPropertyValue("DateFormat", ref _DateFormat, value); }
        }

        public int StringLength
        {
            get { return _StringLength; }
            set { SetPropertyValue("StringLength", ref _StringLength, value); }
        }

        [Association("DataMerge-DataMergeItem",typeof(DataMerge))]
        public DataMerge DataMerge
        {
            get { return _DataMerge; }
            set { SetPropertyValue("DataMerge", ref _DataMerge, value); }
        }
        #endregion Properties

        
    }
}
