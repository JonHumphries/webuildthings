﻿using DevExpress.Xpo;
using WeBuildThings.XAF.Common;
using WeBuildThings.DataMergeProcess.Extensions;
using DevExpress.Persistent.Base;
using WeBuildThings.XAF.Common.Reflection;

namespace WeBuildThings.XAF.DataMergeProcess
{
    [DefaultClassOptions]
    public class DataMerge : StandardTracking
    {
        #region Fields
        private string _Name;
        private PersistedType _TargetObject;
        #endregion Fields

        #region Constructors
        public DataMerge(Session session) : base(session) { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        [Association("DataMerge-DataMergeItem",typeof(DataMergeItem)),Aggregated]
        public XPCollection<DataMergeItem> DataMergeItems
        {
            get { return GetCollection<DataMergeItem>("DataMergeItems"); }
        }

        public PersistedType TargetObject
        {
            get { return _TargetObject; }
            set { SetPropertyValue("TargetObject", ref _TargetObject, value); }
        }
        #endregion Properties

        #region Public Methods
        public string Merge(string template, object targetObject)
        {
            string result = template;
            foreach (var item in DataMergeItems)
            {
                item.SetStringValue(targetObject);
            }
            result = WeBuildThings.DataMergeProcess.DataMerge.Merge(template, DataMergeItems);
            return result;
        }
        #endregion Public Methods
    }
}
