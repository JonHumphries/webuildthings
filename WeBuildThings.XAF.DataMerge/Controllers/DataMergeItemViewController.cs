﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Model.NodeGenerators;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Templates;
using DevExpress.ExpressApp.Utils;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.DC;
using DevExpress.ExpressApp.Win.Core;
using DevExpress.ExpressApp.Win.Editors;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using WeBuildThings.XAF.DataMergeProcess;

namespace WeBuildThings.XAF.DataMerge.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class DataMergeItemViewController : ViewController
    {
        private string _modelBrowserProperties = "PropertyName";
        private string _dataMergeItemWinTitle = "Data Merge Item";

        public DataMergeItemViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();

            DetailView detailView = View as DetailView;

            if (detailView != null && detailView.Caption == _dataMergeItemWinTitle)
            {
                foreach (StringPropertyEditor stringPropertyEditor in detailView.GetItems<StringPropertyEditor>())
                {
                    if (stringPropertyEditor.Control != null)
                    {
                        TextEdit textEdit = (TextEdit)stringPropertyEditor.Control;
                        if (textEdit != null && _modelBrowserProperties == stringPropertyEditor.PropertyName)
                        {
                            textEdit.Click -= ObjectName_Click;
                            textEdit.Click += ObjectName_Click;
                            break;
                        }
                    }
                }
            }
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        private void ObjectName_Click(object sender, EventArgs e)
        {
            DataMergeItem dataMergeItem = View.CurrentObject as DataMergeItem;

            if (dataMergeItem.DataMerge == null)
            {
                var exception = new ArgumentNullException("Data Merge is not initialized.");
                exception.Source = this.GetType().Name;
                throw exception;
            }
            if (dataMergeItem.DataMerge.TargetObject == null)
            {
                var exception = new ArgumentNullException("Target Object must be set before the Property Name can be set.");
                exception.Source = this.GetType().Name;
                throw exception;
            }

            ITypeInfo typeInfo = XafTypesInfo.Instance.FindTypeInfo(dataMergeItem.DataMerge.TargetObject.FullName);
            dataMergeItem.PropertyName = this.ShowModelBrowser(typeInfo);
        }

        /// <summary>
        /// Opens up model browser to select DataSource member.
        /// </summary>
        private string ShowModelBrowser(ITypeInfo typeInfo)
        {
            string result = string.Empty;
            ModelBrowser modelBrowser = new ModelBrowser(typeInfo.Type, false);
            if (modelBrowser.ShowDialog())
            {
                result = modelBrowser.SelectedMember;
            }
            return result;
        }
    }
}
