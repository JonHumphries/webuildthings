﻿namespace WeBuildThings.XAF.DataMergeProcess.Controllers
{
    partial class CommentViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AutoNote = new DevExpress.ExpressApp.Actions.PopupWindowShowAction(this.components);
            // 
            // AutoNote
            // 
            this.AutoNote.AcceptButtonCaption = null;
            this.AutoNote.CancelButtonCaption = null;
            this.AutoNote.Caption = "Auto Note";
            this.AutoNote.Category = "ObjectsCreation";
            this.AutoNote.ConfirmationMessage = null;
            this.AutoNote.Id = "AutoNote";
            this.AutoNote.ToolTip = null;
            this.AutoNote.CustomizePopupWindowParams += new DevExpress.ExpressApp.Actions.CustomizePopupWindowParamsEventHandler(this.AutoNote_CustomizePopupWindowParams);
            this.AutoNote.Execute += new DevExpress.ExpressApp.Actions.PopupWindowShowActionExecuteEventHandler(this.AutoNote_Execute);
            // 
            // CommentController
            // 
            this.Actions.Add(this.AutoNote);
            this.TargetObjectType = typeof(WeBuildThings.XAF.Common.Comment);
            this.TargetViewType = DevExpress.ExpressApp.ViewType.ListView;
            this.TypeOfView = typeof(DevExpress.ExpressApp.ListView);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.PopupWindowShowAction AutoNote;
    }
}
