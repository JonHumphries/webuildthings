﻿using DevExpress.ExpressApp;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Xpo;
using WeBuildThings.XAF.Common;
using WeBuildThings.XAF.DataMergeProcess.Controls;

namespace WeBuildThings.XAF.DataMergeProcess.Controllers
{
    // For more typical usage scenarios, be sure to check out https://documentation.devexpress.com/eXpressAppFramework/clsDevExpressExpressAppViewControllertopic.aspx.
    public partial class CommentViewController : ViewController
    {
        public CommentViewController()
        {
            InitializeComponent();
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }


        protected Comment Comment {get; set;}

        protected virtual Comment CreateComment(Session session)
        {
            return new Comment(session);
        }

        protected virtual AutoNote UpdateNote(AutoNote autoNote)
        {
            return autoNote;
        }

        protected virtual void AutoNote_Execute(object sender, PopupWindowShowActionExecuteEventArgs e)
        {
            Session session = ((XPObjectSpace)View.ObjectSpace).Session;
            Comment = CreateComment(session);
            AutoNote autoNote = e.PopupWindow.View.CurrentObject as AutoNote;
            autoNote = UpdateNote(autoNote);
            autoNote.CopyProperties(Comment);
        }

        private void AutoNote_CustomizePopupWindowParams(object sender, CustomizePopupWindowParamsEventArgs e)
        {
            IObjectSpace objectSpace = Application.CreateObjectSpace();
            AutoNote autoNote = objectSpace.CreateObject<AutoNote>();
            objectSpace.CommitChanges();
            e.View = Application.CreateDetailView(objectSpace, autoNote);
        }
    }
}
