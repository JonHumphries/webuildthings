﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System.ComponentModel;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.DataMergeProcess
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class CommentTemplate : Comment
    {
        #region Fields
        private string _Name;
        private DataMerge _DataMerge;
        #endregion Fields

        #region Constructors

        public CommentTemplate(Session session)
            : base(session)
        { }
        #endregion Constructors

        #region Properties
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public DataMerge DataMerge
        {
            get { return _DataMerge; }
            set { SetPropertyValue("DataMerge", ref _DataMerge, value); }
        }
        #endregion Properties

        #region Override Methods
        protected override void Initialize()
        {
            base.Initialize();
            ValidateCommentPeriod = false;
        }
        #endregion Override Methods

    }
}
