﻿namespace WeBuildThings.XAF.Testing.Controllers
{
    partial class ThreadTestingViewController
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.AddThreadMessage = new DevExpress.ExpressApp.Actions.ParametrizedAction(this.components);
            // 
            // AddThreadMessage
            // 
            this.AddThreadMessage.Caption = "Add Thread Message";
            this.AddThreadMessage.ConfirmationMessage = null;
            this.AddThreadMessage.Id = "AddThreadMessage";
            this.AddThreadMessage.NullValuePrompt = null;
            this.AddThreadMessage.ShortCaption = null;
            this.AddThreadMessage.ToolTip = null;
            this.AddThreadMessage.Execute += new DevExpress.ExpressApp.Actions.ParametrizedActionExecuteEventHandler(this.AddThreadMessage_Execute);
            // 
            // ThreadTestingViewController
            // 
            this.Actions.Add(this.AddThreadMessage);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.ParametrizedAction AddThreadMessage;
    }
}
