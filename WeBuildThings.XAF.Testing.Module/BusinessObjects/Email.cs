﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.XAF.Email;

namespace WeBuildThings.XAF.Testing.BusinessObjects
{
    public class Email : EmailMessage
    {
        public Email(Session session) : base(session) { }
    }
}
