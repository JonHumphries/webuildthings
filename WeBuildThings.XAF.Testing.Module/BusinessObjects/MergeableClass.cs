﻿using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System.ComponentModel;
using System.Linq;
using WeBuildThings.Common.Interfaces;

namespace WeBuildThings.XAF.Testing.BusinessObjects
{
    [DefaultClassOptions]
    [DefaultProperty("Name")]
    public class MergeableClass : BaseObject, IMergeable, IAlsoKnownAs
    {
        private string _Name;
        private int _SomeProperty;


        public MergeableClass(Session session) : base(session) { }

        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public int SomeProperty
        {
            get { return _SomeProperty; }
            set { SetPropertyValue("SomeProperty", ref _SomeProperty, value); }
        }

        [Association("MergeableClass-MyAKA", typeof(MyAKA)), Aggregated]
        public XPCollection<MyAKA> MyAKAs
        {
            get { return GetCollection<MyAKA>("MyAKAs"); }
        }


        public void Merge(IMergeable source)
        {
            var convertedSource = source as MergeableClass;
            if (convertedSource != null)
            {
                for (int i = 0; i < convertedSource.MyAKAs.Count; i++)
                {
                    var myAka = convertedSource.MyAKAs[i];
                    if (!MyAKAs.Contains(myAka))
                    {
                        MyAKAs.Add(myAka);
                    }
                }
                if (SomeProperty == 0)
                {
                    SomeProperty = convertedSource.SomeProperty;
                }
            }
        }


        public System.Collections.Generic.IEnumerable<string> AKAList
        {
            get { return MyAKAs.Select(n => n.Name); }
        }

        public void AddAKA(string newAKA)
        {
            if (!MyAKAs.Where(n => n.Name == newAKA).Any())
            {
                var aKA = new MyAKA(Session);
                aKA.Name = newAKA;
                MyAKAs.Add(aKA);
            }
        }
    }
}
