﻿using DevExpress.Xpo;
using JBTTesting.Module.BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeBuildThings.XAF.ElectronicDataInterchange;

namespace WeBuildThings.XAF.Testing.BusinessObjects
{
    public class SampleFileEDI : FileEDI
    {
        public SampleFileEDI(Session session) : base(session) { }

        protected override void Execute(EDILog currentEDILog)
        {
            using (var uow = new UnitOfWork(currentEDILog.Session.ObjectLayer))
            {
                var task1 = new Task1(currentEDILog.Session);
                task1.Name = string.Format("Created On {0}", DateTime.Now);
                task1.Save();
                uow.CommitChanges();
            }
            base.Execute(currentEDILog);
        }

        protected override void IncomingExecute(EDILog currentEDILog)
        {
            throw new NotImplementedException();
        }

        protected override void OutgoingExecute(EDILog currentEDILog)
        {
            throw new NotImplementedException();
        }

        protected override Type GetSerializedType()
        {
            throw new NotImplementedException();
        }

        protected override object Convert(Type type, object convertedResult)
        {
            throw new NotImplementedException();
        }
    }
}
