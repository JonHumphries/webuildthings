﻿using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;
using System;
using WeBuildThings.XAF.Common;

namespace WeBuildThings.XAF.Testing
{
    public enum TestTypeStatus { Status1, Status2 };

    public class TestType : BaseObject, IIdentity
    {
        private string _Name;
        private DateTime _DateTime;
        private bool _Bool;
        private TestType _ReferenceType;
        private int _Int;
        private decimal _Decimal;
        private Guid _Guid;
        private TestTypeStatus _Enum;
        private int _Number;
        private string _String2;

        public TestType(Session session) : base(session) { }
        public string Name
        {
            get { return _Name; }
            set { SetPropertyValue("Name", ref _Name, value); }
        }

        public TestTypeStatus Enum
        {
            get { return _Enum; }
            set { SetPropertyValue("Enum", ref _Enum, value); }
        }

        public DateTime DateTime
        {
            get { return _DateTime; }
            set { SetPropertyValue("DateTime", ref _DateTime, value); }
        }

        public bool Bool
        {
            get { return _Bool; }
            set { SetPropertyValue("Bool", ref _Bool, value); }
        }

        public TestType ReferenceType
        {
            get { return _ReferenceType; }
            set { SetPropertyValue("ReferenceType", ref _ReferenceType, value); }
        }

        public int Int
        {
            get { return _Int; }
            set { SetPropertyValue("Int", ref _Int, value); }
        }

        public decimal Decimal
        {
            get { return _Decimal; }
            set { SetPropertyValue("Decimal", ref _Decimal, value); }
        }

        public Guid Guid
        {
            get { return _Guid; }
            set { SetPropertyValue("Guid", ref _Guid, value); }
        }

        public int Number
        {
            get { return _Number; }
            set { SetPropertyValue("Number", ref _Number, value); }
        }

        public string String2
        {
            get { return _String2; }
            set { SetPropertyValue("String2", ref _String2, value); }
        }
    }
}
