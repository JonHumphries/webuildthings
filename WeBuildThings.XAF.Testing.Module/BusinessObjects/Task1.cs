﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeBuildThings.XAF.Testing.BusinessObjects;


namespace JBTTesting.Module.BusinessObjects
{
    public class Task1 : WeBuildThings.WorkflowEngine.Task
    {
        public Task1(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            AutomatedTask = true;
            base.AfterConstruction();
        }

        public override void Execute(Session session)
        {
            var fileEDIs = new XPCollection<SampleFileEDI>(session);
            foreach (var edi in fileEDIs)
            {
                edi.StartExecution();
            }
            base.Execute(session);
            Approve();
        }
    }

    public class Task2 : WeBuildThings.WorkflowEngine.Task
    {
        public Task2(Session session) : base(session) { }

        public override void AfterConstruction()
        {
            AutomatedTask = true;
            base.AfterConstruction();
        }

        public override void Execute(Session session)
        {
            base.Execute(session);
            Reject();
        }
    }
}
