﻿using DevExpress.Xpo;
using System.Linq;
using WeBuildThings.Utilities;

namespace WeBuildThings.XAF.Testing.BusinessObjects
{
    public class MyAKA : AKA
    {
        private MergeableClass _MergeableClass;

        public MyAKA(Session session) : base(session) { }

        [Association("MergeableClass-MyAKA", typeof(MergeableClass))]
        public MergeableClass MergeableClass
        {
            get { return _MergeableClass; }
            set { SetPropertyValue("MergeableClass", ref _MergeableClass, value); }
        }
    }
}
